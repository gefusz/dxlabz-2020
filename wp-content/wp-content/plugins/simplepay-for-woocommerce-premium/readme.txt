=== SimplePay for WooCommerce Premium ===
Contributors: oaron
Donate link: https://bitron.hu
Requires at least: 4.6
Tested up to: 5.3.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A SimplePay for WooCommerce bővítmény használatával bankkártyás fizetés biztosítható webshopunkban.

== Description ==

Bővebb infók találhatóak a megkapott telepítési útmutatóban.

== Changelog ==
= 1.3 (2020-03-15)=
* SimplePay tranzakciós azonosító megjelenik a rendeléseknél
* Aktivációs hiba javítása
= 1.2 (2020-02-02)=
* Tranzakciók logolása
* SimplePay átirányítás javítása
* Apróbb módosítások
* Fordítások frissítése
= 1.1 =
* 0 összegű tételek lekezelése
* Recurring terhelés javítása
* Kimaradt fordítások javítása
* kód tisztítás
= 1.0.1 =
* sikeres/sikertelen fizetések jelzése a rendeléseknél
* Angol fordítás frissítése
= 1.0 =
* Ismétlődő fizetés  támogatása