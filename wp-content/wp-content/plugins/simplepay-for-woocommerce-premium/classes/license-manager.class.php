<?php
	class licenseManager {
		private $license;
		private $errorCodes;
		private $url;
		private $storeCode;
		public function __construct($license ="") {
			$this->license =$license;
			$this->errorCodes =array (
			"1" =>__('Hibás kód! Vegyük fel a kapcsolatot a bővítmény fejlesztőjével!', 'woo-simple-premium'),
			"2" =>__('Hibás licenckód!', 'woo-simple-premium'),
			"3" =>__('Ez a licenckód nem ehhez a termékhez tartozik!', 'woo-simple-premium'),
			"4" =>__('Hibás licenckód!', 'woo-simple-premium'),
			"5" =>__('Nem létező  licenckód!', 'woo-simple-premium'),
			"101" =>__('Meg kell adni licenckódot!', 'woo-simple-premium'),
			"200" =>__('A licenckód lejárt!', 'woo-simple-premium'),
			"201" =>__('A licenckód nem aktiválható, mert elértük az aktivációk maximális számát!!', 'woo-simple-premium'),
			"202" =>__('A licenckód nem aktiválható, mert elértük az aktiválható webcímek maximális számát!!', 'woo-simple-premium'),
			"203" =>__('A licenckód nem aktiválható, nem található ilyen aktivációs azonosító!!', 'woo-simple-premium'),
			"901" =>__('A licenckód lejárt!!', 'woo-simple-premium')
			);
			$this->url ="https://bitron.hu/wp-admin/admin-ajax.php";
			$this->storeCode ="F2hEqx0FqVE11V1";
		}
		private function getUrl() {
			return $this->url;
		}
		private function getStoreCode() {
			return $this->storeCode;
		}
		private function sendRequest ($url) {
			$ret =array();
			$ret["license"] =$this->getLicense();
			$result=wp_remote_get($url);
			if(is_array($result)) {
				$response =json_decode($result["body"]);
				//$ret["error"] =$response->error;
				if ($response->error ==1) {
					
					foreach ((array)$response->errors as $key =>$value) {
						$ret["msg"] .=$this->getCodeString($key);
					}
					} else {
					$ret["license"] =json_encode($response->data);
					$ret["msg"] ="success";
					$this->license =$ret["license"];
				}
				return $ret;
				} else {
				$ret["msg"] ="connectionError";
				return $ret;
			}
		}
		public function activate($licenseKey) {
			$url =$this->getUrl()."?action=license_key_activate&store_code=".$this->getStoreCode()."&sku=woo-simple-premium&license_key=".$licenseKey."&domain=".home_url();
			return $this->sendRequest($url);
		}
		public function isValid($the_key, $activation_id) {
			$url =$this->getUrl()."?action=license_key_validate&store_code=".$this->getStoreCode()."&sku=woo-simple-premium&license_key=".$the_key."&domain=".home_url()."&activation_id=".$activation_id;
			return $this->sendRequest($url);
		}
		public function deactivate($the_key, $activation_id) {
			$url =$this->getUrl()."?action=license_key_deactivate&store_code=".$this->getStoreCode()."&sku=woo-simple-premium&license_key=".$the_key."&domain=".home_url()."&activation_id=".$activation_id;
			return $this->sendRequest($url);
		}
		private function getCodeString ($code) {
			return $this->errorCodes[$code];
		}
		private function getLicense() {
			return $this->license;
		}
	}		