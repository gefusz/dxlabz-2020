<?php
	class WC_otp_simple extends WC_Payment_Gateway {
		private $ipn_url;
		private $backref_url;
		private $timeout_url;
		private $license;
		private $license_update;
		private $lm;
		public function __construct() {
			$this->license =get_option('simple_license');
			$this->license_update =get_option("simple_license_activation_date");
			$this->lm =new licenseManager("");
			$this->ipn_url =WC()->api_request_url( 'WC_Gateway_simple_ipn' );    
			$this->backref_url =WC()->api_request_url( 'WC_Gateway_simple_backref');
			$this->id ="otp_simple";
			$this->icon =home_url() . '/wp-content/plugins/simplepay-for-woocommerce-premium/img/simplepay_logo.png';
			$this->order_button_text = __( 'Kifizetem a SimplePay felületén!', 'woo-simple-premium' );
			$this -> has_fields = false;
			$this->method_title ="SimplePay";
			$this->method_description ="Bankkártyás fizetés a SimplePay felületén keresztül";
			if ( class_exists( 'WC_Subscriptions' ) ) {
				$this->supports             = array(
			'products',
			               'subscriptions',
               'subscription_cancellation', 
               'subscription_suspension', 
               'subscription_reactivation',			//'subscription_amount_changes',
'subscription_date_changes',
//'subscription_payment_method_change',
//'subscription_payment_method_change_customer',
//'subscription_payment_method_change_admin',
               'multiple_subscriptions',
			'refunds'
			);
				} else {
				
			$this->supports             = array(
			'products',
			'refunds'
			);
			}
			$this->init_form_fields();
			$this->init_settings();
			$this->enabled =($this->is_valid() ? $this->get_option( 'enabled') : "no");
			$this->title = $this->get_option( 'title' );
			$this->description = $this->get_option( 'description' );
			$this->currency =$this->get_option( 'simple-currency' );
			$this->mode =($this->get_option( 'mode' ) =='test' ? true : false);
			$this->merchant_user =$this->get_option( 'merchant_user' );
			$this->merchant_key =$this->get_option( 'merchant_key' );
			$this->enable_logging =$this->get_option( 'enable_logging' );
			$this->successfull_status =$this->get_option( 'successfull_status' );
			$this->company_name =$this->get_option( 'company_name' );
			$this->company_addr =$this->get_option( 'company_addr' );
			$this->enable_card_hold =$this->get_option( 'enable_card_hold' );
			//$this->enable_two_step =$this->get_option( 'enable_two_step' );
			if ($this->is_valid()) {
				add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
				//add_action( 'woocommerce_receipt_otp_simple', array(&$this, 'simple_payment_redirect_handler' ) );
				//urls
				add_action( 'woocommerce_api_wc_gateway_simple_ipn', array($this, 'simple_ipn_handler' ) );
				add_action( 'woocommerce_api_wc_gateway_simple_backref', array($this, 'simple_backref_handler' ) );
				//checkout
				add_action("woocommerce_review_order_before_submit", array($this, "simple_aszf"));
				add_action('wp_enqueue_scripts', array($this, 'simple_add_jqueryui'));
				//card details
				//add_action( 'woocommerce_checkout_update_order_meta', array($this, "simple_card_details") );
				//add_action('woocommerce_checkout_create_order', array($this, "simple_card_details") );
				//
				if ( class_exists( 'WC_Subscriptions_Order' ) ) {
add_action('woocommerce_scheduled_subscription_payment_'.$this->id, array($this, 'spl_process_subscription'), 10, 2);
add_action( 'woocommerce_subscription_cancelled_' . $this->id, array( $this, 'spl_cancel_subscription' ), 10, 2 );
add_action( 'woocommerce_subscription_status_changed', array($this, "spl_pending_cancel"));
add_filter( 'woocommerce_my_subscriptions_payment_method', array( $this, 'show_subscription_payment_method' ), 10, 2 );
				}
			}
		}
		/**
			* Return the gateway's icon.
			*
			* @return string
		*/
		public function get_icon() {
			$link =(get_locale() =="hu_HU" ? "http://simplepartner.hu/PaymentService/Fizetesi_tajekoztato.pdf" : "http://simplepartner.hu/PaymentService/Payment_information.pdf");
			$icon = $this->icon ? '<a href ="'.WC_HTTPS::force_https_url($link).'" target="_blank"><img src="' . WC_HTTPS::force_https_url( $this->icon ) . '" data-no-lazy="1" title="'.__("SimplePay - Online bankkártyás fizetés", "woo-simple-premium").'" alt="'.__("SimplePay vásárlói tájékoztató", "woo-simple-premium").'" /></a>' : '';
			return apply_filters( 'woocommerce_gateway_icon', $icon, $this->id );
		}
		private function is_valid() {
			if (!empty($this->license)) {
				
				if (strtotime($this->license_update)+86400 >strtotime(date("Y-m-d h:i:s"))) {
					return true;
					} else {
					$result =$this->lm->isValid($this->license->the_key, $this->license->activation_id);
					if ($result["msg"] =="success" ) {
						update_option("simple_license", json_decode($result["license"]));
						update_option("simple_license_activation_date", date("Y-m-d h:i:s"));
						return true;
						}  elseif ($result["msg"] =="connectionError") {
						return (($this->license->expire >strtotime(date("Y-m-d h:i:s"))) ||$this->license->expire =='' ? true : false);
						}
					else {
						delete_option("simple_license");
						delete_option("simple_license_activation_date");
						return false;
					}
				}
				} else {
				return false;
			}
		}
		public function generate_license_text_html( $key, $value ) { 
			ob_start();
			print '<div id ="simple_activation_error"  role ="alert"></div>';
			
			if ($this->is_valid()) {
				print '<table class="form-table license_table">';
				print "<tr><th>".__('A bővítmény aktiválva.', 'woo-simple-premium')."<br>".__('Lejárat dátuma: ', 'woo-simple-premium').(empty($this->license->expire_date) ? "soha" : $this->license->expire_date)."</th></tr>";
				print '<input type ="hidden" id ="license_key" value ="'.$this->license->the_key.'">';
				print '<input type ="hidden" id ="activation_id" value ="'.$this->license->activation_id.'">';
				print '<tr><td>			<button id ="deactivate" type="button">'.__("Deaktiválás", "woo-simple-premium").'</button></td></tr>';
				print '</table>';
				} else {
			?>
			<table class="form-table license_table">
				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="<?php print $key; ?>"><?php print __("Licenckód megadása", "woo-simple-premium"); ?></label>
					</th>
					<td class="forminp">
						<fieldset>
							<legend class="screen-reader-text"><span>Licenckód megadása</span></legend>
							<input class="input-text regular-input " type="text" name="license-code" id="<?php print $key; ?>" style="" placeholder=""   />
						</fieldset>
					</td>
				</tr>
				<tr><td  colspan ="2">
					<button id ="activate" type="button"><?php print __("Aktiválás", "woo-simple-premium"); ?></button>
				</td></tr>
			</table>
			<?php
			}
			return ob_get_clean();
		}
		function init_form_fields() {
			$this->form_fields = array(
			'activation_title' => array(
			'title'       => __( 'SimplePay aktiválás/deaktiválás', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => __('A bővítmény használatához aktiválás szükséges. Adjuk meg az e-mailben kapott licenckódot.'.$this->get_option("license"), 'woo-simple-premium'),
			),
			'license_code' => array(
			'title' => __( 'Licenckód', 'woo-simple-premium' ),
			'type' => 'license_text',
			'label' => __( 'SimplePay bővítményhez kapott licenckód', 'woo-simple-premium' ),
			),
			
			'simplepay_settings_title' => array(
			'title'       => __( 'SimplePay beállításai', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => __('A simplePay-hoz tartozó beállításokat módosíthatjuk itt.', 'woo-simple-premium'),
			),
			'enabled' => array(
			'title' => __( 'Bekapcsolás/Kikapcsolás', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'SimplePay bekapcsolása', 'woo-simple-premium' ),
			),
			'title' => array(
			'title' => __( 'Megjelenő név', 'woo-simple-premium' ),
			'type' => 'text',
			'description' => __( 'Ezen a néven jelenik meg a vásárló számára a pénztár oldalon.', 'woo-simple-premium' ),
			'default' =>'Bankkártyás fizetés',
			'desc_tip'      => true,
			),
			            'description'     => array(
                'title'       => __( 'Leírás', 'woo-simple-premium' ),
                'type'        => 'textarea',
                'description' => __( 'A fizetési módhoz tartozó leírás, amit a vásárló lát a pénztár oldalon', 'woo-simple-premium' ),
                'default'     => __( 'Bankkártyás fizetés a SimplePay felületén keresztül.', 'woo-simple-premium' ),
                'desc_tip'    => true,
            ),
			'merchant_user' => array(
			'title' => __('Kereskedői azonosító (MERCHANT)', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('SimplePay által megadott kereskedői azonosító', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'merchant_key' => array(
			'title' => __('Titkosító kulcs (SECRET_KEY)', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('SimplePay által megadott titkosító kulcs', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'mode' =>array(
			'title' =>__('SimplePay rendszer típusa', 'woo-simple-premium'),
			'type' =>'select',
			'options' =>array(
			'test' =>__('Teszt', 'woo-simple-premium'),
			'prod' =>__('Éles', 'woo-simple-premium')
			),
			'description' =>__('Teszt vagy éles fiók', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'enable_logging' => array(
			'title' => __( 'Tranzakciók naplózása', 'woo-simple' ),
			'type' => 'checkbox',
			'label' => __( 'Tranzakciók naplózása', 'woo-simple' ),
			'description' =>__('Bekapcsolás esetén a tranzakciók naplózása a wp-content/uploads/simple-log mappába történik', 'woo-simple'),
			'desc_tip'      => true,
			),
			'simple-currency' =>array(
			'title' =>__('Fiók devizaneme', 'woo-simple-premium'),
			'type' =>'select',
			'options' =>array(
			'HUF' =>'Ft',
			'EUR' =>'EURO',
			'USD' =>'USD'
			),
			'description' =>''
			),
			'successfull_status' =>array(
			'title' =>__('Rendelés státusza sikeres fizetést követően', 'woo-simple-premium'),
			'type' =>'select',
			'options' =>wc_get_order_statuses(),
			'default' =>'wc-processing',
			'description' =>__('Sikeres fizetést követően a rendelés ebbe a státuszba kerül.', 'woo-simple-premium')
			),
			'company_name' => array(
			'title' => __('Cég neve', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('Pénztár oldalon, a megrendelés előtt jelenik meg a SimplePay által megkövetelt tájékoztatóban', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'company_addr' => array(
			'title' => __('Cég címe', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('Pénztár oldalon, a megrendelés előtt jelenik meg a SimplePay által megkövetelt tájékoztatóban', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			/*
						'enable_card_hold' => array(
        'title' => __( 'Kártya tárolás engedélyezése', 'woo-simple-premium' ),
        'type' => 'checkbox',
'label' => __( 'Kártya letárolása', 'woo-simple-premium' ),
        'default' => 'no',
                'desc_tip'      => true,
		'description' => __('Bekapcsolás esetén a vásárló eltárolhatja kártya adatait, későbbi vásárlás esetén nem kell újból megadnia', 'woo-simple-premium'),
    )
*/
		);
		/*
		if ( class_exists( 'WC_Subscriptions_Order')) {
			$this->form_fields['enable_two_step'] = array(
        'title' => __( 'Összeg zárolásának engedélyezése', 'woo-simple-premium' ),
        'type' => 'checkbox',
'label' => __( 'Összeg zárolásának engedélyezése', 'woo-simple-premium' ),
        'default' => 'no',
                'desc_tip'      => true,
		'description' => __('Bekapcsolás esetén ismétlődő terméknél használható az ingyenes próba lehetőség. Ekkor a kártya hitelesítését 100 Ft-tal végzi el a rendszer, amit rögtön fel is old.', 'woo-simple-premium'),
    );
			}
			*/
		
		$this->form_fields['ipn_process_url'] = array(
			'title'       => __( 'SimplePay IPN beállítása', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => sprintf( __( 'Állítsuk be az alábbi IPN url-t a Simple felületén.<br>URL: %s', 'woo-simple-premium' ), $this->ipn_url)
						);
		}
		private function containsSubscription ($order_id) {
			if ( class_exists('WC_Subscriptions_Order')) {
				return (wcs_order_contains_subscription( $order_id ) ||wcs_order_contains_renewal($order_id) ? true : false);
				} else {
				return false;
				}
			}
		function getSimpleSettings() {
			require_once(ABSPATH . 'wp-content/plugins/simplepay-for-woocommerce-premium/lib/config.php');
			require_once(ABSPATH . 'wp-content/plugins/simplepay-for-woocommerce-premium/lib/SimplePayV21.php');
			require_once(ABSPATH . 'wp-content/plugins/simplepay-for-woocommerce-premium/lib/SimplePayV21CardStorage.php');
			require_once(ABSPATH . 'wp-content/plugins/simplepay-for-woocommerce-premium/lib/SimplePayV21TokenData.php');
			$config[$this->currency.'_MERCHANT'] =$this->merchant_user;
			$config[$this->currency.'_SECRET_KEY'] =$this->merchant_key;
			$config["SANDBOX"] =$this->mode;
			$config["URL"] =$this->backref_url;
						$config["LOGGER"] =($this->enable_logging ==1 ? true : false);
			$config["LOG_PATH"] =($this->enable_logging ==1 ? wp_upload_dir()['basedir'] . '/simple-log' : '');
			return $config;
		}
		private  function payment_start( $order_id ) {
			global $woocommerce;
			$order = new WC_Order( $order_id );
			WC()->session->set('order_id', $order_id);
			//set status
			$order->update_status('pending', __( 'Fizetésre vár', 'woo-simple-premium' ));
			// Reduce stock levels
			//$order->reduce_order_stock();
			
			// Remove cart
			//$woocommerce->cart->empty_cart();
			
			$trans_id =$order->get_id();
			$settings =$this->getSimpleSettings();
			$trx = new SimplePayStart;
			$trx->addData('currency', $this->currency);
			$trx->addConfig($settings);
			$totalprice =0;
			foreach ($order->get_items() as $product) {
				$trx->addItems(
				array(
				'ref' => $product['product_id'],
				'title' => strip_tags($product['name']),
				'description' => '',
				'amount' => $product['qty'],
				'price' => ($product['line_subtotal'] + $product['line_subtotal_tax']) / $product['qty'],
				'tax' => '0',
				)
				);
				$totalprice =$totalprice+(($product['line_subtotal'] + $product['line_subtotal_tax']) / $product['qty']);
			}
			/*
			if ($this->enable_two_step =='yes') {
			if ($totalprice ==0) {
				$trx->addItems(
				array(
				'ref' => 0,
				'title' =>'Trial',
				'description' => '',
				'amount' => 1,
				'price' =>100,
				'tax' => '0',
				)
				);
				$trx->addData('twoStep', true);
				update_post_meta( $order_id, 'two_step', 1);
				} else {
				$trx->addData('twoStep', false);
				}
				}
				*/
			$trx->addData('shippingCost', round( $order->get_total_shipping() + $order->get_shipping_tax(), 2 ));
			$trx->addData('discount', round( $order->get_total_discount(false), 2 ));
			$trx->addData('orderRef', $trans_id);
			$trx->addData('customer', $order->get_billing_last_name().' '.$order->get_billing_first_name());
			$trx->addData('customerEmail', $order->get_billing_email());
			$trx->addData('language', explode("_", get_locale())[1]);
			$trx->addData('timeout', @date("c", time() + 600));
			$trx->addData('methods', array('CARD'));
			$trx->addData('url', $settings['URL']);
			//számlázás
			$trx->addGroupData('invoice', 'name', $order->get_billing_last_name().' '.$order->get_billing_first_name());
			$trx->addGroupData('invoice', 'company', $order->get_billing_company());
			$trx->addGroupData('invoice', 'country', $order->get_billing_country());
			$trx->addGroupData('invoice', 'state', $order->get_billing_state());
			$trx->addGroupData('invoice', 'city', $order->get_billing_city());
			$trx->addGroupData('invoice', 'zip', $order->get_billing_postcode());
			$trx->addGroupData('invoice', 'address', $order->get_billing_address_1());
			$trx->addGroupData('invoice', 'address2', $order->get_billing_address_2());
			$trx->addGroupData('invoice', 'phone', $order->get_billing_phone());
			
			// szállítási adatok
			$trx->addGroupData('delivery', 'name', $order->get_shipping_last_name().' '.$order->get_shipping_first_name());
			$trx->addGroupData('delivery', 'company', $order->get_shipping_company());
			$trx->addGroupData('delivery', 'country', $order->get_shipping_country());
			$trx->addGroupData('delivery', 'state', $order->get_shipping_state());
			$trx->addGroupData('delivery', 'city', (empty($order->get_shipping_city()) ? $order->get_billing_city() : $order->get_shipping_city()));
			$trx->addGroupData('delivery', 'zip', (empty($order->get_shipping_postcode()) ? $order->get_billing_postcode() : $order->get_shipping_postcode()));
			$trx->addGroupData('delivery', 'address', (empty($order->get_shipping_address_1()) ? $order->get_billing_address_1() : $order->get_shipping_address_1()));
			$trx->addGroupData('delivery', 'address2', $order->get_shipping_address_2());
			$trx->addGroupData('delivery', 'phone', $order->get_billing_phone());
			/*
				* Tesztelést igényel
			//kártya tárolása
			if ($this->enable_card_hold =="yes" &&!empty(WC()->session->get( 'card_secret' ))) {
				$trx->addData('cardSecret', WC()->session->get( 'card_secret' )); 
				WC()->session->set( 'card_secret', '');
																																																}
																																																*/
																																																// Ha van subscription
																																																if ($this->containsSubscription($order_id)) {
																																																	$periodPrice =WC_Subscriptions_Order::get_recurring_total($order);
																																																																																																		$trx->addGroupData('recurring', 'times', 24); 
																																																	$trx->addGroupData('recurring', 'until', date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 730 day")).'T00:00:00+02:00'); 
																																																	$trx->addGroupData('recurring', 'maxAmount', $periodPrice); 
																																																	update_post_meta( $order_id, 'registered_card', 1);
																																																	}
			//form generálás
			$trx->formDetails['element'] = 'auto';
			$trx->runStart();
						$trx->getHtmlForm();
									if (isset($trx->returnData["tokens"])) {
				$tokens =$trx->returnData["tokens"];
												update_post_meta($order_id, "tokens", $tokens);
				}
			return $trx->returnData['paymentUrl'];
		}
		private function payment_do($order_id, $recurring =false, $total =0) {
			
						global $woocommerce;
			$order = new WC_Order( $order_id );
			        			$order->update_status('pending', __( 'Fizetésre vár', 'woo-simple-premium' ));
			$trans_id =$order->get_id();
						$settings =$this->getSimpleSettings();
									//Do, vagy recurringdo osztály inicializálása
						if ($recurring ==1) {
									$trx = new SimplePayDorecurring;
									} else {
									$trx = new SimplePayDo;
									}
			$trx->addConfig($settings); 
			$trx->addConfigData('merchantAccount', $this->merchant_user);
			$trx->addData('orderRef', $trans_id);
						$trx->addData('methods', array('CARD'));
						$trx->addData('currency', 'HUF');
						//$trx->addData('twoStep', false);
												if ($recurring ==1) {
													if ( wcs_order_contains_renewal( $order->id ) ) {
            $parent_order_id = WC_Subscriptions_Renewal_Order::get_parent_order_id( $order->id );
        }
							$tokens =get_post_meta($parent_order_id, "tokens", true);
														$cardid =get_post_meta($parent_order_id, "simple_id", true);
							$trx->addData('cardId', $cardid);
			$trx->addData('token', $tokens[0]);
							} else {
							$user_id = $order->get_user_id();
														$cards =get_user_meta($user_id, 'cards', true);
			$trx->addData('cardId', $cards[WC()->session->get('selected_card')]["cardid"]);
			$trx->addData('cardSecret', WC()->session->get('card_secret'));
						
						foreach ($order->get_items() as $product) {
				$total =$total+($product['line_subtotal'] + $product['line_subtotal_tax']);
}
}
						$trx->addData('total', $total);
			$trx->addData('customer', $order->get_billing_last_name().' '.$order->get_billing_first_name());
			$trx->addData('customerEmail', $order->get_billing_email());
			$trx->addData('twoStep', false);
				$ret = ($recurring ==1 ? $trx->runDorecurring() : $trx->runDo());
							return $ret;
						}
						/*
							* Waiting for Simple to fix bug
							*/
						private function finishTransaction ($trans_id, $originalTotal =100) {
							$settings =$this->getSimpleSettings();
			$trx = new SimplePayFinish; 
			$trx->addConfig($settings); 
			$trx->addData('orderRef', $trans_id);
			$trx->addConfigData('merchantAccount', $this->merchant_user);
			$trx->addData('originalTotal', $originalTotal);
			$trx->addData('approveTotal', 0);
			$trx->transactionBase['currency'] = 'HUF';
			$trx->runFinish();
							}
			private function card_redirection($order_id, $ret) {
				$order = new WC_Order( $order_id );
										if (isset($ret["transactionId"])) {
			WC()->session->set('selected_card', "");
			global $woocommerce;
			
			WC()->cart->empty_cart();
						wc_add_notice( sprintf( __( 'Sikeres tranzakció.<br/>SimplePay referencia szám: %s<br/>Megrendelés azonosítója: %s<br/>Dátum: %s', 'woo-simple-premium' ), $ret["transactionId"], $ret['orderRef'], date('Y-m-d H:i:s', time())));
						$order->add_order_note( sprintf( __( 'Sikeres tranzakció. SimplePay referencia szám: %s', 'woo-simple-premium' ), $ret["transactionId"]));
						update_post_meta( $order_id, 'simple_id', $ret["transactionId"]);
						update_post_meta( $order_id, 'registered_card', 1);
												wp_redirect( $order->get_checkout_order_received_url() );
												} else {
												WC()->session->set('selected_card', "");
												$order->update_status( 'failed', __( 'Sikertelen fizetés.', 'woo-simple-premium' ) );
						$order->add_order_note( sprintf( __( 'Sikertelen fizetés. Státusz: %s, SimplePay referencia szám: %s', 'woo-simple-premium' ), $result['e'], $result['t']));
						wc_add_notice( sprintf( __( 'Sikertelen tranzakció. Kérjük, ellenőrizze a tranzakció során megadott adatok helyességét. Amennyiben minden adatot helyesen adott meg, a visszautasítás okának kivizsgálása kapcsán kérjük, szíveskedjen kapcsolatba lépni kártyakibocsátó bankjával.<br/>SimplePay referencia szám: %s<br/>Megrendelés azonosítója: %s<br/>Dátum: %s', 'woo-simple-premium' ), $result['t'], $result['o'], date('Y-m-d H:i:s', time()) ), 'error');
						update_post_meta( $order_id, 'simple_id',  $response['PAYREFNO'] );
						update_post_meta( $order_id, 'simple_status', $response['ORDER_STATUS']);
						$url = $order->get_cancel_order_url();
						wp_redirect( $url );
												}
			
			die();
				}
				public function spl_process_subscription($amount_to_charge, $order) {
										$ret =$this->payment_do($order->get_id(), 1, $amount_to_charge);
																																																		if (isset($ret["transactionId"])) {
																																									$order->add_order_note( __('Sikeres ismétlődő bankkártyás fizetés.', 'woo-simple-premium'));
																																																																																																																																	        WC_Subscriptions_Manager::process_subscription_payments_on_order( $order );
														} else {
														$order->add_order_note( __('Sikertelen ismétlődő bankkártyás fizetés.', 'woo-simple-premium'));
																				        WC_Subscriptions_Manager::process_subscription_payment_failure_on_order( $order, $product_id );
														}
																												}
																												public function spl_pending_cancel ($order_id) {
			$order = wc_get_order( $order_id );
			if ($order->get_status() =='pending-cancel' &&$order->get_payment_method() ==$this->id) {
				$subscription = new WC_Subscription($order_id);
																							
								$cardId =get_post_meta($subscription->parent_id, "simple_id", true);
																																																update_post_meta($subscription->parent_id, "tokens", array());
																																																$order->add_order_note( __('Ismétlődő fizetéshez tartozó tokenek törlése megtörtént.', 'woo-simple-premium'));
																								if ($this->cardCancel($cardId)) {
																									$order->add_order_note( __('Kártyaregisztráció törlése a Simple rendszeréből.', 'woo-simple-premium'));
																									}
																														}

		}
		
																						public function spl_cancel_subscription ($subscription) {
																																														if ($subscription->payment_method ==$this->id &&($subscription->status =='cancelled' ||$subscription->status =='pending-cancellation')) {
																								$cardId =get_post_meta($subscription->parent_id, "simple_id", true);
																								update_post_meta($subscription->parent_id, "tokens", array());
																								$order = wc_get_order( $subscription->id );
																								$order->add_order_note( __('Ismétlődő fizetéshez tartozó tokenek törlése megtörtént.', 'woo-simple-premium'));
																								return $this->cardCancel($cardId);
																								} else {
																								return false;
																							}
																							}
																						
		function process_payment ($order_id) {
			global $woocommerce;
			$order = new WC_Order( $order_id );
			if (WC()->session->get('selected_card') !="") {
																	$ret =$this->payment_do($order_id);
				$this->card_redirection($order_id, $ret);
				} else {
							
			return array(
			'result' => 'success',
			'redirect' =>$this->payment_start($order->get_id())
			);
			}
		}
				function simple_backref_handler() {
			global $woocommerce;
			//$order_id = WC()->session->get( 'order_id' );
			//$order_id =$_GET["order_ref"];
			$order_id = WC()->session->get( 'order_id' );
			$order = new WC_Order( $order_id );
			
			$settings =$this->getSimpleSettings();
			$trx = new SimplePayBack;
			$trx->addConfig($settings);   
			$result = array();
			if (isset($_REQUEST['r']) && isset($_REQUEST['s'])) {
				if ($trx->isBackSignatureCheck($_REQUEST['r'], $_REQUEST['s'])) {
					$result = $trx->getRawNotification();
					if ($result["e"] =='CANCEL') {
						wc_add_notice( sprintf( __( 'Megszakított tranzakció<br/>', 'woo-simple-premium' )), 'error');
						wc_add_notice( sprintf( __( 'Ön megszakította a fizetést!<br/><br/>Dátum: %s<br/>Rendelés azonosító: <b>%s', 'woo-simple-premium' ), date('Y-m-d H:i:s', time()), $result["o"] ), 'error');
						$url = $order->get_cancel_order_url();
						wp_redirect( $url );
					}
					elseif ($result["e"] =='TIMEOUT') {
						wc_add_notice( sprintf( __( 'Időtúllépéses tranzakció<br/>', 'woo-simple-premium' )), 'error');
						wc_add_notice( sprintf( __( 'lejárt a tranzakció maximális ideje!<br/><br/>Dátum: %s<br/>Rendelés azonosító: <b>%s', 'woo-simple-premium' ), date('Y-m-d H:i:s', time()), $result["o"] ), 'error');
						$url = $order->get_cancel_order_url();
						wp_redirect( $url );
					}
					elseif ($result["e"] =='SUCCESS') {
												WC()->cart->empty_cart();
						wc_add_notice( sprintf( __( 'Sikeres tranzakció.<br/>SimplePay referencia szám: %s<br/>Megrendelés azonosítója: %s<br/>Dátum: %s', 'woo-simple-premium' ), $result['t'], $result['o'], date('Y-m-d H:i:s', time())));
						$order->add_order_note( sprintf( __( 'Sikeres tranzakció. SimplePay referencia szám: %s', 'woo-simple-premium' ), $result['t'] ));
						update_post_meta( $order_id, 'simple_id', $result['t']);
						update_post_meta( $order_id, 'simple_status', $result['e']);
						wp_redirect( $order->get_checkout_order_received_url() );
						} else {
						$order->update_status( 'failed', __( 'Sikertelen fizetés.', 'woo-simple-premium' ) );
						$order->add_order_note( sprintf( __( 'Sikertelen fizetés. Státusz: %s, SimplePay referencia szám: %s', 'woo-simple-premium' ), $result['e'], $result['t']));
						wc_add_notice( sprintf( __( 'Sikertelen tranzakció. Kérjük, ellenőrizze a tranzakció során megadott adatok helyességét. Amennyiben minden adatot helyesen adott meg, a visszautasítás okának kivizsgálása kapcsán kérjük, szíveskedjen kapcsolatba lépni kártyakibocsátó bankjával.<br/>SimplePay referencia szám: %s<br/>Megrendelés azonosítója: %s<br/>Dátum: %s', 'woo-simple-premium' ), $result['t'], $result['o'], date('Y-m-d H:i:s', time()) ), 'error');
						update_post_meta( $order_id, 'simple_id',  $response['PAYREFNO'] );
						update_post_meta( $order_id, 'simple_status', $response['ORDER_STATUS']);
						$url = $order->get_cancel_order_url();
						wp_redirect( $url );
					}
				}
			}
			
			
			
			
		}
		public function cardCancel($cardid) {
			$settings =$this->getSimpleSettings();
			$trx = new SimplePayCardCancel; 
			$trx->addConfig($settings); 
			$trx->addConfigData('merchantAccount', $this->merchant_user);
			$trx->addData('cardId', $cardid);
			$ret =$trx->runCardCancel();
			if ($ret["status"] =='DISABLED') {
				return true;
				} else {
				return false;
				}
			}
		function simple_ipn_handler() {
						header('Content-Type: text/html; charset=utf-8');
			$json = file_get_contents('php://input');
			$settings =$this->getSimpleSettings();
			$trx = new SimplePayIpn;
			$headers =json_decode($json, true);
			$order_id =$headers["orderRef"];
			$order = new WC_Order( $order_id );
			$trx->addConfig($settings);
			if ($trx->isIpnSignatureCheck($json)) {
				$trx->runIpnConfirm();
				
				$user_id = $order->get_user_id();
																		if (isset($headers["expiry"])) {
																																						if (wcs_order_contains_subscription($order_id, 'parent')) {
																			update_post_meta( $order_id, 'simple_card_mask', $headers["cardMask"]);
																				update_post_meta( $order_id, 'simple_card_expire', $headers["expiry"]);
																				} 
																			if (get_post_meta( $order_id, 'registered_card', true) !=1) {
								$cards =get_user_meta($user_id, 'cards', true);
																		if (empty($cards)) {
								$cards =array();
							}
							$cards[] =array("cardid" =>$headers['transactionId'], "name" =>$headers["cardMask"], "card_mask" =>$headers["cardMask"], "exp_date" =>$headers["expiry"]);
							update_user_meta($user_id, "cards", $cards);
							}
							} elseif (empty($headers["expiry"]) &&$this->containsSubscription($order_id)) {
																				$parent_order_id = WC_Subscriptions_Renewal_Order::get_parent_order_id( $order_id );
																				$tokens =get_post_meta($parent_order_id, "tokens", true);
																				unset($tokens[0]);
																				update_post_meta($parent_order_id, "tokens", array_values($tokens));
																																								}
																																								/*
							if (get_post_meta( $order_id, 'two_step', true) ==1) {
			$this->finishTransaction($order->get_id());
			}
			*/
				if ( ($order->get_status() == 'on-hold') || ($order->get_status() == 'pending') ) {
					update_post_meta( $order_id, 'simple_id',  $headers['transactionId']);
					update_post_meta( $order_id, 'simple_status', $headers['status']);
					$order->payment_complete($headers['transactionId']);
					$order->update_status($this->successfull_status, __( 'Sikeres fizetés.', 'woo-simple-premium' ) );
					$order->add_order_note( __('Sikeres bankkártyás fizetés, IPN visszahívás megtörtént.', 'woo-simple-premium'));
				}
			}
			exit;
		}
		public function process_refund($order_id, $amount = null, $reason = '') {
			$order = new WC_Order( $order_id );
			$settings =$this->getSimpleSettings();
			$trx = new SimplePayRefund;
			$trx->addConfig($settings);
			$trx->addData('orderRef', $order_id);
			$trx->addData('transactionId', get_post_meta($order_id, 'simple_id', true));
			$trx->addConfigData('merchantAccount', $settings[$this->currency.'_MERCHANT']);
			$trx->addData('refundTotal', $amount);
			$trx->addData('currency', $this->currency);
			$trx->runRefund();
			$result =$trx->getReturnData();
			if (isset($result["refundTransactionId"])) {
				return true;
			}
		}
		function simple_add_jqueryui(){
			$wp_scripts = wp_scripts();
			wp_enqueue_script('jquery-ui-dialog');
			wp_enqueue_style('e2b-admin-ui-css','https://ajax.googleapis.com/ajax/libs/jqueryui/'.$wp_scripts->registered['jquery-ui-core']->ver.'/themes/smoothness/jquery-ui.css',false,$wp_scripts->registered['jquery-ui-core']->ver,false);
			//wp_register_script( "simple_script_frontend", plugins_url( '/simplepay-for-woocommerce-premium/js/frontend.js'), array('jquery'), '1.0', true);
			//wp_register_style( "simple_css_frontend", plugins_url( '/simplepay-for-woocommerce-premium/assets/css/frontend.css','simplepay-for-woocommerce-premium'), array(), date('YmdHis'));
		}
		
		function simple_aszf() {
									// Get the chosen payment gateway (dynamically)
			$chosen_payment_method = WC()->session->get('chosen_payment_method');
			// Set your payment gateways IDs in EACH "IF" statement

			if( $chosen_payment_method == 'otp_simple'){
				/*
				if ($this->enable_card_hold =="yes") {
				echo '<div id ="kartyareg">';
				$cards =get_user_meta(get_current_user_id(), "cards", true);
				if (!empty($cards) &&count($cards) >0) {
					print '<h3>'.__("Regisztrált kártyáim", "woo-simple-premium").'</h3>';
	print "<p>".__('Az alábbi kártyával fizetek: ', 'woo-simple-premium');
	foreach ($cards as $i =>$card) {
	print '<br><input type ="radio" name ="s_card" value ="'.$i.'"> '.$card["name"];
	}
	echo '<br>';
	}
				?>
			<br><label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
				<input id="hold-card-details" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox"  type="checkbox" name="hold_card_details" value="1" /> <span><?php _e("Kártya adatok tárolása későbbi fizetéshez", "woo-simple-premium");?></span>
</label>
<label for ="secret"><b><?php _e("Egyedi jelszó későbbi fizetéshez", "woo-simple-premium");?></b></label>
<span class="woocommerce-input-wrapper"><input id="secret" class="input-text"  type="text" name="secret" value=""/></span>
<?php
				echo '</div>';
				}
				*/
				echo '<div id="checkout_tajekoztato"><small>'.__('A megrendeléssel elfogadom az <a id ="tajekoztato" href ="#">adattovábbítási nyilatkozatot.</a>', 'woo-simple-premium').'</small>';
				$containsSubscription =false;
				$subscrtotal =0;
				foreach( WC()->cart->get_cart() as $cart_item ){
					    $product_id = $cart_item['product_id'];
		$_product = wc_get_product( $product_id );
		if( $_product->is_type( 'subscription' ) || $_product->is_type('variable-subscription')) {
	$containsSubscription =true;
$subscrtotal =$subscrtotal+($cart_item['quantity']*$cart_item['data']->subscription_price);
//print_r($cart_item['data']);
}
}
if ($containsSubscription) {
	echo '<p><small>'.__('A megrendeléssel elfogadom az ismétlődő fizetésre vonatkozó <a id ="tajekoztato-recurring" href ="#">Kártyaregisztrációs nyilatkozatot.</a>', 'woo-simple-premium').'</small></p>';
	}

				echo '<div id="dialog" title="'.__("Adattovábbítási nyilatkozat", "woo-simple-premium").'" style="display:none">';
				printf( __( 'A megrendeléssel tudomásul veszem, hogy a %1$s (%2$s) adatkezelő által a %3$s felhasználói adatbázisában tárolt alábbi személyes adataim átadásra kerülnek az OTP Mobil Kft., mint adatfeldolgozó részére. Az adatkezelő által továbbított adatok köre az alábbi: név, e-mail cím, telefonszám, számlázási cím adatok, szállítási cím adatok. Az adatfeldolgozó által végzett adatfeldolgozási tevékenység jellege és célja a SimplePay Adatkezelési tájékoztatóban, az <a href="http://simplepay.hu/vasarlo-aff " target=_blank">alábbi linken tekinthető meg</a>', 'woo-simple-premium' ), $this->company_name, $this->company_addr, get_home_url());
				echo '</div>';
				echo '<div id="dialog-recurring" title="'.__("Kártyaregisztrációs nyilatkozat", "woo-simple-premium").'" style="display:none">';
				printf( __("Az ismétlődő bankkártyás fizetés (továbbiakban: „Ismétlődő fizetés”) egy, a SimplePay által biztosított bankkártya elfogadáshoz tartozó funkció, mely azt jelenti, hogy a Vásárló által a regisztrációs tranzakció során megadott bankkártyaadatokkal a jövőben újabb fizetéseket lehet kezdeményezni a bankkártyaadatok újbóli megadása nélkül.
<p>Az Ismétlődő fizetés igénybevételéhez jelen nyilatkozat elfogadásával Ön hozzájárul, hogy a sikeres regisztrációs tranzakciót követően jelen webshopban (%s) kezdeményezett későbbi fizetések (max %s db, összeghatár %s %s, lejárata %s) a bankkártyaadatok újbóli megadása és az Ön tranzakciónként hozzájárulása nélkül a Kereskedő által kezdeményezve történjenek.
<p>Figyelem(!): a bankkártyaadatok kezelése a kártyatársasági szabályoknak megfelelően történik. A bankkártyaadatokhoz sem a Kereskedő, sem a SimplePay nem fér hozzá.
<p>A Kereskedő által tévesen vagy jogtalanul kezdeményezett ismétlődő fizetéses tranzakciókért közvetlenül a Kereskedő felel, Kereskedő fizetési szolgáltatójával (SimplePay) szemben bármilyen igényérvényesítés kizárt.
<p>Jelen tájékoztatót átolvastam, annak tartalmát tudomásul veszem és elfogadom.", "woo-simple-premium"), get_home_url(), 24, $subscrtotal, get_woocommerce_currency_symbol(), date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 730 day")));
echo '</div>';        
				echo '</div>';        
		}
							?>
			<script>
				(function($){
										$('form.checkout').on( 'change', 'input[name^="payment_method"]', function() {
												var t = { updateTimer: !1,  dirtyInput: !1,
							reset_update_checkout_timer: function() {
								clearTimeout(t.updateTimer)
								},  trigger_update_checkout: function() {
								t.reset_update_checkout_timer(), t.dirtyInput = !1,
								$(document.body).trigger("update_checkout")
							}
						};
						t.trigger_update_checkout();
					});
					$('#tajekoztato').click(function() {
						$('#dialog').dialog();
						return false;
					});
					$('#tajekoztato-recurring').click(function() {
						$('#dialog-recurring').dialog();
						return false;
					});
				})(jQuery);
			</script>
			<style>
				.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-draggable.ui-resizable {
				width: 90%!important;
				left: 5%!important;
				}
				div#checkout_tajekoztato {
				width: 100%;
				margin-bottom: 20px;
				}
			</style>
			<?php
		}
		/*
			
		function simple_card_details($order_id) {
						
			if (!empty($_POST["secret"])) {
			WC()->session->set('card_secret', $_POST["secret"]);
			
			if (isset($_POST["s_card"])) {
				WC()->session->set('selected_card', $_POST["s_card"]);
				}
										} else {
										WC()->session->set('card_secret', "");
										}
			}
			*/
			public function show_subscription_payment_method($payment_method_to_display, $subscription) {
				if ($subscription->get_payment_method() ==$this->id) {
					$simple_card_mask =get_post_meta( $subscription->get_parent_id(), 'simple_card_mask', true);
					$simple_id =get_post_meta( $subscription->get_parent_id(), 'simple_id', true);
					$simple_card_expire =get_post_meta( $subscription->get_parent_id(), 'simple_card_expire', true);
					$simple_tokens_count =count(get_post_meta($subscription->get_parent_id(), "tokens", true));
					$payment_method_to_display =sprintf(__('SimplePay fizetés. Kártyaszám: %s, lejárata: %s, hátralévő terhelések maximális száma: %s db, SimplePay tranzakció azonosító: %s', 'woo-simple-premium'), $simple_card_mask, $simple_card_expire, $simple_tokens_count, $simple_id);
					return $payment_method_to_display;
					} else {
					return $payment_method_to_display;
					}
				}
	}				