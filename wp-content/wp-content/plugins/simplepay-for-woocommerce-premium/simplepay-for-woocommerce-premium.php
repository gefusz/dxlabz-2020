<?php
	/*
		Plugin Name: SimplePay  for WooCommerce- premium
		Plugin URI: https://bitron.hu/termek/simplepay-for-woocommerce/
		Description: WooCommerce fizetési megoldás a SimplePay rendszeréhez
		Version: 1.3
		Author: bitron.hu
		Author URI: https://bitron.hu
		License:     GPL2
		License URI: https://www.gnu.org/licenses/gpl-2.0.html
		Text Domain: woo-simple-premium
		Domain Path: /lang
		WC requires at least: 3.0
		WC tested up to: 4.0
	*/
	/*
		* updater
	*/
	require 'lib/plugin-update-checker/plugin-update-checker.php';
	$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitron.hu/wpupdater/?action=get_metadata&slug=simplepay-for-woocommerce-premium',
	__FILE__, //Full path to the main plugin file.
	'simplepay-for-woocommerce-premium'
	);
		if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ))) {
		/*
			* plugin activate
		*/
		add_action('activated_plugin', 'simple_activation');
		function simple_activation($plugin) {
			$upload_dir =  wp_upload_dir();

	$files = array(
		array(
			'base' 		=> $upload_dir['basedir'] . '/simple-log',
			'file' 		=> 'index.html',
			'content' 	=> ''
		)
	);

	foreach ( $files as $file ) {
		if ( wp_mkdir_p( $file['base'] ) && ! file_exists( trailingslashit( $file['base'] ) . $file['file'] ) ) {
			if ( $file_handle = @fopen( trailingslashit( $file['base'] ) . $file['file'], 'w' ) ) {
				fwrite( $file_handle, $file['content'] );
				fclose( $file_handle );
			}
		}
		}
			if( $plugin == plugin_basename( __FILE__ ) ) {
				exit(wp_redirect(admin_url( 'admin.php?page=wc-settings&tab=checkout&section=otp_simple' )));
			}
		}
		add_filter( 'plugin_action_links', 'simple_add_links', 10, 2 );
		/*
			* Links for Installed plugins page
		*/
		function simple_add_links( $links, $file ) {
			if ( $file == plugin_basename(dirname(__FILE__) . '/simplepay-for-woocommerce.php') )     {
				if (empty(get_option("simple_license"))) {
					$in = '<a href="'.admin_url( 'admin.php?page=wc-settings&tab=checkout&section=otp_simple' ).'">' . __('Bővítmény aktiválása','woo-simple-premium') . '</a>';
					array_unshift($links, $in);
				}
				$links[] = '<a href="'.admin_url( 'admin.php?page=wc-settings&tab=checkout&section=otp_simple' ).'">' . __('Beállítások','woo-simple-premium') . '</a>';
			}
			return $links;
		}
		/*
			* Simple class init
		*/
		add_action( 'plugins_loaded', 'simple_load' );
		function simple_load() {
			include ("classes/simple-gateway.class.php");
			include("classes/license-manager.class.php");
		}
		/*
			* translations init
		*/
		function simple_init() {
			//translations
			$plugin_dir = basename(dirname(__FILE__));
			load_plugin_textdomain( 'woo-simple-premium', false, plugin_basename( dirname( __FILE__ ) ) . '/lang/' );
		}
		add_action('init', 'simple_init');
		/*
			* Adding to Woocommerce
		*/
		add_filter( 'woocommerce_payment_gateways', 'simple_new_gateway' );
		function simple_new_gateway ($methods) {
			$methods[] = 'WC_otp_simple'; 
			return $methods;
		}
		/*
			* show Woocommerce message on checkout page
		*/
		add_action( 'woocommerce_thankyou', 'simple_showSuccessMessage', 1 );
		function simple_showSuccessMessage( $order_id ) {
			wc_print_notices();
		}
		/*
			* Ajax request for license manager
		*/
		add_action("wp_ajax_licenseManager", "simple_licenseManager");
		function simple_licenseManager() {
			$result =array();
			if (isset($_POST["license_key"])) {
				$lm =new licenseManager("");
				if ($_POST["type"] =="activate") {
					
					$result =$lm->activate($_POST["license_key"]);
					if ($result["msg"] =="success") {
						update_option("simple_license", json_decode($result["license"]));
						update_option("simple_license_activation_date", date("Y-m-d h:i:s"));
					}
				}
				if ($_POST["type"] =="deactivate") {
					$result =$lm->deactivate($_POST["license_key"], $_POST["activation_id"]);
					delete_option("simple_license");
					delete_option("simple_license_activation_date");
				}
			}
			echo json_encode($result, true);
			wp_die();
		}
		
		add_action("wp_ajax_updateCards", "simple_update_cards");
		function simple_update_cards() {
			$cards =get_user_meta(get_current_user_id(), "cards", true);
			$cards[$_POST["id"]]["name"] =$_POST["name"];
						update_user_meta(get_current_user_id(), "cards", $cards);
			$result =array("msg" =>"success");
			echo json_encode($result, true);
			wp_die();
			}
			add_action("wp_ajax_removeCards", "simple_remove_cards");
		function simple_remove_cards() {
			$cards =get_user_meta(get_current_user_id(), "cards", true);
			$cardid =$cards[$_POST["id"]]["cardid"];
			$simpleGW =new WC_otp_simple();
			if ($simpleGW->cardCancel($cardid)) {
			unset($cards[$_POST["id"]]);
			update_user_meta(get_current_user_id(), "cards", $cards);
			$result =array("msg" =>"success");
			} else {
			$result =array("msg" =>"error");
			}
			echo json_encode($result, true);
			wp_die();
			}
			
		/*
			* Loads admin specific scripts
		*/
		add_action( 'admin_init', 'simple_admin_init');
		function simple_admin_init() {
			wp_enqueue_script( "simple_script", plugins_url( '/assets/js/script.js',__FILE__), array('jquery'), '1.0', true);
			wp_enqueue_style( "simple_css", plugins_url( '/assets/css/style.css',__FILE__), array(), date('YmdHis'));
			wp_localize_script( "simple_script", 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
		}
	
	/*
		add_action( 'init', 'simple_frontend_init');
		function simple_frontend_init() {
			wp_enqueue_script( "simple_frontend_script", plugins_url( '/js/frontend.js',__FILE__), array('jquery'), '1.0', true);
			wp_enqueue_style( "simple_frontend_css", plugins_url( '/css/frontend.css',__FILE__), array(), date('YmdHis'));
			wp_localize_script( "simple_frontend_script", 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
		}
		*/
	}		
	/*
		* Alert if plugin not activate
	*/
	add_action( 'admin_notices', 'simple_activate_notice' );
	function simple_activate_notice() {
		if (empty(get_option("simple_license"))) {
			print '    <div class="error notice">'.__("A SimplePay for WooCommerce bővítmény még nincs aktiválva! A továbblépés előtt tegyük ezt meg a beállításokban.", "woo-simple-premium").'</div>';
		}
	}	
	function simple_my_card_endpoint() {
add_rewrite_endpoint( 'my-cards', EP_ROOT | EP_PAGES );
}
add_action( 'init', 'simple_my_card_endpoint' );
function simple_my_cards_query_vars( $vars ) {
$vars[] = 'my-cards';
return $vars;
}
add_filter( 'query_vars', 'simple_my_cards_query_vars', 0 );
function simple_my_cards_link( $menu ) {
	foreach ( $menu as $key => $value ) {
$items[$key] = $value;
			if ($key =='dashboard') {
				$items['my-cards'] = __('Mentett kártyáim', 'woo-simple-premium');
				}
			}
return $items;
}
//add_filter( 'woocommerce_account_menu_items', 'simple_my_cards_link',10, 1);
function simple_my_cards_options() {
$cards =get_user_meta(get_current_user_id(), "cards", true);
			if (!empty($cards) &&count($cards) >0) {
				print '<table id ="my-cards" border ="1">';
				foreach ($cards as $i =>$card) {
					print '<tr><td><input type ="hidden" name ="cards" value ="'.$i.'"><input type ="text" name ="card_name" value ="'.$cards[$i]["name"].'"></td><td><button class ="card-save">Kártya mentése</button></td><td><button class ="card-delete">Kártya eltávolítása</button></td></tr>';
					}
					print '</table>';
				} else {
				_e("Nincsenek regisztrált kártyák!", "woo-simple-premium");
				
				}
	}
	add_action( 'woocommerce_account_my-cards_endpoint', 'simple_my_cards_options' );
	/*
		* Override WooCommerce template for adding SimplePay identifier to related orders table
		Source: https://www.skyverge.com/blog/override-woocommerce-template-file-within-a-plugin/
		*/
	add_filter( 'woocommerce_locate_template', 'simple_woocommerce_locate_template', 10, 3 );
function simple_woocommerce_locate_template( $template, $template_name, $template_path ) {
  global $woocommerce;

  $_template = $template;

  if ( ! $template_path ) $template_path = $woocommerce->template_url;

  $plugin_path  = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/woocommerce/';

  // Look within passed path within the theme - this is priority
  $template = locate_template(

    array(
      $template_path . $template_name,
      $template_name
    )
  );

  // Modification: Get the template from this plugin, if it exists
  if ( ! $template && file_exists( $plugin_path . $template_name ) )
    $template = $plugin_path . $template_name;

  // Use default template
  if ( ! $template )
    $template = $_template;

  // Return what we found
  return $template;
}
