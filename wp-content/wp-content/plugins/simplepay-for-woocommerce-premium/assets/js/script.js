jQuery(function() {
	jQuery("#activate").click(function(e) {
		var license =jQuery("#license_code").val();
		jQuery.ajax({
            dataType: "json",
            type : 'post',
            url: myAjax.ajaxurl,
			data : {
				'action': 'licenseManager',
				'type': 'activate',
				'license_key': license
			},
			success: function(data) {
				if (data["msg"] =="success") {
					window.location.reload();
					} else {
					jQuery("#simple_activation_error").html(data["msg"]);
				}
			}
		});
	});
	
	jQuery("#deactivate").click(function(e) {
		var license =jQuery("#license_key").val();
		jQuery.ajax({
            dataType: "json",
            type : 'post',
            url: myAjax.ajaxurl,
			data : {
				'action': 'licenseManager',
				'type': 'deactivate',
				'license_key': license,
				'activation_id': jQuery("#activation_id").val()
			},
			success: function(data) {
				if (data["msg"] =="success") {
					window.location.reload();
					} else {
					jQuery("#simple_activation_error").html(data["msg"]);
					window.location.reload();
				}
			}
		});
	});
});