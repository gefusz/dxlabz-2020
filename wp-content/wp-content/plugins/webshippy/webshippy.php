<?php
/**
 * Plugin Name: Webshippy Order Sync
 * Plugin URI: http://webshippy.com
 * Description: This plugin syncs all Woocommerce orders with the Webshippy Logistic System
 * Version: 1.2.9
 * Author: Webshippy Ltd.
 * Author URI: http://perenyiandras.com
 * License: All rights reserved
 */

if (defined('WEBSHIPPY_ORDER_SYNC_VERSION') === false) {
    define('WEBSHIPPY_ORDER_SYNC_VERSION', '1.2.9');
}

//  Webshippy URL
if (esc_attr(get_option('webshippy_demo')) == 0) {
    if (defined('webshippy_url') === false) {
        define('webshippy_url', "https://app.webshippy.com/sync_orders_woocommerce.php");
    }
} else {
    if (defined('webshippy_url') === false) {
        define('webshippy_url', "https://demo.webshippy.com/sync_orders_woocommerce.php");
    }
}


// MENU START
add_action('admin_menu', 'webshippy_plugin_menu');

// MENU ELEM
function webshippy_plugin_menu()
{
    add_menu_page('Webshippy', 'Webshippy', 'administrator', 'webshippy-settings', 'webshippy_plugin_settings_page',
      'dashicons-admin-generic');
}

// SETTINGS ELEMEK
add_action('admin_init', 'webshippy_plugin_settings');
function webshippy_plugin_settings()
{
    register_setting('webshippy-plugin-settings-group', 'webshippy_secrect');
    register_setting('webshippy-plugin-settings-group', 'webshippy_demo');
}

/// SETTINGS PAGE START
function webshippy_plugin_settings_page()
{
    ?>
	<div class="wrap">
		<h2>Webshippy Fulfillments - General Settings (<?= WEBSHIPPY_ORDER_SYNC_VERSION ?>)</h2>
		<form method="post" action="options.php">
        <?php settings_fields('webshippy-plugin-settings-group'); ?>
        <?php do_settings_sections('webshippy-plugin-settings-group'); ?>
			<table class="form-table">
				<tr valign="top">
					<th scope="row">Webshippy API Key</th>
					<td><input type="text" name="webshippy_secrect"
										 value="<?php echo esc_attr(get_option('webshippy_secrect')); ?>"/></td>
				</tr>
				<tr valign="top">
					<th scope="row">Demo Mode</th>
					<td><input <?php if (esc_attr(get_option('webshippy_demo')) == 1) {
                  echo "checked";
              } ?> type="checkbox" name="webshippy_demo" value="1"/></td>
				</tr>
			</table>
        <?php submit_button(); ?>
		</form>
	</div>
    <?php
}

add_action('woocommerce_order_status_pending', 'wspy_woocommerce_order_status_pending');
add_action('woocommerce_order_status_failed', 'wspy_woocommerce_order_status_failed');
add_action('woocommerce_order_status_on-hold', 'wspy_woocommerce_order_status_on_hold');
add_action('woocommerce_order_status_processing', 'wspy_woocommerce_order_status_processing');
add_action('woocommerce_order_status_completed', 'wspy_woocommerce_order_status_completed');
add_action('woocommerce_order_status_refunded', 'wspy_woocommerce_order_status_refunded');
add_action('woocommerce_order_status_cancelled', 'wspy_woocommerce_order_status_cancelled');
add_action('edit_post', 'wspy_edit_post', 10, 2);
add_action('updated_post_meta', 'wspy_updated_post_meta', 10, 4);
add_action('woocommerce_payment_complete', 'wspyPaymentCompleteAction');


function wspy_woocommerce_order_status_pending($postId)
{
    wspySendOrder($postId, 'woocommerce_order_status_pending');
}

function wspy_woocommerce_order_status_failed($postId)
{
    wspySendOrder($postId, 'woocommerce_order_status_failed');
}

function wspy_woocommerce_order_status_on_hold($postId)
{
    wspySendOrder($postId, 'woocommerce_order_status_on-hold');
}

function wspy_woocommerce_order_status_processing($postId)
{
    wspySendOrder($postId, 'woocommerce_order_status_processing');
}

function wspy_woocommerce_order_status_completed($postId)
{
    wspySendOrder($postId, 'woocommerce_order_status_completed');
}

function wspy_woocommerce_order_status_refunded($postId)
{
    wspySendOrder($postId, 'woocommerce_order_status_refunded');
}

function wspy_woocommerce_order_status_cancelled($postId)
{
    wspySendOrder($postId, 'woocommerce_order_status_cancelled');
}

function wspy_edit_post($postId) { wspySendOrder($postId, 'edit_post'); }

/**
 * @param int    $metaId
 * @param int    $postId
 * @param string $metKey
 */
function wspy_updated_post_meta($metaId, $postId, $metKey)
{
    if ('_edit_lock' === $metKey) {
        return;
    }
    wspySendOrder($postId, 'updated_post_meta');
}

/**
 * Payment complete
 *
 * @param int $order_id
 */
function wspyPaymentCompleteAction($order_id)
{
    wspySendOrder($order_id, 'woocommerce_payment_complete', 'payment_complete');
}

/**
 * Get shipping method id
 *
 * @param \WC_Order $order
 *
 * @return string|null
 */
function wspyGetShippingMethodId($order)
{

    // detect order shipping methods
    $shipping_methods = $order->get_shipping_methods();

    if (count($shipping_methods)) {
        $shipping_method = array_shift($shipping_methods);

        if (array_key_exists('method_id', $shipping_method)) {
            return stristr($shipping_method['method_id'], ':', true) ?: $shipping_method['method_id'];
        }
    }
}

/**
 * Custom order process
 *
 * @param int         $order_id
 * @param null|string $method
 * @param null|string('payment_complete')|mixed $status
 *
 * @return mixed
 */
function wspySendOrder($order_id, $method = null, $status = null)
{

    // In case of non shop order post type will not send anything!
    if (get_post_field('post_type', $order_id) !== 'shop_order') {
        return;
    }

    // construct WC_Order.
    $order = new WC_Order($order_id);

    // In case of local pickup will not send anything!
    if (wspyGetShippingMethodId($order) === 'local_pickup') {
        return;
    }

    // get status, if it's not payment_complete
    if ($status !== 'payment_complete') {
        $status = $order->get_status();
    }

    // check order status exceptions
    switch ($status) {
        case '':
        case null:
            {
                return;
            }

        // Teljesítve
        case 'completed':
            {
                return;
            }

        // Fizetésre vár
        case 'pending':
        case 'on-hold':
            {
                if ($order->payment_method === 'cod'
                  or $order->payment_method === 'stripe'
                  or $order->payment_method === 'paypal'
                  or $order->payment_method === 'paypal-ec'
                  or $order->payment_method === 'paypal-braintree'
                ) {
                    return;
                }
            }
    }

    $data = array(
      'method'                 => $method,
      'webshippy_version'      => WEBSHIPPY_ORDER_SYNC_VERSION,
      'wp_version'             => isset($wp_version) ? $wp_version : get_bloginfo('version'),
      'woocommerce_version'    => get_option('woocommerce_version'),
      'woocommerce_db_version' => get_option('woocommerce_db_version'),
      'webshippy_secrect'      => esc_attr(get_option('webshippy_secrect')),
      'order'                  => wspyGetOrderData($order, $status),
      'lineitems'              => wspyGetOrderDataLineItems($order),
      'order|orig'             => get_post($order_id),
    );

    $response = wp_remote_post(webshippy_url, array(
      'method'      => 'POST',
      'timeout'     => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'blocking'    => true,
      'headers'     => array(),
      'body'        => json_encode($data)
    ));

    if (is_wp_error($response)) {
        error_log($response->get_error_message());
    }
}

/**
 * @param WC_Order $order
 * @param string   $status
 *
 * @return array
 */
function wspyGetOrderData($order, $status)
{
    $order_id = $order->id;

    // set cod amount
    $cod_amount = $order->payment_method === 'cod' ? $order->get_total() : 0;

    // payment complete status handling
    if ($status === 'payment_complete') {
        $paid = date('Y-m-d H:i:s');
        $payment_status = 'paid';
    } else {
        $paid = null;
        $payment_status = 'pending';
    }

    $billing = $order->get_address('billing');
    $shipping = $order->get_address('shipping');

    $shipping_items = $order->get_items(array('shipping'));
    $bstItem = array_shift($shipping_items);
    $billing_shipping_title = $bstItem ? ($bstItem['name'] ?: '') : '';

    $billing_total_shipping = get_post_meta($order_id, '_order_shipping', true)
      + get_post_meta($order_id, '_order_shipping_tax', true);

    $billing_shipping_vat = get_post_meta($order_id, '_order_shipping_tax', true);
    if ($tmp = get_post_meta($order_id, '_order_shipping', true)) {
        $billing_shipping_vat = $billing_shipping_vat / $tmp;
    }
    $billing_shipping_vat = number_format($billing_shipping_vat, 2);

    $wc_selected_pont = get_post_meta($order_id, 'wc_selected_pont', true) ?: '';

    return array(
      'status'                    => $status,
      'order_id'                  => $order_id,
      'order_name'                => $order_id,
      'created_at'                => $order->order_date,
      'imported_at'               => date('Y-m-d H:i:s'),
      'payment_status'            => $payment_status,
      'paid_at'                   => $paid,
      'payment_gateway'           => $order->payment_method,
      'cod_amount'                => $cod_amount,
      'cod_currency'              => $order->get_order_currency(),
        // shipping
      'shipping_address_name'     => $shipping['first_name'] . ' ' . $shipping['last_name'],
      'shipping_address_company'  => $shipping['company'],
      'shipping_address_address1' => $shipping['address_1'],
      'shipping_address_address2' => $shipping['address_2'],
      'shipping_address_city'     => $shipping['city'],
      'shipping_address_country'  => $shipping['country'],
      'shipping_address_zip'      => $shipping['postcode'],
      'shipping_address_phone'    => $billing['phone'], /// TODO > !BILLING
      'shipping_address_email'    => $billing['email'], /// TODO > !BILLING
      'shipping_note'             => $order->customer_note,
        // billing address
      'billing_address_name'      => $billing['first_name'] . ' ' . $billing['last_name'],
      'billing_address_company'   => $billing['company'],
      'billing_address_address1'  => $billing['address_1'],
      'billing_address_address2'  => $billing['address_2'],
      'billing_address_city'      => $billing['city'],
      'billing_address_country'   => $billing['country'],
      'billing_address_zip'       => $billing['postcode'],
      'billing_address_phone'     => $billing['phone'],
        // billing price
      'billing_total_price'       => $order->get_total(),
      'billing_total_discounts'   => $order->get_total_discount(),
      'billing_total_shipping'    => $billing_total_shipping,
      'billing_shipping_title'    => $billing_shipping_title,
      'billing_shipping_vat'      => $billing_shipping_vat,
        //
      'wc_selected_pont'          => $wc_selected_pont
    );
}


/**
 * @param \WC_Order $order
 *
 * @return array
 */
function wspyGetOrderDataLineItems($order)
{
    $return = array();
    $items = $order->get_items();

    foreach ($items as $item) {
        if (is_object($item) and method_exists($item, 'get_product_id')) {
            $return[] = wspyGetOrderDataLineItemNew($item);
        } else {
            $return[] = wspyGetOrderDataLineItem((object)$item);
        }
    }

    return $return;
}

/**
 * @param $item
 *
 * @return array
 */
function wspyGetOrderDataLineItem($item)
{
    $netUnit = $item->line_total / $item->qty;
    $price = ($item->line_total + $item->line_tax) / $item->qty;

    $variant_name = array();
    if (isset($item->variation_id) && $item->variation_id) {
        $product_sku = get_post_meta($item->variation_id, '_sku', true);
        $variantId = $item->variation_id;
    } else {
        $product_sku = get_post_meta($item->product_id, '_sku', true);
        $variantId = $item->product_id;
    }

    if (empty($product_sku)) {
        $product_sku = get_post_meta($item->product_id, '_sku', true);
    }

    foreach ($item->item_meta_array as $meta) {
        if (strpos($meta->key, '_') === 0) {
            continue;
        }
        $variant_name[] = $meta->value;
    }

    return array(
      'product_sku'            => $product_sku,
      'variant_id'             => $item->product_id,
      'product_name'           => $item->name,
      'variant_name'           => implode(', ', $variant_name),
      'price_net'              => $netUnit,
      'price'                  => number_format($price, 2, '.', ''),
      'vat'                    => number_format($item->line_tax / $item->line_total, 2),
      'required_quantity'      => $item->qty,
      '_yith_wcpb_bundle_data' => get_post_meta($variantId, '_yith_wcpb_bundle_data', true)
    );
}

/**
 * @param \WC_Order_Item_Product $item
 *
 * @return array
 */
function wspyGetOrderDataLineItemNew($item)
{
    $productId = $item->get_product_id();
    $product_name = get_post_field('post_title', $productId);

    $quantity = $item->get_quantity();

    $total = $item->get_total();
    $netUnit = $item->get_total() / $quantity;
    $total_tax = $item->get_total_tax();

    $price = ($total + $total_tax) / $quantity;
    $vat = $total_tax / $total;

    if ($variantId = $item->get_variation_id()) {
        $product_sku = get_post_meta($variantId, '_sku', true);
        $product_name = get_post_field('post_title', $productId);
        $variant_name = $item->get_name();
    } else {
        $product_sku = get_post_meta($productId, '_sku', true);
        $variantId = $productId;
        $variant_name = '';
    }

    return array(
      'product_sku'            => $product_sku,
      'variant_id'             => $variantId,
      'product_name'           => $product_name,
      'variant_name'           => $variant_name,
      'price_net'              => $netUnit,
      'price'                  => number_format($price, 2, '.', ''),
      'vat'                    => number_format($vat, 2),
      'required_quantity'      => $quantity,
      '_yith_wcpb_bundle_data' => get_post_meta($variantId, '_yith_wcpb_bundle_data', true)
    );
}
