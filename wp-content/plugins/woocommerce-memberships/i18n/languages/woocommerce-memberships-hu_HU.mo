��    1      �      ,      ,  
   -     8     @     G  
   c     n     u  	   }  	   �     �     �     �  +   �     �     �     �     	     %     ?  &   X          �     �     �     �     �     �                     $  
   -  
   8     C     O  #   V  5   z  '   �  2   �       5     2   G     z          �  J   �     �     �  �  �     �     �     �     	     		  	   	     	     (	     8	     F	     V	     ]	  -   e	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     	
  	   
  
   
     '
     ;
     P
     ^
  	   c
     m
     u
     }
     �
     �
     �
  %   �
  /   �
     %  a   1  I   �     �  	   �     �  Y     	   ^  	   h   Accessible Actions Active Admin menu nameMemberships Back to %s Cancel Content Dashboard Discounts Excerpt Expires Expires: Looks like you don't have a membership yet! Manage Membership Details Membership StatusActive Membership StatusCancelled Membership StatusExpired Membership StatusPaused Membership StatusPending Cancellation Membership end dateExpires Membership planPlan Membership start dateStart Membership statusStatus Memberships My Membership Next Bill On Next Bill On: Notes Now Products Start Date Start date Start date: Status Thanks for purchasing a membership! There are no discounts available for this membership. There are no notes for this membership. There are no products assigned to this membership. Title To access this content, you must purchase {products}. To access this page, you must purchase {products}. Type View View Billing You can view more details about your membership from %1$syour account%2$s. memberships view Project-Id-Version: WooCommerce Memberships 1.17.2
Report-Msgid-Bugs-To: https://woocommerce.com/my-account/marketplace-ticket-form/
POT-Creation-Date: 2020-04-02 21:14:16+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-06-09 07:04+0000
Last-Translator: Kriston Gábor
Language-Team: Magyar
Language: hu_HU
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.0; wp-5.4.1 Hozzáférhetőség Lehetőségek Aktív Bónusz Vissza a %s Lemondás Tartalom Vezérlőpultba Kedvezmények Rövid leírás Lejár Lejár: Úgy tűnik, még nem rendelkezel bónusszal! Kezelés Bónusz részletei Aktív Lemondva Lejárt Szüneteltetve Függőben lévő lemondás Lejár Bónusz Kezdete Státusz Bónuszok Bónuszaim Következő számla Következő számla: Megjegyzések Most Termékek Kezdete Kezdete Kezdede: Státusz Köszönjük a vásárlásodat! Ez a bónusz nem kedvezményes. A bónuszhoz nincsenek megjegyzések. Ehhez a bónuszhoz nincs hozzárendelt termék. Megnevezés Ahhoz, hogy hozzá férj az oldal tartalmához, szükséged lesz a {products} megvásárlására. A tartalmak eléréshez meg kell vásárolnod a következőt: {products}. Típus Megtekint Számla megtekintése A bónusszal kapcsolatos további részleteket a %1$sFiókom%2$s oldalon tekintheted meg, bónuszok Megtekint 