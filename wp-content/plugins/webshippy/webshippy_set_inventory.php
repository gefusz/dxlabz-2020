<?php
/**
 * webshippy_set_inventory.php
 * 2017.12.18.
 *
 * @author András Würth <wuertha@live.com>
 */

// turn on output buffering
@ob_start();

// content type
@header('Content-type:text/plan;charset=utf-8');

try {

	$sku = str_replace(array("\n", "\t", "\r", "\0", "\x0B", ';'), '', trim($_GET['sku']));
	$stock = intval(preg_replace("/[^0-9]/", "", $_GET['stock']));
	$secret = str_replace(array("\n", "\t", "\r", "\0", "\x0B"), '', trim($_GET['secret']));

	require_once __DIR__ . '/../../../wp-config.php';
	global $wpdb;

	// check secret
	if ($secret != get_option('webshippy_secrect')) {
		throw new \Exception('incorrect secret');
	}

	$productId = $wpdb->get_var('SELECT post_id FROM ' . $table_prefix
		. 'postmeta WHERE meta_key="_sku" AND meta_value="' . $sku . '";');

	if (empty($productId)) {
		throw new \Exception('product not found');
	}

	$productFactory = new WC_Product_Factory;

	$product = $productFactory->get_product($productId);

	if (is_object($product) === false) {
		throw new \Exception('product can not be loaded');
	}

	if (($type = $product->get_type()) !== 'simple' and $type !== 'variation') {
		throw new \Exception('invalid product type:' . $type);
	}

	$needSave = false;
	$stockStatusNew = (0 < $stock) ? 'instock' : 'outofstock';

	if ($stockStatusNew !== $product->get_stock_status()) {
		$needSave = true;
		$product->set_stock_status($stockStatusNew);
	}

	if ($stock != $product->get_stock_quantity()) {
		$needSave = true;
		$product->set_stock_quantity($stock);
	}

	if ($needSave and $product->save()) {
		die('success|ok');
	}
	else if ($needSave) {
		throw new \Exception('error while save product');
	}

	die('success|ok');

} catch (\Exception $e) {
	die('error|sku:' . $sku . '|' . $e->getMessage());
}