<?php

/**
 * /wp-content/plugins/webshippy/webshippy_get_products.php
 *
 * Download products 'manual'
 *
 * page: the page num to download with limit 100
 * secret: the api key
 *
 * webshippy_secrect:
 *  - Webshippy Secret API Key
 */

// turn on output buffering
@ob_start();

// content type
@header('Content-type:text/plan;charset=utf-8');

require_once __DIR__ . '/../../../wp-config.php';
global $wpdb;

// get page num
$page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT) ?: 0;

// check secret
if ($secret = filter_input(INPUT_GET, 'secret', FILTER_SANITIZE_STRING)) {
    $secret = $wpdb->get_var('SELECT option_id FROM ' . $table_prefix . 'options '
        . ' WHERE option_name="webshippy_secrect" AND option_value = "' . $secret . '"');
}

if (empty($secret)) {
    die('error|authentication failed.' . $secret);
}

/**
 * Get products list
 */
$list = $wpdb->get_results('SELECT 
		wp.ID AS variant_id, 
		wp.post_title AS product_name, 
		wp.post_content AS product_description, 
		(SELECT MAX(m.meta_value) FROM ' . $table_prefix . 'postmeta AS m WHERE m.post_id = wp.ID AND m.meta_key = "_sku") AS sku,
		(SELECT MAX(m.meta_value) FROM ' . $table_prefix . 'postmeta AS m WHERE m.post_id = wp.ID AND m.meta_key = "_barcode") AS barcode, 
		(SELECT MAX(m.meta_value) FROM ' . $table_prefix . 'postmeta AS m WHERE m.post_id = wp.ID AND m.meta_key = "_weight") AS weight, 
		(SELECT MAX(m.meta_value) FROM ' . $table_prefix . 'postmeta AS m WHERE m.post_id = wp.ID AND m.meta_key = "_length") AS deep, 
		(SELECT MAX(m.meta_value) FROM ' . $table_prefix . 'postmeta AS m WHERE m.post_id = wp.ID AND m.meta_key = "_width") AS width, 
		(SELECT MAX(m.meta_value) FROM ' . $table_prefix . 'postmeta AS m WHERE m.post_id = wp.ID AND m.meta_key = "_height") AS height,
		COALESCE((SELECT MAX(tax_rate) FROM ' . $table_prefix . 'woocommerce_tax_rates WHERE tax_rate_country  = "HU"), 0) AS tax_rate, 
		COALESCE((SELECT MAX(m.meta_value) FROM ' . $table_prefix . 'postmeta AS m WHERE m.post_id = wp.ID AND m.meta_key = "_regular_price"), 0) AS price,
		(SELECT MAX(m.meta_value) FROM ' . $table_prefix . 'postmeta AS m WHERE m.post_id = wp.ID AND m.meta_key = "_tax_status") AS tax_status,
		(SELECT MAX(m.meta_value) FROM ' . $table_prefix . 'postmeta AS m WHERE m.post_id = wp.ID AND m.meta_key = "_prices_include_tax") AS prices_include_tax
	FROM 
		' . $table_prefix . 'posts AS wp 
	WHERE 
		(
			wp.post_type = "product"
			OR wp.post_type = "product_variation"
		) 
	GROUP BY wp.ID 
	ORDER BY wp.ID 
	LIMIT ' . ($page * 100) . ', 100;', ARRAY_A);


foreach ($list as $i => $product) {
    $results = $wpdb->get_results("
            SELECT guid 
            FROM " . $table_prefix . "posts 
            WHERE post_type = 'attachment' 
                AND post_parent = " . $product['variant_id'], ARRAY_A);

    $list[$i]['images'] = array_map(function ($item) {
        return $item['guid'];
    }, $results);

    $taxRate = $product['tax_rate'];
    $price = $product['price'] ?: 0;
    $list[$i]['vat'] = $taxRate / 100;

    if ($product['prices_include_tax'] === 'yes') {
        if ($product['tax_status'] === 'taxable') {
            $list[$i]['net_price'] = $price / (($taxRate + 100) / 100);
        } else {
            $list[$i]['net_price'] = $price;
        }
    } else {
        $list[$i]['net_price'] = $price;
    }

    unset($list[$i]['price'], $list[$i]['prices_include_tax'], $list[$i]['tax_status']);
}

// set response content type and character encoding
header('Content-type:application/json;charset=utf-8');

// print json encoded result array
echo json_encode($list);

// exit from code
exit;
