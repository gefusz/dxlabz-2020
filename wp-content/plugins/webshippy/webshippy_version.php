<?php
/**
 * /wp-content/plugins/webshippy/webshippyVersion.php
 *
 * Author: Webshippy Ltd.
 * Author URI: https://webshippy.com
 *
 * webshippy_secrect:
 *  - Webshippy Secret API Key
 */

@ob_start();

require_once __DIR__ . '/../../../wp-config.php';
global $wpdb;


if (defined('WEBSHIPPY_ORDER_SYNC_VERSION') === false) {
    define('WEBSHIPPY_ORDER_SYNC_VERSION', '1.2.9');
}

if ($secret = filter_input(INPUT_GET, 'secret', FILTER_SANITIZE_STRING)) {
    $secret = $wpdb->get_var('SELECT option_id FROM ' . $table_prefix . 'options '
      . ' WHERE option_name="webshippy_secrect" AND option_value = "' . $secret . '"');
}

if (empty($secret)) {
    die('error|authentication failed.' . $secret);
}

header('Content-type:application/json;charset=utf-8');
echo json_encode(array(
  'webshippy_version'      => WEBSHIPPY_ORDER_SYNC_VERSION,
  'wp_version'             => isset($wp_version) ? $wp_version : get_bloginfo('version'),
  'woocommerce_version'    => get_option('woocommerce_version'),
  'woocommerce_db_version' => get_option('woocommerce_db_version'),
));
exit;
