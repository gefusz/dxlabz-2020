<?php

/**
 * User 'manual'
 *
 * @link (to call)
 * http://example.com/wp-content/plugins/webshippy/webshippy_set_status.php?id={order_id}&status={order_status}&secret={webshippy_secrect}
 *
 * order_id:
 *  the post id in wordpress posts table
 *
 * order_status (like enum):
 *  - pending     - Fizetésre vár
 *  - on-hold     - Fizetésre vár
 *  - processing  - Feldolgozás alatt
 *  - failed      - Sikertelen
 *  - completed   - Teljesítve
 *  - refunded    - Visszatérítve
 *  - cancelled   - Visszamondva
 *
 * webshippy_secrect:
 *  - Webshippy Secret API Key
 */

// turn on output buffering
@ob_start();

// content type
@header('Content-type:text/plan;charset=utf-8');

$statusArr = array(
	'pending', // Fizetésre vár
	'on-hold', // Fizetésre vár
	'processing', // Feldolgozás alatt
	'failed', // Sikertelen
	'completed', // Teljesítve
	'refunded', // Visszatérítve
	'cancelled', // Visszamondva
);

$status = trim($_GET['status']);

if (in_array($status, $statusArr) === false) {
	die('error|incorrect status.');
}

require_once __DIR__ . '/../../../wp-config.php';
global $wpdb;

// check secret
$secretID = $wpdb->get_var('SELECT option_id FROM ' . $table_prefix . 'options '
	. ' WHERE option_name="webshippy_secrect" AND option_value = "' . $_GET['secret'] . '"');

if (empty($secretID)) {
	die('error|authentication failed. (1)');
}


$orderId = intval($_GET['id']);

/**
 * Check order
 */
$type = $wpdb->get_var('SELECT post_type FROM ' . $table_prefix . 'posts WHERE ID = ' . $orderId);
if ($type !== 'shop_order') {
	die('error|incorrect post type.');
}


$order = new \WC_Order($orderId);

if ($order->get_status() === $status) {
	die('success|ok');
}

if ($order->update_status($status, 'Triggered by Webshippy' . PHP_EOL) === true) {
	die('success|ok');
}


die('success|no-changes');