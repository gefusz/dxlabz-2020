=== Integration for Szamlazz.hu & WooCommerce ===
Contributors: passatgt
Tags: szamlazz.hu, szamlazz, woocommerce, szamlazo, magyar
Requires at least: 5.0
Tested up to: 5.7
Stable tag: 5.3.0.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Számlázz.hu összeköttetés WooCommerce-hez.

== Description ==

> **PRO verzió**
> A bővítménynek elérhető a PRO verziója 10.000 Ft-ért, amelyet itt vásárolhatsz meg: [https://visztpeter.me](https://visztpeter.me/woocommerce-szamlazz-hu/)
> A licensz kulcs egy weboldalon aktiválható, 1 évig érvényes és természetesen emailes support is jár hozzá beállításhoz, testreszabáshoz, konfiguráláshoz.
> A vásárlással támogathatod a fejlesztést akkor is, ha esetleg a PRO verzióban elérhető funkciókra nincs is szükséged.

= Funkciók =

* **Manuális számlakészítés**
Minden rendelésnél a jobb oldalon megjelenik egy új gomb, rákattintáskor elküldi az adatokat számlázz.hu-nak és legenerálja a számlát.
* **Automata számlakészítés** _PRO_
Lehetőség van a számlát automatikusan elkészíteni bizonyos fizetési módoknál, vagy ha a rendelés teljesítve lett
* **Mennyiségi egység**
A tételek mellett a mennyiségi egységet is feltüntetni a számlát, amelyet a beállításokban minden termékhez külön-külön meg tudod adni és megjegyzést is tudsz megadni a tételhez
* **E-Nyugta** _PRO_
Ha elektronikus termékeket, jegyeket, letölthető tartalmakat értékesítesz, nem fontos bekérni a vásárló számlázási címét, elég csak az email címét, a bővítmény pedig elekronikus nyugtát készít
* **Számlaértesítő** _PRO_
Az ingyenes verzióban a számlázz.hu küldi ki a számlaértesítőt a vásárlónak. A PRO verzióban csatolni lehet a WooCommerce által küldött emailekhez, így nem fontos használni a Számlázz.hu számlaértesítőjét és a vásárlód egyel kevesebb emailt fog kapni
* **Nemzetközi számla**
Ha külföldre értékesítesz például euróban, lehetőség van a számla nyelv átállítására és az aktuális MNB árfolyam feltüntetésére a számlán. Kompatibilis WPML-el és Polylang-al is.
* **Automata díjbekérő létrehozás** _PRO_
Ha a rendelés állapota átállítódik függőben lévőre, automatán legenerálja a díjbekérő számlát. Lehet kézzel egy-egy rendeléshez külön díjbekérőt is csinálni.
* **Előleg számla**
A díjbekérő helyett lehetőség van előleg számlát készíteni
* **IPN és teljesítettnek jelölés** _PRO_
A kifizetettségéről értesítést kaphat a webáruház, illetve automatán teljesítettnek jelölheti a számlát
* **Naplózás**
Minden számlakészítésnél létrehoz egy megjegyzést a rendeléshoz, hogy mikor, milyen néven készült el a számla
* **Sztornózás**
A számla sztornózható a rendelés oldalon, vagy kikapcsolható 1-1 rendeléshez
* **Szállítólevél**
Lehetőség van arra, hogy a számlakészítés mellett szállítólevelet is készítsen automatikusan a rendszer
* **Adószám mező**
A WooCommerce-ben alapértelmezetten nincs adószám mező. Ezzel az opcióval bekapcsolható, hogy a számlázási adatok között megjelenjen. Az adószámot a rendszer eltárolja, a vásárlónak küldött emailben és a rendelés adatai között is megjelenik. Lehetőség van arra, hogy csak 100.000 Ft áfatartalom felett látszódjon.
* **Könyvelési adatok** _PRO_
Termékkategóriánként megadhatók a könyveléssel kapcsolatos adatok magyar és külföldi vásárlásoknál: főkönyvi szám, árbevételi főkönyvi szám, gazdasági esemény, áfa gazdasági esemény. A felhasználó azonosítóját pedig a vevő főkönyvi azonosítójaként eltárolja.
* **Automata teljesítettnek jelölés**
Beállítható, melyik fizetési módoknál jelölje meg a számlát teljesítettnek automatikusan
* **És még sok más**
Papír és elektronikus számla állítás, áfakulcs állítás, számlaszám előtag módosítása, letölthető számlák a vásárló profiljában, hibás számlakészítésről e-mailes értesítő stb...

= Fontos kiemelni =
* A generált számlát letölti saját weboldalra is, egy véletlenszerű fájlnéven tárolja a wp-content/uploads/wc_szamlazz mappában
* Fizetési határidő és megjegyzés írható a számlákhoz
* Kuponokkal is működik, a számlán negatív tételként fog megjelenni a végén
* Szállítást is ráírja a számlára
* A PDF fájl letölthető egyből a Rendelések oldalról is(táblázat utolsó oszlopa)

= Használat =
Részletes dokumentációt [itt](https://visztpeter.me/dokumentacio/) találsz.
Telepítés után a WooCommerce / Beállítások oldalon meg kell adni a szamlazz.hu felhasználónevet és jelszót, illetve az ott található többi beállításokat igény szerint.
Minden rendelésnél jobb oldalon megjelenik egy új doboz, ahol egy gombnyomással létre lehet hozni a számlát. Az Opciók gombbal felül lehet írni a beállításokban megadott értékeket 1-1 számlához.
Ha az automata számlakészítés be van kapcsolva, akkor a rendelés lezárásakor(Teljesített rendelés státuszra állítás) automatikusan létrehozza a számlát a rendszer.
A számlakészítés kikapcsolható 1-1 rendelésnél az Opciók legördülőn belül.
Az elkészült számla a rendelés aloldalán és a rendelés listában az utolsó oszlopban található PDF ikonra kattintva letölthető.

**FONTOS:** Mindenen esetben ellenőrizd le, hogy a számlakészítés megfelelő e és konzultálj a könyvelőddel, neki is megfelelnek e a generált számlák. Sajnos minden esetet nem tudok tesztelni, különböző áfakulcsok, termékvariációk, kuponok stb..., így mindenképp teszteld le éles használat előtt és ha valami gond van, jelezd felém és megpróbálom javítani.

= Fejlesztőknek =

A plugin egy XML fájlt generál, ezt küldi el a szamlazz.hu-nak, majd az egy pdf-ben visszaküldi az elkészített számlát. Az XML fájl generálás előtt módosítható a `wc_szamlazz_xml` filterrel. Ez minden esetben az éppen aktív téma functions.php fájlban történjen, hogy az esetleges plugin frissítés ne törölje ki a módosításokat!

Lehetőség van sikeres és sikertelen számlakészítés után egyedi funckiók meghívására a bővítmény módosítása nélkül:

   <?php
   add_action('wc_szamlazz_after_invoice_success', 'sikeres_szamlakeszites',10,4);
   function($order, $response, $szamlaszam, $pdf_link) {
     //...
   }

   add_action('wc_szamlazz_after_invoice_error', 'sikeres_szamlakeszites',10,5);
   function($order, $response, $agent_error_code, $agent_error, $agent_body) {
     //...
   }
   ?>

== Installation ==

1. Töltsd le a bővítményt
2. Wordpress-ben bővítmények / új hozzáadása menüben fel kell tölteni
3. WooCommerce / Integráció menüpontban találhatod a Számlázz.hu beállítások, itt legalább az agent kulcs mezőt kötelező kitölteni
4. Működik

== Frequently Asked Questions ==

= Mi a különbség a PRO verzió és az ingyenes között? =

A PRO verzió néhány hasznos funkciót tud, amiről [itt](https://visztpeter.me/woocommerce-szamlazz-hu/) olvashatsz. De a legfontosabb az automata számlakészítés, díjbekérő létrehozás és az E-Nyugta támogatás, amivel digitális termékeidet(letölthető könyvek, tanfolyamok, jegyek) számla helyett nyugtával is eladhatsz, így a vásárlódnak nem kell megadnia az összes személyes adatát(csak név + email). Továbbá 1 éves emailes support is jár hozzá.

= Hogyan lehet tesztelni a számlakészítést? =

A számlázz.hu-tól lehet kérni kapcsolat űrlapon keresztül, hogy állítsák át a fiókot Teszt üzemmódba, így lehet próbálgatni a számlakészítést.

= Teszt módban vagyok, de a számlaértesítő nem a vásárló email címére megy. =

A számlaértesítő teszt módban nem a vásárló email címére érkezik, hanem a számlázz.hu-n használt fiók email címére.

== Screenshots ==

1. Beállítások képernyő(WooCommerce / Beállítások)
2. Számlakészítés doboz a rendelés oldalon

== Changelog ==

= 5.3.0.4 =
* Nyugtakészítés bugfix

= 5.3.0.3 =
* A nyugtára is lehet feltételes megjegyzést írni
* Woo Subscription bugfix

= 5.3.0.2 =
* Az "ingyenes rendeléshez nem kell számla" opciót a nyugtára is vonatkozik

= 5.3.0.1 =
* Bugfix az új automatizáláshoz

= 5.3 =
* Új beállítási lehetőségek automatikus számlakészítéshez - egyelőre beta funkcióként, a meglévő beállításokat nem módosítja.
* Beállítások link hozzáadása a WooCommerce kezdőoldalhoz, hogy ne kelljen mindig az Integráció menüig elnavigálni

= 5.2.9.1 =
* Hibajavítás az "Adószám mező mindig látszódjon" opcióhoz

= 5.2.9 =
* Adószám mező javítások
* Kategória szerinti könyvelési adatok javítása
* PHP warning javítás az előnézeten
* Egyedi számla logo beállítás hibajavítása
* WP 5.7 és WC 5.1 kompatibilitás ellenőrzése / megjelölése

= 5.2.8 =
* WooCommerce Subscriptions kompatibilitás fejlesztések
* Cím kitöltés javítása adószám alapján

= 5.2.7 =
* TEHK és TAHK áfakulcs kompatibilitás

= 5.2.6 =
* A számla megjegyzésben feltüntethető a szállítási cím({shipping_address}) és a vásárlói megjegyzés({customer_note}). További placeholder egyszerűen hozzáadható a wc_szamlazz_get_order_note_placeholders filterrel.
* A megjegyzés mezőben használt rövidkódok működnek a számlaértesítű levél címében és tárgyában is
* EU és EUK áfakulcsok átnevezése EUT-re és EUKT-re
* Kompatibilitás megjelölése legújabb WC verzióval

= 5.2.5 =
* Adószámot elfogadja kötőjelek nélkül is(mentéskor automatán hozzáadja a kötőjeleket)
* Új megjegyzés feltétel: számlázási ország

= 5.2.4 =
* Sztornózáskor a díjbekérőt is letörli automatikusan(opcionális)
* WooCommerce Advanced Quantity kompatibilitás javítása
* Manuális számlakészítéskor az Opciók gombra kattintva nem görget fel az oldal tetejére
* Minified admin és frontend JS

= 5.2.3 =
* WooCommerce Checkout Manager kompatibilitás javítása
* WooCommerce Advanced Quantity kompatibilitás javítása

= 5.2.2.1 =
* Egy PHP warning javítása
* Wordpress 5.6 kompatibilitás
* WooCommerce 4.8 kompatibilitás

= 5.2.2 =
* WooCommerce Currency Switcher by WooBeWoo kompatibilitás
* Bugfix max_input_vars-al kapcsolatban
* Számla megjegyzésnél feltételnek kiválasztható, hogy EU-n belüli, vagy kívüli-e a vásárló
* A wc_szamlazz_ipn_document_type filterrel módosítható, hogy a számlázz.hu milyen típusú dokumentumot küld IPN-en keresztül
* A bővítmény fájlméretének csökkentése
* Hibajavítás több fiók használata esetén

= 5.2.1 =
* Adószám mező kompatibilitás a Checkout Manager for WooCommerce bővítménnyel
* Egy potenciális PHP warning javítása
* Kompatibilitás megjelölése a legújabb WC verzióval(4.7.0)

= 5.2.0.1 =
* Hibajavítás az számla továbbítás funkcióhoz

= 5.2 =
* Termék beállításoknál kikapcsolható az automata számlakészítés: ha a rendelésben van az adott termék, nem készül számla automatán
* Translatepress kompatibilitás: a kiválasztott nyelv alapján készül a számla is
* Ha több fiókot használsz, működni fog a sztornózás manuális számlakészítés után
* Csatolható a sztornó számla a visszatérített rendelés e-mailhez
* Kompatibilitás a WooCommerce EU Vat Number 2.3.21+ verziókkal
* A rendelés előnézetben is látszódnak a számlák
* Hibajavítás csoportos számla letöltéshez és nyomtatáshoz
* Apróbb hibajavítások az előnézet funkciónál
* Fejlesztői módban az előnézeten látszik az XML fájl is(fejlesztéshez és supporthoz is hasznos)
* Javítás a számla megjegyzésben lévő sortöréshez
* Ha egy rendelésnél ki van kapcsolva a számlakészítés, nem látszódik feleslegesen a fizetettnek jelölés gomb
* Angol nyelvű lett a bővítmény, amihez mellékelve van a magyar fordítás
* WooCommerce 4.6.1 kompatibilitás megjelölése

= 5.1.3 =
* Kompatibilitás a megbízott számlakibocsátáshoz és önszámlázáshoz
* PHP Warning javítása automata sztornózásnál

= 5.1.2 =
* Hibajavítás a rendelési tétel attribútumok megjelenítésével kapcsolatban
* Kompatibilitás megjelölése legújabb WC verziókkal

= 5.1.1 =
* Adószám funkciók megfelelően működnek a fiókom oldalon is
* Az előnézeten látszik az adószám is
* Hibajavítás az előnézeten akkor, ha több tétel van a számlán
* PHP warning javítása belépett felhasználóknál pénztár űrlap elküldése után üres adószámmal

= 5.1.0.1 =
* Közösségi adószám bugfix

= 5.1 =
* Ha egyedi dátummal van megjelölve a teljesítettnek jelölés, akkor megfelelő dátumot tárol el az adatbázisban is
* Apróbb hibajavítások az automata adatkitöltés adószám alapján funkcióhoz
* Fizetettnek jelöléskor megfelelő dátumot ír ki a válaszban(oldal frissítés után eddig is jó volt)
* Hibajavítás adószám mező megjelenítésével kapcsolatosan
* Hibajavítás fixen beállított százalékos áfakulcsnál a negatív kedvezmény tételhez
* Kompatibilitás WooCommerce 4.4-el és WordPress 5.5-el

= 5.0.0.3 =
* Kompatibilitás régebbi WooCommerce verzióval

= 5.0.0.2 =
* Hibajavítás a WooCommerce Product Bundles kompatibilitással kapcsolatban

= 5.0.0.1 =
* Kompatibilitás javítása régebbi WC verziókkal

= 5.0 =
* Az adószám megadása után automatikusan ki tudja tölteni a pénztár űrlapot(PRO)
* A rendeléskezelőben egy kattintással fizetettnek lehet jelölni a fizetetlen számlákat(és látni, hogy melyik mikor lett fizetve)(PRO)
* Előnézet funkció: számlakészítés előtt meg tudod nézni, hogy hogyan fog kinézni a számla(PRO)
* Okosabban kezeli az áfakulcsokat ingyenes tételeknél(terméknél és szállításnál is)
* Beállítások az EU és EUK áfakulcs automatizálására
* Az üzenetek(például a bővítmény aktiválásakor, hibás számlakészítéskor stb...) átkerültek a WooCommerce Admin-ba, így nem keverednek a többi Wordpress üzenettel és csak a WooCommerce menüben látszódnak
* Az adószámnál ellenőrzi az adóalany ÁFA jellegét is(9. számjegy)
* Ha manuálisan van létrehozva a rendelés és kiválasztasz egy meglévő vásárlót, akkor kitölti az adószám mezőt is automatán(ha volt neki elmentve)
* Lehet megadni egyedi tétel megjegyzést és mennyiségi egységet a szállítási tételnek is
* Hibajavítás egy olyan bughoz, ami bizonyos PHP verziónál jött csak elő
* EU adószám kompatibilitás javítás
* A wc_szamlazz_xml_adoszam és wc_szamlazz_xml_adoszam_eu filterekkel könnyen módosítható az adószám
* Kompatibilitás megjelölése WooCommerce 4.3.0-val

= 4.8.1 =
* Az "adószám mező a mindig látszódik" beállítás használatakor csak Magyarországot kiválasztva látszódik a mező, így több értelme van
* WooCommerce Product Bundles kompatibilitás(az ingyenes tételek helyett a fő tétel megjegyzésébe írja bele a csomagban lévő termék nevét és mennyiségét)
* A `wc_szamlazz_pdf_file_name` filterbe bekerült a számla paraméter is, így a fájlnévben megjeleníthető a számla száma is

= 4.8.0.1 =
* Kompatibilitás javítása a HuCommerce bővítménnyel

= 4.8 =
* Csoportos műveletekben van egy új dokumentum készítés opció, amivel egyedi paraméterekkel(fizetési határidő, teljesítés ideje, fiók stb...) lehet csoportosan létrehozni dokumentumokat(PRO verzió része)
* A `wc_szamlazz_pdf_file_name` filterrel módosítható a generált PDF fájl neve(például ha a vásárló nevét, vagy rendelés számát szeretnéd megjeleníteni)
* WooCommerce EU Vat Number kompatibilitás

= 4.7.3.1 =
* Hibajavítás a WooCommerce Advanced Quantity kompatibilitással kapcsolatban
* Adószám lekérdezés eltárolja a címet is, nem csak a nevet

= 4.7.3 =
* Az adószám mező ellenőrzése mindig megtörténik(nem kell külön bekapcsolni)
* Az adószám mezőt meg lehet jeleníteni állandóan(alapértelmezetten csak akkor látszik, ha a vásárló megadott egy cégnevet)
* Az adószámos JS fájlt csak a pénztár és a fiókom oldalon tölti be
* Hibajavítás fixen beállított százalékos áfakulcsokhoz
* Ha már készült számla a rendeléshez, akkor az IPN nem állítja át a rendelés státuszt
* IPN kérelem után készült számla mellé is készül automatikusan szállítólevél
* Az IPN kérelmet akkor is naplózza, ha nincs bekapcsolva a fejlesztői mód
* Hibajavítás csoportos szállítólevél letöltéssel és nyomtatással kapcsolatban
* Kompatibilitás a WooCommerce Advanced Quantity bővítménnyel(mennyiségi egység megjelenítéséhez)

= 4.7.2 =
* A számlán megjelenő dátumoknál figyelembe veszi a WordPress beállításaiban megadott időzónát(éjfél utáni számlakészítéskor átcsúszhatott a kelt dátum az előző napra)

= 4.7.1.3 =
* Hibajavítás az IPN kapcsolatnál

= 4.7.1.2 =
* A beérkező IPN hívásnál beállítható, hogy pontosan milyen státuszba kerüljön a rendelés
* Kompatibilitás megjelölése legújabb WC verzióval

= 4.7 =
* Az adószám megjelenik a köszönöm oldalon, a kiküldött e-mailekben, a fiókom menüben(rendelések és címek oldalon).
* Az adószám módosítható a rendelés adatainál
* A csoportos műveleteknél szállítólevelet is lehet letölteni és nyomtatni
* A `wc_szamlazz_ipn_request_parameters` filterrel módosítható a beérkező IPN kérelem(például hogy az egyedi rendelésszámot le tudd cserélni az adatbázisban lévő ID-ra)
* A `wc_szamlazz_notes_conditions` és `wc_szamlazz_notes_conditions_values` filterekkel létrehozható egyedi feltétel a megjegyzésekhez
* Lehetőség van arra, hogy a csoportos számlaletöltéskor ne egy PDF fájlt generáljon, hanem egy ZIP fájlba becsomagolja a kiválasztott számlákat
* Beállítható a számlakép opció is
* Kompatibilitás megjelölése legújabb WC és WP verziókkal

= 4.6 =
* A fizetési módok elnevezése felülírható a számlán
* Kikapcsolható az automata számlakészítés bizonyos fizetési módoknál
* A megjegyzésnél az agent kulcs is kiválasztható feltételként
* A tétel megjegyzésben nem szerepel az "Utánrendelve" leírás
* Kiválasztható normál %-os áfakulcs felülírás a beállításokban
* Ha be van kapcsolva az automata számlakészítés, akkor a számlakészítő dobozban kiírja, hogy melyik státusznál fog automatán számla készülni
* Támogatás kiegészítő bővítményekhez
* A `wc_szamlazz_settings_fields` filterrel lehet hozzáadni egyedi beállításokat
* A számlakészítő doboz testreszabható a `wc_szamlazz_metabox_generate_before`, `wc_szamlazz_metabox_generate_after`, `wc_szamlazz_metabox_generate_options_before` és `wc_szamlazz_metabox_generate_options_after` action-ökkel
* Webhook hibajavítás
* PRO verzióban kikapcsolható az automata számlakészítés(egy hiba miatt mindig ment)
* Hibajavítás a Webhely Egészség menüben(jól mutatja, ha a fejlesztői mód bekapcsolva maradt)
