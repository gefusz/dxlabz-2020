<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//Translatepress Compatibility
class WC_Szamlazz_Translatepress_Compatibility {

	public static function init() {
		add_filter( 'wc_szamlazz_xml', array( __CLASS__, 'change_language' ), 10, 2 );
		add_filter( 'wc_szamlazz_get_order_language', array( __CLASS__, 'get_language'), 10, 2);
	}

	public static function change_language( $invoice, $order ) {
		if($locale = $order->get_meta('_translatepress_locale')) {
			$locale = substr($locale, 0, 2);
			if($locale && in_array($locale, array('hu', 'de', 'en', 'it', 'fr', 'hr', 'ro', 'sk', 'es', 'pl', 'cz'))) {
				$invoice->fejlec->szamlaNyelve = $locale;
			}
		}

		return $invoice;
	}

	public static function get_language($lang_code, $order) {
		if($locale = $order->get_meta('_translatepress_locale')) {
			$locale = substr($locale, 0, 2);
			if($locale && in_array($locale, array('hu', 'de', 'en', 'it', 'fr', 'hr', 'ro', 'sk', 'es', 'pl', 'cz'))) {
				$lang_code = $locale;
			}
		}
		return $lang_code;
	}

}

WC_Szamlazz_Translatepress_Compatibility::init();
