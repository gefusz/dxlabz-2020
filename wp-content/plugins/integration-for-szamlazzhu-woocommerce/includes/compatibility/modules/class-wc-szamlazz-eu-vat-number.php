<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//WooCommerce EU Vat Number Compatibility
class WC_Szamlazz_WooCommerce_EU_Vat_Number_Compatibility {

	public static function init() {
		add_filter( 'wc_szamlazz_xml_adoszam_eu', array( __CLASS__, 'add_eu_vat_number' ), 10, 2 );

	}

	public static function add_eu_vat_number( $adoszam_eu, $order ) {

		//EU VAT Assistant
		if(function_exists('wc_eu_vat_get_vat_from_order')) {
			$adoszam_eu = wc_eu_vat_get_vat_from_order($order);
		} else {
			$adoszam_eu = $order->get_meta( '_vat_number' );
		}

		return $adoszam_eu;
	}

}

WC_Szamlazz_WooCommerce_EU_Vat_Number_Compatibility::init();
