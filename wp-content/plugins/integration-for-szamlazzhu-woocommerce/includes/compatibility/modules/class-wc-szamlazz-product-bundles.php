<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//WooCommerce Product Bundles Compatibility
class WC_Szamlazz_Product_Bundles_Compatibility {

	public static function init() {
		add_filter( 'wc_szamlazz_invoice_line_item', array( __CLASS__, 'add_product_bundle_info' ), 10, 4 );
	}

	public static function add_product_bundle_info( $tetel, $order_item, $order, $szamla ) {

		//Check if line item is a container
		if(wc_pb_is_bundle_container_order_item($order_item) && $tetel->bruttoErtek != 0) {

			//Get bundled items
			$bundled_items = wc_pb_get_bundled_order_items($order_item, $order);
			foreach ($bundled_items as $bundled_order_item) {
				$tetel->megjegyzes .= '• '.$bundled_order_item->get_quantity().'× '.$bundled_order_item->get_name()."\n";
			}

		}

		//Hide separate bundle items
		if(wc_pb_is_bundled_order_item($order_item, $order) && $tetel->bruttoErtek == 0) {
			$tetel = false;
		}

		//Hide main item if its free
		if(wc_pb_is_bundle_container_order_item($order_item) && $tetel->bruttoErtek == 0) {
			$tetel = false;
		}

		return $tetel;
	}

}

WC_Szamlazz_Product_Bundles_Compatibility::init();
