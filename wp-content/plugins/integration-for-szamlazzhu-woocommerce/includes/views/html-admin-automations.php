<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//Get saved values
$saved_values = get_option('wc_szamlazz_automations');
$countries_obj = new WC_Countries();
$countries = $countries_obj->__get('countries');

//Set valid documents for automation
$document_types = array(
	'invoice' => __('Invoice', 'wc-szamlazz'),
	'proform' => __('Proforma invoice', 'wc-szamlazz'),
	'deposit' => __('Deposit invoice', 'wc-szamlazz'),
	'void' => __('Reverse invoice', 'wc-szamlazz')
);

//When to generate these documents
$trigger_types = array(
	'order_created' => _x('When order created', 'Automation trigger', 'wc-szamlazz'),
	'payment_complete' => _x('On successful payment', 'Automation trigger', 'wc-szamlazz')
);

//Merge with order status settings
foreach ($this->get_order_statuses() as $key => $label) {
	$trigger_types[$key] = sprintf( esc_html__( 'after %1$s status', 'wc-szamlazz' ), $label);
}

//Set custom completion dates
$complete_date_types = array(
	'order_created' => _x('Order created', 'Invoice complete date type', 'wc-szamlazz'),
	'payment_complete' => _x('Payment complete', 'Invoice complete date type', 'wc-szamlazz'),
	'now' => _x('Document created', 'Invoice complete date type', 'wc-szamlazz'),
);

//Setup conditions
$conditions = array(
	'payment_method' => array(
		"label" => __('Payment method', 'wc-szamlazz'),
		'options' => $this->get_payment_methods()
	),
	'shipping_method' => array(
		"label" => __('Shipping method', 'wc-szamlazz'),
		'options' => $this->get_shipping_methods()
	),
	'type' => array(
		"label" => __('Order type', 'wc-szamlazz'),
		'options' => array(
			'individual' => __('Individual', 'wc-szamlazz'),
			'company' => __('Company', 'wc-szamlazz'),
		)
	),
	'product_category' => array(
		'label' => __('Product category', 'wc-szamlazz'),
		'options' => array()
	),
	'billing_address' => array(
		"label" => __('Billing address', 'wc-szamlazz'),
		'options' => array(
			'eu' => __('Inside the EU', 'wc-szamlazz'),
			'world' => __('Outside of the EU', 'wc-szamlazz'),
		)
	),
	'billing_country' => array(
		"label" => __('Billing country', 'wc-szamlazz'),
		'options' => $countries
	),
);

//Add category options
foreach (get_terms(array('taxonomy' => 'product_cat')) as $category) {
	$conditions['product_category']['options'][$category->term_id] = $category->name;
}

//Apply filters
$conditions = apply_filters('wc_szamlazz_notes_conditions', $conditions);

?>

<tr valign="top">
	<th scope="row" class="titledesc"><?php echo esc_html( $data['title'] ); ?></th>
	<td class="forminp <?php echo esc_attr( $data['class'] ); ?>">
		<div class="wc-szamlazz-settings-automations">
			<?php if($saved_values): ?>
				<?php foreach ( $saved_values as $automation_id => $automation ): ?>

					<div class="wc-szamlazz-settings-automation">
						<div class="wc-szamlazz-settings-automation-title">
							<div class="select-field">
								<label>
									<i class="icon"></i>
									<span>-</span>
								</label>
								<select class="wc-szamlazz-settings-automation-document" data-name="wc_szamlazz_automations[X][document]">
									<?php foreach ($document_types as $value => $label): ?>
										<option value="<?php echo esc_attr($value); ?>" <?php if(isset($automation['document'])) selected( $automation['document'], $value ); ?>><?php echo esc_html($label); ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<span class="text"><?php esc_html_e('creation', 'wc-szamlazz'); ?></span>
							<div class="select-field">
								<label>
									<span>-</span>
								</label>
								<select class="wc-szamlazz-settings-automation-trigger" data-name="wc_szamlazz_automations[X][trigger]">
									<?php foreach ($trigger_types as $value => $label): ?>
										<option value="<?php echo esc_attr($value); ?>" <?php if(isset($automation['trigger'])) selected( $automation['trigger'], $value ); ?>><?php echo esc_html($label); ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<label class="conditional-toggle">
								<input type="checkbox" data-name="wc_szamlazz_automations[X][condition_enabled]" <?php checked( $automation['conditional'] ); ?> class="condition" value="yes">
								<span><?php esc_html_e('Conditional logic', 'wc-szamlazz'); ?></span>
							</label>
							<a href="#" class="delete-automation"><?php _e('delete', 'wc-szamlazz'); ?></a>
						</div>
						<div class="wc-szamlazz-settings-automation-options">
							<div class="wc-szamlazz-settings-automation-option">
								<label><?php esc_html_e('Completion date','wc-szamlazz'); ?></label>
								<div class="wc-szamlazz-settings-automation-option-complete">
									<select data-name="wc_szamlazz_automations[X][complete]">
										<?php foreach ($complete_date_types as $value => $label): ?>
											<option value="<?php echo esc_attr($value); ?>" <?php if(isset($automation['complete'])) selected( $automation['complete'], $value ); ?>><?php echo esc_html($label); ?></option>
										<?php endforeach; ?>
									</select>
									<input type="text" data-name="wc_szamlazz_automations[X][complete_delay]" value="<?php echo esc_attr($automation['complete_delay']); ?>">
									<small><?php esc_html_e('± day', 'wc-szamlazz'); ?></small>
								</div>
								<small><?php esc_html_e('day', 'wc-szamlazz'); ?></small>
							</div>
							<div class="wc-szamlazz-settings-automation-option">
								<label><?php esc_html_e('Payment deadline','wc-szamlazz'); ?></label>
								<input type="text" data-name="wc_szamlazz_automations[X][deadline]" value="<?php echo esc_attr($automation['deadline']); ?>">
								<small><?php esc_html_e('day', 'wc-szamlazz'); ?></small>
							</div>
							<div class="wc-szamlazz-settings-automation-option">
								<label><?php esc_html_e( 'Mark as paid', 'wc-szamlazz' ); ?></label>
								<input type="checkbox" value="yes" data-name="wc_szamlazz_automations[X][paid]" <?php checked( $automation['paid'] ); ?>>
							</div>
							<div class="wc-szamlazz-settings-automation-option">
								<label><?php esc_html_e( 'Unique ID', 'wc-szamlazz' ); ?></label>
								<input type="text" data-name="wc_szamlazz_automations[X][id]" value="<?php echo esc_attr($automation['id']); ?>">
							</div>
						</div>
						<ul class="wc-szamlazz-settings-automation-if-options" <?php if(!$automation['conditional']): ?>style="display:none"<?php endif; ?> <?php if(isset($automation['conditions'])): ?>data-options="<?php echo esc_attr(json_encode($automation['conditions'])); ?>"<?php endif; ?>></ul>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<div class="wc-szamlazz-settings-automation-add">
			<a href="#"><span class="dashicons dashicons-plus-alt"></span> <span><?php _e('Add new automation', 'wc-szamlazz'); ?></span></a>
		</div>
		<?php echo $this->get_description_html( $data ); // WPCS: XSS ok. ?>
	</td>
</tr>

<script type="text/html" id="wc_szamlazz_automation_sample_row">
	<div class="wc-szamlazz-settings-automations">
		<div class="wc-szamlazz-settings-automation">
			<div class="wc-szamlazz-settings-automation-title">
				<div class="select-field">
					<label>
						<i class="icon"></i>
						<span>-</span>
					</label>
					<select class="wc-szamlazz-settings-automation-document" data-name="wc_szamlazz_automations[X][document]">
						<?php foreach ($document_types as $value => $label): ?>
							<option value="<?php echo esc_attr($value); ?>"><?php echo esc_html($label); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<span class="text"><?php esc_html_e('creation', 'wc-szamlazz'); ?></span>
				<div class="select-field">
					<label>
						<span>-</span>
					</label>
					<select class="wc-szamlazz-settings-automation-trigger" data-name="wc_szamlazz_automations[X][trigger]">
						<?php foreach ($trigger_types as $value => $label): ?>
							<option value="<?php echo esc_attr($value); ?>"><?php echo esc_html($label); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<label class="conditional-toggle">
					<input type="checkbox" data-name="wc_szamlazz_automations[X][condition_enabled]" class="condition" value="yes">
					<span><?php esc_html_e('Conditional logic', 'wc-szamlazz'); ?></span>
				</label>
				<a href="#" class="delete-automation"><?php _e('delete', 'wc-szamlazz'); ?></a>
			</div>
			<div class="wc-szamlazz-settings-automation-options">
				<div class="wc-szamlazz-settings-automation-option">
					<label><?php esc_html_e('Completion date','wc-szamlazz'); ?></label>
					<div class="wc-szamlazz-settings-automation-option-complete">
						<select data-name="wc_szamlazz_automations[X][complete]">
							<?php foreach ($complete_date_types as $value => $label): ?>
								<option value="<?php echo esc_attr($value); ?>"><?php echo esc_html($label); ?></option>
							<?php endforeach; ?>
						</select>
						<input type="text" value="0" data-name="wc_szamlazz_automations[X][complete_delay]">
						<small><?php esc_html_e('± day', 'wc-szamlazz'); ?></small>
					</div>
				</div>
				<div class="wc-szamlazz-settings-automation-option">
					<label><?php esc_html_e('Payment deadline','wc-szamlazz'); ?></label>
					<input type="text" value="0" data-name="wc_szamlazz_automations[X][deadline]">
					<small><?php esc_html_e('day', 'wc-szamlazz'); ?></small>
				</div>
				<div class="wc-szamlazz-settings-automation-option">
					<label><?php esc_html_e( 'Mark as paid', 'wc-szamlazz' ); ?></label>
					<input type="checkbox" value="yes" data-name="wc_szamlazz_automations[X][paid]">
				</div>
				<div class="wc-szamlazz-settings-automation-option">
					<label><?php esc_html_e( 'Unique ID', 'wc-szamlazz' ); ?></label>
					<input type="text" data-name="wc_szamlazz_automations[X][id]">
				</div>
			</div>
			<ul class="wc-szamlazz-settings-automation-if-options" style="display:none"></ul>
		</div>
	</div>
</script>

<script type="text/html" id="wc_szamlazz_automation_condition_sample_row">
	<li>
		<select class="condition" data-name="wc_szamlazz_automations[X][conditions][Y][category]">
			<?php foreach ($conditions as $condition_id => $condition): ?>
				<option value="<?php echo esc_attr($condition_id); ?>"><?php echo esc_html($condition['label']); ?></option>
			<?php endforeach; ?>
		</select>
		<select class="comparison" data-name="wc_szamlazz_automations[X][conditions][Y][comparison]">
			<option value="equal"><?php _e('Equal', 'wc-szamlazz'); ?></option>
			<option value="not_equal"><?php _e('Not equal', 'wc-szamlazz'); ?></option>
		</select>
		<?php foreach ($conditions as $condition_id => $condition): ?>
			<select class="value <?php if($condition_id == 'payment_method'): ?>selected<?php endif; ?>" data-condition="<?php echo esc_attr($condition_id); ?>" data-name="wc_szamlazz_automations[X][conditions][Y][<?php echo esc_attr($condition_id); ?>]" <?php if($condition_id != 'payment_method'): ?>disabled="disabled"<?php endif; ?>>
				<?php foreach ($condition['options'] as $option_id => $option_name): ?>
					<option value="<?php echo esc_attr($option_id); ?>"><?php echo esc_html($option_name); ?></option>
				<?php endforeach; ?>
			</select>
		<?php endforeach; ?>
		<a href="#" class="add-row"><span class="dashicons dashicons-plus-alt"></span></a>
		<a href="#" class="delete-row"><span class="dashicons dashicons-dismiss"></span></a>
	</li>
</script>
