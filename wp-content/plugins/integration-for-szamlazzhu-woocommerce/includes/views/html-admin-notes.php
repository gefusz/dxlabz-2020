<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//Get saved values
$saved_values = get_option('wc_szamlazz_notes');
$countries_obj = new WC_Countries();
$countries = $countries_obj->__get('countries');

//Setup conditions
$conditions = array(
	'payment_method' => array(
		"label" => __('Payment method', 'wc-szamlazz'),
		'options' => $this->get_payment_methods()
	),
	'shipping_method' => array(
		"label" => __('Shipping method', 'wc-szamlazz'),
		'options' => $this->get_shipping_methods()
	),
	'type' => array(
		"label" => __('Order type', 'wc-szamlazz'),
		'options' => array(
			'individual' => __('Individual', 'wc-szamlazz'),
			'company' => __('Company', 'wc-szamlazz'),
		)
	),
	'product_category' => array(
		'label' => __('Product category', 'wc-szamlazz'),
		'options' => array()
	),
	'language' => array(
		'label' => __('Invoice language', 'wc-szamlazz'),
		'options' => $this->get_languages()
	),
	'document' => array(
		'label' => __('Document type', 'wc-szamlazz'),
		'options' => array(
			'invoice' => __('Invoice', 'wc-szamlazz'),
			'proform' => __('Proforma invoice', 'wc-szamlazz'),
			'deposit' => __('Deposit invoice', 'wc-szamlazz'),
			'delivery' => __('Delivery note', 'wc-szamlazz'),
			'receipt' => __('Receipt', 'wc-szamlazz'),
		)
	),
	'account' => array(
		'label' => __('Számlázz.hu account', 'wc-szamlazz'),
		'options' => array()
	),
	'billing_address' => array(
		"label" => __('Billing address', 'wc-szamlazz'),
		'options' => array(
			'eu' => __('Inside the EU', 'wc-szamlazz'),
			'world' => __('Outside of the EU', 'wc-szamlazz'),
		)
	),
	'billing_country' => array(
		"label" => __('Billing country', 'wc-szamlazz'),
		'options' => $countries
	),
);

//Add category options
foreach (get_terms(array('taxonomy' => 'product_cat')) as $category) {
	$conditions['product_category']['options'][$category->term_id] = $category->name;
}

//Add account options
foreach (WC_Szamlazz()->get_szamlazz_accounts() as $account_key => $account_name) {
	$conditions['account']['options'][$account_key] = $account_name.' - '.substr(esc_html($account_key), 0, 10).'...';
}

//Apply filters
$conditions = apply_filters('wc_szamlazz_notes_conditions', $conditions);

?>

<tr valign="top">
	<th scope="row" class="titledesc"><?php echo esc_html( $data['title'] ); ?></th>
	<td class="forminp <?php echo esc_attr( $data['class'] ); ?>">
		<div class="wc-szamlazz-settings-notes">
			<?php if($saved_values): ?>
				<?php foreach ( $saved_values as $note_id => $note ): ?>

					<div class="wc-szamlazz-settings-note">
						<textarea placeholder="<?php _e('Note', 'wc-szamlazz'); ?>" data-name="wc_szamlazz_notes[X][note]"><?php echo esc_textarea($note['comment']); ?></textarea>
						<div class="wc-szamlazz-settings-note-if">
							<div class="wc-szamlazz-settings-note-if-header">
								<label>
									<input type="checkbox" data-name="wc_szamlazz_notes[X][condition_enabled]" <?php checked( $note['conditional'] ); ?> class="condition" value="yes">
									<span><?php _e('Add note to invoice, if', 'wc-szamlazz'); ?></span>
								</label>
								<select data-name="wc_szamlazz_notes[X][logic]">
									<option value="and" <?php if(isset($note['logic'])) selected( $note['logic'], 'and' ); ?>><?php _e('All', 'wc-szamlazz'); ?></option>
									<option value="or" <?php if(isset($note['logic'])) selected( $note['logic'], 'or' ); ?>><?php _e('One', 'wc-szamlazz'); ?></option>
								</select>
								<span><?php _e('of the following match', 'wc-szamlazz'); ?></span>
								<a href="#" class="delete-note"><?php _e('delete', 'wc-szamlazz'); ?></a>
							</div>
							<ul class="wc-szamlazz-settings-note-if-options" <?php if(!$note['conditional']): ?>style="display:none"<?php endif; ?> <?php if(isset($note['conditions'])): ?>data-options="<?php echo esc_attr(json_encode($note['conditions'])); ?>"<?php endif; ?>></ul>
							<div class="wc-szamlazz-settings-note-if-header wc-szamlazz-settings-note-if-append" <?php if(!$note['conditional']): ?>style="display:none"<?php endif; ?>>
								<label>
									<input type="checkbox" data-name="wc_szamlazz_notes[X][append]" <?php if(isset($note['append'])) { checked( $note['append'] ); } ?> value="yes">
									<span><?php _e('Add to the end of an existing note', 'wc-szamlazz'); ?></span>
								</label>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<div class="wc-szamlazz-settings-note-add">
			<a href="#"><span class="dashicons dashicons-plus-alt"></span> <span><?php _e('Add a new note', 'wc-szamlazz'); ?></span></a>
		</div>
		<?php echo $this->get_description_html( $data ); // WPCS: XSS ok. ?>
	</td>
</tr>

<script type="text/html" id="wc_szamlazz_note_sample_row">
	<div class="wc-szamlazz-settings-notes">
		<div class="wc-szamlazz-settings-note">
			<textarea placeholder="<?php _e('Note', 'wc-szamlazz'); ?>" data-name="wc_szamlazz_notes[X][note]"><?php if(!get_option('wc_szamlazz_notes')) { echo esc_textarea($this->get_option('note')); } ?></textarea>
			<div class="wc-szamlazz-settings-note-if">

				<div class="wc-szamlazz-settings-note-if-header">
					<label>
						<input type="checkbox" data-name="wc_szamlazz_notes[X][condition_enabled]" class="condition" value="yes">
						<span><?php _e('Add note to invoice, if', 'wc-szamlazz'); ?></span>
					</label>
					<select data-name="wc_szamlazz_notes[X][logic]">
						<option value="and"><?php _e('All', 'wc-szamlazz'); ?></option>
						<option value="or"><?php _e('One', 'wc-szamlazz'); ?></option>
					</select>
					<span><?php _e('of the following match', 'wc-szamlazz'); ?></span>
					<a href="#" class="delete-note"><?php _e('delete', 'wc-szamlazz'); ?></a>
				</div>
				<ul class="wc-szamlazz-settings-note-if-options" style="display:none">

				</ul>
				<div class="wc-szamlazz-settings-note-if-header wc-szamlazz-settings-note-if-append" style="display:none">
					<label>
						<input type="checkbox" data-name="wc_szamlazz_notes[X][append]" value="yes">
						<span><?php _e('Add to the end of an existing note', 'wc-szamlazz'); ?></span>
					</label>
				</div>
			</div>
		</div>
	</div>
</script>

<script type="text/html" id="wc_szamlazz_note_condition_sample_row">
	<li>
		<select class="condition" data-name="wc_szamlazz_notes[X][conditions][Y][category]">
			<?php foreach ($conditions as $condition_id => $condition): ?>
				<option value="<?php echo esc_attr($condition_id); ?>"><?php echo esc_html($condition['label']); ?></option>
			<?php endforeach; ?>
		</select>
		<select class="comparison" data-name="wc_szamlazz_notes[X][conditions][Y][comparison]">
			<option value="equal"><?php _e('Equal', 'wc-szamlazz'); ?></option>
			<option value="not_equal"><?php _e('Not equal', 'wc-szamlazz'); ?></option>
		</select>
		<?php foreach ($conditions as $condition_id => $condition): ?>
			<select class="value <?php if($condition_id == 'payment_method'): ?>selected<?php endif; ?>" data-condition="<?php echo esc_attr($condition_id); ?>" data-name="wc_szamlazz_notes[X][conditions][Y][<?php echo esc_attr($condition_id); ?>]" <?php if($condition_id != 'payment_method'): ?>disabled="disabled"<?php endif; ?>>
				<?php foreach ($condition['options'] as $option_id => $option_name): ?>
					<option value="<?php echo esc_attr($option_id); ?>"><?php echo esc_html($option_name); ?></option>
				<?php endforeach; ?>
			</select>
		<?php endforeach; ?>
		<a href="#" class="add-row"><span class="dashicons dashicons-plus-alt"></span></a>
		<a href="#" class="delete-row"><span class="dashicons dashicons-dismiss"></span></a>
	</li>
</script>
