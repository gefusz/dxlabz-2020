<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="wc-szamlazz-settings-sidebar">

	<?php if(!get_option( 'woocommerce_calc_taxes' ) && !$this->get_option('afakulcs') && get_option('wc_szamlazz_payment_method_options_v2')): ?>
		<div class="wc-szamlazz-settings-widget wc-szamlazz-settings-widget-vat">
			<h3><span class="dashicons dashicons-warning"></span> <?php _e('TAX rate setup', 'wc-szamlazz'); ?></h3>
			<p><?php _e('You are using the default VAT rate and taxes are disabled in WooCommerce, so the VAT rate will be 0% on the invoice.', 'wc-szamlazz'); ?></p>
			<p><?php _e("If you don't need to charge sales tax, you can set a fixed VAT rate in the settings, like AAM. If you need percentage-based VAT rates, or you have a more complicated situation, you need to configure the WooCommerce TAX settings properly. Here is a guide to do this:", 'wc-szamlazz'); ?></p>
			<p><a href="https://visztpeter.me/2020/05/13/afakulcsok-beallitasa-woocommerce-aruhazakban/" target="_blank"><?php _e('Setting up VAT/TAX rates in WooCommerce shops.', 'wc-szamlazz'); ?></a></p>
		</div>
	<?php endif; ?>

	<?php if(get_option('_wc_szamlazz_pro_enabled')): ?>

		<div class="wc-szamlazz-settings-widget wc-szamlazz-settings-widget-pro wc-szamlazz-settings-widget-pro-active wc-szamlazz-settings-widget-pro-state-<?php echo esc_attr($pro_key_status['state']); ?>">

			<?php if($pro_key_status['state'] == 'active'): ?>
				<h3><span class="dashicons dashicons-yes-alt"></span> <?php _e('The PRO version is active', 'wc-szamlazz'); ?></h3>
				<p><?php _e('You have successfully activated the PRO version.', 'wc-szamlazz'); ?></p>
			<?php elseif ($pro_key_status['state'] == 'active-limited'): ?>
				<h3><span class="dashicons dashicons-yes-alt"></span> <?php _e('The PRO version is active', 'wc-szamlazz'); ?></h3>
				<p>
					<?php echo sprintf(__('You have successfully activated the PRO version at this time: <strong>%1$s</strong>.', 'wc-szamlazz'), esc_html($pro_key_status['activation']->format(get_option('date_format')))); ?>
					<?php echo sprintf(__('The license key is valid for %1$s year(s), so the PRO version will remain active for <strong>%2$s</strong> day(s).', 'wc-szamlazz'), esc_html($pro_key_status['years']), esc_html($pro_key_status['days'])); ?>
				</p>
			<?php elseif ($pro_key_status['state'] == 'almost-expired'): ?>
				<h3><span class="dashicons dashicons-warning"></span> <?php _e('The PRO version is about to expire', 'wc-szamlazz'); ?></h3>
				<p>
					<?php echo sprintf(__('You have successfully activated the PRO version at this time: <strong>%1$s</strong>.', 'wc-szamlazz'), esc_html($pro_key_status['activation']->format(get_option('date_format')))); ?>
					<?php echo sprintf(__('The license key is valid for %1$s year(s), so the PRO version will remain active for <strong>%2$s</strong> day(s).', 'wc-szamlazz'), esc_html($pro_key_status['years']), esc_html($pro_key_status['days'])); ?>
				</p>
			<?php elseif ($pro_key_status['state'] == 'expired'): ?>
				<h3><span class="dashicons dashicons-warning"></span> <?php _e('The PRO version is expired', 'wc-szamlazz'); ?></h3>
				<p>
					<?php echo sprintf(__('You have successfully activated the PRO version at this time: <strong>%1$s</strong>.', 'wc-szamlazz'), esc_html($pro_key_status['activation']->format(get_option('date_format')))); ?>
					<?php echo sprintf(__('The license key is valid for %1$s year(s), so the following license key is expired:', 'wc-szamlazz'), esc_html($pro_key_status['years'])); ?>
				</p>
			<?php endif; ?>

			<p>
				<span class="wc-szamlazz-settings-widget-pro-label"><?php _e('License key', 'wc-szamlazz'); ?></span><br>
				<?php echo esc_html(get_option('_wc_szamlazz_pro_key')); ?>
			</p>
			<p>
				<span class="wc-szamlazz-settings-widget-pro-label"><?php _e('E-mail address', 'wc-szamlazz'); ?></span><br>
				<?php echo esc_html(get_option('_wc_szamlazz_pro_email')); ?>
			</p>

			<?php if ($pro_key_status['state'] == 'expired' || $pro_key_status['state'] == 'almost-expired'): ?>
			<div class="wc-szamlazz-settings-widget-pro-renew">
				<p><?php _e('If you want to continue using the PRO version, you will need to purchase a new license key or renew the existing one.', 'wc-szamlazz'); ?></p>
				<p><a href="https://visztpeter.me/woocommerce-szamlazz-hu/" target="_blank"><span class="dashicons dashicons-cart"></span> <span><?php _e('Purchase a new license key', 'wc-szamlazz'); ?></span></a></p>
				<p><a href="https://visztpeter.me/?renew_license=<?php echo esc_attr(get_option('_wc_szamlazz_pro_key')); ?>" target="_blank"><span class="dashicons dashicons-cart"></span> <span><?php _e('Renew existing license key', 'wc-szamlazz'); ?></span></a></p>
			</div>
			<?php endif; ?>

			<?php if ($pro_key_status['state'] == 'expired' || $pro_key_status['state'] == 'almost-expired'): ?>
				<div class="wc-szamlazz-settings-widget-pro-deactivate">
					<p>
						<a class="button-secondary" id="wc_szamlazz_deactivate_pro"><?php esc_html_e( 'Activate new license', 'wc-szamlazz' ); ?></a>
						<a class="button-secondary" id="wc_szamlazz_reactivate_pro" data-license-key="<?php echo esc_attr(get_option('_wc_szamlazz_pro_key')); ?>" data-email="<?php echo esc_attr(get_option('_wc_szamlazz_pro_email')); ?>"><?php esc_html_e( 'Reactivate', 'wc-szamlazz' ); ?></a>
					</p>
					<p><small><?php _e('If you purchased a new license key, click the Activate new license button. If you have extended the validity of your current license key, simply click on the reactivate button.', 'wc-szamlazz'); ?></small></p>
				</div>
			<?php else: ?>
				<div class="wc-szamlazz-settings-widget-pro-deactivate">
					<p>
						<a class="button-secondary" id="wc_szamlazz_deactivate_pro"><?php esc_html_e( 'Deactivate license', 'wc-szamlazz' ); ?></a>
					</p>
					<p><small><?php esc_html_e( 'If you want to activate the license on another website, you must first deactivate it on this website.', 'wc-szamlazz' ); ?></small></p>
				</div>
			<?php endif; ?>
		</div>

	<?php else: ?>

		<div class="wc-szamlazz-settings-widget wc-szamlazz-settings-widget-pro">
			<h3><?php esc_html_e( 'PRO version', 'wc-szamlazz' ); ?></h3>
			<p><?php esc_html_e( 'If you have already purchased the PRO version, enter the license key and the e-mail address used to purchase:', 'wc-szamlazz' ); ?></p>

			<div class="wc-szamlazz-settings-widget-pro-notice" style="display:none">
				<span class="dashicons dashicons-warning"></span>
				<p></p>
			</div>

			<fieldset>
				<input class="input-text regular-input" type="text" name="woocommerce_wc_szamlazz_pro_email" id="woocommerce_wc_szamlazz_pro_email" value="" placeholder="<?php esc_html_e( 'E-mail address used for the purchase', 'wc-szamlazz' ); ?>">
				<input class="input-text regular-input" type="text" name="woocommerce_wc_szamlazz_pro_key" id="woocommerce_wc_szamlazz_pro_key" value="" placeholder="<?php esc_html_e( 'License key', 'wc-szamlazz' ); ?>"><br>
			</fieldset>
			<p>
				<button class="button-primary" type="button" id="wc_szamlazz_activate_pro"><?php _e('Activate', 'wc-szamlazz'); ?></button>
			</p>
			<h4><?php esc_html_e( 'Why should I use the PRO version?', 'wc-szamlazz' ); ?></h4>
			<ul>
				<li><span class="dashicons dashicons-yes"></span> <?php esc_html_e( 'Automatic invoicing', 'wc-szamlazz' ); ?></li>
				<li><span class="dashicons dashicons-yes"></span> <?php esc_html_e( 'Create automatic proforma invoices', 'wc-szamlazz' ); ?></li>
				<li><span class="dashicons dashicons-yes"></span> <?php esc_html_e( 'Mark invoice as paid automatically', 'wc-szamlazz' ); ?></li>
				<li><span class="dashicons dashicons-yes"></span> <?php esc_html_e( 'Generating receipts', 'wc-szamlazz' ); ?></li>
				<li><span class="dashicons dashicons-yes"></span> <?php esc_html_e( 'Premium support and a lot more', 'wc-szamlazz' ); ?></li>
			</ul>
			<div class="wc-szamlazz-settings-widget-pro-cta">
				<a href="https://visztpeter.me/woocommerce-szamlazz-hu/"><span class="dashicons dashicons-cart"></span> <span><?php esc_html_e( 'Purchase PRO version', 'wc-szamlazz' ); ?></span></a>
				<span>
					<small><?php esc_html_e( 'net', 'wc-szamlazz' ); ?></small>
					<strong><?php esc_html_e( '10.000 Ft / year', 'wc-szamlazz' ); ?></strong>
				</span>
			</div>

		</div>

	<?php endif; ?>

	<?php if(!get_option('_wc_szamlazz_hide_addons') && count($addons) > 0): ?>
		<div class="wc-szamlazz-settings-widget wc-szamlazz-settings-widget-addons">
			<h3><?php esc_html_e( 'Recommended addons and extensions', 'wc-szamlazz' ); ?></h3>
			<p><?php esc_html_e( 'Recommended extensions for WooCommerce and addon extensions for unique use-cases.', 'wc-szamlazz' ); ?>.</p>
			<ul>
				<?php if(!defined( 'SURBMA_HC_PLUGIN_VERSION' )): ?>
					<li>
						<a href="https://hu.wordpress.org/plugins/surbma-magyar-woocommerce/" target="_blank">
							<span>
								<strong><?php esc_html_e( '🇭🇺 HuCommerce', 'wc-szamlazz' ); ?></strong>
								<small><?php esc_html_e( 'Useful fixes for Hungarian WooCommerce stores', 'wc-szamlazz' ); ?></small>
							</span>
							<span class="dashicons dashicons-arrow-right-alt2"></span>
						</a>
					</li>
				<?php endif; ?>
				<?php foreach ($addons as $addon): ?>
					<li<?php if(isset($addon['active']) && $addon['active']): ?> class="active"<?php endif; ?>>
						<a href="https://visztpeter.me/woocommerce-szamlazz-hu/" target="_blank">
							<span>
								<strong><?php echo esc_html($addon['name']); ?></strong>
								<small><?php echo esc_html($addon['desc']); ?></small>
							</span>
							<?php if(isset($addon['active']) && $addon['active']): ?>
								<span class="dashicons dashicons-yes"></span>
							<?php else: ?>
								<span class="dashicons dashicons-arrow-right-alt2"></span>
							<?php endif; ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
			<?php if(count($addons) == 0): ?>
			<p>
				<a class="button-secondary" data-nonce="<?php echo wp_create_nonce( 'wc-szamlazz-hide-rate-request' )?>"><?php esc_html_e( 'Hide this section', 'wc-szamlazz' ); ?></a>
			</p>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<div class="wc-szamlazz-settings-widget">
		<h3><?php esc_html_e('Support', 'wc-szamlazz'); ?></h3>
		<p><?php esc_html_e('It is important to point out that this extension was not created by Számlázz.hu, so if you have any questions about the operation of the extension, please contact me at one of the following contacts:', 'wc-szamlazz'); ?></p>
		<ul>
			<li><a href="https://visztpeter.me/dokumentacio/" target="_blank"><?php esc_html_e('Documentation', 'wc-szamlazz'); ?></a></li>
			<li><a href="mailto:support@visztpeter.me"><?php esc_html_e('E-mail (support@visztpeter.me)', 'wc-szamlazz'); ?></a></li>
			<li><a href="https://wordpress.org/support/plugin/integration-for-szamlazzhu-woocommerce/" target="_blank"><?php esc_html_e('Forum thread on WordPress.org', 'wc-szamlazz'); ?></a></li>
		</ul>
	</div>

</div>
