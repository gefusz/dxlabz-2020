<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="notice notice-error wc-szamlazz-notice wc-szamlazz-welcome">
	<div class="wc-szamlazz-welcome-body">
		<button type="button" class="notice-dismiss wc-szamlazz-hide-notice" data-nonce="<?php echo wp_create_nonce( 'wc-szamlazz-hide-notice' )?>" data-notice="error"><span class="screen-reader-text"><?php esc_html_e( 'Dismiss', 'woocommerce' ); ?></span></button>
		<h2><?php esc_html_e('Invoice generation failed', 'wc-szamlazz'); ?></h2>
		<p><?php printf( esc_html__( 'The invoice for order %s could not be created automatically for some reason. You will see the exact error in the order notes.', 'wc-szamlazz' ), esc_html($order_number) ); ?></p>
		<p>
			<a class="button-secondary" href="<?php echo esc_url($order_link); ?>"><?php esc_html_e( 'Order details', 'wc-szamlazz' ); ?></a>
		</p>
	</div>
</div>
