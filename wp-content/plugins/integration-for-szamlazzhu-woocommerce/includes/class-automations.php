<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'WC_Szamlazz_Automations', false ) ) :

	class WC_Szamlazz_Automations {

		//Setup triggers
		public static function init() {

			//When order created
			add_action( 'woocommerce_checkout_order_processed', array( __CLASS__, 'on_order_created' ), 10, 3 );

			//On successful payment
			add_action( 'woocommerce_payment_complete',  array( __CLASS__, 'on_payment_complete' ) );

			//On status change
			add_action( 'woocommerce_order_status_changed',  array( __CLASS__, 'on_status_change' ), 10, 3 );

		}

		public static function on_order_created($order_id, $posted_data, $order) {
			$automations = self::find_automations($order_id, 'order_created');
		}

		public static function on_payment_complete( $order_id ) {
			$automations = self::find_automations($order_id, 'payment_complete');

		}

		public static function on_status_change( $order_id, $old_status, $new_status ) {
			$automations = self::find_automations($order_id, $new_status);
		}

		public static function find_automations($order_id, $trigger) {

			//Get main data
			$order = wc_get_order($order_id);
			$automations = get_option('wc_szamlazz_automations');

			//Get order type
			$order_type = ($order->get_billing_company()) ? 'company' : 'individual';

			//Get billing address location
			$eu_countries = WC()->countries->get_european_union_countries('eu_vat');
			$billing_address = 'world';
			if(in_array($order->get_billing_country(), $eu_countries)) {
				$billing_address = 'eu';
			}

			//Get payment method id
			$payment_method = $order->get_payment_method();

			//Get shipping method id
			$shipping_method = '';
			$shipping_methods = $order->get_shipping_methods();
			if($shipping_methods) {
				foreach( $shipping_methods as $shipping_method_obj ){
					$shipping_method = $shipping_method_obj->get_method_id().':'.$shipping_method_obj->get_instance_id();
				}
			}

			//Get product category ids
			$product_categories = array();
			$order_items = $order->get_items();
			foreach ($order_items as $order_item) {
				if($order_item->get_product() && $order_item->get_product()->get_category_ids()) {
					$product_categories = $product_categories+$order_item->get_product()->get_category_ids();
				}
			}

			//Setup parameters for conditional check
			$order_details = array(
				'payment_method' => $payment_method,
				'shipping_method' => $shipping_method,
				'type' => $order_type,
				'billing_address' => $billing_address,
				'billing_country' => $order->get_billing_country()
			);

			//Custom conditions
			$order_details = apply_filters('wc_szamlazz_automations_conditions_values', $order_details, $order);

			//We will return the matched automations at the end
			$final_automations = array();

			//Loop through each automation
			foreach ($automations as $automation_id => $automation) {

				//Check if trigger is a match. If not, just skip
				if(str_replace( 'wc-', '', $automation['trigger'] ) != str_replace( 'wc-', '', $trigger )) {
					continue;
				}

				//If this is based on a condition
				if($automation['conditional']) {

					//Check if the conditions match
					foreach ($automation['conditions'] as $condition_id => $condition) {
						$comparison = ($condition['comparison'] == 'equal');

						switch ($condition['category']) {
							case 'product_category':
								if(in_array($condition['value'], $product_categories)) {
									$automations[$automation_id]['conditions'][$condition_id]['match'] = $comparison;
								} else {
									$automations[$automation_id]['conditions'][$condition_id]['match'] = !$comparison;
								}
								break;
							default:
								if($condition['value'] == $order_details[$condition['category']]) {
									$automations[$automation_id]['conditions'][$condition_id]['match'] = $comparison;
								} else {
									$automations[$automation_id]['conditions'][$condition_id]['match'] = !$comparison;
								}
								break;
						}
					}

					//Count how many matches we have
					$matched = 0;
					foreach ($automations[$automation_id]['conditions'] as $condition) {
						if($condition['match']) $matched++;
					}

					//Check if we need to match all or just one
					$automation_is_a_match = false;
					$automation['logic'] = 'and';
					if($automation['logic'] == 'and' && $matched == count($automation['conditions'])) $automation_is_a_match = true;
					if($automation['logic'] == 'or' && $matched > 0) $automation_is_a_match = true;

					//If its not a match, continue to next not
					if(!$automation_is_a_match) continue;

					//If its a match, add to found automations
					$final_automations[] = $automation;

				} else {
					$final_automations[] = $automation;
				}

			}

			//If we found some automations, try to generate documents
			if(count($final_automations) > 0) {

				//First sort by document types, so proform and deposit runs before invoice
				$ordered_automations = array();
				$document_order = array('proform', 'deposit', 'invoice', 'void');
				foreach($document_order as $value) {
					foreach ($final_automations as $final_automation) {
						if($final_automation['document'] == $value) {
							$ordered_automations[] = $final_automation;
						}
					}
				}

				//Loop through documents(usually it will be only one, but who knows)
				self::run_automations($order_id, $ordered_automations);

			}

			return $final_automations;
		}

		public static function run_automations($order_id, $automations) {

			//Get data
			$order = wc_get_order($order_id);
			$deferred = (WC_Szamlazz()->get_option('defer', 'no') == 'yes');
			$need_delivery_note = (WC_Szamlazz()->get_option('delivery_note', 'no') == 'yes');
			$need_delivery_note = apply_filters('wc_szamlazz_need_delivery_note', $need_delivery_note, $order);
			$order_total = $order->get_total();

			//Don't create deferred if we are in an admin page and only mark one order completed
			if(is_admin() && isset( $_GET['action']) && $_GET['action'] == 'woocommerce_mark_order_status') {
				$deferred = false;
			}

			//Don't defer if we are just changing one or two order status using bulk actions
			if(is_admin() && isset($_GET['_wp_http_referer']) && isset($_GET['post']) && count($_GET['post']) < 3) {
				$deferred = false;
			}

			//Don't create for free orders
			$is_order_free = false;
			if($order_total == 0 && (WC_Szamlazz()->get_option('disable_free_order', 'yes') == 'yes')) {
				$is_order_free = true;
			}

			//Check payment method settings
			$should_generate_auto_invoice = true;
			$payment_method = $order->get_payment_method();
			if(WC_Szamlazz()->check_payment_method_options($order->get_payment_method(), 'auto_disabled')) {
				$should_generate_auto_invoice = false;
			}

			//Check for product option
			$order_items = $order->get_items();
			foreach( $order_items as $order_item ) {
				if($order_item->get_product() && $order_item->get_product()->get_meta('wc_szamlazz_disable_auto_invoice') && $order_item->get_product()->get_meta('wc_szamlazz_disable_auto_invoice') == 'yes') {
					$should_generate_auto_invoice = false;
				}
			}

			//Allow customization with filters
			$should_generate_auto_invoice = apply_filters('wc_szamlazz_should_generate_auto_invoice', $should_generate_auto_invoice, $order_id);

			//Loop through automations
			foreach ($automations as $automation) {

				//Check if it was already generated
				$generated = WC_Szamlazz()->is_invoice_generated($order_id, $automation['document']);

				//Skip, if already generated or not needed for free orders
				if($generated || $is_order_free) continue;

				//If its an invoice, check for the should auto generate option
				if($automation['document'] == 'invoice' && !$should_generate_auto_invoice) continue;

				//If we are still here, we can generate the actual document
				//First, get custom options, like complete date and deadline
				$complete_date = $automation['complete'];
				$complete_date_delay = intval($automation['complete_delay']);
				$deadline = intval($automation['deadline']);
				$paid = $automation['paid'];
				$automation_id = $automation['id'];
				$timestamp = current_time('timestamp');

				//Get dates related to the order
				if($complete_date == 'order_created') {
					$timestamp = $order->get_date_created()->getTimestamp();
				}

				if($complete_date == 'payment_complete' && $order->get_date_paid()) {
					$timestamp = $order->get_date_paid()->getTimestamp();
				}


				//Calculate document dates
				$deadline_delay = $complete_date_delay+$deadline;
				$document_complete_date = date_i18n('Y-m-d', strtotime('+'.$complete_date_delay.' days', $timestamp));
				$document_deadline_date = date_i18n('Y-m-d', strtotime('+'.$deadline_delay.' days', $timestamp));

				//Setup options
				$options = array(
					'deadline_date' => $document_deadline_date,
					'completed_date' => $document_complete_date,
					'paid' => $paid,
					'automation_id' => $automation_id
				);

				if($deferred) {
					WC_Szamlazz()::$background_generator->push_to_queue(
						array(
							'invoice_type' => 'invoice',
							'order_id' => $order_id
						)
					);
					if($need_delivery_note && $automation['document'] == 'invoice') {
						WC_Szamlazz()::$background_generator->push_to_queue(
							array(
								'invoice_type' => 'delivery',
								'order_id' => $order_id
							)
						);
					}
				} else {

					//Now we can finally create the document
					$return_info = WC_Szamlazz()->generate_invoice($order_id, $automation['document'], $options);

					//Generate delivery not alongside with invoice if needed
					if($need_delivery_note && $automation['document'] == 'invoice') {
						$return_info = WC_Szamlazz()->generate_invoice($order_id, 'delivery');
					}

				}

			}

			//If it was deferred, start it now
			if($deferred) {
				WC_Szamlazz()::$background_generator->save()->dispatch();
			}

		}

	}

	WC_Szamlazz_Automations::init();

endif;
