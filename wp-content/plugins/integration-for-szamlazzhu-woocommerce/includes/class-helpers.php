<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'WC_Szamlazz_Helpers', false ) ) :

	class WC_Szamlazz_Helpers {

		//Get supported languages
		public static function get_supported_languages() {
			return apply_filters('wc_szamlazz_supported_languages', array(
				'hu' => __( 'Hungarian', 'wc-szamlazz' ),
				'de' => __( 'German', 'wc-szamlazz' ),
				'en' => __( 'English', 'wc-szamlazz' ),
				'it' => __( 'Italian', 'wc-szamlazz' ),
				'fr' => __( 'French', 'wc-szamlazz' ),
				'hr' => __( 'Croatian', 'wc-szamlazz' ),
				'ro' => __( 'Romanian', 'wc-szamlazz' ),
				'sk' => __( 'Slovak', 'wc-szamlazz' ),
				'es' => __( 'Spanish', 'wc-szamlazz' ),
				'pl' => __( 'Polish', 'wc-szamlazz' ),
				'cz' => __( 'Czech', 'wc-szamlazz' ),
			));
		}

		//Get document type ids and labels
		public static function get_document_types() {
			return apply_filters('wc_szamlazz_document_types', array(
				'deposit' => esc_html__('Deposit invoice','wc-szamlazz'),
				'proform' => esc_html__('Proforma invoice','wc-szamlazz'),
				'invoice' => esc_html__('Invoice','wc-szamlazz'),
				'receipt' => esc_html__('Receipt','wc-szamlazz'),
				'delivery' => esc_html__('Delivery note','wc-szamlazz'),
				'void' => esc_html__('Reverse invoice','wc-szamlazz'),
				'void_receipt' => esc_html__('Reverse receipt','wc-szamlazz'),
				'corrected' => esc_html__('Correction invoice','wc-szamlazz')
			));
		}

		//Duplicate wc_display_item_meta for small customizations(mainly to hide the backordered meta info)
		public static function get_item_meta($item, $args) {
			$strings = array();
			$html = '';
			$args    = wp_parse_args(
				$args,
				array(
					'before'       => '<ul class="wc-item-meta"><li>',
					'after'        => '</li></ul>',
					'separator'    => '</li><li>',
					'echo'         => true,
					'autop'        => false,
					'label_before' => '<strong class="wc-item-meta-label">',
					'label_after'  => ':</strong> ',
				)
			);

			foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
				if(__( 'Backordered', 'woocommerce' ) == $meta->key) continue;
				$value     = wp_kses_post( make_clickable( trim( $meta->display_value ) ) );
				$strings[] = wp_kses_post( $meta->display_key ) . $args['label_after'] . $value;
			}

			if ( $strings ) {
				$html = $args['before'] . implode( $args['separator'], $strings );
			}

			return apply_filters( 'woocommerce_display_item_meta', $html, $item, $args );
		}

		public static function get_pro_state() {
			$pro_key_status = array(
				'state' => 'inactive'
			);

			if(get_option('_wc_szamlazz_pro_enabled')) {
				$pro_key_status['state'] = 'active';

				if(get_option('_wc_szamlazz_pro_expiration_date')) {
					$pro_key_status['state'] = 'active-limited';
					$current_date = new DateTime('now');
					$expiration_date_data = get_option('_wc_szamlazz_pro_expiration_date');
					$expiration_date_data = explode('|', $expiration_date_data);
					$activation_date = new DateTime('@' . $expiration_date_data[0]);
					$expiration_date = new DateTime('@' . $expiration_date_data[1]);
					$interval = $expiration_date->diff($activation_date);
					$years_difference = $interval->format('%y');
					$days_until_expiration = $expiration_date->diff($current_date)->days;

					$pro_key_status['activation'] = $activation_date;
					$pro_key_status['years'] = $years_difference;
					$pro_key_status['expiration'] = $expiration_date;

					if(($expiration_date < $current_date) || ($days_until_expiration < 1)) {
						$pro_key_status['state'] = 'expired';
					} else {
						$pro_key_status['days'] = $days_until_expiration;
						if($days_until_expiration < 30) {
							$pro_key_status['state'] = 'almost-expired';
						}
					}
				}
			}

			return $pro_key_status;
		}

		public static function is_pro_enabled() {
			$state = self::get_pro_state();
			return (in_array($state['state'], array('active', 'active-limited', 'almost-expired')));
		}

		//Get the language of the invoice
		public static function get_order_language($order) {
			$orderId = $order->get_id();
			$lang_code = WC_Szamlazz()->get_option('language', 'hu');

			if(WC_Szamlazz()->get_option('language_wpml') == 'yes') {
				$wpml_lang_code = get_post_meta( $orderId, 'wpml_language', true );
				if(!$wpml_lang_code && function_exists('pll_get_post_language')){
					$wpml_lang_code = pll_get_post_language($orderId, 'locale');
				}
				if($wpml_lang_code && in_array($wpml_lang_code, array('hu', 'de', 'en', 'it', 'fr', 'hr', 'ro', 'sk', 'es', 'pl', 'cz'))) {
					$lang_code = $wpml_lang_code;
				}
			}

			return apply_filters('wc_szamlazz_get_order_language', $lang_code, $order);
		}

		//Helper to get order currency
		public static function get_currency($order) {
			$currency = $order->get_currency() ?: 'HUF';
			$invoice_lang = WC_Szamlazz()->get_option('language', 'hu');
			if($currency == 'HUF' && $invoice_lang == 'hu') $currency = 'Ft';
			return $currency;
		}

		public static function is_order_hungarian($order) {
			return $order->get_billing_country() == 'HU';
		}

		public static function get_invoice_type($order) {
			$type = WC_Szamlazz()->get_option('invoice_type', 'paper');
			if($order->get_billing_company() && WC_Szamlazz()->get_option('invoice_type_company')) {
				$type = WC_Szamlazz()->get_option('invoice_type_company');
			}
			return ($type == 'electronic') ? 'true' : 'false';
		}

		//Get available checkout methods and ayment gateways
		public static function get_available_payment_gateways() {
			$available_gateways = WC()->payment_gateways->payment_gateways();
			$available = array();
			$available['none'] = __('Select a payment method','wc-szamlazz');
			foreach ($available_gateways as $available_gateway) {
				$available[$available_gateway->id] = $available_gateway->title;
			}
			return $available;
		}

		//Replace placeholders in invoice note
		public static function replace_note_placeholders($note, $order) {

			//Setup replacements
			$note_replacements = apply_filters('wc_szamlazz_get_order_note_placeholders', array(
				'{customer_email}' => $order->get_billing_email(),
				'{customer_phone}' => $order->get_billing_phone(),
				'{order_number}' => $order->get_order_number(),
				'{transaction_id}' => $order->get_transaction_id(),
				'{shipping_address}' => preg_replace('/\<br(\s*)?\/?\>/i', "\n", $order->get_formatted_shipping_address()),
				'{customer_notes}' => $order->get_customer_note()
			), $order);

			//Replace stuff:
			$note = str_replace( array_keys( $note_replacements ), array_values( $note_replacements ), $note);

			//Return fixed note
			return $note;
		}


	}

endif;
