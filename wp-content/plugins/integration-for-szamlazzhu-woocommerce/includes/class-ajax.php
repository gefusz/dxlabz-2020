<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'WC_Szamlazz_Ajax', false ) ) :

	class WC_Szamlazz_Ajax {

		public static function init() {

			//Ajax functions related to invoices
			add_action( 'wp_ajax_wc_szamlazz_generate_invoice', array( __CLASS__, 'generate_invoice_with_ajax' ) );
			add_action( 'wp_ajax_wc_szamlazz_void_invoice', array( __CLASS__, 'void_invoice_with_ajax' ) );
			add_action( 'wp_ajax_wc_szamlazz_mark_completed', array( __CLASS__, 'mark_completed_with_ajax' ) );
			add_action( 'wp_ajax_wc_szamlazz_toggle_invoice', array( __CLASS__, 'toggle_invoice' ) );

			//Ajax functions related to receipts
			add_action( 'wp_ajax_wc_szamlazz_generate_receipt', array( __CLASS__, 'generate_receipt_with_ajax' ) );
			add_action( 'wp_ajax_wc_szamlazz_void_receipt', array( __CLASS__, 'void_receipt_with_ajax' ) );
			add_action( 'wp_ajax_wc_szamlazz_reverse_receipt', array( __CLASS__, 'reverse_receipt_with_ajax' ) );

		}

		//Generate Invoice with Ajax
		public static function generate_invoice_with_ajax() {
			check_ajax_referer( 'wc_szamlazz_generate_invoice', 'nonce' );
			$order_id = intval($_POST['order']);

			//Generate invoice(either final, proform or deposit, based on $_POST['type'])
			$type = sanitize_text_field($_POST['type']);
			$response = WC_Szamlazz()->generate_invoice($order_id, $type);

			//Check if we need to create delivery note too, only if we already generated an invoice
			$order = wc_get_order($order_id);
			$need_delivery_note = (WC_Szamlazz()->get_option('delivery_note', 'no') == 'yes');
			$need_delivery_note = apply_filters('wc_szamlazz_need_delivery_note', $need_delivery_note, $order);
			if(!$response['error'] && $need_delivery_note && $_POST['type'] == 'invoice' && !WC_Szamlazz()->is_invoice_generated($order_id, 'delivery')) {
				$response_delivery_note = WC_Szamlazz()->generate_invoice($order_id, 'delivery');
				$response['delivery'] = array(
					'name' => $response_delivery_note['name'],
					'link' => $response_delivery_note['link']
				);
			}

			wp_send_json_success($response);
		}

		//Generate Receipt with Ajax
		public static function generate_receipt_with_ajax() {
			check_ajax_referer( 'wc_szamlazz_generate_invoice', 'nonce' );
			$order_id = intval($_POST['order']);
			$response = WC_Szamlazz()->generate_receipt($order_id);
			wp_send_json_success($response);
		}

		//Cancel Invoice with Ajax
		public static function void_invoice_with_ajax() {
			check_ajax_referer( 'wc_szamlazz_generate_invoice', 'nonce' );
			$order_id = intval($_POST['order']);
			$response = WC_Szamlazz()->generate_void_invoice($order_id, 'void');
			wp_send_json_success($response);
		}

		//Cancel receipt with ajax
		public static function void_receipt_with_ajax() {
			check_ajax_referer( 'wc_szamlazz_generate_invoice', 'nonce' );
			$order_id = intval($_POST['order']);
			$response = WC_Szamlazz()->generate_void_receipt($order_id);
			wp_send_json_success($response);
		}

		//Mark completed with Ajax
		public static function mark_completed_with_ajax() {
			check_ajax_referer( 'wc_szamlazz_generate_invoice', 'nonce' );
			$order_id = intval($_POST['order']);
			$date = false;
			if(isset($_POST['date'])) $date = sanitize_text_field($_POST['date']);
			$response = WC_Szamlazz()->generate_invoice_complete($order_id, $date);
			wp_send_json_success($response);
		}

		//If the invoice is already generated without the plugin
		public static function toggle_invoice() {
			check_ajax_referer( 'wc_szamlazz_generate_invoice', 'nonce' );

			if ( !current_user_can( 'edit_shop_orders' ) ) {
				wp_die( __( 'You do not have sufficient permissions to access this page.', 'wc-szamlazz' ) );
			}
			$orderid = intval($_POST['order']);
			$order = wc_get_order($orderid);
			$note = sanitize_text_field($_POST['note']);
			$invoice_own = $order->get_meta('_wc_szamlazz_own');
			$response = array();

			if($invoice_own) {
				$response['state'] = 'on';
				$order->delete_meta_data('_wc_szamlazz_own');
				$response['messages'][] = esc_html__('Invoice generation turned on.', 'wc-szamlazz');
			} else {
				$response['state'] = 'off';
				$order->update_meta_data( '_wc_szamlazz_own', $note );
				$response['messages'][] = esc_html__('Invoice generation turned off.', 'wc-szamlazz');
			}

			//Save the order
			$order->save();

			wp_send_json_success($response);
		}

		//If the invoice is already generated without the plugin
		public static function reverse_receipt_with_ajax() {
			check_ajax_referer( 'wc_szamlazz_generate_invoice', 'nonce' );

			if ( !current_user_can( 'edit_shop_orders' ) ) {
				wp_die( __( 'You do not have sufficient permissions to access this page.', 'wc-szamlazz' ) );
			}
			$orderid = intval($_POST['order']);
			$order = wc_get_order($orderid);
			$order->delete_meta_data('_wc_szamlazz_type_receipt');
			$order->save();
			wp_send_json_success();
		}

	}

	WC_Szamlazz_Ajax::init();

endif;
