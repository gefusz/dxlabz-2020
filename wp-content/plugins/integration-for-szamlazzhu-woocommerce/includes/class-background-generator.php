<?php
/**
 * Background Emailer
 *
 * @version 3.0.1
 * @package WooCommerce/Classes
 */

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WC_Background_Process', false ) ) {
	include_once dirname( WC_PLUGIN_FILE ) . '/abstracts/class-wc-background-process.php';
}

class WC_Szamlazz_Background_Generator extends WC_Background_Process {

	/**
	 * Initiate new background process.
	 */
	public function __construct() {
		$this->prefix = 'wp_' . get_current_blog_id();
		$this->action = 'wc_szamlazz_invoices';
		parent::__construct();
	}

	protected function task( $callback ) {
		if ( isset( $callback['invoice_type'], $callback['order_id'] ) ) {
			try {
				global $wc_szamlazz;

				$wc_szamlazz->log_debug_messages($callback, 'bg-generate-request');

				//If custom options is set(coming from bulk actions)
				$options = array();
				if(isset($callback['options'])) $options = $callback['options'];

				if(!$wc_szamlazz->is_invoice_generated($callback['order_id'], $callback['invoice_type'])) {
					$wc_szamlazz->generate_invoice($callback['order_id'], $callback['invoice_type'], $options);
				}

				//Store in progress status
				add_option('_wc_szamlazz_bg_generate_in_progress', true);

				return false;

			} catch ( Exception $e ) {
				if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
					trigger_error( sprintf(__('Unable to create an invoice in the background with these details: %s'), serialize( $callback )), E_USER_WARNING ); // phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_trigger_error
				}
			}
		}
		return false;
	}

	protected function complete() {
		//Remove in progress status
		delete_option('_wc_szamlazz_bg_generate_in_progress');
		parent::complete();
	}

}
