<?php
	class WC_otp_simple extends WC_Payment_Gateway {
		private $ipn_url;
		private $backref_url;
		private $timeout_url;
		protected $license;
		protected $license_update;
		protected $lm;
		protected $valid;
		public function __construct() {
			$this->license =get_option('simple_license');
			$this->license_update =get_option("simple_license_activation_date");
			$this->lm =new licenseManager("");
			$this->ipn_url =WC()->api_request_url( 'WC_Gateway_simple_ipn' );    
			$this->backref_url =WC()->api_request_url( 'WC_Gateway_simple_backref');
			$this->id ="otp_simple";
			$this->icon =site_url() . '/wp-content/plugins/simplepay-for-woocommerce-premium/img/simplepay_logo.png';
			$this -> has_fields = false;
			$this->method_title ="SimplePay";
			$this->method_description ="Bankkártyás fizetés a SimplePay felületén keresztül";
			if ( class_exists( 'WC_Subscriptions' ) ) {
				$this->supports             = array(
				'products',
				'subscriptions',
				'subscription_cancellation', 
				'subscription_suspension', 
				'subscription_reactivation',
				'pre-orders',
				'subscription_date_changes',
				'subscription_amount_changes',
				'multiple_subscriptions',
				'refunds'
				);
				} else {
				
				$this->supports             = array(
				'products',
				'refunds'
				);
			}
			$this->init_form_fields();
			$this->init_settings();
			$this->valid =$this->is_valid();
			$this->enabled =($this->valid ? $this->get_option( 'enabled') : "no");
			$this->title = $this->get_option( 'title' );
			$this->description = $this->get_option( 'description' );
			$this->mode =($this->get_option( 'mode' ) =='test' ? true : false);
			$this->enable_logging =$this->get_option( 'enable_logging' );
			$this->enable_cert =$this->get_option( 'enable_cert' );
			$this->enable_trial =$this->get_option( 'enable_trial' );
			$this->enable_simple_tokens =$this->get_option( 'enable_simple_tokens' );
			$this->showMessageOnCart =$this->get_option( 'showMessageOnCart' );
			$this->showMessageOnCheckout =$this->get_option( 'showMessageOnCheckout' );
			$this->successfull_status =$this->get_option( 'successfull_status' );
			$this->company_name =$this->get_option( 'company_name' );
			$this->company_addr =$this->get_option( 'company_addr' );
			$this->enable_card_hold =$this->get_option( 'enable_card_hold' );
			$this->enable_two_step =$this->get_option( 'enable_two_step' );
			$this->close_transaction =$this->get_option( 'close_transaction' );
			$this->hideAgreementsOnCheckout =$this->get_option( 'hideAgreementsOnCheckout' );
			$this->orderButtonText =$this->get_option( 'orderButtonText' );
			if (!empty($this->orderButtonText)) {
				$this->order_button_text =$this->orderButtonText;
			}
			if ($this->valid) {
				add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
				//urls
				add_action( 'woocommerce_api_wc_gateway_simple_ipn', array($this, 'simple_ipn_handler' ) );
				add_action( 'woocommerce_api_wc_gateway_simple_backref', array($this, 'simple_backref_handler' ) );
				//checkout
				add_action("woocommerce_review_order_before_submit", array($this, "simple_aszf"));
				add_action('wp_enqueue_scripts', array($this, 'simple_add_frontend_scripts'));				
				if ($this->enable_two_step =='yes') {
					$trans_gen = str_replace("wc-", "", $this->close_transaction);
					add_action( 'woocommerce_order_status_'.$trans_gen, array( $this, 'finishTransaction' ) );
				}
				//card details
				add_action( 'woocommerce_checkout_update_order_meta', array($this, "simple_card_details") );
				if ( class_exists( 'WC_Subscriptions_Order' ) ) {
					add_action('woocommerce_scheduled_subscription_payment_'.$this->id, array($this, 'spl_process_subscription'), 10, 2);
					add_action( 'woocommerce_subscription_cancelled_' . $this->id, array( $this, 'spl_cancel_subscription' ), 10, 2 );
					add_action( 'woocommerce_subscription_status_changed', array($this, "spl_pending_cancel"));
					add_action( 'wcs_resubscribe_order_created', array( $this, 'delete_resubscribe_meta' ), 10 );
					add_action( 'woocommerce_subscription_failing_payment_method_updated_' . $this->id, array($this, 'update_failing_payment_method',), 10, 2 );
					add_filter( 'woocommerce_my_subscriptions_payment_method', array( $this, 'show_subscription_payment_method' ), 10, 2 );
				}
			}
		}
		/**
			* Return the gateway's icon.
			*
			* @return string
		*/
		public function get_icon() {
			$link =(get_locale() =="hu_HU" ? "http://simplepartner.hu/PaymentService/Fizetesi_tajekoztato.pdf" : "http://simplepartner.hu/PaymentService/Payment_information.pdf");
			$icon = $this->icon ? '<a href ="'.WC_HTTPS::force_https_url($link).'" target="_blank"><img src="' . WC_HTTPS::force_https_url( $this->icon ) . '" data-no-lazy="1" title="'.__("SimplePay - Online bankkártyás fizetés", "woo-simple-premium").'" alt="'.__("SimplePay vásárlói tájékoztató", "woo-simple-premium").'" /></a>' : '';
			return apply_filters( 'woocommerce_gateway_icon', $icon, $this->id );
		}
		public function get_ipn() {
			return $this->ipn_url;
		}
		/*
			* Check if license is valid or not
			*
			* @return bool
		*/
		protected function is_valid() {
			if (!empty($this->license)) {
				
				if (strtotime($this->license_update)+86400 >strtotime(date("Y-m-d h:i:s"))) {
					return true;
					} else {
					$result =$this->lm->isValid($this->license->the_key, $this->license->activation_id);
					if ($result["msg"] =="success" ) {
						update_option("simple_license", json_decode($result["license"]));
						update_option("simple_license_activation_date", date("Y-m-d h:i:s"));
						return true;
						}  elseif ($result["msg"] =="connectionError") {
						return (($this->license->expire >strtotime(date("Y-m-d h:i:s"))) ||$this->license->expire =='' ? true : false);
					}
					else {
						if ($this->license->expire <strtotime(date("Y-m-d h:i:s"))) {
							delete_option("simple_license");
							delete_option("simple_license_activation_date");
							return false;
							} else {
							return true;
						}
					}
				}
				} else {
				return false;
			}
		}
		/*
			* Shows license activation panel on settings page
			*
			* @return void
		*/
		public function generate_license_text_html( $key, $value ) { 
			ob_start();
			print '<div id ="simple_activation_error"  role ="alert"></div>';
			
			if ($this->valid) {
				print '<table class="form-table license_table">';
				print "<tr><th>".__('A bővítmény aktiválva.', 'woo-simple-premium')."<br>".__('Lejárat dátuma: ', 'woo-simple-premium').(empty($this->license->expire_date) ? "soha" : $this->license->expire_date)."</th></tr>";
				print '<input type ="hidden" id ="license_key" value ="'.$this->license->the_key.'">';
				print '<input type ="hidden" id ="activation_id" value ="'.$this->license->activation_id.'">';
				print '<tr><td>			<button id ="deactivate" type="button">'.__("Deaktiválás", "woo-simple-premium").'</button></td></tr>';
				print '</table>';
				} else {
			?>
			<table class="form-table license_table">
				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="<?php print $key; ?>"><?php print __("Licenckód megadása", "woo-simple-premium"); ?></label>
					</th>
					<td class="forminp">
						<fieldset>
							<legend class="screen-reader-text"><span>Licenckód megadása</span></legend>
							<input class="input-text regular-input " type="text" name="license-code" id="<?php print $key; ?>" style="" placeholder=""   />
						</fieldset>
					</td>
				</tr>
				<tr><td  colspan ="2">
					<button id ="activate" type="button"><?php print __("Aktiválás", "woo-simple-premium"); ?></button>
				</td></tr>
			</table>
			<?php
			}
			return ob_get_clean();
		}
		/*
			* Initialise the form fields
			*
			* @return void
		*/
		function init_form_fields() {
			$this->form_fields = array(
			'activation_title' => array(
			'title'       => __( 'SimplePay aktiválás/deaktiválás', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => __('A bővítmény használatához aktiválás szükséges. Adjuk meg az e-mailben kapott licenckódot.'.$this->get_option("license"), 'woo-simple-premium'),
			),
			'license_code' => array(
			'title' => __( 'Licenckód', 'woo-simple-premium' ),
			'type' => 'license_text',
			'label' => __( 'SimplePay bővítményhez kapott licenckód', 'woo-simple-premium' ),
			),
			
			'simplepay_settings_title' => array(
			'title'       => __( 'SimplePay beállításai', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => __('A simplePay-hoz tartozó beállításokat módosíthatjuk itt.', 'woo-simple-premium'),
			),
			'enabled' => array(
			'title' => __( 'Bekapcsolás/Kikapcsolás', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'SimplePay bekapcsolása', 'woo-simple-premium' ),
			),
			'title' => array(
			'title' => __( 'Megjelenő név', 'woo-simple-premium' ),
			'type' => 'text',
			'description' => __( 'Ezen a néven jelenik meg a vásárló számára a pénztár oldalon.', 'woo-simple-premium' ),
			'default' =>'Bankkártyás fizetés',
			'desc_tip'      => true,
			),
			'description'     => array(
			'title'       => __( 'Leírás', 'woo-simple-premium' ),
			'type'        => 'textarea',
			'description' => __( 'A fizetési módhoz tartozó leírás, amit a vásárló lát a pénztár oldalon', 'woo-simple-premium' ),
			'default'     => __( 'Bankkártyás fizetés a SimplePay felületén keresztül.', 'woo-simple-premium' ),
			'desc_tip'    => true,
            ),
			'HUF_merchant_user' => array(
			'title' => __('FT Kereskedői azonosító (MERCHANT)', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('SimplePay által megadott kereskedői azonosító FT alapú fiókhoz', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'HUF_merchant_key' => array(
			'title' => __('FT Titkosító kulcs (SECRET_KEY)', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('SimplePay által megadott titkosító kulcs FT alapú fiókhoz', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'EUR_merchant_user' => array(
			'title' => __('EURO Kereskedői azonosító (MERCHANT)', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('SimplePay által megadott kereskedői azonosító EURO alapú fiókhoz', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'EUR_merchant_key' => array(
			'title' => __('EURO Titkosító kulcs (SECRET_KEY)', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('SimplePay által megadott titkosító kulcs EURO fiókhoz', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'USD_merchant_user' => array(
			'title' => __('USD Kereskedői azonosító (MERCHANT)', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('SimplePay által megadott kereskedői azonosító USD fiókhoz', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'USD_merchant_key' => array(
			'title' => __('USD Titkosító kulcs (SECRET_KEY)', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('SimplePay által megadott titkosító kulcs USD fiókhoz', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'mode' =>array(
			'title' =>__('SimplePay rendszer típusa', 'woo-simple-premium'),
			'type' =>'select',
			'options' =>array(
			'test' =>__('Teszt', 'woo-simple-premium'),
			'prod' =>__('Éles', 'woo-simple-premium')
			),
			'description' =>__('Teszt vagy éles fiók', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'enable_logging' => array(
			'title' => __( 'Tranzakciók naplózása', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'Tranzakciók naplózása', 'woo-simple-premium' ),
			'description' =>__('Bekapcsolás esetén a tranzakciók naplózása a wp-content/uploads/simple-log mappába történik', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'enable_cert' => array(
			'title' => __( 'Helyi tanusítvány használata', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'Helyi tanusítvány használata', 'woo-simple-premium' ),
			'description' =>__('Bekapcsolás esetén a SimplePay által kiadott *.pem tanusítvánnyal történik a hitelesítés.', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'successfull_status' =>array(
			'title' =>__('Rendelés státusza sikeres fizetést követően', 'woo-simple-premium'),
			'type' =>'select',
			'options' =>wc_get_order_statuses(),
			'default' =>'wc-processing',
			'description' =>__('Sikeres fizetést követően a rendelés ebbe a státuszba kerül.', 'woo-simple-premium')
			),
			'enable_card_hold' => array(
			'title' => __( 'Kártya elmentésének engedélyezése későbbi fizetéshez', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'Kártya elmentésének engedélyezése későbbi fizetéshez', 'woo-simple-premium' ),
			'default' => 'no',
			'desc_tip'      => true,
			'description' => __('Bekapcsolás esetén a vásárló elmentheti kártya adatait, így egy későbbi vásárláskor nem kell újból megadnia', 'woo-simple-premium'),
			),
			'company_name' => array(
			'title' => __('Cég neve', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('Pénztár oldalon, a megrendelés előtt jelenik meg a SimplePay által megkövetelt tájékoztatóban', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'company_addr' => array(
			'title' => __('Cég címe', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('Pénztár oldalon, a megrendelés előtt jelenik meg a SimplePay által megkövetelt tájékoztatóban', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'ipn_process_url' => array(
			'title'       => __( 'SimplePay IPN beállítása', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => sprintf( __( 'Állítsuk be az alábbi IPN url-t a Simple felületén.<br>URL: %s', 'woo-simple-premium' ), $this->ipn_url)
			),
			'two_step_settings' =>array(
			'title'       => __( 'Kétlépcsős tranzakció beállításai', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => __('Kétlépcsős tranzakció használata esetén a fizetés után a megadott összeg be lesz foglalva a vásárló kártyáján, azonban a terhelés ekkor még nem történik meg. Ez után 21 napja van a kereskedőnek, hogy elindítsa a kártyaterhelést, vagy felszabadítsa a befoglalt összeget. Ha a 21. napig nem történik meg a terhelés, akkor a befoglalt összeg automatikusan felszabadul és a tranzakcióra terhelés már nem indítható.', 'woo-simple'),
			),
			
			'enable_two_step' =>array(
			'title' => __( 'Kétlépcsős tranzakció engedélyezése', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'Kétlépcsős tranzakciók engedélyezése', 'woo-simple-premium' ),
			'default' => 'no',
			'desc_tip'      => true,
			'description' => __('Bekapcsolás esetén a rendeléskor az összeg csak zárolásra kerül, levonásra nem.', 'woo-simple-premium'),
			),
			'close_transaction' =>array(
			'title' =>__('Tranzakció lezárása', 'woo-simple-premium'),
			'type' =>'select',
			'options' =>wc_get_order_statuses(),
			'default' =>'wc-processing',
			'description' =>__('Kétlépcsős tranzakció esetén a tranzakció lezárása a kiválasztott státusznál történik meg.', 'woo-simple-premium')
			),
			);
			if ( class_exists( 'WC_Subscriptions_Order')) {
				$this->form_fields['subscription_settings'] =array(
				'title'       => __( 'Ismétlődő fizetés beállításai', 'woo-simple-premium' ),
				'type'        => 'title',
				'description' => __('Ismétlődő fizetés esetén kétféle módon terhelhető a vásárló kártyája. Alapértelmezetten a terhelés az OTP bankon keresztül történik, ebben az esetben nincs megkötve a terhelés száma és a hozzátartozó összeg. A bankfüggetlen megoldás használata esetén csak egy előre megadott összeghatárig terhelhető a vásárló kártyája, illetve a terhelések száma is fix. A bankfüggetlen megoldás előnye, hogy amennyiben az OTP rendszere nem érhető el, a SimplePay más külső szolgáltató bevonásával is el tudja indítani a kártya terhelését.', 'woo-simple-premium'),
				);
				
				$this->form_fields['enable_simple_tokens'] =array(
				'title' => __( 'Bankfüggetlen ismétlődő fizetés használata', 'woo-simple-premium' ),
				'type' => 'checkbox',
				'label' => __( 'Bankfüggetlen ismétlődő fizetés használata', 'woo-simple-premium' ),
				'default' => 'no',
                'desc_tip'      => true,
				'description' => __('Bekapcsolás esetén az ismétlődő összeg nem haladhatja meg az első rendelés értékét, illetve az ismétlődések száma is fix (max 48 db).', 'woo-simple-premium')
				);
				$this->form_fields['trial_settings_title'] = array(
				'title'       => __( 'Próba verzió beállításai', 'woo-simple-premium' ),
				'type'        => 'title',
				'description' => __( 'WooCommerce subscription használata esetén lehetőség van a kártya nem azonnali terhelésére, így ezzel ingyenes próba verziót lehet biztosítani a vásárló számára. Fontos, hogy ehhez engedélyezve legyen a SimplePay fiókban a kétlépcsős tranzakció.', 'woo-simple-premium' )
				);
				$this->form_fields['enable_trial'] = array(
				'title' => __( 'próba verzió engedélyezése ismétlődő fizetés esetén', 'woo-simple-premium' ),
				'type' => 'checkbox',
				'label' => __( 'próba verzió engedélyezése ismétlődő fizetés esetén', 'woo-simple-premium' ),
				'description' =>__('Bekapcsolás esetén megtörténik a kártyaregisztráció ingyenes próbavásárláskor.', 'woo-simple-premium'),
				'desc_tip'      => true,
				);
			}
			$this->form_fields['other_woo_settings']=array(
			'title'       => __( 'Egyéb WooCommerce beállítások', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => __('Néhány egyéb beállítás a megfelelő működéshez.', 'woo-simple-premium'),
			);
			$this->form_fields['showMessageOnCheckout'] = array(
			'title' => __( 'Üzenetek megjelenítésének kikényszerítése a pénztár oldalon', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'Üzenetek megjelenítésének kikényszerítése a pénztár oldalon', 'woo-simple-premium' ),
			'description' =>__('Egyes esetekben nem jelennek meg a bővítmény üzenetei a pénztár oldalon. Ebben az esetben a funkció bekapcsolásával ez orvosolható.', 'woo-simple-premium'),
			'desc_tip'      => true,
			);
			$this->form_fields['showMessageOnCart'] = array(
			'title' => __( 'Üzenetek megjelenítésének kikényszerítése a kosár oldalon', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'Üzenetek megjelenítésének kikényszerítése a kosár oldalon', 'woo-simple-premium' ),
			'description' =>__('Egyes esetekben nem jelennek meg a bővítmény üzenetei a kosár oldalon. Ebben az esetben a funkció bekapcsolásával ez orvosolható.', 'woo-simple-premium'),
			'desc_tip'      => true,
			);
			
			$this->form_fields['hideAgreementsOnCheckout'] = array(
			'title' => __('Nyilatkozatok elrejtése a pénztár oldaról', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'Nyilatkozatok elrejtése a pénztár oldaról', 'woo-simple-premium' ),
			'description' =>__('Amennyiben az ÁSZF tartalmazza a SimplePay által elvárt nyilatkozatokat, ezek elrejthetőek a pénztár oldalról.', 'woo-simple-premium'),
			'desc_tip'      => true,
			);
			$this->form_fields['orderButtonText'] = array(
			'title' => __( 'Megrendelés gomb szövege', 'woo-simple-premium' ),
			'type' => 'text',
			'description' => __( 'A SimplePay fizetési mód választásához megadható egyedi megrendelés gomb szöveg.', 'woo-simple-premium' ),
			'default' =>'Kifizetem a SimplePay felületén!',
			'desc_tip'      => true,
			);
		}
		private function containsSubscription ($order_id) {
			if ( class_exists('WC_Subscriptions_Order')) {
				return (wcs_order_contains_subscription( $order_id ) ||wcs_order_contains_renewal($order_id) ? true : false);
				} else {
				return false;
			}
		}
		/*
			* Loads SimplePay library, and Generates config details
			*
			* @return array
		*/
		protected function getSimpleSettings($currency) {
			$cert =$this->enable_cert;
			require_once(__DIR__ . '/../lib/config.php');
			require_once(__DIR__ . '/../lib/SimplePayV21.php');
			require_once(__DIR__ . '/../lib/SimplePayV21CardStorage.php');
			require_once(__DIR__ . '/../lib/SimplePayV21TokenData.php');
			$config[$currency.'_MERCHANT'] =$this->get_option($currency.'_merchant_user');
			$config[$currency.'_SECRET_KEY'] =$this->get_option($currency.'_merchant_key');
			$config["SANDBOX"] =$this->mode;
			$config["URL"] =$this->backref_url;
			$config["LOGGER"] =($this->enable_logging ==1 ? true : false);
			$config["LOG_PATH"] =($this->enable_logging ==1 ? wp_upload_dir()['basedir'] . '/simple-log' : '');
			$config["cert"] =$cert;
			return $config;
		}
		protected  function payment_start( $order_id, $type ='CARD' ) {
			global $woocommerce;
			$order = new WC_Order( $order_id );
			WC()->session->set('order_id', $order_id);
			$settings =$this->getSimpleSettings($order->get_currency());
			//set status
			if ($type =='CARD') {
				$order->update_status('pending', __( 'Fizetésre vár', 'woo-simple-premium' ));
				} else {
				$order->update_status('on-hold', __( 'Utalásra vár', 'woo-simple-premium' ));
				$settings["URL"] =home_url();
				
			}
			// Reduce stock levels
			//$order->reduce_order_stock();
			
			// Remove cart
			//$woocommerce->cart->empty_cart();
			
			$trans_id =$order->get_id();
			
			WC()->session->set('order_currency', $order->get_currency());
			$trx = new SimplePayStart;
			$trx->addData('currency', $order->get_currency());
			$trx->addConfig($settings);
			$totalprice =0;
			$has_trial =false;
			foreach ($order->get_items() as $product) {
				if ($product['line_subtotal'] >0) {
					$trx->addItems(
					array(
					'ref' => $product['product_id'],
					'title' => strip_tags($product['name']),
					'description' => '',
					'amount' => $product['qty'],
					'price' => ($product['line_subtotal'] + $product['line_subtotal_tax']) / $product['qty'],
					'tax' => '0',
					)
					);
					} else {
					$has_trial =true;
				}
				$totalprice =$totalprice+(($product['line_subtotal'] + $product['line_subtotal_tax']) / $product['qty']);
			}
			if ($has_trial ==true &&$this->enable_trial =='yes') {
				$trx->addItems(
				array(
				'ref' => 0,
				'title' =>'Trial',
				'description' => '',
				'amount' => 1,
				'price' =>100,
				'tax' => '0',
				)
				);
				$trx->addData('twoStep', true);
				update_post_meta( $order_id, 'enable_trial', 1);
			} 
			elseif ($this->enable_two_step =='yes') {
				$trx->addData('twoStep', true);
				update_post_meta( $order_id, 'two_step', 1);
				} else {
				$trx->addData('twoStep', false);
			}
			//extra
			$otherProducts =apply_filters("spl_add_product_extra", $order->get_items());
			if (isset($otherProducts[0]["ref"])) {
				foreach ($otherProducts as $prod) {
					$trx->addItems($prod);
				}
			}
			$trx->addData('shippingCost', round( $order->get_total_shipping() + $order->get_shipping_tax(), 2 ));
			$trx->addData('discount', round( $order->get_total_discount(false), 2 ));
			$trx->addData('orderRef', $trans_id);
			$trx->addData('customer', $order->get_billing_last_name().' '.$order->get_billing_first_name());
			$trx->addData('customerEmail', $order->get_billing_email());
			if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
				$trx->addData('language', ICL_LANGUAGE_CODE);
				$settings['URL'] =str_replace('lang='.ICL_LANGUAGE_CODE.'/', 'lang='.ICL_LANGUAGE_CODE.'&', $settings['URL']);
				} else {
				$trx->addData('language', explode("_", get_locale())[1]);	
			}
			$trx->addData('timeout', @date("c", time() + 600));
			$trx->addData('methods', array($type));
			$trx->addData('url', $settings['URL']);
			//számlázás
			$szamlazz_receipt =WC()->session->wc_szamlazz_receipt;
			$trx->addGroupData('invoice', 'name', $order->get_billing_last_name().' '.$order->get_billing_first_name());
			$trx->addGroupData('invoice', 'company', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_billing_company());
			$trx->addGroupData('invoice', 'country', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_billing_country());
			$trx->addGroupData('invoice', 'state', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_billing_state());
			$trx->addGroupData('invoice', 'city', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_billing_city());
			$trx->addGroupData('invoice', 'zip', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_billing_postcode());
			$trx->addGroupData('invoice', 'address', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_billing_address_1());
			$trx->addGroupData('invoice', 'address2', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_billing_address_2());
			$trx->addGroupData('invoice', 'phone', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_billing_phone());
			
			// szállítási adatok
			$trx->addGroupData('delivery', 'name', $order->get_shipping_last_name().' '.$order->get_shipping_first_name());
			$trx->addGroupData('delivery', 'company', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_shipping_company());
			$trx->addGroupData('delivery', 'country', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_shipping_country());
			$trx->addGroupData('delivery', 'state', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_shipping_state());
			$trx->addGroupData('delivery', 'city', ($szamlazz_receipt == 'receipt') ? ' ' : (empty($order->get_shipping_city()) ? $order->get_billing_city() : $order->get_shipping_city()));
			$trx->addGroupData('delivery', 'zip', ($szamlazz_receipt == 'receipt') ? ' ' : (empty($order->get_shipping_postcode()) ? $order->get_billing_postcode() : $order->get_shipping_postcode()));
			$trx->addGroupData('delivery', 'address', ($szamlazz_receipt == 'receipt') ? ' ' : (empty($order->get_shipping_address_1()) ? $order->get_billing_address_1() : $order->get_shipping_address_1()));
			$trx->addGroupData('delivery', 'address2', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_shipping_address_2());
			$trx->addGroupData('delivery', 'phone', ($szamlazz_receipt == 'receipt') ? ' ' : $order->get_billing_phone());
			//kártya tárolása
			if (WC()->session->get('hold_card_details') =="1" ) {
				$trx->addData('threeDSReqAuthMethod', '02'); 
				$trx->addData('legacyCardRegister', 'true'); 
				update_post_meta( $order_id, 'registered_card', 1);
				WC()->session->set('hold_card_details', "");
			}
			// Ha van subscription
			if ($this->containsSubscription($order_id)) {
				if ($this->enable_simple_tokens =='yes') {
					//$periodPrice =WC_Subscriptions_Order::get_recurring_total($order);
					$periodPrice =WC()->session->get( 'subscrtotal');
					$trx->addGroupData('recurring', 'times', 24); 
					$trx->addGroupData('recurring', 'until', date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 730 day")).'T00:00:00+02:00'); 
					$trx->addGroupData('recurring', 'maxAmount', $periodPrice); 
					} else {
					$trx->addData('legacyRecurring', true); 
				}
				
				$trx->addData('threeDSReqAuthMethod', '02');
				update_post_meta( $order_id, 'registered_card', 1);
			}
			//form generálás
			$trx->formDetails['element'] = 'auto';
			$trx->runStart();
			$trx->getHtmlForm();
			if (isset($trx->returnData["tokens"])) {
				$tokens =$trx->returnData["tokens"];
				update_post_meta($order_id, "tokens", $tokens);
			}
			if (isset($trx->returnData['paymentUrl'])) {
				$ret =array("url" =>$trx->returnData['paymentUrl']);
				} else {
				if (isset($trx->returnData['errorCodes'])) {
					$ret =array("error" =>$trx->returnData['errorCodes'][0]);
					} else {
					$ret =array("error" =>__("Egyéb hiba", "woo-simple"));
				}
			}
			return $ret;
		}
		private function payment_do($order_id, $recurring =false, $total =0) {
			
			global $woocommerce;
			$order = new WC_Order( $order_id );
			$order->update_status('pending', __( 'Fizetésre vár', 'woo-simple-premium' ));
			$trans_id =$order->get_id();
			$settings =$this->getSimpleSettings($order->get_currency());
			//Do, vagy recurringdo osztály inicializálása
			if ( $recurring ==1 &&wcs_order_contains_renewal( $order->id ) ) {
				$parent_order_id = WC_Subscriptions_Renewal_Order::get_parent_order_id( $order->id );
				$tokens =get_post_meta($parent_order_id, "tokens", true);
			}
			if (!empty($tokens)) {
				$trx = new SimplePayDorecurring;
				} else {
				$trx = new SimplePayDo;
			}
			$trx->addConfig($settings); 
			$trx->addConfigData('merchantAccount', $this->get_option($order->get_currency().'_merchant_user'));
			$trx->addData('orderRef', $trans_id);
			$trx->addData('methods', array('CARD'));
			$trx->addData('currency', $order->get_currency());
			if ($recurring ==1) {
				if (!empty($tokens)) {
					$trx->addData('token', $tokens[0]);
					$trx->addData('type', 'MIT');
					$trx->addData('threeDSReqAuthMethod', '02');
					} else {
					$trx->addData('legacyRecurring', true);
					$trx->addData('type', 'REC');
					$trx->addData('threeDSReqAuthMethod', '02');
					$trx->addData('cardId', get_post_meta($parent_order_id, "simple_id", true));
				}
				} else {
				$user_id = $order->get_user_id();
				$cards =get_user_meta($user_id, 'cards', true);
				
				$trx->addData('cardId', $cards[WC()->session->get('selected_card')]["cardid"]);
				$trx->addData('threeDSReqAuthMethod', '02');
				$trx->addData('type', 'CIT');
				$trx->addData('url', $settings['URL']);
				WC()->session->set('order_id', $order_id);
			}
			$trx->addData('total', $total);
			$trx->addData('customer', $order->get_billing_last_name().' '.$order->get_billing_first_name());
			$trx->addData('customerEmail', $order->get_billing_email());
			$trx->addData('twoStep', false);
			$ret = (!empty($tokens) ? $trx->runDorecurring() : $trx->runDo());
			return $ret;
		}
		/*
			* Waiting for Simple to fix bug
		*/
		public function finishTransaction ($order_id) {
			$order = new WC_Order( $order_id );
			$extra =0;
			$fees =0;
			if (!empty($order->get_items('fee'))) {
				foreach( $order->get_items('fee') as $item_id => $item_fee ){
					$fee_name = $item_fee->get_name();
					$fee_total = round($item_fee->get_total()+$item_fee->get_total_tax());
					if ($fee_total <0) {
						$fees =$fees+(-1*$fee_total);
						} else {
						$fees =$fees+$fee_total;
					}
				}
			}
			$otherProducts =apply_filters("spl_add_product_extra", $order->get_items());
			if (isset($otherProducts[0]["ref"])) {
				foreach ($otherProducts as $product) {
					$extra =$extra+$product["price"];
				}
			}
			$total =$order->get_total()+$extra;
			$aprovedTotal =apply_filters("spl_aproved_total", $order->get_total(), $order_id);
			if ($aprovedTotal ==0) {
				$aprovedTotal =$total;
			}
			$returned =$this->startFinishTransaction ($order_id, get_post_meta($order_id, 'simple_id', true), $order->get_total()+$extra, $aprovedTotal);
			if ($returned["approveTotal"] ==$aprovedTotal) {
				$order->add_order_note(  __( 'Sikeres tranzakció lezárás.', 'woo-simple-premium'));
				update_post_meta($order_id, "approved", $aprovedTotal);
				} else {
				$order->add_order_note(  __( 'Sikertelen tranzakció lezárás.', 'woo-simple-premium'));
			}
		}
		public function startFinishTransaction ($order_id, $trans_id, $originalTotal, $approveTotal) {
			$order = new WC_Order( $order_id );
			$settings =$this->getSimpleSettings($order->get_currency());
			$trx = new SimplePayFinish; 
			$trx->addConfig($settings); 
			$trx->addData('orderRef', $order_id);
			$trx->addData('transactionId', $trans_id);
			$trx->addConfigData('merchantAccount', $this->merchant_user);
			$trx->addData('originalTotal', $originalTotal);
			$trx->addData('approveTotal', $approveTotal);
			$trx->transactionBase['currency'] = $order->get_currency();
			return $trx->runFinish();
		}
		private function card_redirection($order_id, $ret) {
			$order = new WC_Order( $order_id );
			if (isset($ret["transactionId"])) {
				WC()->session->set('selected_card', "");
				global $woocommerce;
				
				WC()->cart->empty_cart();
				wc_add_notice( sprintf( __( 'Sikeres tranzakció.<br/>SimplePay referencia szám: %s<br/>Megrendelés azonosítója: %s<br/>Dátum: %s', 'woo-simple-premium' ), $ret["transactionId"], $ret['orderRef'], date('Y-m-d H:i:s', time())));
				$order->add_order_note( sprintf( __( 'Sikeres tranzakció. SimplePay referencia szám: %s', 'woo-simple-premium' ), $ret["transactionId"]));
				update_post_meta( $order_id, 'simple_id', $ret["transactionId"]);
				update_post_meta( $order_id, 'registered_card', 1);
				wp_redirect( $order->get_checkout_order_received_url() );
				} else {
				WC()->session->set('selected_card', "");
				$order->update_status( 'failed', __( 'Sikertelen fizetés.', 'woo-simple-premium' ) );
				$order->add_order_note( sprintf( __( 'Sikertelen fizetés. Státusz: %s, SimplePay referencia szám: %s', 'woo-simple-premium' ), $result['e'], $result['t']));
				wc_add_notice( sprintf( __( 'Sikertelen tranzakció. Kérjük, ellenőrizze a tranzakció során megadott adatok helyességét. Amennyiben minden adatot helyesen adott meg, a visszautasítás okának kivizsgálása kapcsán kérjük, szíveskedjen kapcsolatba lépni kártyakibocsátó bankjával.<br/>SimplePay referencia szám: %s<br/>Megrendelés azonosítója: %s<br/>Dátum: %s', 'woo-simple-premium' ), $result['t'], $result['o'], date('Y-m-d H:i:s', time()) ), 'error');
				update_post_meta( $order_id, 'simple_id',  $response['PAYREFNO'] );
				update_post_meta( $order_id, 'simple_status', $response['ORDER_STATUS']);
				$url = $order->get_cancel_order_url();
				wp_redirect( $url );
			}
			
			die();
		}
		public function spl_process_subscription($amount_to_charge, $order) {
			$ret =$this->payment_do($order->get_id(), 1, $amount_to_charge);
			if (isset($ret["transactionId"])) {
				$order->add_order_note( __('Sikeres ismétlődő bankkártyás fizetés.', 'woo-simple-premium'));
				WC_Subscriptions_Manager::process_subscription_payments_on_order( $order );
				$subscription = wcs_get_subscriptions_for_order(WC_Subscriptions_Renewal_Order::get_parent_order_id( $order->id ));
				$parent_order =WC_Subscriptions_Renewal_Order::get_parent_order_id( $order->id );
				foreach ($subscription as $subscription_id =>$subscription_obj) {
					if($subscription_obj->order->id == $parent_order) {
						do_action("spl_success_recurring", $subscription_id);
					}
				}
				
				} else {
				if (isset($ret["errorCodes"])) {
					$order->add_order_note( __('Sikertelen ismétlődő bankkártyás fizetés, visszakapott hibakód: '.implode(", ", $ret["errorCodes"]), 'woo-simple-premium'));
					} else {
					$order->add_order_note( __('Sikertelen ismétlődő bankkártyás fizetés, a SimplePay nem adott vissza hibakódot.', 'woo-simple-premium'));
				}
				//WC_Subscriptions_Manager::process_subscription_payment_failure_on_order( $order, $product_id );
				$order->update_status( 'failed', __( 'Sikertelen ismétlődő bankkártyás fizetés.', 'woo-simple-premium' ) );
				$parent_order_id = WC_Subscriptions_Renewal_Order::get_parent_order_id( $order->id );
				$tokens =get_post_meta($parent_order_id, "tokens", true);
				if (!empty($tokens)) {
					unset($tokens[0]);
					update_post_meta($parent_order_id, "tokens", array_values($tokens));
				}
			}
		}
		public function spl_pending_cancel ($order_id) {
			$order = wc_get_order( $order_id );
			if ($order->get_status() =='pending-cancel' &&$order->get_payment_method() ==$this->id) {
				$subscription = new WC_Subscription($order_id);
				
				$cardId =get_post_meta($subscription->parent_id, "simple_id", true);
				update_post_meta($subscription->parent_id, "tokens", array());
				$order->add_order_note( __('Ismétlődő fizetéshez tartozó tokenek törlése megtörtént.', 'woo-simple-premium'));
				if ($this->cardCancel($cardId, $order->get_currency())) {
					$order->add_order_note( __('Kártyaregisztráció törlése a Simple rendszeréből.', 'woo-simple-premium'));
				}
			}
			
		}
		
		public function spl_cancel_subscription ($subscription) {
			if ($subscription->payment_method ==$this->id &&($subscription->status =='cancelled' ||$subscription->status =='pending-cancellation')) {
				$cardId =get_post_meta($subscription->parent_id, "simple_id", true);
				update_post_meta($subscription->parent_id, "tokens", array());
				$order = wc_get_order( $subscription->id );
				$order->add_order_note( __('Ismétlődő fizetéshez tartozó tokenek törlése megtörtént.', 'woo-simple-premium'));
				return $this->cardCancel($cardId, $order->get_currency());
				} else {
				return false;
			}
		}
		public function delete_resubscribe_meta( $resubscribe_order ) {
			delete_post_meta( get_woo_id( $resubscribe_order ), 'simple_id' );
			delete_post_meta( get_woo_id( $resubscribe_order ), 'tokens' );
		}
		public function update_failing_payment_method( $subscription, $renewal_order ) {
			update_post_meta( get_woo_id( $subscription ), 'simple_id', get_post_meta($renewal_order->id, "simple_id", true));
			update_post_meta( get_woo_id( $subscription ), 'tokens', get_post_meta($renewal_order->id, "simple_id", true));
		}
		function process_payment ($order_id) {
			global $woocommerce;
			$order = new WC_Order( $order_id );
			if (WC()->session->get('selected_card') !="") {
				$ret =$this->payment_do($order_id, false, $order->get_total());
				update_post_meta($order_id, "_selected_card", WC()->session->get('selected_card'));
				WC()->session->set('selected_card', '');
				if (isset($ret["redirectUrl"])) {
					return array(
					'result' => 'success',
					'redirect' =>$ret["redirectUrl"]
					);
				}
				if (isset($ret["orderRef"])) {
					WC()->cart->empty_cart();
					$msg =sprintf( __( 'Sikeres tranzakció.<br/>SimplePay referencia szám: %s<br/>Megrendelés azonosítója: %s<br/>Dátum: %s', 'woo-simple-premium' ), $ret["transactionId"], $ret["orderRef"], date('Y-m-d H:i:s', time()));
					wc_add_notice($msg, 'success');
					WC()->session->set('simple_msg', $msg);
					$order->add_order_note( sprintf( __( 'Sikeres egygombos tranzakció. SimplePay referencia szám: %s', 'woo-simple-premium' ), $result['t'] ));
					update_post_meta( $order_id, 'simple_id', $ret['transactionId']);
					return array(
					'result' => 'success',
					'redirect' =>$order->get_checkout_order_received_url()
					);
				}
				} else {
				$response =$this->payment_start($order->get_id());
				if (isset($response["url"])) {
					return array(
					'result' => 'success',
					'redirect' =>$response["url"]
					);
					} else {
					wc_add_notice( __('Hiba történt a fizetés indításakor, visszakapott hibakód:', 'woo-simple-premium') . $response["error"], 'error' );
					return;
				}
				//end
			}
		}
		
		function simple_backref_handler() {
			
			
			global $woocommerce;
			$order_id = WC()->session->get( 'order_id' );
			$currency =WC()->session->get('order_currency');
			$settings =$this->getSimpleSettings($currency);
			$trx = new SimplePayBack;
			$trx->addConfig($settings);   
			$result = array();
			if (isset($_REQUEST['r']) && isset($_REQUEST['s'])) {
				if ($trx->isBackSignatureCheck($_REQUEST['r'], $_REQUEST['s'])) {
					$result = $trx->getRawNotification();
					if (empty($order_id)) {
						$order_id =$result["o"];
					}
					$order = new WC_Order( $order_id );
					if ($result["e"] =='CANCEL') {
						if (empty(WC()->session->get('simple_msg'))) {
							$msg =sprintf( __( 'Megszakított tranzakció<br/>Ön megszakította a fizetést!<br/><br/>Dátum: %s<br/>Rendelés azonosító: <b>%s', 'woo-simple-premium' ), date('Y-m-d H:i:s', time()), $result["o"] );
							wc_add_notice($msg, 'error');
							WC()->session->set('simple_msg', $msg);
						}
						$url = $order->get_cancel_order_url();
						wp_redirect( $url );
					}
					elseif ($result["e"] =='TIMEOUT') {
						$msg =sprintf( __( 'lejárt a tranzakció maximális ideje!<br/><br/>Dátum: %s<br/>Rendelés azonosító: <b>%s', 'woo-simple-premium' ), date('Y-m-d H:i:s', time()), $result["o"] );
						if (empty(WC()->session->get('simple_msg'))) {
							WC()->session->set('simple_msg', $msg);
							wc_add_notice($msg, 'error');
						}
						$url = $order->get_cancel_order_url();
						wp_redirect( $url );
					}
					elseif ($result["e"] =='SUCCESS') {
						WC()->cart->empty_cart();
						$msg =sprintf( __( 'Sikeres tranzakció.<br/>SimplePay referencia szám: %s<br/>Megrendelés azonosítója: %s<br/>Dátum: %s', 'woo-simple-premium' ), $result['t'], $result['o'], date('Y-m-d H:i:s', time()));
						if (empty(WC()->session->get('simple_msg'))) {
							wc_add_notice($msg, 'success');
							WC()->session->set('simple_msg', $msg);
						}
						$order->add_order_note( sprintf( __( 'Sikeres tranzakció. SimplePay referencia szám: %s', 'woo-simple-premium' ), $result['t'] ));
						update_post_meta( $order_id, 'simple_id', $result['t']);
						update_post_meta( $order_id, 'simple_status', $result['e']);
						wp_redirect( $order->get_checkout_order_received_url() );
						} else {
						$order->update_status( 'failed', __( 'Sikertelen fizetés.', 'woo-simple-premium' ) );
						$order->add_order_note( sprintf( __( 'Sikertelen fizetés. Státusz: %s, SimplePay referencia szám: %s', 'woo-simple-premium' ), $result['e'], $result['t']));
						$msg =sprintf( __( 'Sikertelen tranzakció. Kérjük, ellenőrizze a tranzakció során megadott adatok helyességét. Amennyiben minden adatot helyesen adott meg, a visszautasítás okának kivizsgálása kapcsán kérjük, szíveskedjen kapcsolatba lépni kártyakibocsátó bankjával.<br/>SimplePay referencia szám: %s<br/>Megrendelés azonosítója: %s<br/>Dátum: %s', 'woo-simple-premium' ), $result['t'], $result['o'], date('Y-m-d H:i:s', time()) );
						if (empty(WC()->session->get('simple_msg'))) {
							wc_add_notice($msg, 'error');
							WC()->session->set('simple_msg', $msg);
						}
						update_post_meta( $order_id, 'simple_id',  $response['PAYREFNO'] );
						update_post_meta( $order_id, 'simple_status', $response['ORDER_STATUS']);
						$url = $order->get_cancel_order_url();
						wp_redirect( $url );
					}
				}
			}
			
			
			
			
		}
		public function cardCancel($cardid, $currency) {
			$settings =$this->getSimpleSettings($currency);
			$trx = new SimplePayCardCancel; 
			$trx->addConfig($settings); 
			$trx->addConfigData('merchantAccount', $this->get_option($currency.'_merchant_user'));
			$trx->addData('cardId', $cardid);
			$ret =$trx->runCardCancel();
			if ($ret["status"] =='DISABLED') {
				return true;
				} else {
				return false;
			}
		}
		function simple_ipn_handler() {
			header('Content-Type: text/html; charset=utf-8');
			$json = file_get_contents('php://input');
			$headers =json_decode($json, true);
			$order_id =$headers["orderRef"];
			$order = new WC_Order( $order_id );
			$settings =$this->getSimpleSettings($order->get_currency());
			$trx = new SimplePayIpn;
			$trx->addConfig($settings);
			if ($trx->isIpnSignatureCheck($json)) {
				$trx->runIpnConfirm();
				
				$user_id = $order->get_user_id();
				if (isset($headers["expiry"])) {
					if (wcs_order_contains_subscription($order_id, 'parent')) {
						update_post_meta( $order_id, 'simple_card_mask', $headers["cardMask"]);
						update_post_meta( $order_id, 'simple_card_expire', $headers["expiry"]);
						$subscription = wcs_get_subscriptions_for_order($order_id);
						foreach ($subscription as $subscription_id =>$subscription_obj) {
							if($subscription_obj->order->id == $order_id) {
								do_action("spl_success_first_subscription", $subscription_id);
							}
						}
					} 
					if (get_post_meta( $order_id, 'registered_card', true) ==1 &&!wcs_order_contains_subscription($order_id, 'parent')) {
						$cards =get_user_meta($user_id, 'cards', true);
						if (empty($cards)) {
							$cards =array();
						}
						$cards[] =array("cardid" =>$headers['transactionId'], "name" =>$headers["cardMask"], "card_mask" =>$headers["cardMask"], "exp_date" =>$headers["expiry"]);
						update_user_meta($user_id, "cards", $cards);
					}
					} elseif (empty($headers["expiry"]) &&$this->containsSubscription($order_id)) {
					$parent_order_id = WC_Subscriptions_Renewal_Order::get_parent_order_id( $order_id );
					$tokens =get_post_meta($parent_order_id, "tokens", true);
					if (!empty($tokens)) {
						unset($tokens[0]);
						update_post_meta($parent_order_id, "tokens", array_values($tokens));
					}
				}
				if (get_post_meta( $order_id, 'enable_trial', true) ==1) {
					$returned =$this->startFinishTransaction ($order->get_id(), $headers['transactionId'], ($order->get_total()+100), $order->get_total());
					if ($returned["approveTotal"] ==($order->get_total() -100)) {
						$order->add_order_note(  __( 'Sikeres trial tranzakció visszatérítés.', 'woo-simple-premium'));
						//update_post_meta($order_id, "approved", $aprovedTotal);
						} else {
						$order->add_order_note(  __( 'Sikertelen tranzakció lezárás.', 'woo-simple-premium'));
					}
				}
				if ( ($order->get_status() == 'on-hold') || ($order->get_status() == 'pending') ) {
					update_post_meta( $order_id, 'simple_id',  $headers['transactionId']);
					update_post_meta( $order_id, 'simple_status', $headers['status']);
					//$order->payment_complete($headers['transactionId']);
					$order->update_status($this->successfull_status, __( 'Sikeres fizetés.', 'woo-simple-premium' ) );
					$order->add_order_note( __('Sikeres bankkártyás fizetés, IPN visszahívás megtörtént.', 'woo-simple-premium'));
				}
			}
			exit;
		}
		
		
		public function process_refund($order_id, $amount = null, $reason = '') {
			$order = new WC_Order( $order_id );
			$settings =$this->getSimpleSettings($order->get_currency());
			$trx = new SimplePayRefund;
			$trx->addConfig($settings);
			$trx->addData('orderRef', $order_id);
			$trx->addData('transactionId', get_post_meta($order_id, 'simple_id', true));
			$trx->addConfigData('merchantAccount', $settings[$order->get_currency().'_MERCHANT']);
			$trx->addData('refundTotal', $amount);
			$trx->addData('currency', $order->get_currency());
			$trx->runRefund();
			$result =$trx->getReturnData();
			if (isset($result["refundTransactionId"])) {
				return true;
			}
		}
		function simple_add_frontend_scripts(){
			wp_enqueue_style( "simple_css_frontend", plugins_url( '/simplepay-for-woocommerce-premium/assets/css/frontend.css','simplepay-for-woocommerce-premium'), array(), date('YmdHis'));
		}
		function simple_aszf() {
			require_once(__DIR__ . '/../lib/idna_convert.class.php');
			$IDN = new idna_convert();
			// Get the chosen payment gateway (dynamically)
			$chosen_payment_method = WC()->session->get('chosen_payment_method');
			// Set your payment gateways IDs in EACH "IF" statement
			if( $chosen_payment_method ==$this->id){
				$containsSubscription =false;
				$subscrtotal =0;
				foreach( WC()->cart->get_cart() as $cart_item ){
					$product_id = $cart_item['product_id'];
					$_product = wc_get_product( $product_id );
					if( $_product->is_type( 'subscription' ) || $_product->is_type('variable-subscription') ||isset($cart_item["wcsatt_data"]["active_subscription_scheme"])) {
						$containsSubscription =true;
						if ($_product->is_type('variable-subscription')) {
							$_product = wc_get_product( $cart_item["variation_id"]);
						}
						$subscrtotal =$subscrtotal+($cart_item['quantity']*wc_get_price_including_tax($_product));
					}
				}
				if ($containsSubscription) {
					$packages = WC()->shipping->get_packages();
					foreach ($packages as $i => $package) {
						$chosen_method = isset(WC()->session->chosen_shipping_methods[$i]) ? WC()->session->chosen_shipping_methods[$i] : '';
						$rate = $package['rates'][$chosen_method]->cost;
						if ($rate >0) {
							$subscrtotal =$subscrtotal+round($rate+$package['rates'][$chosen_method]->taxes[1]);
						}
					}
					WC()->session->set('subscrtotal', $subscrtotal);
				}
				if (!$containsSubscription) {
					if ($this->enable_card_hold =="yes") {
					?>
					<div id ="hold-card"><h3><?php echo __('Új kártya eltárolása', 'woo-simple-premium');?></h3>
						<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
							<input id="hold-card-details" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox"  type="checkbox" name="hold_card_details" value="1" /> <span><?php _e("Kártyám elmentése későbbi fizetéshez", "woo-simple-premium");?></span>
						</label>
					</div>
					<?php
						if (is_user_logged_in()) {
							echo '<div id ="kartyareg">';
							$cards =get_user_meta(get_current_user_id(), "cards", true);
							if (!empty($cards) &&count($cards) >0) {
								print '<h3>'.__("Regisztrált kártyáim", "woo-simple-premium").'</h3>';
								print "<p>".__('Az alábbi kártyával fizetek: ', 'woo-simple-premium');
								foreach ($cards as $i =>$card) {
									print '<br><label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox"><input type ="radio" class ="s_card" name ="s_card" value ="'.$i.'"> '.$card["name"].'</label>';
								}
								echo '<br>';
							}
						?>
						<br>
					</div>
					<?php
					}
					
				}
			}
			if ($this->hideAgreementsOnCheckout !='yes' ) {
				echo '<div id="checkout_tajekoztato"><small>'.__('A megrendeléssel elfogadom az <a id ="tajekoztato" href ="#">adattovábbítási nyilatkozatot.</a>', 'woo-simple-premium').'</small>';
				if ($containsSubscription) {
					echo '<p><small>'.__('A megrendeléssel elfogadom az ismétlődő fizetésre vonatkozó <a id ="tajekoztato-recurring" href ="#">Kártyaregisztrációs nyilatkozatot.</a>', 'woo-simple-premium').'</small></p>';
				}
				echo '<div style ="display: none" id ="oneclick-tajekoztato"><p><small>'.__('A megrendeléssel elfogadom az egykattintásos (oneclick)  fizetésre vonatkozó <a id ="tajekoztato-oneclick" href ="#">Kártyaregisztrációs nyilatkozatot.</a>', 'woo-simple-premium').'</small></p></div>';
				echo '<div style ="display: none" id="tajekoztatoszoveg" class="simplepay-tajekoztatok" title="'.__("Adattovábbítási nyilatkozat", "woo-simple-premium").'" tabindex ="-1" role ="dialog" aria-modal ="true" aria-labelledby ="adattovabbitasi"><button id ="tajclose" class="simplepay-tajekoztatok-close" aria-label ="'.__("Bezárás", "woo-simple-premium").'">X</button><h2 class ="dialog-title" id="adattovabbitasi">'.__("Adattovábbítási nyilatkozat", "woo-simple-premium").'</h2><div class ="modal-text">';
				printf( __( 'A megrendeléssel tudomásul veszem, hogy a %1$s (%2$s) adatkezelő által a %3$s felhasználói adatbázisában tárolt alábbi személyes adataim átadásra kerülnek az OTP Mobil Kft., mint adatfeldolgozó részére. Az adatkezelő által továbbított adatok köre az alábbi: név, e-mail cím, telefonszám, számlázási cím adatok, szállítási cím adatok. Az adatfeldolgozó által végzett adatfeldolgozási tevékenység jellege és célja a SimplePay Adatkezelési tájékoztatóban, az <a href="http://simplepay.hu/vasarlo-aff " target=_blank">alábbi linken tekinthető meg</a>', 'woo-simple-premium' ), $this->company_name, $this->company_addr, $IDN->decode(get_home_url()));
				echo '</div></div>';        
				echo '<div style ="display: none" id="tajekoztatorec" title="'.__("Kártyaregisztrációs nyilatkozat", "woo-simple-premium").'" tabindex ="-1" role ="dialog" aria-modal ="true" aria-labelledby ="nyilatkozat-rec"><button id ="recclose" aria-label ="'.__("Bezárás", "woo-simple-premium").'">X</button><h2 class ="dialog-title" id ="nyilatkozat-rec">'.__("Kártyaregisztrációs nyilatkozat", "woo-simple-premium").'</h2><div class ="modal-text">';
				printf( __("Az ismétlődő bankkártyás fizetés (továbbiakban: „Ismétlődő fizetés”) egy, a SimplePay által biztosított bankkártya elfogadáshoz tartozó funkció, mely azt jelenti, hogy a Vásárló által a regisztrációs tranzakció során megadott bankkártyaadatokkal a jövőben újabb fizetéseket lehet kezdeményezni a bankkártyaadatok újbóli megadása nélkül.
				<p>Az Ismétlődő fizetés igénybevételéhez jelen nyilatkozat elfogadásával Ön hozzájárul, hogy a sikeres regisztrációs tranzakciót követően jelen webshopban (%s) kezdeményezett későbbi fizetések (max %s db, összeghatár %s %s, lejárata %s) a bankkártyaadatok újbóli megadása és az Ön tranzakciónként hozzájárulása nélkül a Kereskedő által kezdeményezve történjenek.
				<p>Figyelem(!): a bankkártyaadatok kezelése a kártyatársasági szabályoknak megfelelően történik. A bankkártyaadatokhoz sem a Kereskedő, sem a SimplePay nem fér hozzá.
				<p>A Kereskedő által tévesen vagy jogtalanul kezdeményezett ismétlődő fizetéses tranzakciókért közvetlenül a Kereskedő felel, Kereskedő fizetési szolgáltatójával (SimplePay) szemben bármilyen igényérvényesítés kizárt.
				<p>Jelen tájékoztatót átolvastam, annak tartalmát tudomásul veszem és elfogadom.", "woo-simple-premium"), $IDN->decode(get_home_url()), 24, $subscrtotal, get_woocommerce_currency_symbol(), date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 730 day")));
				echo '</div></div>';        
				
				echo '<div style ="display: none" id="tajekoztatooneclick" class="simplepay-tajekoztatok" title="'.__("Oneclick kártya regisztrációs nyilatkozat ", "woo-simple-premium").'" tabindex ="-1" role ="dialog" aria-modal ="true" aria-labelledby ="nyilatkozat-oneclick"><button id ="oneclickclose" class="simplepay-tajekoztatok-close" aria-label ="'.__("Bezárás", "woo-simple-premium").'">X</button><h2 class ="dialog-title" id ="nyilatkozat-oneclick">'.__("Kártyaregisztrációs nyilatkozat", "woo-simple-premium").'</h2><div class ="modal-text">';
				printf( __("Az ismétlődő bankkártyás fizetés (továbbiakban: \"Ismétlődő fizetés\") egy, a SimplePay által biztosított bankkártya elfogadáshoz tartozó funkció, mely azt jelenti, hogy a Vásárló által a regisztrációs tranzakció során megadott bankkártyaadatokkal a jövőben újabb fizetéseket lehet kezdeményezni a bankkártyaadatok újbóli megadása nélkül. Az ismétlődő fizetés ún. „eseti hozzájárulásos” típusa minden tranzakció esetében a Vevő eseti jóváhagyásával történik, tehát, Ön valamennyi jövőbeni fizetésnél jóvá kell, hogy hagyja a tranzakciót. A sikeres fizetés tényéről Ön minden esetben a hagyományos bankkártyás fizetéssel megegyező csatornákon keresztül értesítést kap. 
				<p>Az Ismétlődő fizetés igénybevételéhez jelen nyilatkozat elfogadásával Ön hozzájárul, hogy a sikeres regisztrációs tranzakciót követően jelen webshopban (%s) Ön az itt található felhasználói fiókjából kezdeményezett későbbi fizetések a bankkártyaadatok újbóli megadása nélkül menjenek végbe. 
				<p>Figyelem(!): a bankkártyaadatok kezelése a kártyatársasági szabályoknak megfelelően történik. A bankkártyaadatokhoz sem a Kereskedő, sem a SimplePay nem fér hozzá. A Kereskedő által tévesen vagy jogtalanul kezdeményezett ismétlődő fizetéses tranzakciókért közvetlenül a Kereskedő felel, Kereskedő fizetési szolgáltatójával (SimplePay) szemben bármilyen igényérvényesítés kizárt. 
				<p>Jelen tájékoztatót átolvastam, annak tartalmát tudomásul veszem és elfogadom.", "woo-simple-premium"), $IDN->decode(get_home_url()));
				echo '</div></div>';        
				echo '</div>';        
			}
		?>
		<script>
			(function($){
				$('form.checkout').on( 'change', 'input[name^="payment_method"]', function() {
					var t = { updateTimer: !1,  dirtyInput: !1,
						reset_update_checkout_timer: function() {
							clearTimeout(t.updateTimer)
							},  trigger_update_checkout: function() {
							t.reset_update_checkout_timer(), t.dirtyInput = !1,
							$(document.body).trigger("update_checkout")
						}
					};
					t.trigger_update_checkout();
				});
				$('#tajekoztato').click(function(e) {
				    $("#tajekoztatoszoveg").show();
					$("#tajekoztatoszoveg").focus();
				});
				$('#tajclose').click(function(e) {
					$("#tajekoztatoszoveg").hide();
					e.preventDefault();
				});
				
				$('#tajekoztato-recurring').click(function(e) {
				    $("#tajekoztatorec").show();
					$("#tajekoztatorec").focus();
				});
				$('#recclose').click(function(e) {
					$("#tajekoztatorec").hide();
					e.preventDefault();
				});
				
				//
				$('#tajekoztato-oneclick').click(function(e) {
				    $("#tajekoztatooneclick").show();
					$("#tajekoztatooneclick").focus();
				});
				$('#oneclickclose').click(function(e) {
					$("#tajekoztatooneclick").hide();
					e.preventDefault();
				});
				$("#hold-card-details").on('click', function() {
					if($(this).is(':checked')) {
						$("#kartyareg").hide();
						$("#oneclick-tajekoztato").show() ;
						} else {
						$("#oneclick-tajekoztato").hide();
						$("#kartyareg").show();
					}
				});
			})(jQuery);
		</script>
		<?php
		}
	}
	function simple_card_details($order_id) {
		if (isset($_POST["s_card"])) {
			WC()->session->set('selected_card', $_POST["s_card"]);
		}
		if (!empty($_POST["hold_card_details"])) {
			WC()->session->set('hold_card_details', 1);
			} else {
			WC()->session->set('hold_card_details', "");
		}
		
	}
	
	public function show_subscription_payment_method($payment_method_to_display, $subscription) {
		if ($subscription->get_payment_method() ==$this->id) {
			$simple_card_mask =get_post_meta( $subscription->get_parent_id(), 'simple_card_mask', true);
			$simple_id =get_post_meta( $subscription->get_parent_id(), 'simple_id', true);
			$simple_card_expire =get_post_meta( $subscription->get_parent_id(), 'simple_card_expire', true);
			$simple_tokens_count =count(get_post_meta($subscription->get_parent_id(), "tokens", true));
			$payment_method_to_display =sprintf(__('SimplePay fizetés. Kártyaszám: %s, lejárata: %s, hátralévő terhelések maximális száma: %s db, SimplePay tranzakció azonosító: %s', 'woo-simple-premium'), $simple_card_mask, $simple_card_expire, $simple_tokens_count, $simple_id);
			return $payment_method_to_display;
			} else {
			return $payment_method_to_display;
		}
	}
}				