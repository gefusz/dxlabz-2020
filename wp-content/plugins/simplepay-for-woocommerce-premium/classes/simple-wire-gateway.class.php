<?php
	class WC_otp_simple_wire extends WC_otp_simple {
		private $ipn_url;
		private $backref_url;
		private $timeout_url;
		protected $license;
		protected $license_update;
		protected $lm;
		protected $valid;
		public function __construct() {
			$this->license =get_option('simple_license');
			$this->license_update =get_option("simple_license_activation_date");
			$this->lm =new licenseManager("");
			
			$this->backref_url =WC()->api_request_url( 'WC_Gateway_simple_backref');
			$this->id ="otp_simple_wire";
			$this->icon =site_url() . '/wp-content/plugins/simplepay-for-woocommerce-premium/img/simplepay_logo.png';
			$this->order_button_text = __( 'Kifizetem átutalással!', 'woo-simple-premium' );
			$this -> has_fields = false;
			$this->method_title ="Banki átutalás";
			$this->method_description ="Banki átutalás a SimplePay felületén keresztül";
			$this->supports             = array(
			'products',
			'refunds'
			);
			parent::init_form_fields();
			$this->form_fields["ipn_process_url"]["description"] = sprintf( __( 'Állítsuk be az alábbi IPN url-t a Simple felületén.<br>URL: %s', 'woo-simple-premium' ), WC()->api_request_url( 'WC_Gateway_simple_ipn' ));
			unset($this->form_fields["two_step_settings"]);
			unset($this->form_fields["enable_two_step"]);
			unset($this->form_fields["close_transaction"]);
			unset($this->form_fields["trial_settings_title"]);
			unset($this->form_fields["enable_trial"]);
			unset($this->form_fields["showMessageOnCheckout"]);
			unset($this->form_fields["showMessageOnCart"]);
			$this->init_settings();
			$this->valid =$this->is_valid();
			$this->enabled =($this->valid ? $this->get_option( 'enabled') : "no");
			$this->title = $this->get_option( 'title' );
			$this->description = $this->get_option( 'description' );
			$this->currency =$this->get_option( 'simple-currency' );
			$this->mode =($this->get_option( 'mode' ) =='test' ? true : false);
			$this->enable_logging =$this->get_option( 'enable_logging' );
			$this->enable_cert =$this->get_option( 'enable_cert' );
			$this->merchant_user =$this->get_option( 'merchant_user' );
			$this->merchant_key =$this->get_option( 'merchant_key' );
			$this->successfull_status =$this->get_option( 'successfull_status' );
			$this->company_name =$this->get_option( 'company_name' );
			$this->company_addr =$this->get_option( 'company_addr' );
			$this->hideAgreementsOnCheckout =$this->get_option( 'hideAgreementsOnCheckout' );
			$this->orderButtonText =$this->get_option( 'orderButtonText' );
			if (!empty($this->orderButtonText)) {
				$this->order_button_text =$this->orderButtonText;
			}
			if ($this->valid) {
				add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
				//add_action( 'woocommerce_receipt_otp_simple', array(&$this, 'simple_payment_redirect_handler' ) );
				//urls
				//add_action( 'woocommerce_api_wc_gateway_simple_ipn', array($this, 'simple_ipn_handler' ) );
				//checkout
				add_action("woocommerce_review_order_before_submit", array($this, "simple_aszf"));
				add_action('wp_enqueue_scripts', array($this, 'simple_add_frontend_scripts'));
			}
		}
		/*
			* Shows license activation panel on settings page
			*
			* @return void
		*/
		public function generate_license_text_html( $key, $value ) { 
			ob_start();
			print '<div id ="simple_activation_error"  role ="alert"></div>';
			if ($this->is_valid()) {
				print '<table class="form-table license_table">';
				print "<tr><th>".__('A bővítmény aktiválva.', 'woo-simple-premium')."<br>".__('Lejárat dátuma: ', 'woo-simple-premium').(empty($this->license->expire_date) ? "soha" : $this->license->expire_date)."</th></tr>";
				print '<input type ="hidden" id ="license_key" value ="'.$this->license->the_key.'">';
				print '<input type ="hidden" id ="activation_id" value ="'.$this->license->activation_id.'">';
				print '<tr><td>			<button id ="deactivate" type="button">'.__("Deaktiválás", "woo-simple").'</button></td></tr>';
				print '</table>';
				} else {
			?>
			<table class="form-table license_table">
				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="<?php print $key; ?>"><?php _e("Licenckód megadása", "woo-simple"); ?></label>
					</th>
					<td class="forminp">
						<fieldset>
							<legend class="screen-reader-text"><span><?php _e("Licenckód megadása", "woo-simple");?></span></legend>
							<input class="input-text regular-input " type="text" name="license-code" id="<?php print $key; ?>" style="" placeholder=""   />
						</fieldset>
					</td>
				</tr>
				<tr><td  colspan ="2">
					<button id ="activate" type="button"><?php _e("Aktiválás", "woo-simple"); ?></button>
				</td></tr>
			</table>
			<?php
			}
			return ob_get_clean();
		}
		/*
			* Initialise the form fields
			*
			* @return void
		*/
		function init_form_fields() {
			$this->form_fields = array(
			'activation_title' => array(
			'title'       => __( 'SimplePay aktiválás/deaktiválás', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => __('A bővítmény használatához aktiválás szükséges. Adjuk meg az e-mailben kapott licenckódot.', 'woo-simple-premium')
			),
			'license_code' => array(
			'title' => __( 'Licenckód', 'woo-simple-premium' ),
			'type' => 'license_text',
			'label' => __( 'SimplePay bővítményhez kapott licenckód', 'woo-simple-premium' ),
			),
			
			'simplepay_settings_title' => array(
			'title'       => __( 'SimplePay beállításai', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => __('A simplePay-hoz tartozó beállításokat módosíthatjuk itt.', 'woo-simple-premium'),
			),
			'enabled' => array(
			'title' => __( 'Bekapcsolás/Kikapcsolás', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'SimplePay bekapcsolása', 'woo-simple-premium' ),
			),
			'title' => array(
			'title' => __( 'Megjelenő név', 'woo-simple-premium' ),
			'type' => 'text',
			'description' => __( 'Ezen a néven jelenik meg a vásárló számára a pénztár oldalon.', 'woo-simple-premium' ),
			'default' =>'Bankkártyás fizetés',
			'desc_tip'      => true,
			),
			'description'     => array(
			'title'       => __( 'Leírás', 'woo-simple-premium' ),
			'type'        => 'textarea',
			'description' => __( 'A fizetési módhoz tartozó leírás, amit a vásárló lát a pénztár oldalon', 'woo-simple-premium' ),
			'default'     => __( 'Bankkártyás fizetés a SimplePay felületén keresztül.', 'woo-simple-premium' ),
			'desc_tip'    => true,
            ),
			'merchant_user' => array(
			'title' => __('Kereskedői azonosító (MERCHANT)', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('SimplePay által megadott kereskedői azonosító', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'merchant_key' => array(
			'title' => __('Titkosító kulcs (SECRET_KEY)', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('SimplePay által megadott titkosító kulcs', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'mode' =>array(
			'title' =>__('SimplePay rendszer típusa', 'woo-simple-premium'),
			'type' =>'select',
			'options' =>array(
			'test' =>__('Teszt', 'woo-simple-premium'),
			'prod' =>__('Éles', 'woo-simple-premium')
			),
			'description' =>__('Teszt vagy éles fiók', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'enable_logging' => array(
			'title' => __( 'Tranzakciók naplózása', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'Tranzakciók naplózása', 'woo-simple-premium' ),
			'description' =>__('Bekapcsolás esetén a tranzakciók naplózása a wp-content/uploads/simple-log mappába történik', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'enable_cert' => array(
			'title' => __( 'Helyi tanusítvány használata', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'Helyi tanusítvány használata', 'woo-simple-premium' ),
			'description' =>__('Bekapcsolás esetén a SimplePay által kiadott *.pem tanusítvánnyal történik a hitelesítés.', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'simple-currency' =>array(
			'title' =>__('Fiók devizaneme', 'woo-simple-premium'),
			'type' =>'select',
			'options' =>array(
			'HUF' =>'Ft',
			'EUR' =>'EURO',
			'USD' =>'USD'
			),
			'description' =>''
			),
			'successfull_status' =>array(
			'title' =>__('Rendelés státusza sikeres fizetést követően', 'woo-simple-premium'),
			'type' =>'select',
			'options' =>wc_get_order_statuses(),
			'default' =>'wc-processing',
			'description' =>__('Sikeres fizetést követően a rendelés ebbe a státuszba kerül.', 'woo-simple-premium')
			),
			'company_name' => array(
			'title' => __('Cég neve', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('Pénztár oldalon, a megrendelés előtt jelenik meg a SimplePay által megkövetelt tájékoztatóban', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'company_addr' => array(
			'title' => __('Cég címe', 'woo-simple-premium'),
			'type' => 'text',
			'description' => __('Pénztár oldalon, a megrendelés előtt jelenik meg a SimplePay által megkövetelt tájékoztatóban', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'ipn_process_url' => array(
			'title'       => __( 'SimplePay IPN beállítása', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => sprintf( __( 'Állítsuk be az alábbi IPN url-t a Simple felületén.<br>URL: %s', 'woo-simple-premium' ), $this->ipn_url),
			),
			'other_woo_settings' =>array(
			'title'       => __( 'Egyéb WooCommerce beállítások', 'woo-simple-premium' ),
			'type'        => 'title',
			'description' => __('Néhány egyéb beállítás a megfelelő működéshez.', 'woo-simple-premium'),
			),
			'showMessageOnCheckout' =>array(
			'title' => __( 'Üzenetek megjelenítésének kikényszerítése a pénztár oldalon', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'Üzenetek megjelenítésének kikényszerítése a pénztár oldalon', 'woo-simple-premium' ),
			'description' =>__('Egyes esetekben nem jelennek meg a bővítmény üzenetei a pénztár oldalon. Ebben az esetben a funkció bekapcsolásával ez orvosolható.', 'woo-simple-premium'),
			'desc_tip'      => true,
			),
			'showMessageOnCart' => array(
			'title' => __( 'Üzenetek megjelenítésének kikényszerítése a kosár oldalon', 'woo-simple-premium' ),
			'type' => 'checkbox',
			'label' => __( 'Üzenetek megjelenítésének kikényszerítése a kosár oldalon', 'woo-simple-premium' ),
			'description' =>__('Egyes esetekben nem jelennek meg a bővítmény üzenetei a kosár oldalon. Ebben az esetben a funkció bekapcsolásával ez orvosolható.', 'woo-simple-premium'),
			'desc_tip'      => true,
			)
			);
		}
		/*
			* Manages the payment
			*
			* @return array
		*/
		function process_payment ($order_id) {
			global $woocommerce;
			$order = new WC_Order( $order_id );
			$response =parent::payment_start($order->get_id(), 'WIRE');
			if (isset($response["url"])) {
				WC()->cart->empty_cart();
				return array(
				'result' => 'success',
				'redirect' =>$response["url"]
				);
				} else {
				wc_add_notice( __('Hiba történt a fizetés indításakor, visszakapott hibakód:', 'woo-simple-premium') . $response["error"], 'error' );
				return;
			}
		}
		/*
			* Manages the refund process
			*
			* @return bool
		*/
		public function process_refund($order_id, $amount = null, $reason = '') {
			$order = new WC_Order( $order_id );
			$settings =parent::getSimpleSettings($order->get_currency());
			$settings["URL"] =home_url();
			$trx = new SimplePayRefund;
			$trx->addConfig($settings);
			$trx->addData('orderRef', $order_id);
			$trx->addData('transactionId', get_post_meta($order_id, 'simple_id', true));
			$trx->addConfigData('merchantAccount', $settings[$this->currency.'_MERCHANT']);
			$trx->addData('refundTotal', $amount);
			$trx->addData('currency', $this->currency);
			$trx->runRefund();
			$result =$trx->getReturnData();
			if (isset($result["refundTransactionId"])) {
				return true;
			}
		}
		/*
			* Loads frontend scripts
			*
			* @return void
		*/
		function simple_add_frontend_scripts(){
			wp_enqueue_style( "simple_css_frontend", plugins_url( '/simplepay-for-woocommerce-premium/assets/css/frontend.css','simplepay-for-woocommerce'), array(), date('YmdHis'));
		}
		/*
			* Shows SimplePay agreements on Checkout page
			*
			* @return void
		*/
		function simple_aszf() {
			require_once(__DIR__ . '/../lib/idna_convert.class.php');
			$IDN = new idna_convert();
			
			// Get the chosen payment gateway (dynamically)
			$chosen_payment_method = WC()->session->get('chosen_payment_method');
			// Set your payment gateways IDs in EACH "IF" statement
			if( $chosen_payment_method == $this->id){
				if ($this->hideAgreementsOnCheckout !='yes' ) {
					echo '<div id="checkout_tajekoztato"><small>'.__('A megrendeléssel elfogadom az <a id ="tajekoztato" href ="javascript:">adattovábbítási nyilatkozatot.</a>', 'woo-simple-premium').'</small>';
					echo '<div style ="display: none" id="tajekoztatoszoveg" title="'.__("Adattovábbítási nyilatkozat", "woo-simple").'" tabindex ="-1" role ="dialog" aria-modal ="true" aria-labelledby ="dialog-title"><button id ="tajclose" aria-label ="'.__("Bezárás", "woo-simple").'">X</button><h2 id ="dialog-title">'.__("Adattovábbítási nyilatkozat", "woo-simple").'</h2><div class ="modal-text">';
					printf( __( 'A megrendeléssel tudomásul veszem, hogy a %1$s (%2$s) adatkezelő által a %3$s felhasználói adatbázisában tárolt alábbi személyes adataim átadásra kerülnek az OTP Mobil Kft., mint adatfeldolgozó részére. Az adatkezelő által továbbított adatok köre az alábbi: név, e-mail cím, telefonszám, számlázási cím adatok, szállítási cím adatok. Az adatfeldolgozó által végzett adatfeldolgozási tevékenység jellege és célja a SimplePay Adatkezelési tájékoztatóban, az <a href="http://simplepay.hu/vasarlo-aff " target=_blank">alábbi linken tekinthető meg</a>', 'woo-simple-premium' ), $this->company_name, $this->company_addr, $IDN->decode(get_home_url()));
					echo '</div></div></div>';        
				}
			}
		?>
		<script>
			(function($){
				$("#tajekoztatoszoveg").hide();
				$('form.checkout').on( 'change', 'input[name^="payment_method"]', function() {
					var t = { updateTimer: !1,  dirtyInput: !1,
						reset_update_checkout_timer: function() {
							clearTimeout(t.updateTimer)
							},  trigger_update_checkout: function() {
							t.reset_update_checkout_timer(), t.dirtyInput = !1,
							$(document.body).trigger("update_checkout");
						}
					};
					t.trigger_update_checkout();
				});
				$('#tajekoztato').click(function(e) {
				    $("#tajekoztatoszoveg").show();
					$("#tajekoztatoszoveg").focus();
				});
				$('#tajclose').click(function(e) {
					$("#tajekoztatoszoveg").hide();
					e.preventDefault();
				});
				$(document).keyup(function(e) {
					if (e.keyCode === 27) $('#tajclose').click();   // esc
				});
			})(jQuery);
		</script>
		<?php
			
		}
		
	}	