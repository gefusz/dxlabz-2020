#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: SimplePay  for WooCommerce- premium\n"
"POT-Creation-Date: 2021-02-06 22:34+0100\n"
"PO-Revision-Date: 2021-02-06 22:34+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: simplepay-for-woocommerce-premium.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: classes/license-manager.class.php:13
msgid "Hibás kód! Vegyük fel a kapcsolatot a bővítmény fejlesztőjével!"
msgstr ""

#: classes/license-manager.class.php:14 classes/license-manager.class.php:16
msgid "Hibás licenckód!"
msgstr ""

#: classes/license-manager.class.php:15
msgid "Ez a licenckód nem ehhez a termékhez tartozik!"
msgstr ""

#: classes/license-manager.class.php:17
msgid "Nem létező  licenckód!"
msgstr ""

#: classes/license-manager.class.php:18
msgid "Meg kell adni licenckódot!"
msgstr ""

#: classes/license-manager.class.php:19
msgid "A licenckód lejárt!"
msgstr ""

#: classes/license-manager.class.php:20
msgid ""
"A licenckód nem aktiválható, mert elértük az aktivációk maximális számát!!"
msgstr ""

#: classes/license-manager.class.php:21
msgid ""
"A licenckód nem aktiválható, mert elértük az aktiválható webcímek maximális "
"számát!!"
msgstr ""

#: classes/license-manager.class.php:22
msgid "A licenckód nem aktiválható, nem található ilyen aktivációs azonosító!!"
msgstr ""

#: classes/simple-gateway.class.php:96
msgid "SimplePay - Online bankkártyás fizetés"
msgstr ""

#: classes/simple-gateway.class.php:96
msgid "SimplePay vásárlói tájékoztató"
msgstr ""

#: classes/simple-gateway.class.php:146
#: classes/simple-wire-gateway.class.php:74
msgid "A bővítmény aktiválva."
msgstr ""

#: classes/simple-gateway.class.php:146
#: classes/simple-wire-gateway.class.php:74
msgid "Lejárat dátuma: "
msgstr ""

#: classes/simple-gateway.class.php:149
#: classes/simple-wire-gateway.class.php:77
msgid "Deaktiválás"
msgstr ""

#: classes/simple-gateway.class.php:156
#: classes/simple-wire-gateway.class.php:84
#: classes/simple-wire-gateway.class.php:88
msgid "Licenckód megadása"
msgstr ""

#: classes/simple-gateway.class.php:166
#: classes/simple-wire-gateway.class.php:94
msgid "Aktiválás"
msgstr ""

#: classes/simple-gateway.class.php:181
#: classes/simple-wire-gateway.class.php:109
msgid "SimplePay aktiválás/deaktiválás"
msgstr ""

#: classes/simple-gateway.class.php:183
#: classes/simple-wire-gateway.class.php:111
msgid ""
"A bővítmény használatához aktiválás szükséges. Adjuk meg az e-mailben kapott "
"licenckódot."
msgstr ""

#: classes/simple-gateway.class.php:186
#: classes/simple-wire-gateway.class.php:114
msgid "Licenckód"
msgstr ""

#: classes/simple-gateway.class.php:188
#: classes/simple-wire-gateway.class.php:116
msgid "SimplePay bővítményhez kapott licenckód"
msgstr ""

#: classes/simple-gateway.class.php:192
#: classes/simple-wire-gateway.class.php:120
msgid "SimplePay beállításai"
msgstr ""

#: classes/simple-gateway.class.php:194
#: classes/simple-wire-gateway.class.php:122
msgid "A simplePay-hoz tartozó beállításokat módosíthatjuk itt."
msgstr ""

#: classes/simple-gateway.class.php:197
#: classes/simple-wire-gateway.class.php:125
msgid "Bekapcsolás/Kikapcsolás"
msgstr ""

#: classes/simple-gateway.class.php:199
#: classes/simple-wire-gateway.class.php:127
msgid "SimplePay bekapcsolása"
msgstr ""

#: classes/simple-gateway.class.php:202
#: classes/simple-wire-gateway.class.php:130
msgid "Megjelenő név"
msgstr ""

#: classes/simple-gateway.class.php:204
#: classes/simple-wire-gateway.class.php:132
msgid "Ezen a néven jelenik meg a vásárló számára a pénztár oldalon."
msgstr ""

#: classes/simple-gateway.class.php:209
#: classes/simple-wire-gateway.class.php:137
msgid "Leírás"
msgstr ""

#: classes/simple-gateway.class.php:211
#: classes/simple-wire-gateway.class.php:139
msgid "A fizetési módhoz tartozó leírás, amit a vásárló lát a pénztár oldalon"
msgstr ""

#: classes/simple-gateway.class.php:212
#: classes/simple-wire-gateway.class.php:140
msgid "Bankkártyás fizetés a SimplePay felületén keresztül."
msgstr ""

#: classes/simple-gateway.class.php:216
msgid "FT Kereskedői azonosító (MERCHANT)"
msgstr ""

#: classes/simple-gateway.class.php:218
msgid "SimplePay által megadott kereskedői azonosító FT alapú fiókhoz"
msgstr ""

#: classes/simple-gateway.class.php:222
msgid "FT Titkosító kulcs (SECRET_KEY)"
msgstr ""

#: classes/simple-gateway.class.php:224
msgid "SimplePay által megadott titkosító kulcs FT alapú fiókhoz"
msgstr ""

#: classes/simple-gateway.class.php:228
msgid "EURO Kereskedői azonosító (MERCHANT)"
msgstr ""

#: classes/simple-gateway.class.php:230
msgid "SimplePay által megadott kereskedői azonosító EURO alapú fiókhoz"
msgstr ""

#: classes/simple-gateway.class.php:234
msgid "EURO Titkosító kulcs (SECRET_KEY)"
msgstr ""

#: classes/simple-gateway.class.php:236
msgid "SimplePay által megadott titkosító kulcs EURO fiókhoz"
msgstr ""

#: classes/simple-gateway.class.php:240
msgid "USD Kereskedői azonosító (MERCHANT)"
msgstr ""

#: classes/simple-gateway.class.php:242
msgid "SimplePay által megadott kereskedői azonosító USD fiókhoz"
msgstr ""

#: classes/simple-gateway.class.php:246
msgid "USD Titkosító kulcs (SECRET_KEY)"
msgstr ""

#: classes/simple-gateway.class.php:248
msgid "SimplePay által megadott titkosító kulcs USD fiókhoz"
msgstr ""

#: classes/simple-gateway.class.php:252
#: classes/simple-wire-gateway.class.php:156
msgid "SimplePay rendszer típusa"
msgstr ""

#: classes/simple-gateway.class.php:255
#: classes/simple-wire-gateway.class.php:159
msgid "Teszt"
msgstr ""

#: classes/simple-gateway.class.php:256
#: classes/simple-wire-gateway.class.php:160
msgid "Éles"
msgstr ""

#: classes/simple-gateway.class.php:258
#: classes/simple-wire-gateway.class.php:162
msgid "Teszt vagy éles fiók"
msgstr ""

#: classes/simple-gateway.class.php:262 classes/simple-gateway.class.php:264
#: classes/simple-wire-gateway.class.php:166
#: classes/simple-wire-gateway.class.php:168
msgid "Tranzakciók naplózása"
msgstr ""

#: classes/simple-gateway.class.php:265
#: classes/simple-wire-gateway.class.php:169
msgid ""
"Bekapcsolás esetén a tranzakciók naplózása a wp-content/uploads/simple-log "
"mappába történik"
msgstr ""

#: classes/simple-gateway.class.php:269 classes/simple-gateway.class.php:271
#: classes/simple-wire-gateway.class.php:173
#: classes/simple-wire-gateway.class.php:175
msgid "Helyi tanusítvány használata"
msgstr ""

#: classes/simple-gateway.class.php:272
#: classes/simple-wire-gateway.class.php:176
msgid ""
"Bekapcsolás esetén a SimplePay által kiadott *.pem tanusítvánnyal történik a "
"hitelesítés."
msgstr ""

#: classes/simple-gateway.class.php:276
#: classes/simple-wire-gateway.class.php:190
msgid "Rendelés státusza sikeres fizetést követően"
msgstr ""

#: classes/simple-gateway.class.php:280
#: classes/simple-wire-gateway.class.php:194
msgid "Sikeres fizetést követően a rendelés ebbe a státuszba kerül."
msgstr ""

#: classes/simple-gateway.class.php:283 classes/simple-gateway.class.php:285
msgid "Kártya elmentésének engedélyezése későbbi fizetéshez"
msgstr ""

#: classes/simple-gateway.class.php:288
msgid ""
"Bekapcsolás esetén a vásárló elmentheti kártya adatait, így egy későbbi "
"vásárláskor nem kell újból megadnia"
msgstr ""

#: classes/simple-gateway.class.php:291
#: classes/simple-wire-gateway.class.php:197
msgid "Cég neve"
msgstr ""

#: classes/simple-gateway.class.php:293 classes/simple-gateway.class.php:299
#: classes/simple-wire-gateway.class.php:199
#: classes/simple-wire-gateway.class.php:205
msgid ""
"Pénztár oldalon, a megrendelés előtt jelenik meg a SimplePay által "
"megkövetelt tájékoztatóban"
msgstr ""

#: classes/simple-gateway.class.php:297
#: classes/simple-wire-gateway.class.php:203
msgid "Cég címe"
msgstr ""

#: classes/simple-gateway.class.php:303
#: classes/simple-wire-gateway.class.php:209
msgid "SimplePay IPN beállítása"
msgstr ""

#: classes/simple-gateway.class.php:305
#: classes/simple-wire-gateway.class.php:27
#: classes/simple-wire-gateway.class.php:211
#, php-format
msgid "Állítsuk be az alábbi IPN url-t a Simple felületén.<br>URL: %s"
msgstr ""

#: classes/simple-gateway.class.php:308
msgid "Kétlépcsős tranzakció beállításai"
msgstr ""

#: classes/simple-gateway.class.php:310
msgid ""
"Kétlépcsős tranzakció használata esetén a fizetés után a megadott összeg be "
"lesz foglalva a vásárló kártyáján, azonban a terhelés ekkor még nem történik "
"meg. Ez után 21 napja van a kereskedőnek, hogy elindítsa a kártyaterhelést, "
"vagy felszabadítsa a befoglalt összeget. Ha a 21. napig nem történik meg a "
"terhelés, akkor a befoglalt összeg automatikusan felszabadul és a "
"tranzakcióra terhelés már nem indítható."
msgstr ""

#: classes/simple-gateway.class.php:314
msgid "Kétlépcsős tranzakció engedélyezése"
msgstr ""

#: classes/simple-gateway.class.php:316
msgid "Kétlépcsős tranzakciók engedélyezése"
msgstr ""

#: classes/simple-gateway.class.php:319
msgid ""
"Bekapcsolás esetén a rendeléskor az összeg csak zárolásra kerül, levonásra "
"nem."
msgstr ""

#: classes/simple-gateway.class.php:322
msgid "Tranzakció lezárása"
msgstr ""

#: classes/simple-gateway.class.php:326
msgid ""
"Kétlépcsős tranzakció esetén a tranzakció lezárása a kiválasztott státusznál "
"történik meg."
msgstr ""

#: classes/simple-gateway.class.php:331
msgid "Ismétlődő fizetés beállításai"
msgstr ""

#: classes/simple-gateway.class.php:333
msgid ""
"Ismétlődő fizetés esetén kétféle módon terhelhető a vásárló kártyája. "
"Alapértelmezetten a terhelés az OTP bankon keresztül történik, ebben az "
"esetben nincs megkötve a terhelés száma és a hozzátartozó összeg. A "
"bankfüggetlen megoldás használata esetén csak egy előre megadott "
"összeghatárig terhelhető a vásárló kártyája, illetve a terhelések száma is "
"fix. A bankfüggetlen megoldás előnye, hogy amennyiben az OTP rendszere nem "
"érhető el, a SimplePay más külső szolgáltató bevonásával is el tudja "
"indítani a kártya terhelését."
msgstr ""

#: classes/simple-gateway.class.php:337 classes/simple-gateway.class.php:339
msgid "Bankfüggetlen ismétlődő fizetés használata"
msgstr ""

#: classes/simple-gateway.class.php:342
msgid ""
"Bekapcsolás esetén az ismétlődő összeg nem haladhatja meg az első rendelés "
"értékét, illetve az ismétlődések száma is fix (max 48 db)."
msgstr ""

#: classes/simple-gateway.class.php:345
msgid "Próba verzió beállításai"
msgstr ""

#: classes/simple-gateway.class.php:347
msgid ""
"WooCommerce subscription használata esetén lehetőség van a kártya nem "
"azonnali terhelésére, így ezzel ingyenes próba verziót lehet biztosítani a "
"vásárló számára. Fontos, hogy ehhez engedélyezve legyen a SimplePay fiókban "
"a kétlépcsős tranzakció."
msgstr ""

#: classes/simple-gateway.class.php:350 classes/simple-gateway.class.php:352
msgid "próba verzió engedélyezése ismétlődő fizetés esetén"
msgstr ""

#: classes/simple-gateway.class.php:353
msgid ""
"Bekapcsolás esetén megtörténik a kártyaregisztráció ingyenes "
"próbavásárláskor."
msgstr ""

#: classes/simple-gateway.class.php:358
#: classes/simple-wire-gateway.class.php:214
msgid "Egyéb WooCommerce beállítások"
msgstr ""

#: classes/simple-gateway.class.php:360
#: classes/simple-wire-gateway.class.php:216
msgid "Néhány egyéb beállítás a megfelelő működéshez."
msgstr ""

#: classes/simple-gateway.class.php:363 classes/simple-gateway.class.php:365
#: classes/simple-wire-gateway.class.php:219
#: classes/simple-wire-gateway.class.php:221
msgid "Üzenetek megjelenítésének kikényszerítése a pénztár oldalon"
msgstr ""

#: classes/simple-gateway.class.php:366
#: classes/simple-wire-gateway.class.php:222
msgid ""
"Egyes esetekben nem jelennek meg a bővítmény üzenetei a pénztár oldalon. "
"Ebben az esetben a funkció bekapcsolásával ez orvosolható."
msgstr ""

#: classes/simple-gateway.class.php:370 classes/simple-gateway.class.php:372
#: classes/simple-wire-gateway.class.php:226
#: classes/simple-wire-gateway.class.php:228
msgid "Üzenetek megjelenítésének kikényszerítése a kosár oldalon"
msgstr ""

#: classes/simple-gateway.class.php:373
#: classes/simple-wire-gateway.class.php:229
msgid ""
"Egyes esetekben nem jelennek meg a bővítmény üzenetei a kosár oldalon. Ebben "
"az esetben a funkció bekapcsolásával ez orvosolható."
msgstr ""

#: classes/simple-gateway.class.php:378 classes/simple-gateway.class.php:380
msgid "Nyilatkozatok elrejtése a pénztár oldaról"
msgstr ""

#: classes/simple-gateway.class.php:381
msgid ""
"Amennyiben az ÁSZF tartalmazza a SimplePay által elvárt nyilatkozatokat, "
"ezek elrejthetőek a pénztár oldalról."
msgstr ""

#: classes/simple-gateway.class.php:385
msgid "Megrendelés gomb szövege"
msgstr ""

#: classes/simple-gateway.class.php:387
msgid ""
"A SimplePay fizetési mód választásához megadható egyedi megrendelés gomb "
"szöveg."
msgstr ""

#: classes/simple-gateway.class.php:426 classes/simple-gateway.class.php:571
msgid "Fizetésre vár"
msgstr ""

#: classes/simple-gateway.class.php:428
msgid "Utalásra vár"
msgstr ""

#: classes/simple-gateway.class.php:562
msgid "Egyéb hiba"
msgstr ""

#: classes/simple-gateway.class.php:648
msgid "Sikeres tranzakció lezárás."
msgstr ""

#: classes/simple-gateway.class.php:651 classes/simple-gateway.class.php:921
msgid "Sikertelen tranzakció lezárás."
msgstr ""

#: classes/simple-gateway.class.php:674 classes/simple-gateway.class.php:770
#: classes/simple-gateway.class.php:832
#, php-format
msgid ""
"Sikeres tranzakció.<br/>SimplePay referencia szám: %s<br/>Megrendelés "
"azonosítója: %s<br/>Dátum: %s"
msgstr ""

#: classes/simple-gateway.class.php:675 classes/simple-gateway.class.php:837
#, php-format
msgid "Sikeres tranzakció. SimplePay referencia szám: %s"
msgstr ""

#: classes/simple-gateway.class.php:681 classes/simple-gateway.class.php:842
msgid "Sikertelen fizetés."
msgstr ""

#: classes/simple-gateway.class.php:682 classes/simple-gateway.class.php:843
#, php-format
msgid "Sikertelen fizetés. Státusz: %s, SimplePay referencia szám: %s"
msgstr ""

#: classes/simple-gateway.class.php:683 classes/simple-gateway.class.php:844
#, php-format
msgid ""
"Sikertelen tranzakció. Kérjük, ellenőrizze a tranzakció során megadott "
"adatok helyességét. Amennyiben minden adatot helyesen adott meg, a "
"visszautasítás okának kivizsgálása kapcsán kérjük, szíveskedjen kapcsolatba "
"lépni kártyakibocsátó bankjával.<br/>SimplePay referencia szám: %s<br/"
">Megrendelés azonosítója: %s<br/>Dátum: %s"
msgstr ""

#: classes/simple-gateway.class.php:695
msgid "Sikeres ismétlődő bankkártyás fizetés."
msgstr ""

#: classes/simple-gateway.class.php:707
msgid "Sikertelen ismétlődő bankkártyás fizetés, visszakapott hibakód: "
msgstr ""

#: classes/simple-gateway.class.php:709
msgid ""
"Sikertelen ismétlődő bankkártyás fizetés, a SimplePay nem adott vissza "
"hibakódot."
msgstr ""

#: classes/simple-gateway.class.php:712
msgid "Sikertelen ismétlődő bankkártyás fizetés."
msgstr ""

#: classes/simple-gateway.class.php:728 classes/simple-gateway.class.php:741
msgid "Ismétlődő fizetéshez tartozó tokenek törlése megtörtént."
msgstr ""

#: classes/simple-gateway.class.php:730
msgid "Kártyaregisztráció törlése a Simple rendszeréből."
msgstr ""

#: classes/simple-gateway.class.php:773
#, php-format
msgid "Sikeres egygombos tranzakció. SimplePay referencia szám: %s"
msgstr ""

#: classes/simple-gateway.class.php:788
#: classes/simple-wire-gateway.class.php:250
msgid "Hiba történt a fizetés indításakor, visszakapott hibakód:"
msgstr ""

#: classes/simple-gateway.class.php:814
#, php-format
msgid ""
"Megszakított tranzakció<br/>Ön megszakította a fizetést!<br/><br/>Dátum: "
"%s<br/>Rendelés azonosító: <b>%s"
msgstr ""

#: classes/simple-gateway.class.php:822
#, php-format
msgid ""
"lejárt a tranzakció maximális ideje!<br/><br/>Dátum: %s<br/>Rendelés "
"azonosító: <b>%s"
msgstr ""

#: classes/simple-gateway.class.php:918
msgid "Sikeres trial tranzakció visszatérítés."
msgstr ""

#: classes/simple-gateway.class.php:928
msgid "Sikeres fizetés."
msgstr ""

#: classes/simple-gateway.class.php:929
msgid "Sikeres bankkártyás fizetés, IPN visszahívás megtörtént."
msgstr ""

#: classes/simple-gateway.class.php:989
msgid "Új kártya eltárolása"
msgstr ""

#: classes/simple-gateway.class.php:991
msgid "Kártyám elmentése későbbi fizetéshez"
msgstr ""

#: classes/simple-gateway.class.php:999
msgid "Regisztrált kártyáim"
msgstr ""

#: classes/simple-gateway.class.php:1000
msgid "Az alábbi kártyával fizetek: "
msgstr ""

#: classes/simple-gateway.class.php:1015
msgid ""
"A megrendeléssel elfogadom az <a id =\"tajekoztato\" href =\"#"
"\">adattovábbítási nyilatkozatot.</a>"
msgstr ""

#: classes/simple-gateway.class.php:1017
msgid ""
"A megrendeléssel elfogadom az ismétlődő fizetésre vonatkozó <a id ="
"\"tajekoztato-recurring\" href =\"#\">Kártyaregisztrációs nyilatkozatot.</a>"
msgstr ""

#: classes/simple-gateway.class.php:1019
msgid ""
"A megrendeléssel elfogadom az egykattintásos (oneclick)  fizetésre vonatkozó "
"<a id =\"tajekoztato-oneclick\" href =\"#\">Kártyaregisztrációs "
"nyilatkozatot.</a>"
msgstr ""

#: classes/simple-gateway.class.php:1020
#: classes/simple-wire-gateway.class.php:299
msgid "Adattovábbítási nyilatkozat"
msgstr ""

#: classes/simple-gateway.class.php:1020 classes/simple-gateway.class.php:1023
#: classes/simple-gateway.class.php:1031
#: classes/simple-wire-gateway.class.php:299
msgid "Bezárás"
msgstr ""

#: classes/simple-gateway.class.php:1021
#: classes/simple-wire-gateway.class.php:300
#, php-format
msgid ""
"A megrendeléssel tudomásul veszem, hogy a %1$s (%2$s) adatkezelő által a "
"%3$s felhasználói adatbázisában tárolt alábbi személyes adataim átadásra "
"kerülnek az OTP Mobil Kft., mint adatfeldolgozó részére. Az adatkezelő által "
"továbbított adatok köre az alábbi: név, e-mail cím, telefonszám, számlázási "
"cím adatok, szállítási cím adatok. Az adatfeldolgozó által végzett "
"adatfeldolgozási tevékenység jellege és célja a SimplePay Adatkezelési "
"tájékoztatóban, az <a href=\"http://simplepay.hu/vasarlo-aff \" target=_blank"
"\">alábbi linken tekinthető meg</a>"
msgstr ""

#: classes/simple-gateway.class.php:1023 classes/simple-gateway.class.php:1031
msgid "Kártyaregisztrációs nyilatkozat"
msgstr ""

#: classes/simple-gateway.class.php:1024
#, php-format
msgid ""
"Az ismétlődő bankkártyás fizetés (továbbiakban: „Ismétlődő fizetés”) egy, a "
"SimplePay által biztosított bankkártya elfogadáshoz tartozó funkció, mely "
"azt jelenti, hogy a Vásárló által a regisztrációs tranzakció során megadott "
"bankkártyaadatokkal a jövőben újabb fizetéseket lehet kezdeményezni a "
"bankkártyaadatok újbóli megadása nélkül.\n"
"<p>Az Ismétlődő fizetés igénybevételéhez jelen nyilatkozat elfogadásával Ön "
"hozzájárul, hogy a sikeres regisztrációs tranzakciót követően jelen "
"webshopban (%s) kezdeményezett későbbi fizetések (max %s db, összeghatár %s "
"%s, lejárata %s) a bankkártyaadatok újbóli megadása és az Ön tranzakciónként "
"hozzájárulása nélkül a Kereskedő által kezdeményezve történjenek.\n"
"<p>Figyelem(!): a bankkártyaadatok kezelése a kártyatársasági szabályoknak "
"megfelelően történik. A bankkártyaadatokhoz sem a Kereskedő, sem a SimplePay "
"nem fér hozzá.\n"
"<p>A Kereskedő által tévesen vagy jogtalanul kezdeményezett ismétlődő "
"fizetéses tranzakciókért közvetlenül a Kereskedő felel, Kereskedő fizetési "
"szolgáltatójával (SimplePay) szemben bármilyen igényérvényesítés kizárt.\n"
"<p>Jelen tájékoztatót átolvastam, annak tartalmát tudomásul veszem és "
"elfogadom."
msgstr ""

#: classes/simple-gateway.class.php:1031
msgid "Oneclick kártya regisztrációs nyilatkozat "
msgstr ""

#: classes/simple-gateway.class.php:1032
#, php-format
msgid ""
"Az ismétlődő bankkártyás fizetés (továbbiakban: \"Ismétlődő fizetés\") egy, "
"a SimplePay által biztosított bankkártya elfogadáshoz tartozó funkció, mely "
"azt jelenti, hogy a Vásárló által a regisztrációs tranzakció során megadott "
"bankkártyaadatokkal a jövőben újabb fizetéseket lehet kezdeményezni a "
"bankkártyaadatok újbóli megadása nélkül. Az ismétlődő fizetés ún. „eseti "
"hozzájárulásos” típusa minden tranzakció esetében a Vevő eseti "
"jóváhagyásával történik, tehát, Ön valamennyi jövőbeni fizetésnél jóvá kell, "
"hogy hagyja a tranzakciót. A sikeres fizetés tényéről Ön minden esetben a "
"hagyományos bankkártyás fizetéssel megegyező csatornákon keresztül "
"értesítést kap. \n"
" <p>Az Ismétlődő fizetés igénybevételéhez jelen nyilatkozat elfogadásával Ön "
"hozzájárul, hogy a sikeres regisztrációs tranzakciót követően jelen "
"webshopban (%s) Ön az itt található felhasználói fiókjából kezdeményezett "
"későbbi fizetések a bankkártyaadatok újbóli megadása nélkül menjenek "
"végbe. \n"
" <p>Figyelem(!): a bankkártyaadatok kezelése a kártyatársasági szabályoknak "
"megfelelően történik. A bankkártyaadatokhoz sem a Kereskedő, sem a SimplePay "
"nem fér hozzá. A Kereskedő által tévesen vagy jogtalanul kezdeményezett "
"ismétlődő fizetéses tranzakciókért közvetlenül a Kereskedő felel, Kereskedő "
"fizetési szolgáltatójával (SimplePay) szemben bármilyen igényérvényesítés "
"kizárt. \n"
" <p>Jelen tájékoztatót átolvastam, annak tartalmát tudomásul veszem és "
"elfogadom."
msgstr ""

#: classes/simple-gateway.class.php:1112
#, php-format
msgid ""
"SimplePay fizetés. Kártyaszám: %s, lejárata: %s, hátralévő terhelések "
"maximális száma: %s db, SimplePay tranzakció azonosító: %s"
msgstr ""

#: classes/simple-wire-gateway.class.php:18
msgid "Kifizetem átutalással!"
msgstr ""

#: classes/simple-wire-gateway.class.php:144
msgid "Kereskedői azonosító (MERCHANT)"
msgstr ""

#: classes/simple-wire-gateway.class.php:146
msgid "SimplePay által megadott kereskedői azonosító"
msgstr ""

#: classes/simple-wire-gateway.class.php:150
msgid "Titkosító kulcs (SECRET_KEY)"
msgstr ""

#: classes/simple-wire-gateway.class.php:152
msgid "SimplePay által megadott titkosító kulcs"
msgstr ""

#: classes/simple-wire-gateway.class.php:180
msgid "Fiók devizaneme"
msgstr ""

#: classes/simple-wire-gateway.class.php:298
msgid ""
"A megrendeléssel elfogadom az <a id =\"tajekoztato\" href =\"javascript:"
"\">adattovábbítási nyilatkozatot.</a>"
msgstr ""

#: simplepay-for-woocommerce-premium.php:65
msgid "Bővítmény aktiválása"
msgstr ""

#: simplepay-for-woocommerce-premium.php:68
msgid "Beállítások"
msgstr ""

#: simplepay-for-woocommerce-premium.php:236
msgid ""
"A SimplePay for WooCommerce bővítmény még nincs aktiválva! A továbblépés "
"előtt tegyük ezt meg a beállításokban."
msgstr ""

#: simplepay-for-woocommerce-premium.php:252
msgid "Mentett kártyáim"
msgstr ""

#: simplepay-for-woocommerce-premium.php:267
msgid "Nincsenek regisztrált kártyák!"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:15
msgid "Kapcsolódó rendelések"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:22
msgid "Rendelés"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:23
#: templates/woocommerce/myaccount/related-orders.php:48
msgid "Dátum"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:24
#: templates/woocommerce/myaccount/related-orders.php:51
msgid "Státusz"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:25
msgctxt "table heading"
msgid "Összeg"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:26
msgctxt "table heading"
msgid "SimplePay azonosító"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:43
msgid "Rendelési azonosító"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:45
#, php-format
msgid "#%s"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:54
msgid "Összeg"
msgstr ""

#. translators: $1: formatted order total for the order, $2: number of items bought
#: templates/woocommerce/myaccount/related-orders.php:57
#, php-format
msgid "%1$s, %2$d tétel"
msgid_plural "%1$s, %2$d tétel"
msgstr[0] ""
msgstr[1] ""

#: templates/woocommerce/myaccount/related-orders.php:60
msgid "SimplePay azonosító"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:68
msgctxt "előfizetés fizetése"
msgid "Fizetés"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:81
msgctxt "egy előfizetéssel kapcsolatos művelet"
msgid "Lemond"
msgstr ""

#: templates/woocommerce/myaccount/related-orders.php:87
msgctxt "Előfizetés megtekintése"
msgid "Megtekint"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "SimplePay  for WooCommerce- premium"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://bitron.hu/termek/simplepay-for-woocommerce/"
msgstr ""

#. Description of the plugin/theme
msgid "WooCommerce fizetési megoldás a SimplePay rendszeréhez"
msgstr ""

#. Author of the plugin/theme
msgid "bitron.hu"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://bitron.hu"
msgstr ""
