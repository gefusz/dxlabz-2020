=== SimplePay for WooCommerce Premium ===
Contributors: oaron
Donate link: https://bitron.hu
Requires at least: 4.6
Tested up to: 5.4.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A SimplePay for WooCommerce bővítmény használatával bankkártyás fizetés biztosítható webshopunkban.

== Description ==

Bővebb infók találhatóak a megkapott telepítési útmutatóban.

== Changelog ==
= 2.0(2021-02-08) =
* SimplePay nyilatkozat eltüntethető a pénztár oldalról
* Megváltoztatható a megrendelés gomb szövege
* Több valuta egyidejű kezelése
* WPML javítások
* Legacy ismétlődő fizetés
* Egygombos fizetés támogatása
* Variable subscription hiba javítása
= 1.8.1(2020-12-30) =
* Felugró ablak css javítása
* ÁFA javítása subscription indításakor
= 1.8(2020-10-26) =
* Instant transzfer támogatása
* Felugró ablak javítása
* WpUpdater frissítése
= 1.7.1 (2020-09-26) =
* 3DS paraméter kiegészítés
= 1.7(2020-09-14) =
* Honlapcím helyett a WordPress cím használata a logó betöltésekor
* Ékezetes domainnevek lekezelése
* Egyéb díjjak támogatása
* Ismétlődő kuponok támogatása
* Terhelés újrapróbálásának javítása
= 1.6 (2020-07-11) =
* Aktiváció ellenőrzésének optimalizálása
* Külső filterek rendbetétele
* Üzenetek kikényszerítése
* Beállítások oldal átszabása
* Kétlépcsős tranzakció támogatása
* Próba verzió támogatása
* Hibakód kiírása sikertelen tranzakció indításkor
* Egyéb hibajavítások
= 1.4 (2020-06-01) =
* Bizonyos szervereken nem frissül a SimplePay ssl tanusítványa, ebben az esetben lehetőség van a beépítettet használni
= 1.3 (2020-03-15)=
* SimplePay tranzakciós azonosító megjelenik a rendeléseknél
* Aktivációs hiba javítása
= 1.2 (2020-02-02)=
* Tranzakciók logolása
* SimplePay átirányítás javítása
* Apróbb módosítások
* Fordítások frissítése
= 1.1 =
* 0 összegű tételek lekezelése
* Recurring terhelés javítása
* Kimaradt fordítások javítása
* kód tisztítás
= 1.0.1 =
* sikeres/sikertelen fizetések jelzése a rendeléseknél
* Angol fordítás frissítése
= 1.0 =
* Ismétlődő fizetés  támogatása