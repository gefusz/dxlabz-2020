<?php
/**
 * The main template file
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>

<section id="hero" class="front-page">
	<div class="hero-gradient"></div>
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-wide-orange.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 text">
				<h1>WEBOLDAL<br><span class="hidden">FEJLESZTÉS</span><span class="changing"></span></h1>
				<p class="idea">AZ ÖTLETTŐL A PROFITIG</p>
				<h2>Lenyűgöző és garantáltan eredményeket hozó weboldalakat készítünk.<div class="gradient-text-bg"></div></h2>
				<p>Gyorsítósávon juttatunk el az üzleti tervezéstől a profitábilis értékesítésig, elszámoltathatóan, hogy a vállalkozás álmaid beteljesülése és ne rémálom legyen.</p>
				<div class="call-to-action">
					<a href="#successfull" class="btn-gradient">ÍGY SZÁLLÍTJUK AZ EREDMÉNYEKET</a>
				</div>
			</div>
			<div class="col-lg-6 image">
				<div class="calculator">
                <div class="col8 white-side">
                    <div class="inputfields-box">
                        <ul>
                            <li>
                                <label for="range1">Hirdetések hatékonysága</label>
                                <div class="input-range"><span>Rossz</span><input type="range" name="range1" id="range1" min="0" max="1.5" value="0" step="0.15"><span>Kiváló</span></div>
                            </li>
                            <li>
                                <label for="range2">Betöltési sebesség</label>
                                <div class="input-range"><span>Rossz</span><input type="range" name="range2" id="range2" min="0" max="1.5" value="0" step="0.15"><span>Kiváló</span></div>
                            </li>
                            <li>
                                <label for="range5">Szöveges tartalom</label>
                                <div class="input-range"><span>Rossz</span><input type="range" name="range3" id="range3" min="0" max="1.5" value="0" step="0.15"><span>Kiváló</span></div>
                            </li>
                            <li>
                                <label for="range3">Vizuális megjelenés</label>
                                <div class="input-range"><span>Rossz</span><input type="range" name="range4" id="range4" min="0" max="1.5" value="0" step="0.15"><span>Kiváló</span></div>
                            </li>
                            <li>
                                <label for="range4">Használhatóság</label>
                                <div class="input-range"><span>Rossz</span><input type="range" name="range5" id="range5" min="0" max="1.5" value="0" step="0.15"><span>Kiváló</span></div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col8 blue-side">
                    <div class="inputfields-box">
                        <div class="group">
                        	<label for="visitors">Havi látogatószám</label>
                        	<div class="input-box"><input type="number" name="visitors" id="visitors" value="10000"></div>
						</div>

						<div class="group">
							<label for="productprice">Átlagos kosárérték</label>
							<div class="input-box"><input type="number" name="productprice" id="productprice" value="5000"></div>
						</div>

						<div class="result-box">
							<div class="group">
                            	<p class="title">Konverziós ráta</p>
                            	<p class="result"><span id="result"></span>%</p>
							</div>
							<div class="group">
                            	<p class="title">Havi árbevétel</p>
                            	<p class="result2"><span id="result2"></span>e Ft</p>
							</div>
                        </div>
                    </div>
                </div>
	        </div>
				<!-- <?php picture('profitkalkulator', 'png', '',  true, 'calculator'); ?> -->
<!-- 				<div class="founder">
					<div class="col-lg-4 col-4 align-center">
						<?php picture('dx-labz-kriston-gabor', 'png', '',  true, 'gabor'); ?>
					</div>
					<div class="col-lg-8 col-8 align-left">
						<h4 class="name">KRISTON GÁBOR</h4>
						<p class="qualification">Az ország egyetlen<br>Google Mobil Web Specialistája</p>
					</div>
				</div> -->
				<p style="color: white;margin-top: 20px;font-weight: bold;">NÉZD meg mi a különbség a jól és rosszul teljesítő webdoldalak között!</p>
			</div>
		</div>
	</div>
</section> <!-- end of header -->
<div class="section-separator">
	<?php picture('orange-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<section id="successfull" class="front-page">
	<div class="container relative-and-z-index">
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/target.svg" alt="Bullseye Arrow"  class="lazyload bullseye-arrow">
		<h2 class="section-title"><span class="text-gradient-bg">HOGYAN LEGYÉL SIKERES,</span><br> amikor farkastörvények uralkodnak?</h2>
		<hr class="gradient-separator-short">
		<p class="align-center bigger-paragraph less-line-height lesser-width">Hisszük, hogy csak teljes transzparenciával, közös nevezők és érdekek mentén dolgozva lehet a mai online világban kiemelkedő eredményeket elérni. Megtervezzük és kivitelezzük azt a hosszútávú akciótervet, mellyel elkerülheted a feltörekvő vállalkozások legnagyobb buktatóit és eredményeidet hónapról-hónapra mérhető módon növeljük.</p>
		<div class="row less-width">
			<div class="col-lg-12">
				<h3 class="align-left extra-margin_bottom less-line-height text-purple"><strong class="bold-only">MIBEN MÁS A DX LABZ FORMULA MINT EGY ÁTLAG KIVITELEZŐ MEGOLDÁSA:</strong></h3>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-frontpage-1', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/processor.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<h3 class="align-left less-margin-bottom less-margin-top text-purple"><strong class="bold-only">Csúcsra pörgetett betöltési sebesség</strong></h3>
					<p class="align-left">Sebesség optimalizált weboldal motorunkkal <strong>nem veszítesz látogatókat és a Google is szeretni fog</strong></p>
				</div>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-frontpage-2', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/workflow.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<h3 class="align-left less-margin-bottom less-margin-top text-purple"><strong class="bold-only">Tévutakat nélkülöző munkamenet</strong></h3>
					<p class="align-left">Tökéletesre csiszolt fejlesztési folyamatunkkal <strong>megspórolod a felesleges köröket és a lehető legkevesebb idő alatt és költséggel juthatsz el a profitig</strong></p>
				</div>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-frontpage-3', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/knowledge.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<h3 class="align-left less-margin-bottom less-margin-top text-purple"><strong class="bold-only">Valós visszajelzésekre épített fejlesztési terv</strong></h3>
					<p class="align-left">Tesztalapú módszertanunkkal <strong>már a kezdetektől a vásárlóid igényeire alapozva építed fel üzletedet</strong>, hogy ne csak menet közben derüljön ki, mire is vágynak igazán.</p>
				</div>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-frontpage-4', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/insights.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<h3 class="align-left less-margin-bottom less-margin-top text-purple"><strong class="bold-only">Teljeskörű analitikai megoldások</strong></h3>
					<p class="align-left">Egyedülálló analitikai rendszerünkkel <strong>olvasni fogod a vevőid gondolatait, és mindig tudni fogod, hogyan tégy a kedvükre</strong></p>
				</div>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-frontpage-5', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/award.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<h3 class="align-left less-margin-bottom less-margin-top text-purple"><strong class="bold-only">Eredményfókuszú projektvezetés</strong></h3>
					<p class="align-left">Lényeglátó projektmenedzsment megoldásunkkal <strong>mindig tudni fogod, hogy melyik lesz a jövő legeredményesebb fejlesztési lépése</strong></p>
				</div>
			</div>
		</div>
		<div class="call-to-action u-text-center">
			<a href="#work-together" class="btn-gradient">DOLGOZZUNK EGYÜTT</a>
		</div>
	</div>
</section>
<div class="section-separator_mirror">
	<div class="skew-separator"></div>
</div>
<section id="enthusiastic" class="front-page">
	<div class="container relative-and-z-index">
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/quote-mark.svg" alt="Quote Mark"  class="lazyload quote-mark">
		<div class="row less-width">
			<div class="col-lg-6 testimonial-box">
				<p class="italic">Gábor mindent tud a témájában, évekkel előbbre jár a konkurenciánál.</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-meszaros-robi', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Mészáros Robi</h4>
						<p class="qualification">BrandBirds</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">Szakmailag egyedülállóan felkészült!</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-doman-zsolt', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Domán Zsolt</h4>
						<p class="qualification">DO! Marketing</p>
					</div>
				</div>
			</div>
		</div>
		<div class="recommendation align-center lesser-width">
			<h3>Tekintsd meg <span class="text-gradient-bg">DOMÁN ZSOLT</span> videós ajánlóját</h3>
			<video controls muted poster="<?php echo get_stylesheet_directory_uri();?>/assets/images/doman-zsolt-video-preview.<?php echo (WEBP ? 'webp' : 'jpg'); ?>">
			  <source src="<?php echo get_stylesheet_directory_uri();?>/assets/videos/doman-zsolt-ajanlo.mp4" type="video/mp4">
			Your browser does not support the video tag.
			</video>
		</div>
	</div>
</section>
</section>
	<div class="section-separator_right_top section_point-of-view">
		<?php picture('purple-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		<div class="skew-separator"></div>
	</div>
<section id="less-and-more" class="align-center">
	<div class="gradient-orange lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/less-and-more-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title section-title_smaller dark-bg">KEVESEBB KERÜLŐÚT + TÖBB ÜGYFÉL </h2>
		<h2 class="section-title section-title_smaller text-purple-bg">KEVESEBB STRESSZ ÉS AGGODALOM + TÖBB BEVÉTEL</h2>
	</div>
</section>
	<div class="section-separator_right_bottom section_point-of-view">
		<div class="skew-separator"></div>
	</div>
<section id="point-of-view" class="front-page">
	<div class="container">
		<h2 class="section-title less-line-height">Eljött a <span class="text-gradient-bg">SZINTLÉPÉS</span> ideje!</h2>
		<hr class="gradient-separator-short">
		<div class="lesser-width">
			<p class="bigger-paragraph align-left">A sikerhez a régi sztenderdek már nem elegendőek az új világban. Megmutatjuk, hogy mi az a szint, amit ma meg kell ugranod, hogy sikeres lehess.</p>
			<hr class="separator-left">
			<p class="less-line-height">Vállalkozóként olyan vizekre evezünk, ami sok veszélyt rejteget és legtöbbünknek egyedül kellett megtanulnunk, hogy hogyan kerüljük el ezeket. <strong>Statisztikák szerint csak minden 3. újonnan induló vállalkozás éri meg az 5. üzleti évét. </strong>Ez legtöbbször az előre nem belátható nehézségek és a hiányos tervezés eredménye. Kritikus a tudatos tervezés és az eredmények mérése, mert ennek hiányában akár úgy is csődbe mehet vállalkozásunk, hogy soha nem jövünk rá az igazi okára. </p>
			<p class="less-line-height">Ezzel szemben ha követed a számok alakulását és szisztematikusan igyekszel javítani az ügyfélélményen, hónapról-hónapra jobb számokat tudsz majd hozni és mindig láthatod, hogy hol tudod a legnagyobb javulást elérni a közeljövőben. Egy jól tervezett és egy rosszul konvertáló üzlet bevételei között akár többtízszeres is lehet a különbség.</p>
			<p class="less-line-height">Nyilvánvaló, hogy üzleted online felülete kritikus a vállalkozás sikeressége szempontjából, azonban a mikéntekkel kapcsolatos információáradatból nehéz kiszűrni a lényeget, ennek a terhét szeretnénk egyszer és mindenkorra levenni a válladról. Nem egy újabb csodapirulát kínálunk, hanem egy olyan általad az üzlet egyéb területein is használható módszertant, amit következetesen betartva, megsokszorozod hosszútávú sikereid esélyeit.</p>
			<p class="less-line-height">Bármennyire is vonzóak azok az ígéretek, amelyek rövid idő alatt nagy sikereket ígérnek, valódi eredmények csak folyamatos munka által, tisztán kijelölt célok mentén dolgozva érhetőek el. Ebben szeretnénk mi a partnereid lenni. Minden vállalkozás valaki álmainak formát öltő megtestesülése, mi azért dolgozunk, hogy ezekből az álmokból sikertörtének és ne rémálmok váljanak.</p>
			<p class="less-line-height"><strong>Célunk, hogy növekedési módszertanunk segítségével pontosan megtervezett fejlődési útmutatást adjunk az új vagy megrekedt online vállalkozásoknak. A buktatók elkerülésével így a lehető leggyorsabban válhatnak nemcsak versenyképessé, de piacuk meghatározó képviselőivé.</strong></p>
			<p class="less-line-height">A már több éves múltú vállalkozásokat pedig szeretnénk egy magasabb sebességi fokozatba kapcsolni, hogy biztosítsák pozíciójukat a kihívókkal és a konkurenciával szemben, a folyamatos innováción és folyamatoptimalizáláson keresztül.</p>
			<p class="less-line-height"><strong>Mi ahelyett, hogy csupán végrehajtanánk a ránk bízott feladatot, már a kezdetektől a “tulajdonosi szemlélet” szerint járunk el. </strong>A Ti sikereteket a miénkkel egyenértékűvé téve, megnézzük még a projekt indulása előtt, hogy van-e elegendő kereslet a vállalkozás által kínált eszközre/szolgáltatásra. Kijelöljük azokat a forgalmi és konverziós célokat, amelyek elérése esetén vállalkozásod valóban több lehet egy álomnál és stabil pénzszerző gépezetként tud működni.</p>
			<p class="less-line-height">5 éve működő vállalkozásként mi is megjártuk a fejlődés lépcsőit és ma már sokkal tudatosabban járnánk végig ezt az utat. Ez idő alatt sok céggel dolgoztunk együtt és azt láttuk,  hogy nagyszerű emberek, nagyszerű motivációkkal, de az online üzlet terén meglehetősen hiányos ismeretekkel vágnak neki a webes ügyfélszerzésnek. Mindig is azt éreztük, hogy fel kell vállalnunk a feladatot, hogy ügyfeleinket a lehetőségekről tájékoztatva, maximalizáljuk sikerük esélyét, mert<strong>számunkra nem a projektzárás a siker, hanem ha az ügyfél valóban egy hosszútávon jól működő vállalkozást tud építeni a mi közbenjárásunkkal.</strong></p>
		</div>
		<div class="call-to-action u-text-center">
			<a href="#work-together" class="btn-gradient">ÉPÍTSÜK EGYÜTT A JÖVŐDET</a>
		</div>
	</div>
</section>
<div class="section-separator section_progress">
	<?php picture('orange-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<section id="progress">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/choose-your-path-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-white.svg" alt="Pattern Top White"  class="lazyload pattern-top_white">
	<div class="container relative-and-z-index">
			<h2 class="section-title dark-bg"><span class="text-purple text-gradient-bg">A FOLYAMAT,</span> ami garantálja a sikeredet</h2>
			<hr class="gradient-separator-short">
		<div class="lesser-width dark-bg">
			<ol>
				<li class="dark-bg"><strong>Üzleti alapok meglétének felmérése</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>Nem fognak kellemetlen meglepetések érni</span></li>
				<li><strong>Ügyfélprofil és ideális ügyfél életút meghatározása</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>Olvasni fogod a célpiacod gondolatait</span></li>
				<li><strong>A startupoktól átemelt ötlet életképesség kiértékelési technika</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>Még azelőtt meggyőződhetsz, hogy működik-e az ötleted, hogy vagyonokat öltél volna bele</span></li>
				<li><strong>Tartalomkészítés</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>Úgy leszel képes átadni az ügyfeleknek a gondolataidat, hogy imádni fogják</span></li>
				<li><strong>A szükséges legminimálisabb, de már ügyfelek számára értéket adó első körös fejlesztési specifikáció megalkotása</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>Nem fogsz senki által használt megoldásokért pénzt fizetni</span></li>
				<li><strong>Drótváz/design/prototípus készítés</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>Még időben képes leszel módosításokat eszközölni, költséghatékonyan</span></li>
				<li><strong>Stabil technikai alapok</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>Nem az oldalad megépítése után fog csak kiderülni, hogy több sebből vérzik a rendszered</span></li>
				<li><strong>Gyors felhasználói visszajelzés gyűjtés és ezen alapuló fejlesztés</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>Napról-napra egyre jobban fognak imádni az ügyfeleid, amiért leginkább a pénztárcád lesz hálás</span></li>
				<li><strong>Minőségkontroll</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>Nem hagyjuk, hogy weboldalad megállíthatatlan hízókúrába kezdjen és hibáktól hemzsegjen</span></li>
			</ol>
		</div>
		<div class="call-to-action u-text-center">
			<a href="#work-together" class="btn-gradient">INDULHATUNK?</a>
		</div>
	</div>
</section>
	<div class="section-separator_right_mirror_bottom section_progress">
		<?php picture('orange-gradient-effect-right_mirror', 'png', '',  true, 'gradient-effect-right_mirror'); ?>
		<div class="skew-separator"></div>
	</div>
<section id="experience">
	<div class="container relative-and-z-index">
		<h2 class="section-title hide-on-mobile"><span class="text-gradient-bg">NAGYVÁLLALATI TAPASZTALAT,</span><br> KKV-kra szabott megoldások</h2>
		<h2 class="section-title hide-on-desktop"><span class="text-gradient-bg">NAGYVÁLLALATI</span><span class="text-gradient-bg"> TAPASZTALAT,<br></span> KKV-kra szabott megoldások</h2>
		<hr class="gradient-separator-short">
		<div class="row">
			<div class="col-lg-8 text">
				<p class="align-left less-line-height">Mi motivál minket? Az évek folyamán tapasztalatra tettünk szert a világ legnagyobb digitális ügynökségénél (WPP/Possible) és a régió legnagyobb szoftverfejlesztő cégénél (EPAM) szoftvermérnökként, több millió dolláros projekteken, az egyik vezető FMCG márka online marketing menedzsereként, valamint egy több mint 160 országban jelenlévő startup fejlesztési vezetőjeként is (Innonic/Optimonk). <strong>Kötelességünknek éreztük, hogy a nagyvállalati szintű megoldásokat elérhetővé tegyük a KKV-k számára is</strong>, ez hívta életre a DX Labz kft. megalapításának ötletét.</p>
			</div>
			<div class="col-lg-4 align-center founder">
				<?php picture('dx-labz-kriston-gabor', 'png', '',  false, 'gabor'); ?>
				<h4 class="name">Kriston Gábor</h4>
				<p class="qualification">Google Web Specialista,</br>a DX Labz alapítója</p>
			</div>
		</div>
		<?php picture('webdeveloper-experiences', 'png', '',  true, 'webdeveloper-experiences extra-margin_top extra-margin_bottom'); ?>
		<p class="bigger-paragraph align-center">Az ország egyetlen Google Mobil Web Specialistája<br>& Minősített Growth Master</p>
		<div class="row">
			<div class="col-lg-6 certifications">
				<?php picture('google-mobil-web-specialist', 'png', '',  true, 'certificate'); ?>
			</div>
			<div class="col-lg-6 certifications">
				<?php picture('growth-master', 'png', '',  true, 'certificate'); ?>
			</div>
		</div>
	</div>
</section>
<div class="section-separator section_work-together">
	<?php picture('purple-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<section id="work-together">
	<div class="gradient-orange lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/time-spent-on-writing-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-white.svg" alt="Pattern Top White"  class="lazyload pattern-top_white">
	<div class="container relative-and-z-index">
		<h2 class="section-title dark-bg less-line-height hide-on-desktop">Hogyan tudunk<br><span class="text-purple-bg_gradient">EGYÜTT DOLGOZNI?</span></h2>
		<h2 class="section-title dark-bg less-line-height hide-on-mobile">Hogyan tudunk <span class="text-purple-bg_gradient">EGYÜTT DOLGOZNI?</span></h2>
		<hr class="separator-short_purple">
		<div class="lesser-width">
			<p class="dark-bg less-line-height align-center bigger-paragraph extra-margin_top"><strong class="bold-only">Hiszünk benne, hogy akkor tudunk neked a legtöbbet segíteni, ha megvan a közös nevező. Írtunk egy könyvet arról, hogy mi kell <?php echo date("Y"); ?>-ben egy profitábilis weboldalhoz és hogyan tudjuk ezt megvalósítani együtt.</strong></p>
			<!-- <p class="dark-bg less-line-height align-center"><strong class="bold-only">Részedről ez 5990 Ft-os befektetést és pár óra időráfordítást igényel.</strong></p> -->
		</div>
		<div class="row">
			<div class="col-lg-6">
				<?php picture('online-gyorsitosav-book-standing', 'png', '',  true, 'book'); ?>
				<p class="dark-bg less-line-height align-center">Ha szeretnéd megismerni munkamódszerünket, ajánljuk figyelmedbe könyvünket.</p>
				<a href="<?php echo site_url();?>/online-gyorsitosav" class="btn-gradient_purple">A KÖNYVRŐL BŐVEBBEN</a>
			</div>
			<div class="col-lg-6">
				<?php picture('dx-labz_webdesign', 'png', '',  true, 'dx-labz_webdesign'); ?>
				<p class="dark-bg less-line-height align-center">Szeretnél többet megtudni weboldal kivitelezési szolgáltatásunkról és konzultánál velünk?</p>
				<a target="_blank" href="<?php echo site_url();?>/weboldal-keszites" class="btn-gradient_purple">ÉRDEKEL A KÖZÖS MUNKA</a>
				<!-- <div class="col-lg-4 col-5 align-center">
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/guarantee.svg" alt="Checkmark Star"  class="lazyload checkmark-star-circle">
				</div>
				<div class="col-lg-8 col-7">
					<h4 class="dark-bg"><strong>30 napos</strong></h4>
					<p class="uppercase dark-bg">ELÉGEDETTSÉGI GARANCIA!</p>
				</div>
				<div class="col-lg-12 col-12 text">
					<p class="dark-bg less-line-height">Olyannyira biztosak vagyunk a könyv árának tízszeres megtérülésében, hogy 30 napos pénzvisszafizetési garanciát vállalunk rá.</p>
				</div>

				<div class="col-lg-12 col-12 call-to-action align-left">
					<a href="<?php echo site_url();?>/online-gyorsitosav" class="btn-gradient_purple">MÉG TÖBB INFÓ A KÖNYVRŐL</a>
				</div> -->
			</div>
		</div>
		<!-- <div class="lesser-width">
			<p class="dark-bg less-line-height align-center bigger-paragraph extra-margin_top"><strong class="bold-only">TETSZIK A SZEMLÉLETÜNK? DOLGOZZUNK EGYÜTT!</strong></p>
			<p class="dark-bg less-line-height align-center">Ha a könyv elolvasását követően úgy látod, hogy szimpatikus a fejlesztési modellünk, <strong><a href="<?php echo site_url(); ?>/#contact">nyugodtan keress meg minket.</a></strong></p>
		</div> -->
	</div>
</section>
<div class="section-separator_right_bottom section_mission">
	<?php picture('purple-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
	<div class="skew-separator"></div>
</div>
<section id="testimonials" class="front-page">
	<div class="gradient-purple"></div>
	<div class="container">
		<h2 class="section-title less-line-height"><span class="text-gradient-bg">VÉLEMÉNYEK</span> a DX Labz-ról</h2>
		<hr class="gradient-separator-short">
		<p class="align-center extra-margin_bottom less-width bigger-paragraph">Megbecsült ügyfeleink listáját öregbítik többek között az Optimonk, a BrandBirds, Domán Zsolt és az Édesburgonya.bio is, akik mind-mind piacvezetők a saját területükön. Ők elégedettek voltak a munkánkkal, nálad sem elégszünk meg kevesebbel, mint hogy projekted többszörösen megtérülő befektetés legyen.</p>
		<div class="row">
			<div class="col-lg-6 testimonial-box">
				<p class="italic">"Gábor személyes konzultációra jelentkezett hozzám, így kerültem kapcsolatba vele. A tanácsadás során egy <strong class="bold-only">szakmailag egyedülállóan felkészült</strong>, stabil értékrenddel rendelkező korrekt személyt ismerhettem meg benne.</p>
				<p class="italic">Csapatával elsősorban egyedi weboldalak készítésével és gyorsabbá tételével foglalkoznak. Nekem is segített Gábor a domarketing.hu sebességoptimalizálásában.</p>
				<p class="italic">Sőt! Amikor a Klub nyitókampányában behalt az eredeti tárhely, Gábor hatalmas erőfeszítések árán, 24 óra alatt átköltöztette egy sokkal gyorsabb, jobb tárhelyre a weboldalt.<strong class="bold-only"> Nagyon elégedett voltam a közös munkával, jó szívvel ajánlom!"</strong></p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-doman-zsolt', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Domán Zsolt</h4>
						<p class="qualification">DO! Marketing</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“2 évvel ezelőtt volt először tanácsadáson nálunk Gábor, és már akkor szimpatikus volt a szemlélete, hozzáállása. Május elején lecsapott a "mini tanácsadás" szolgáltatásomra, és immár a saját márkáját dobta be, hogy nézzem át. Nagyon megtetszett amit ő művel, a brutális szakértelem és emberi megfogalmazás. Kértem tőle egy konverzió optimalizálási elemzést a honlapunkra, ami überhasznos volt. Gábor <strong class="bold-only">mindent tud a témájában, évekkel előbbre jár a konkurenciánál.</strong></p>
				<p class="italic">Köszönöm Gábor ezt a vérprofi szakmai munkát és hogy megosztottad a tudásod (alapjait, ami nekünk hónapokra elég...) velünk.”</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-meszaros-robi', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Mészáros Robi</h4>
						<p class="qualification">BrandBirds</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“A DX Labz Kft.-t egy jó ismerősöm ajánlotta, mert nagyon esedékessé vált a Tudástár Klub oldalon tapasztalható lassulási gondok és egyéb felmerült problémák megoldása. <strong class="bold-only"> Nagyon elégedett vagyok a munkátokkal, mindig összeszedett, érthető, segítőkész válaszokat kaptam, és a felmerült hibákat is szakszerűen, gyorsan oldottátok meg.</strong> A jövőben is számítok az együttműködésünkre és szívesen fordulok hozzátok WordPress támogatásért.”</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-szendrei-adam', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Szendrei Ádám</h4>
						<p class="qualification">Tudástár Klub</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“Azonnal felkeltették a figyelmemet, nem is haboztam egyből ajánlatot kértem. Azóta is együtt dolgozunk, profi kivitelezést, dinamikus és könnyen érthető egyenes kommunikációt kaptam a csapattól, most készítik a második weblapomat, de lesz harmadik is. <strong class="bold-only"> Bonyolult technikai problémák esetén is tudják mi a teendő.</strong></p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-kovacs-andras', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Kovács András</h4>
						<p class="qualification">Édesburgonya.bio</p>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
	<div class="section-separator_mirror_bottom section_mission">
		<div class="skew-separator"></div>
	</div>
<section id="dxengine">
	<div class="container">
		<h2 class="section-title"><span class="text-gradient-bg">DX ENGINE,</span> a villámgyors <br>weboldalak motorja</h2>
		<hr class="gradient-separator-short">
		<div class="lesser-width">
			<div class="row">
				<div class="col-lg-6 align-center">
					<!-- <?php picture('dxengine-logo', 'png', '',  true, 'dxengine-logo'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/dx-engine.svg" class="lazyload dxengine-logo">
				</div>
				<div class="col-lg-6">
					<ul>
						<li><strong class="bold-only">90+ PageSpeed pontszám</strong></li>
						<li><strong class="bold-only">Akár 4x gyorsabb oldalbetöltődés</strong></li>
						<li><strong class="bold-only">75%-kal kisebb oldalméret</strong></li>
						<li><strong class="bold-only">Akár 50%-kal több ügyfél</strong></li>
						<li><strong class="bold-only">Nincs SEO büntetés</strong></li>
					</ul>
				</div>
			</div>
			<p>A most debütált DX Engine új alapokra helyezi a weboldalakat. A legújabb ajánlásokat követő architektúra segítségével elfeledkezhetünk a lassú betöltődésről, a kereső optimalizáltsági büntetésről és fókuszálhatunk arra, ami igazán fontos, a minél jobb látogatói élmény megteremtésére.</p>
			<p>Minden egyes megspórolt másodperc a betöltődési időnél újabb megmentett látogatókat és a gördülékenyebb felhasználói élmény révén magasabb konverziót jelent.</p>
			<p>A DX Engine nemcsak villámgyorsan tölti be a weboldalakat, de mindig az éppen aktuálisan használt készülékhez igazodó képfájlokat használja, támogatja a Google új webP képformátumát, illetve csak a felhasználó számára látható tartalmakat tölti be, így csökkentve a hálózati adatforgalmat.</p>
		</div>
		<div class="call-to-action u-text-center">
			<a href="#contact" class="btn-gradient">EZ KELL NEKEM!</a>
		</div>
	</div>
</section>
<div class="section-separator section_time-spent-on-writing">
	<?php picture('orange-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<section id="speed">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/key-of-success-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title dark-bg"><span class="text-purple text-gradient-bg">NÉGYSZERES SEBESSÉG,</span><br>dupla konverzió</h2>
		<hr class="gradient-separator-short">

			<div class="row">
				 <div class="col-lg-6 align-center">
				 	<?php picture('edesburgonya-windows', 'png', '',  true, 'ref-image'); ?>
				 </div>
	 			 <div class="col-lg-6">
					<p class="bigger-paragraph align-left dark-bg"><strong class="bold-only">Sebesség optimalizálási esettanulmány</strong></p>
					<hr class="separator-left">
					<p class="less-line-height dark-bg">A sablonokból összerakott oldalak bár olcsóak és mutatósak lehetnek, de pont az univerzalitásuk a rákfenéjük. Mivel minden komponens alapból elérhető bennük ezért feleslegesen nagy a kód és a sebesség optimalizálási szempontokat mellőző „összekattintgatott” felület végeredménye egy még lassabb és így még kevesebb pénzt termelő oldal lesz. </p>
					<p class="dark-bg">Az Édesburgonya.bio is hasonló módon készült eredetileg, az oldalbetöltődés akár 15 másodpercet is igénybe vett. <strong><a href="<?php echo site_url(); ?>/webfejlesztes-esettanulmany/">Találtunk megoldást a problémára.</a></strong></p>
				 </div>
			</div>
		<div class="less-width">
			<h3 class="dark-bg align-center extra-margin_top extra-margin_bottom">Főbb eredmények</h3>
			<div class="row align-center">
				<div class="col-lg-4 results">
					<h4 class="number">25<span>%</span></h4>
					<p class="text dark-bg">Adatforgalom <br>csökkenés</p>
				</div>
				<div class="col-lg-4 results">
					<h4 class="number">4<span>X</span></h4>
					<p class="text dark-bg">Gyorsabb kezdőképernyő betöltődés</p>
				</div>
				<div class="col-lg-4 results">
					<h4 class="number">3<span>X</span></h4>
					<p class="text dark-bg">Gyorsabb teljes <br>oldalbetöltődés</p>
				</div>
			</div>
			<div class="call-to-action align-center">
				<a href="<?php echo site_url(); ?>/webfejlesztes-esettanulmany/" class="btn-gradient">TELJES ESETTANULMÁNY</a>
			</div>
<!-- 			<p class="dark-bg align-center extra-margin_top">Kíváncsi vagy rá, hogy a te oldaladdal is meg tudnánk-e csinálni?</p>
			<p class="text-orange align-center"><a href="<?php echo site_url();?>/#contact"><strong class="text-orange bold-only">Kérd ki ingyenesen a véleményünket!</strong></a></p> -->
		</div>
	</div>
</section>
	<div class="section-separator_right_mirror_bottom section_speed">
		<?php picture('orange-gradient-effect-right_mirror', 'png', '',  true, 'gradient-effect-right_mirror'); ?>
		<div class="skew-separator"></div>
	</div>
<section id="reference">
	<div class="container">
		<h2 class="section-title"><span class="text-gradient-bg">REFERENCIÁINK</span> a közelmúltból</h2>
		<hr class="gradient-separator-short">
<!--			<p class="bigger-paragraph align-center extra-margin_top">Nemcsak mondjuk, csináljuk is</p>-->
	</div>
	<div class="ref-box relative-and-z-index">
		<div class="wide-container">
			<div class="row unique unique_makad">
				<div class="col-lg-6 image align-left">
					<?php picture('makad-img', 'png', '',  true, 'ref-image makad-img'); ?>
				</div>
				<div class="col-lg-6 logo-and-text">
					<?php picture('makad-hills-logo', 'png', '',  true, 'ref-logo makad-hills-logo'); ?>
					<p>A Makad Hills asztalitenisz és fitnessterem számára egy, a kiemelkedő szakmai felkészültségükre és prémium színvonalú szolgáltatásaik széles választékára is kellő hangsúlyt fektető, modern és sportos arculatot álmodtunk meg.</p>
					<div class="call-to-action">
						<a href="https://makadhillsaklub.com/" target="_blank" class="btn-gradient btn-makad">MEGNÉZEM</a>
					</div>
				</div>
			</div>
		</div>
		<div class="unique-skewbox_makad">
			<?php picture('unique-blue-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		</div>
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-left_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-left">
	</div>
	<div class="ref-box relative-and-z-index">
		<div class="wide-container">
			<div class="row unique">
				<div class="col-lg-6 logo-and-text">
					<?php picture('palma-es-tenger-logo', 'png', '',  true, 'ref-logo palma-logo'); ?>
					<p>A Pálma & Tenger oldalánál a fő cél az volt, hogy amennyire csak lehetséges, adjuk vissza a trópusok hangulatát. Nézd meg magad, mennyire sikerült ez a küldetés és utazz egzotikus tájakra!</p>
					<div class="call-to-action">
						<a href="https://palmaestenger.hu" target="_blank" class="btn-gradient">MEGNÉZEM</a>
					</div>
				</div>
				<div class="col-lg-6 image align-right">
					<?php picture('palma-es-tenger-img', 'png', '',  true, 'ref-image palma-img'); ?>
				</div>
			</div>
		</div>
		<div class="unique-skewbox_orange">
			<?php picture('unique-purple-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
		</div>
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-right_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-right">
	</div>
	<div class="ref-box relative-and-z-index">
		<div class="wide-container">
			<div class="row unique unique_fixbeton">
				<div class="col-lg-6 image align-left">
					<?php picture('fixbeton-img', 'png', '',  true, 'ref-image fixbeton-img'); ?>
				</div>
				<div class="col-lg-6 logo-and-text">
					<?php picture('fixbeton-logo', 'png', '',  true, 'ref-logo fixbeton-logo'); ?>
					<p>A fixbetonkerites.hu weboldalának megtervezésekor olyan fő szempontokat tartottunk szem előtt, mint az általuk gyártott kerítéselemek minőségének és eleganciájának tükrözése a dizájnban és egy könnyen átlátható, ugyanakkor rendkívül átfogó információt nyújtó felület kialakítása az oda látogatók számára.</p>
					<div class="call-to-action">
						<a href="https://fixbetonkerites.hu/" target="_blank" class="btn-gradient btn-fixbeton">MEGNÉZEM</a>
					</div>
				</div>

			</div>
		</div>
		<div class="unique-skewbox_fixbeton">
			<?php picture('unique-light-green-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		</div>
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-left_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-left">
	</div>
	<div class="ref-box relative-and-z-index">
		<div class="wide-container">
			<div class="row unique">
				<div class="col-lg-6 logo-and-text">
					<?php picture('it-logo', 'png', '',  true, 'ref-logo it-logo'); ?>
					<p>Az it.hu arculati és tartalmi tervezése során azt a célt tűztük ki, hogy a 25 éves múltra visszatekintő cég megbízhatósága és prémium színvonalú szolgáltatásai kihangsúlyozásra kerüljenek.</p>
					<div class="call-to-action">
						<a href="https://it.hu" target="_blank" class="btn-gradient btn-it">MEGNÉZEM</a>
					</div>
				</div>
				<div class="col-lg-6 image align-right">
					<?php picture('it-img', 'png', '',  true, 'ref-image'); ?>
				</div>
			</div>
		</div>
		<div class="unique-skewbox_purple">
			<?php picture('unique-orange-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
		</div>
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-right_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-right">
	</div>
	<div class="ref-box relative-and-z-index">
		<div class="wide-container">
			<div class="row unique unique_qth">
				<div class="col-lg-6 image align-left">
					<?php picture('qth-img', 'png', '',  true, 'ref-image qth-img'); ?>
				</div>
				<div class="col-lg-6 logo-and-text">
					<?php picture('qth-logo', 'png', '',  true, 'ref-logo qth-logo'); ?>
					<p>A Quality Tours Hungary az ország egyik legmeghatározóbb csoportos beutazási ügynöksége. Fontos volt, hogy a cég megjelenése méltó köntöst kapjon és egyben hazánkat vonzó módon mutassuk be.
					<div class="call-to-action">
						<a href="https://qualitytours.hu" target="_blank" class="btn-gradient btn-qth">MEGNÉZEM</a>
					</div>
				</div>

			</div>
		</div>
		<div class="unique-skewbox_qth">
			<?php picture('unique-green-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		</div>
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-left_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-left">
	</div>
</section>
<!--<div class="straight-white"></div>-->
<!--<section id="many-question" class="front-page">-->
<!--	<div class="container">-->
<!--		<h2 class="section-title hide-on-desktop">Tudom, hogy <span class="text-gradient-bg">SOK KÉRDÉS</span> van a fejedben</h2>-->
<!--		<h2 class="section-title hide-on-mobile">Tudom, hogy <span class="text-gradient-bg">SOK KÉRDÉS</span><br> van a fejedben</h2>-->
<!--		<hr class="gradient-separator-short">-->
<!--		<div class="row less-width">-->
<!--			<div class="col-lg-12 col-12 set-of-answers">-->
<!--				<div class="col-lg-1 col-3 question-img">-->
<!--					--><?php //picture('plus-mark', 'png', '',  true, 'plus-mark'); ?>
<!--				</div>-->
<!--				<div class="col-lg-11 answers">-->
<!--					<h5 class="less-line-height question">Mit jelent az hogy Google Mobil Web Specialista vagyok?</h5>-->
<!--					<div class="answers-text-box hide">-->
<!--						<p class="less-line-height">Amellett, hogy sokmindenbe belekóstoltam, folyamatosan dolgoztam a  szakmai tudásom elmélyítésén. Többek között a közelmúltban én lettem az ország 1. Google Minősített Mobil Web Specialistája. Miért tartottam ezt a minősítést olyan fontosnak megszerezni?</p>-->
<!--						<p class="less-line-height">Ma már a mobilos weboldal megtekintések túlsúlyban vannak, azonban a mobilra optimalizálás legtöbbször még mindig csak másodgondolat. Ha most nem elsődlegesen mobil fókusszal tervezed a weboldalad, azzal többéves lemaradásra ítéled magad, hiszen a weboldaladat nem fogod túl gyakran cserélgetni.</p>-->
<!--						<p class="less-line-height">Szeretnélek felkészíteni téged a jövőre, és betekintést adni abba, mi kell ahhoz, hogy mobilon legalább olyan jól jelenjen meg az oldalad mint asztali gépeken. Feketeöves technikák garmadáját állítjuk majd hadrendbe, hogy még jobb eredményeket tudj elérni a mobil eszközökön. </p>-->
<!--						<p class="less-line-height">A minősítésnek hála, az a tudás, amit ma mobilbarát weboldal-opttimalizációval kapcsolatban érdemes tudnod, bekerült a könyv anyagába. Sőt, már az elkövetkező 3 évben elterjedő technológiák is figyelembe lettek véve a könyv írása során. Te ezekről már most tudni fogsz, mielőtt a konkurensek egyáltalán tudomást szereznének róluk, így óriási lépéselőnybe kerülhetsz velük szemben.</p>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="col-lg-12 col-12 set-of-answers">-->
<!--				<div class="col-lg-1 col-3 question-img">-->
<!--					--><?php //picture('plus-mark', 'png', '',  true, 'plus-mark'); ?>
<!--				</div>-->
<!--				<div class="col-lg-11 answers">-->
<!--					<h5 class="less-line-height question">Mit csinál egy Growth Master?</h5>-->
<!--					<div class="answers-text-box hide">-->
<!--						<p class="less-line-height">Egy másik képzési kitérőm a Growth Hackers University-n megszerzett Growth Master titulus volt. Mitől is lesz valaki Growth Master, a növekedés mestere? </p>-->
<!--						<p class="less-line-height">Korábbi munkatapasztalaim révén már betekinthettem a startupoknál 2 alkalmazott gyors növekedést lehetővé tévő technikákba. Olyan módszerekről van szó, amelyekkel rövid idő alatt képes vagy skálázni a vállalkozásodat és képes lehetsz eredményesen betörni célpiacodra. Ezekkel lehetséges biztosítani, hogy a legkisebb energia, idő- és pénz befektetésével a lehető legnagyobb eredményeket érd el adott idő alatt a vállalkozásod számára.</p>-->
<!--						<p class="less-line-height">Feltételezem, benned is felébredt a kíváncsiság, hogy megismerd ezeket a praktikákat. Ahogyan most te is, én is hajthatatlan vágyat éreztem, hogy még többet tudjak meg erről a területről, amire sokan a marketing következő szintjeként tekintenek, ezért fontosnak véltem, hogy beleássam magam a témába. Az ezen a téren megszerzett ismereteimbe is beavatlak. </p>-->
<!--						<p class="less-line-height">A Growth Master nemcsak a weboldaladon belüli vásárlások arányát optimalizálja, hanem már a weboldalra érkező forgalom hatékony megszerzésével is foglalkozik. Ezzel a tudással felvértezve folyamatosan valós adatokra épülő továbblépési opciókat tárok eléd, hogy ne a sötétben kelljen tapogatóznod, amikor vállalkozásod lehetséges fejlesztési pontjait kell mérlegelned.</p>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="col-lg-12 col-12 set-of-answers">-->
<!--				<div class="col-lg-1 col-3 question-img">-->
<!--					--><?php //picture('plus-mark', 'png', '',  true, 'plus-mark'); ?>
<!--				</div>-->
<!--				<div class="col-lg-11 answers">-->
<!--					<h5 class="less-line-height question">Micsoda az a validált tanulás?</h5>-->
<!--					<div class="answers-text-box hide">-->
<!--						<p class="less-line-height">A validált tanulás azt jelenti, hogyan kapjunk választ a célpiacunkról alkotott feltételezéseinkre még azelőtt, hogy túl sok pénzt invesztálnánk egy esetleg működésképtelen ötletbe.</p>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--</section>-->
<!--<div class="section-separator section_methodology">-->
<!--	--><?php //picture('purple-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
<!--	<div class="skew-separator"></div>-->
<!--</div>-->
<section id="methodology">
	<div class="gradient-orange lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/who-is-gabor-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title dark-bg">Hol beszéltünk már<br><span class="text-purple-bg_gradient">MÓDSZERTANUNKRÓL</span></h2>
		<hr class="separator-short_purple">
		<div class="lesser-width">
			<p class="dark-bg less-line-height">Amellett, hogy piacvezető cégeknél szerzett szakmai tudásunkkal támogatunk téged, nagyon szívesen osztjuk meg tudásunkat másokkal is. Előadtunk az <strong class="purple-bg">Innonic Academy</strong>-n is a Growth Hacking módszertanról, illetve az ország legnagyobb márkaépítő csoportjában, a <strong class="purple-bg">BrandShip</strong>-ben is tartottunk zártkörű szakmai képzést a konverzió optimalizálás fortélyairól. <strong class="purple-bg">Wolf Gábor</strong>nak is meséltünk már villámgyors honlapmotorunkról, illetve startupoktól átemelt ügyféleredmény központú fejlesztési módszertanunkról.</p>
		</div>
		<div class="row extra-margin_top">
			<div class="col-lg-6">
				<iframe width="100%" height="280" data-src="https://www.youtube.com/embed/oXfU8KNpULk" class="lazyload" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="col-lg-6">
				<iframe width="100%" height="280" data-src="https://www.youtube.com/embed/Ld3LvwYBivQ" class="lazyload" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
		<div class="call-to-action u-text-center">
			<a href="#contact" class="btn-gradient">DOLGOZZUNK EGYÜTT</a>
		</div>
	</div>
</section>
<!--	<div class="section-separator_right_mirror_bottom section_methodology">-->
<!--		--><?php //picture('purple-gradient-effect-right_mirror', 'png', '',  true, 'gradient-effect-right_mirror'); ?>
<!--		<div class="skew-separator"></div>-->
<!--	</div>-->
<section id="pre-market" class="front-page">
	<div class="container">
		<h2 class="section-title hide-on-desktop">Hogyan tudunk mindig egy lépéssel<br><span class="text-gradient-bg">A PIAC ELŐTT JÁRNI</span></h2>
		<h2 class="section-title hide-on-mobile">Hogyan tudunk mindig egy lépéssel<br><span class="text-gradient-bg">A PIAC ELŐTT JÁRNI</span></h2>
		<hr class="gradient-separator-short">
		<div class="lesser-width">
			<p class="bigger-paragraph align-left">Hogy naprakészek legyünk a legújabb üzletfejlesztési lehetőségekkel, még angol nyelven magunkévá tesszük a tengerentúli sikerszerzők tanításait. A legjobb praktikákat így folyamatainkba építve alkalmazzuk, hogy időről-időre egyre több értéket adjunk általuk ügyfeleinknek.</p>
			<hr class="separator-left">
			<p class="less-line-height">Tagjai vagyunk a legjobb fizetős üzleti csoportoknak a Facebook-on, így nemcsak tudásunk naprakészségét biztosítjuk, de ha bármi kérdés merülne fel projekteddel kapcsolatban, az ország legnagyobb szaktekintélyeitől tudunk véleményt kérni úgy, hogy neked ez egy filléredbe sem kerül.</p>
		</div>
		<div class="row">
			<div class="col-lg-4 align-center extra-margin_top">
				<?php picture('brandbirds-logo', 'png', '',  true, 'partner-logo'); ?>
				<p>Márkaépítés mesterfokon</p>
			</div>
			<div class="col-lg-4 align-center extra-margin_top">
				<?php picture('domarketing-logo', 'png', '',  true, 'partner-logo'); ?>
				<p>Nem az számít, mit tudsz, hanem mit csinálsz a marketingben!</p>
			</div>
			<div class="col-lg-4 align-center extra-margin_top">
				<?php picture('szemleletvalto-logo', 'jpeg', '',  true, 'partner-logo'); ?>
				<p>Jóból kiválót</p>
			</div>
		</div>
	</div>
</section>
<section id="contact" class="front-page">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/time-spent-on-writing-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
			<h2 class="section-title dark-bg"><span class="text-purple text-gradient-bg">BESZÉLJÜNK RÓLA</span><br>hogyan tudnánk neked segíteni</h2>
			<hr class="gradient-separator-short">
			<div class="lesser-width">
				<p class="dark-bg less-line-height align-center extra-margin_bottom">Felkeltettük érdeklődésed? Írj egy pár sort projektedről, és elmondjuk, miben fejlődhet szerintünk a branded, weboldalad, hogyan hagyhatod magad mögött végre a konkurenseidet és milyen megoldásokkal válhatsz piacvezetővé.</p>
				<?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
			</div>

	</div>
</section>
<!-- Page Content -->



  <?php

  get_footer();
