<?php

/**
 * Template Name: 404
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<section id="page-404">
  <div class="page-banner">
  </div>
  <div class="section-separator">
  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
  <div class="skew-separator"></div>
</div>
	<main class="content container align-center">
		<h1>404</h1>
		<h2 class="section-title less-line-height"><span class="text-gradient-bg">HOPPÁ!</span> A keresett oldal nem található</h2>
		<p>Vagy valami baj van, vagy az oldal már nem létezik.</p>
		<div class="call-to-action">
			<a href="<?php echo site_url(); ?>" class="btn-gradient">VISSZA A FŐOLDALRA</a>
		</div>
	</main><!-- .content -->
</section>
<?php
get_footer();
