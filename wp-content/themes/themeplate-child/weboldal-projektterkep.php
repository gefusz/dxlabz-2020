<?php

/**
 * Template Name: Weboldal Projekttérkép
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<style>
	#projektterkep-view-switcher {
		width: 90px;
	    position: fixed;
	    left: -2px;
	    top: 130px;
	    padding: 10px;
	    border: 2px solid #f68529;
	    z-index: 5;
	    background: #ffffff;
	    font-size: 12px;
	    line-height: 1;
	    border-radius: 3px;
	    -webkit-box-shadow: 0 4px 20px 0 rgb(0 0 0 / 50%);
	    box-shadow: 0 4px 20px 0 rgb(0 0 0 / 50%);
	    text-align: center;
	    color: inherit;
	}

	#projektterkep-view-switcher a {
		color: #3f3f3f!important;
	}

	#projektterkep-view-switcher img {
		font-style: italic;
	    max-width: 100%;
	    vertical-align: middle;
	    display: inline-block;
	    margin-bottom: 5px;
	}

	.projektmap-intro-popup-overlay {
		display: none;
	}

	.projektmap-intro-popup-overlay.book-popup img {
		padding: 0;
		max-width: 700px;
	}

	@media (max-width: 768px) {
		.projektmap-intro-popup-overlay.book-popup img {
			max-width: 300px;
		}
	}
</style>
<div id="projektterkep-view-switcher">
	<a href="<?php echo get_site_url() . '/projektterkep'; ?>">
		<img src="<?php echo get_site_url() . '/wp-content/uploads/2021/04/webpage-project-map-redesigned.svg'; ?>">
		Részletes nézet
	</a>
</div>

  <div class="page-banner simple-page">
        <h1 class="section-title dark-bg less-line-height"><span class="text-gradient-bg">WEBOLDAL</span> projekttérkép</h1>
        <hr class="gradient-separator-short">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-2x.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
  </div>
  <div class="section-separator">
	  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
	  <div class="skew-separator"></div>
  </div>
<?php if(is_user_logged_in()) {?>
	<style type="text/css">
		#html5lightbox-watermark {
			display: none!important;
		}
	</style>
	<div class="projektmap-intro-popup-overlay tooltip">
		<div class="projektmap-intro-popup">
			<div class="projektmap-intro-popup__close-icon">X</div>
			<?php picture('weboldal-projekt-terkep-magyarazat', 'jpeg', '',  false, 'gradient-effect-left'); ?>
		</div>
	</div>
	<div class="projektmap-intro-popup-overlay book-popup">
		<div class="projektmap-intro-popup">
			<div class="projektmap-intro-popup__close-icon">X</div>
			<a href="<?php echo get_site_url() . '/online-gyorsitosav'; ?>">
				<img src="<?php echo get_stylesheet_directory_uri() . '/dx-labz-online-gyorsitosav-konyv-popup.jpg'; ?>">
			</a>
		</div>
	</div>
<?php } ?>
  <div id="web-projektmap">
	<main class="content container container-wide">
		<div class="gradient-white"></div>
	<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; ?>

	</main><!-- .content -->
</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/3.1.0/intro.min.js" integrity="sha512-8HbqTH7QzK30vhgVF/hTJ4loXwV85UU9vjI4nK04AfdOFzl8zG7b3LLAEHDmvIM8I0rvserMXmQx4Hw+kRknjw==" crossorigin="anonymous"></script>
<script>
	if (
			document.readyState === "complete" ||
			(document.readyState !== "loading" && !document.documentElement.doScroll)
	) {
		init();
	} else {
		document.addEventListener("DOMContentLoaded", init);
	}

	function init() {
		if (localStorage.getItem('projektmap_tooltip_viewed') != 'true') {
			document.querySelector('.projektmap-intro-popup-overlay.tooltip').style.display = 'flex';
			localStorage.setItem('projektmap_tooltip_viewed', true);
		}

		document.querySelectorAll('.projektmap-intro-popup').forEach(function(popup) {
			popup.addEventListener('click', function() {
				document.querySelectorAll('.projektmap-intro-popup-overlay').forEach(function(overlay) {
					overlay.style.display = 'none';
				});
			});
		});

		setTimeout(function() {
			if (localStorage.getItem('book_popup_viewed') != 'true' && window.innerWidth > 768) {
				document.querySelector('.projektmap-intro-popup-overlay.book-popup').style.display = 'flex';
				localStorage.setItem('book_popup_viewed', true);
			}
		}, 90000);

		// initIntro();
	}

	function initIntro() {
		if (window.innerWidth > 1199) {
			introJs().setOptions({
				prevLabel: 'Előző',
				nextLabel: 'Következő',
				doneLabel: 'Bezárás'
			}).start();
		} else {
			introJs().setOption("hintButtonLabel", "OK").addHints();
		}
	}
</script>
<?php get_footer();
