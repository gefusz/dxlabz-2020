<?php

/**
 * Template Name: My Account
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<section id="page-account">
  <div class="page-banner">
  		<?php if (is_user_logged_in()) { ?>
			<h1 class="section-title dark-bg less-line-height extra-margin_bottom"><span class="text-purple text-gradient-bg">FIÓKOD</span> beállításai</h1>
  		<?php } else { ?>
  			<h1 class="section-title dark-bg less-line-height extra-margin_bottom"><span class="text-purple text-gradient-bg">BEJELENTKEZÉS</span> a fiókodba</h1>
  		<?php } ?>
		
		<hr class="gradient-separator-short">
		<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-2x.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
  </div>
  <div class="section-separator">
  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
  <div class="skew-separator"></div>
</div>
	<main class="content container">

		<?php if ( ! is_front_page() ) : ?>
		<?php endif; ?>

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>

	</main><!-- .content -->


</section>
<?php
get_footer();
