if (
    document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
  executeDOMScripts();
} else {
  document.addEventListener("DOMContentLoaded", executeDOMScripts);
}

function executeDOMScripts() {
	if (document.getElementById('web-projektmap')) {
		document.querySelectorAll('#web-projektmap a').forEach(function(anchor) {anchor.setAttribute('target', '_blank');});
	}

	WebFont.load({
		google: {
			families: ['Montserrat:400,600,700,800,900']
		}
	});

	document.addEventListener('lazybeforeunveil', function(e){
		var bg = e.target.getAttribute('data-bg');
		if(bg){
			e.target.style.backgroundImage = 'url(' + bg + ')';
		}
	});

	// Toggle mobile menu
	if (clientWidth < 992) {
		let navBar = document.getElementById('navbarResponsive');
		if (document.querySelector('[data-target="#navbarResponsive"]')) {
			document.querySelectorAll('[data-target="#navbarResponsive"], #navbarResponsive .menu-item').forEach(function(el) {
				el.addEventListener('click', function (e) {
					e.target.classList.toggle('collapsed');
					navBar.classList.toggle('show');
				});
			});
		}
	}

	lazyLoadStyles();
	lazyLoadScripts();


	// Scroll to top
	//Get the button:
	mybutton = document.getElementById("scroll-to-top");

	// When the user scrolls down 20px from the top of the document, show the button
	window.onscroll = function() {scrollFunction()};

	function scrollFunction() {
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
			mybutton.style.display = "block";
		} else {
			mybutton.style.display = "none";
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
		document.body.scrollTop = 0; // For Safari
		document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
	}

	mybutton.addEventListener('click', topFunction);

	document.querySelectorAll('#many-question .question-img, #many-question .question').forEach(function(el) {
		el.addEventListener('click', function(e) {
			let answer = e.currentTarget.closest('.set-of-answers').querySelector('.answers-text-box');
			if (answer.classList.contains('hide')) {
				document.querySelectorAll('.answers-text-box').forEach(function(el) {el.classList.add('hide')});
				el.closest('.set-of-answers').querySelector('.answers-text-box').classList.toggle('hide');
			} else {
				answer.classList.add('hide');
			}

		});
	});
		// Sticky menu
	// After scrolling 50px from the top...
	window.addEventListener('scroll',function() {
		// Load contact form JS deferred on scroll
		// if (window.scrollY > 50 && isFrontPage && !contactJSLoaded) {
		// 	let documentFragment = document.createDocumentFragment();
		// 	[
		// 		//'/contact-form-7/includes/js/scripts.js'
		// 	].forEach(function(path) {
		// 		documentFragment.appendChild(constructElement('script', {
		// 			src: pluginsUrl + path,
		// 			defer: true
		// 		}));
		// 	});
		//
		// 	document.head.appendChild(documentFragment);
		// 	contactJSLoaded = true;
		// }


		var topBar = document.getElementById('topbar'),
			gradientSeparator = document.querySelector('.gradient-separator'),
			navBar = document.querySelector('.navbar'),
			navBarPatternRight = document.querySelector('.navbar-pattern-right'),
			navBarLogo = navBar.querySelector('.logo'),
			navBarResponsive = document.getElementById('navbarResponsive');

		if (window.scrollY > 50) {
			// topBar.style.transform = 'translateY(-70px)'; //periodiacal-banner esetében.
			// gradientSeparator.style.transform = 'translateY(-70px)';
			navBar.style.background = 'linear-gradient(90deg, rgba(79,39,122,0.98) 0%, rgba(39,39,39,0.98) 100%)';
			navBar.style.top = '0';
			navBar.style.boxShadow = '0 10px 20px rgba(0,0,0,.19), 0 6px 6px rgba(0,0,0,.23)';
			navBarPatternRight.style.visibility = 'visible';
			if (clientWidth > 768) {
				navBarLogo.style.width ='40%';
				navBarLogo.style.height = 'unset';
			}
			if (clientWidth < 768) {
				navBarResponsive.style.background = 'transparent';
			}

			//Otherwise remove inline styles and thereby revert to original stying
		} else {
			// topBar.style.transform = 'translateY(0px)'; //periodiacal-banner esetében.
			// gradientSeparator.style.transform = 'translateY(0px)';
			navBar.style.background = 'transparent';
			navBar.style.top = 'auto';
			navBar.style.boxShadow = 'none';
			navBarPatternRight.style.visibility = 'hidden';
			if (clientWidth > 768) {
				navBarLogo.style.width = '70%';
				navBarLogo.style.height = '60px';
			}
			if (clientWidth < 768) {
				navBarResponsive.style.background = 'linear-gradient(90deg,rgba(79,39,122,.98),rgba(39,39,39,.98))';
			}
		}
	});
}

function lazyLoadStyles() {
	let documentFragment = document.createDocumentFragment();

	if (isFrontPage || isBookLanding) {
		// Load CSS async
		let stylesArray = [
			'/woocommerce/packages/woocommerce-blocks/build/style.css',
			'/woocommerce/assets/css/woocommerce-layout.css',
			'/woocommerce/assets/css/woocommerce-smallscreen.css',
			'/woocommerce/assets/css/woocommerce.css',
			'/woocommerce-memberships/assets/css/frontend/wc-memberships-frontend.min.css'
		];

		if (isFrontPage) stylesArray.push('/contact-form-7/includes/css/styles.css');

		stylesArray.forEach(function(path) {
			documentFragment.appendChild(constructElement('link', {
				rel: 'stylesheet',
				href: pluginsUrl + path
			}));
		});
	}

	document.body.appendChild(documentFragment);
}

function lazyLoadScripts() {
	setTimeout(function() {
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-PGW3FGP');


		// Load jQuery deferred on book landing
		// if (isBookLanding) {
		// 	let documentFragment = document.createDocumentFragment();
		// 	[
		// 		'/wp-includes/js/jquery/jquery.js'
		// 	].forEach(function(path) {
		// 		documentFragment.appendChild(constructElement('script', {
		// 			src: siteUrl + path,
		// 			defer: true
		// 		}));
		// 	});
		//
		// 	document.head.appendChild(documentFragment);
		// }
	}, 3000);


	// GDPR cookie acceptance
	if (getCookie('hasConsent') !== 'true') {
		let cookiesEUBanner = document.getElementById('cookies-eu-banner');
		cookiesEUBanner.style.display = 'block';

		document.querySelectorAll('#cookies-eu-banner button').forEach(function(button) {
			button.addEventListener('click', function(e) {
				cookiesEUBanner.style.display = 'none';
				let date = new Date();
				document.cookie = 'hasConsent=true; path=/; expires=' + new Date(2147483647 * 1000).toUTCString();
			});
		});
	}
}

function getCookie(cookieName){
	return (document.cookie.match('(^|; )' + cookieName + '=([^;]*)') || 0)[2];
}

var contactJSLoaded = false;
