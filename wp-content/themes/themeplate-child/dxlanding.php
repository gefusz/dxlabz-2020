<?php
/**
 * Template Name: DxLanding
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>

<?php //get_template_part( 'template-parts/header', 'without-nav' ); ?>

<section id="hero" class="landing-page">
	<div class="hero-gradient"></div>
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-wide-orange.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 text">
				<h1>ONLINE<br><span>GYORSÍTÓSÁV</span></h1>
				<h2><span class="uppercase">Tervezés, ügyfélszerzés, működtetés, profitmaximalizálás.</span> <br>Egyetlen könyvben, minden, ami fontos, hogy sikerre vidd vállalkozásod a weben.<div class="gradient-text-bg"></div></h2>
				<p>Az eredményes online vállalkozások GPS-e.</p>
				<div class="call-to-action">
					<a href="#successfull" class="btn-gradient">LÉPJÜNK A GÁZPEDÁLRA</a>
				</div>
			</div>
			<div class="col-lg-6 image">
				<?php picture('online-gyorsitosav-book', 'png', '',  false, 'book'); ?>
				<div class="founder">
					<div class="col-lg-4 col-4 align-center">
						<?php picture('dx-labz-kriston-gabor', 'png', '',  false, 'gabor'); ?>
					</div>
					<div class="col-lg-8 col-8 align-left">
						<h4 class="name">Kriston Gábor</h4>
						<p class="qualification">Az ország egyetlen<br>Google Minősített <br>Mobil Web Specialistája</p>
					</div>
				</div>
			</div>
			<div class="testimonial-box">
				<h4 class="opininon sub-title align-left text-normal italic dark-bg">“Életmentő útikönyv az online vállalkozáshoz.</h4>
				<p class="italic dark-bg">Gábor könyve nélkül az online vállalkozás olyan, mint elutazni egy ismeretlen országba útikönyv, tervek, ismerősök és okostelefon nélkül: veszélyes kalandtúra. Ha a kedves szerző pár évvel korábban megírta volna, néhány millió forinttal biztosan gazdagabb és néhány ősz hajszállal szegényebb lennék. Túlélőtúra helyett válaszd útitársnak, és sikeres leszel!”</p>
				<div class="row testimonial-person">
					<div class="col-lg-2 col-3 col-md-2 image align-left">
						<?php picture('testimonial-meszaros-robi', 'png', '',  false, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-8 col-9 col-md-10 text">
						<h4 class="name dark-bg">Mészáros Róbert</h4>
						<p class="qualification dark-bg">márkaépítő, BrandBirds</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> <!-- end of header -->
<div class="section-separator">
	<?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<section id="successfull">
	<div class="container">
		<h2 class="section-title"><span class="text-gradient-bg">RENDSZERBE FOGLALTUK</span><br> a kulcsinformációkat, hogy célba érj</h2>
		<h4 class="sub-title med-width">Új időszámítás kezdődött az online világban és csak a legfelkészültebbek lesznek képesek versenyben maradni!</h4>
		<hr class="gradient-separator-short">
		<p class="align-center less-line-height med-width">Egyre többen kívánnak online bizniszt építeni, azonban a siker is egyre komplexebb tudást igényel. <strong class="bold-only">A könyv által megismerheted mi az a belépési szint, ahonnan megnyílik a siker kapuja.</strong> Csak kevesen fognak időben rátalálni a helyes ösvényre. Te melyik csoportba fogsz tartozni?</p>
		<p class="bigger-paragraph align-center med-width">Olvasd el a könyvet, ami miatt a legtöbb webes kivitelező nem fog szeretni, mégis <br><span class="uppercase">HOZZA AZ EREDMÉNYEKET!</span></p>
		<div class="row extra-margin_bottom">
			<div class="col-lg-6">
				<ul>MILYEN INFORMÁCIÓKKAL FOGSZ GAZDAGODNI:
					<li><strong>Csak a lényegre szorítkozó tudás birtokába kerülsz, amiről a konkurenseid 99%-a még nem hallott</strong>, mert elvesznek az információs dömpingben, így köröket verhetsz rájuk</li>
					<li><strong>Mindent érteni fogsz</strong>, ha egy kivitelezővel kell tárgyalnod és megtanulsz hatékonyan együttműködni velük, hogy te az üzletfejlesztésre fókuszálhass</li>
					<li><strong>Megismered az online vállalkozások legnagyobb buktatóit</strong>, így elkerülheted őket</li>
					<li><strong>Nem fogsz feleslegesen vagyonokat kidobni</strong> az ablakon félmegoldásokra</li>
					<li>Egy olyan <strong>útikalauzt kapsz</strong>, amihez mindig vissza tudsz nyúlni ha kételyeid támadnak, mert a weboldal minden életszakaszában pontos útmutatást nyújt a teendőiddel kapcsolatban</li>
					<li>Az ismertetett praktikákkal a siker csak idő és kitartás kérdése lesz, mert <strong>"kézen foglak",</strong> és végigkísérlek az úton</li>
					<li>Jobb <strong>rálátásod lesz a sikeres weboldal kulcselemeire</strong> mint a kivitelezők 95%-ának, akik csak a saját szakterületükre fókuszálnak</li>
				</ul>
			</div>
			<div class="col-lg-6 row guarantee">
				<div class="col-lg-4 col-5 align-center">
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/guarantee.svg" alt="Checkmark Star"  class="lazyload checkmark-star-circle">
				</div>
				<div class="col-lg-8 col-7">
					<h4><strong>30 napos</strong></h4>
					<p class="uppercase">ELÉGEDETTSÉGI GARANCIA!*</p>
				</div>
				<div class="col-lg-12 col-12">
					<p class="purple-bg_dark">Ha úgy gondolod, hogy a könyv nem hozza vissza legalább az árának a 10x-esét az elkövetkezendő 1 évben, a vásárlástól számított 30 napon belül visszatérítjük az árát kérdés nélkül!</p>
					<a href="#how-it-works" class="how-it-works">*Hogyan működik az elégedettségi garancia?</a>
				</div>
			</div>
		</div>
		<div class="call-to-action align-center extra-margin_top">
			<a href="#choose-your-path" class="btn-gradient">HOGYAN JUTHATOK HOZZÁ?</a>
		</div>
	</div>
</section>
<div class="section-separator_mirror">
	<div class="skew-separator"></div>
</div>
<section id="enthusiastic">
	<div class="container">
		<h2 class="section-title hide-on-desktop">Miért tartják <span class="text-gradient-bg">HIÁNYPÓTLÓNAK<br></span> a könyvet az olvasók?</h2>
		<h2 class="section-title hide-on-mobile">Miért tartják <span class="text-gradient-bg">HIÁNYPÓTLÓNAK<br></span> a könyvet az olvasók?</h2>
		<hr class="gradient-separator-short">
		<div class="row less-width">
			<div class="col-lg-6 testimonial-box">
				<p class="italic">"Baromi királyul van felépítve a könyv. Miközben olvastam, komolyan mondom, hogy megirigyeltem a nyelvezetét és a felépítését. Le a kalappal!</p>
				<h4 class="opininon sub-title align-left text-normal italic">Olyan könyv, amire a piac már évek óta várt. Pótolhatatlan mankó lesz, ha online üzletet szeretnétek felépíteni.</h4>
				<p class="italic">Minden cégnek ennek kéne lennie az alapjának, amit itt bemutatsz. Komolyan mondom, hogy e nélkül még egy domain nevet sem szabadna megvásárolni."</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-dave-wolf', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Farkas-Balog Dávid</h4>
						<p class="qualification">online marketing szakértő</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“Nagyon alapos, rendszerezett, széleskörű tudásanyagra épülő információbomba. Bombasztikus a felismerések és a felismertetések tekintetében. Tabukat döntöget. Rávilágít olyan összefüggésekre, melyeket sok webfejlesztő, közösségi médiákat és/vagy online hirdetéseket kezelő ügynökség tudatlanságból elhallgat, vagy ami még rosszabb, tudatosan leplez és agyonhallgat.</p>
				<h4 class="opininon sub-title align-left text-normal italic">Enélkül senkinek sem volna szabad egyetlen mozdulatot sem tenni az online üzletfejlesztések világában!”</h4>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-kamilla-akhmin', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Kamilla Akhmin</h4>
						<p class="qualification">GoldHair hajhosszabbítás projekt</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">"A könyvedet egy 110%-ig gyakorlatias könyvnek látom, amire soha nem volt ennyire szükség. Púdermentes. Nem fest lila ködöt, hanem azt nyújtja, amire szüksége van ma egy vállalkozónak, aki online akar értékesíteni és mostantól mindenki akar online értékesíteni. Lépésről lépsre viszi végig az olvasót a számára még ismeretlen területeken és lerántja a leplet azokról a sötét sarkokról, amik a hiábavaló vállalkozásokhoz vezethetnének.</p>
				<h4 class="opininon sub-title align-left text-normal italic">Igazából fizetni kéne neked a könyvön túl azoknak, akik így nem veszítik el a pénzüket.”</h4>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-jeckel-eszter', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Jeckel Eszter</h4>
						<p class="qualification">diplomás coach</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<h4 class="opininon sub-title align-left text-normal italic">“Egy nagyon tagolt, átlátható, tankönyvszerű, jól áttekinthető és értelmezhető olvasmány.</h4>
				<p class="italic">Közvetlen, megnyerő hangvétel, személyes történetek bemutatásával, sorsközösséget vállalsz, ami szimpatikus az olvasónak. Szerintem ezzel szintet léptél, nemcsak azokhoz szól a könyv akik most akarnak online vállalkozást indítani, hanem kezdő webdesignereknek is, hiszen egy nagyon átfogó képet kaphatnak erről a világról, de legfőképpen a leendő klienseik szemével. Azokkal a problémákkal, gondolatokkal, kerdésekkel találkozhatnak a könyvben, amik az ügyfeleikben is megfogalmazódhatnak.”</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-kovacs-tamas', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Kovács Tamás</h4>
						<p class="qualification">brand manager</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<h4 class="opininon sub-title align-left text-normal italic">„Baromi jó az egész amit összeraktál, nagyon tetszik.</h4>
				<p class="italic">Az fogott meg a legjobban, hogy olyan lényegretörő és közben fiatalos, úgy olvastatja magát. Magamra ismertem."</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-bohut-gergo', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Bohut Gergő</h4>
						<p class="qualification">BubbleBook alapító</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<h4 class="opininon sub-title align-left text-normal italic">„Bárcsak 1 évvel ezelőtt került volna elém!!”</h4>
				<p class="italic">Sok időt, pénzt és kínlódást spóroltam volna meg magamnak a weboldalam menedzselését illetően!!</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-kertesz-farkas-aniko', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Kertész-Farkas Anikó</h4>
						<p class="qualification">Franciázz.hu</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<h4 class="opininon sub-title align-left text-normal italic">„Aranybánya, kincsestár.”</h4>
				<p class="italic">Nagyon bírom a stílusát, mert őszinte, és megmondja miből-mi következik, hová juthat el, és mindennek a mélyére leás, megmutatja. Remek könyv, olyan témával, amely nagyon sokunknak fog segíteni és tovább fejleszteni!</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-mate-brigitta', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Máté Brigitta</h4>
						<p class="qualification">Lépcsőfok</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<h4 class="opininon sub-title align-left text-normal italic">„Jó szakirodalom.”</h4>
				<p class="italic">"Konkrét utat ír le, ami olyan érzést kelt, hogy ha véletlenül elfelejtenék mindent, amit eddig megtanultam, elolvasva ezt a könyvet újra képben lehetnék a vállalkozás témakörben. Nagyon jó dolgok vannak benne rendesen leírva, így biztosan jó szakirodalom lesz!”</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-kovacs-andras', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Kovács András</h4>
						<p class="qualification">Édesburgonya.bio</p>
					</div>
				</div>
			</div>
		</div>
		<div class="recommendation align-center lesser-width">
			<h3><span class="text-gradient-bg">DOMÁN ZSOLT</span> is bizalommal ajánlja a könyvet a zökkenőmentes induláshoz</h3>
			<video controls poster="<?php echo get_stylesheet_directory_uri();?>/assets/images/doman-zsolt-video-preview.<?php echo (WEBP ? 'webp' : 'jpg'); ?>">
				<source src="<?php echo get_stylesheet_directory_uri();?>/assets/videos/doman-zsolt-ajanlo.mp4" type="video/mp4">
				Your browser does not support the video tag.
			</video>
		</div>
	</div>
</section>
	<div class="section-separator_right_top section_point-of-view">
		<?php picture('purple-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		<div class="skew-separator"></div>
	</div>
<section id="less-and-more" class="align-center">
	<div class="gradient-orange lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/less-and-more-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title section-title_smaller dark-bg">KEVESEBB KERÜLŐÚT + TÖBB ÜGYFÉL </h2>
		<h2 class="section-title section-title_smaller text-purple-bg">KEVESEBB STRESSZ ÉS AGGODALOM + TÖBB BEVÉTEL</h2>
	</div>
</section>
	<div class="section-separator_right_bottom section_point-of-view">
		<div class="skew-separator"></div>
	</div>
<section id="point-of-view">
	<div class="container">
		<h2 class="section-title">Ne bajlódj a fórumok bújásával<br><span class="text-gradient-bg"> elmondjuk, mi a lényeg</span></h2>
		<hr class="gradient-separator-short">
		<div class="lesser-width">
			<p class="bigger-paragraph align-left">Ha ezeket a sorokat olvasod, akkor valószínűleg Te is nagy reményekkel, izgalommal, ugyanakkor félelemmel és sok-sok kétséggel állsz hozzá az online vállalkozás világához és a weben történő megjelenésedhez. </p>
			<hr class="separator-left">
			<p class="less-line-height"><strong>Nekem való ez? Fel vagyok rá készülve? </strong>- teheted fel joggal a kérdést. Az is lehet, hogy már van webes felületed, de nem vagy elégedett az eddigi eredményeiddel és bizonytalan vagy a jövőt illetően. Az igazság az, hogy a teljes fegyverarzenál, amit birtokolnod- és hatékonyan használnod kellene ahhoz, hogy sikeres lehess, még attól is összetettebb, mint azt eddig gondoltad! </p>
			<p class="less-line-height">Az elmúlt években annyira sok információ vált elérhetővé az online vállalkozások indításával és üzemeltetésével kapcsolatban, amivel még a szakemberek is csak nehezen tudják tartani a lépést. <strong>Nagyon kevesen látják át, hogy mi az, ami igazán fontos és hasznos. </strong>Ezzel egyidejűleg kiszélesedett és komplexebbé vált a lehetőségek tárháza is. Minden lehetőséget megismerni pedig még inkább lehetetlen egy, a webes világba éppen csak belecsöppenő vállalkozónak.</p>
			<p class="less-line-height">Sajnos ezidáig a sikerhez szükséges információk csak különböző médiákon szétszórva, rendezetlenül voltak jelen. A piacra lépő újdonsült vállalkozók, ha kerestek sem találtak volna olyan átfogó kiadványt, amivel nem kellene hosszú ideig a sötétben tapogatózniuk. </p>
			<p class="less-line-height">Van remény! Nem szeretném tovább tétlenül nézni, hogy jó szándékú vállalkozók vásárra viszik a bőrüket, úgy, hogy közben óriási kockázatokat vállalnak azok kivédésének ismerete nélkül.<strong>A könyvet elolvasva egy olyan szemléletet fogsz kapni, amivel nem veszel el a részletekben és a kivitelezés útvesztőiben. </strong></p>
			<p class="less-line-height">Nem arra a lehetetlen feladatra vállalkozok, hogy mindent megtanítok neked, hanem megismertetem veled az eredmények nagy részét hozó legfontosabb kulcsmozzanatokat.  Egy olyan szemléletet kapsz, ami segít kitűzni a megfelelő célokat, ezáltal egy előre lefektetett haditerv mentén tudsz majd haladni. Kitartó munkád eredményeként birtokodba kerül majd az a fegyverarzenál is, amivel csatát-csatáról megnyerve győzedelmeskedhetsz az ügyfelek kegyeiért vívott “versengésben”.</p>
		</div>
		<div class="call-to-action align-center">
			<a href="#choose-your-path" class="btn-gradient">ÉRDEKEL A KÖNYV</a>
		</div>
	</div>
</section>
	<div class="section-separator_right_bottom">
	<div class="skew-separator"></div>
	</div>
<section id="book-in-numbers">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/book-in-numbers-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<h2 class="section-title"><span class="text-gradient-bg">10 év tapasztalata</span> 320 oldalban</h2>
	<hr class="gradient-separator-short">
		<div class="lesser-width relative-and-z-index">
			<div class="row align-center">
				<div class="col-lg-3 results">
					<h4 class="number">100+</h4>
					<p class="text dark-bg">Több mint 100 sikeres projekt tapasztalata alapján íródott</p>
				</div>
				<div class="col-lg-3 results">
					<h4 class="number">50+</h4>
					<p class="text dark-bg">Több mint 50 szakmai könyv lényegi információi lettek belegyúrva</p>
				</div>
				<div class="col-lg-3 results">
					<h4 class="number">10</h4>
					<p class="text dark-bg">Az elmúlt 10 év alatt felmerült leggyakoribb ügyfélkérdések kerülnek megválaszolásra</p>
				</div>
				<div class="col-lg-3 results">
					<h4 class="number">320</h4>
					<p class="text dark-bg">320 oldalon keresztül vezet téged lépésről-lépésre, hogy lásd az összképet</p>
				</div>
			</div>
		</div>
</section>
<div class="section-separator_mirror section_book-in-numbers">
	<?php picture('orange-gradient-effect-left-mirror', 'png', '',  true, 'gradient-effect-left_mirror'); ?>
	<div class="skew-separator"></div>
</div>
<section id="who-is-gabor">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/who-is-gabor-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-white.svg" alt="Pattern Top White"  class="lazyload pattern-top_white">
	<div class="container relative-and-z-index">
			<h2 class="section-title dark-bg">Jól hangzik, de <span class="text-purple text-gradient-bg">KICSODA </span><br>az a Kriston Gábor?</h2>
			<hr class="gradient-separator-short">
		<div class="lesser-width">
			<p class="bigger-paragraph align-left dark-bg">Jelenleg én vagyok az ország egyetlen Google Minősített Mobil Web Specialistája, emellett megszereztem a Growth Master titulust is. Mit jelentenek ezek számodra? Megkérdőjelezhetetlenül a mobilé a jövő. A Google minősítésnek hála, gyakorlatban alkalmazom azokat a mobil optimalizációs technológiákat, amelyek az elkövetkező években fognak csak piaci sztenderddé válni. Growth Master-ként elsajátítottam azokat a praktikákat, amelyek a sikeres startupok gyors felfutását segítik elő, ezeket is igyekeztem a KKV-k nyelvére lefordítani a könyvben. Ugye te sem bánnád, ha pár év versenyelőnyre tennél szert?</p>
			<hr class="separator-left">
			<p class="dark-bg align-left">Az elmúlt 10 év folyamán tapasztalatot szereztem a világ legnagyobb digitális ügynökségénél, valamint a régió legnagyobb szoftverfejlesztő cégénél szoftvermérnökként is. Több millió dolláros projekteken dolgoztam, egy másik kitérő során pedig egy 10 országra kiterjedő marketingrendszert hoztam létre és koordináltam. Az Optimonk fejlesztési vezetőjeként egyenesen a frontvonalról kerültem gyakorlati tudás birtokába a startupokról. Emellett elvégeztem a Moholy-Nagy Művészeti Egyetem felhasználói élménytervezés kurzusát is és a mai napig folyamatosan dolgozok szakmai tudásom elmélyítésén.</p>
			<p class="dark-bg align-left">Úgy gondolom, hogy olyan átfogó szemléletet sikerült kialakítanom, melynek révén egyedi rálátásom van a weboldal mint értékesítési rendszer elemeire és komplex összefüggéseire.</p>
			<p class="dark-bg align-left orange-bg">“Kötelességemnek éreztem, hogy a nagyvállalati szintű megoldásokat elérhetővé tegyem a KKV-k számára is, rájuk szabott módon. Ez hívta életre vállalkozásunkat, a DX Labz-ot.“ </p>
			<p class="dark-bg align-left">Sok olyan weboldal készítéséhez- és működtetéséhez kapcsolódó praktikát ismertem meg a gyakorlatban, amelyek óriási versenyelőnyt biztosítanak a nagy cégek számára, ugyanakkor ezen praktikákat a kisvállalkozók többsége még csak hallomásból sem ismeri. </p>
			<p class="dark-bg align-left">Közel 100 céggel dolgoztunk együtt, mire megalkottuk a siker receptjét. </p>
			<div class="certification align-center">
			<?php picture('mobile-web-specialist', 'png', '',  true, 'mobile-web-specialist'); ?>
			<?php picture('growt-master-certificate', 'png', '',  true, 'growt-master-certificate'); ?>
		</div>
	</div>
</section>
	<div class="section-separator_right_bottom section_mission">
		<?php picture('orange-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		<div class="skew-separator"></div>
	</div>
<section id="mission">
	<div class="container">
		<h2 class="section-title"><span class="text-gradient-bg">ELOSZLATJUK</span> a kivitelezőkkel kapcsolatos kétségeidet</h2>
		<hr class="gradient-separator-short">
		<h4 class="sub-title less-line-height med-width">Azt tűztem a zászlómra, hogy az ügyfelek érdekeinek védelmében magasabbra emeljük az alapvető elvárásokat a webes kivitelezőkkel szemben.</h4>
		<p class="align-center less-line-height med-width">Ezt úgy kívánom elérni, hogy a weboldal mint rendszer működését átláthatóvá teszem, és a kivitelezők felelősségeiről és a lehetőségekről teljes körű tájékoztatást nyújtok. </p>
	</div>
</section>
	<div class="section-separator_mirror_bottom section_mission">
		<div class="skew-separator"></div>
	</div>
<section id="secret-of-success">
	<div class="container">
		<h2 class="section-title hide-on-desktop"><span class="text-gradient-bg">A SIKERES WEBOLDALAK</span><br><span class="text-gradient-bg"> 3 FŐ TITKA,<br></span> amit a könyvből megtudhatsz</h2>
		<h2 class="section-title hide-on-mobile"><span class="text-gradient-bg">A SIKERES WEBOLDALAK 3 FŐ TITKA,</span><br> amit a könyvből megtudhatsz</h2>
		<hr class="gradient-separator-short">
		<div class="row less-width">
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-1', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/magnifier.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<p class="align-left"><strong class="bold-only">1. Ne vessz el a részletekben. A siker a hatékony koordináláson múlik.</strong></p>
					<p class="align-left">Megrendelőként a te feladatod az ügyfelek minél magasabb színvonalú kiszolgálása. Hagyd meg a munkát a profiknak, de tudd mi várható el tőlük és hogyan kérheted számon a megfelelő végeredményt. </p>
				</div>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-2', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/tasks.svg" class="lazyload secret-of-success-icon tasks">
				</div>
				<div class="col-lg-10">
					<p class="align-left"><strong class="bold-only">2. Tervezz alaposan. A célok helyes megtervezése már maga fél siker.</strong></p>
					<p class="align-left">A körültekintő tervezés fontosabb még a kivitelező választásnál is. Egy jó tervből könnyű dolgozni, de egy rossz tervet még a legjobb kivitelező sem tud megmenteni. Lépésről-lépésre vezetlek végig, hogy stabil alapokkal indulhass.</p>
				</div>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-3', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/timer.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<p class="align-left"><strong class="bold-only">3. Gondolkodj hosszú távra. Fokozatosan pörgesd fel az értékesítés motorját.</strong></p>
					<p class="align-left">A weboldal projekt nem egy “pár hónap alatt letudom és elfelejtem” munka, hanem következetes figyelmet igénylő folyamat, amibe bár tőkét és időt kell fektetned, de a végeredmény egy folyamatosan, egyre olajozottabban működő, egyre nagyobb profitot termelő gépezet lesz. A könyvet elolvasva nem lesz kérdés, hogyan juss el rövid időn belül a kiszámítható profitig.</p>
				</div>
			</div>
		</div>
		<div class="call-to-action align-center">
			<a href="#choose-your-path" class="btn-gradient">MEGRENDELEM A KÖNYVET</a>
		</div>
	</div>
</section>
	<div class="section-separator section_time-spent-on-writing">
		<?php picture('purple-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
		<div class="skew-separator"></div>
	</div>
<section id="time-spent-on-writing">
	<div class="gradient-orange lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/time-spent-on-writing-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title dark-bg less-line-height hide-on-desktop">Útmutatót kapsz vállalkozásod kockázatainak <span class="text-purple-bg_gradient">MINIMALIZÁLÁSÁHOZ</span></h2>
		<h2 class="section-title dark-bg less-line-height hide-on-mobile">Útmutatót kapsz vállalkozásod kockázatainak <span class="text-purple-bg_gradient">MINIMALIZÁLÁSÁHOZ</span></h2>
		<hr class="separator-short_purple">
		<div class="lesser-width">
			<p class="dark-bg less-line-height">Feltételezem, hogy anno akárcsak én, te is azért indultál el a vállalkozás útján, mert úgy gondoltad, hogy több van benned, minthogy mások arassák le fáradozásaid babérjait és volt egy ötleted, amivel véleményed szerint jobbá tudnád tenni mások életét. Azonban a vállalkozóvá válással elengedjük azt a védőhálót, ami a legtöbb esetben megvéd minket a veszélyektől és közvetlenül viseljük döntéseink következményeit. Vállalkozni egy izgalmas kaland, ugyanakkor óriási felelősség is, mert kockáztatjuk nemcsak a saját, de családunk és munkatársaink anyagi biztonságát is.</p>
			<p class="dark-bg purple-bg less-line-height">Kötelességemnek éreztem, hogy megmutassam neked azt az általam megismert optimális útvonalat, ami nagyban megnöveli az esélyét, hogy online vállalkozásod sikertörténet és ne egy rémálom legyen.</p>
			<p class="dark-bg less-line-height">Ezért az elmúlt 1 évben többszáz órát töltöttem azzal, hogy ezt az információt emészthető formában eléd tárjam, megkíméljelek az elkerülhető kudarcoktól és ezáltal magasabb sebességi fokozatba kapcsolva roboghass az álmaid felé.</p>
		</div>
	</div>
</section>
<div class="section-separator_mirror section_book-in-numbers">
	<div class="skew-separator"></div>
</div>
<section id="richfull-informations">
	<h2 class="section-title">A <span class="text-gradient-bg">tökéletes recept</span> vállalkozásod <br>felpörgetésére</h2>
	<hr class="gradient-separator-short">
		<div class="lesser-width relative-and-z-index">
			<div class="row align-center">
				<div class="col-lg-3 results">
					<h4 class="number">30<span>%</span></h4>
					<p class="text dark-bg">A legoptimálisabb projektforgatókönyv ismertetése</p>
				</div>
				<div class="col-lg-3 results">
					<h4 class="number">30<span>%</span></h4>
					<p class="text dark-bg">A sikerhez nélkülözhetetlen tervezési lépések részletes bemutatása</p>
				</div>
				<div class="col-lg-3 results">
					<h4 class="number">20<span>%</span></h4>
					<p class="text dark-bg">Hatékonyság-többszöröző <br>projektmenedzsment <br>praktikák</p>
				</div>
				<div class="col-lg-3 results">
					<h4 class="number">20<span>%</span></h4>
					<p class="text dark-bg">Gyakorlati tesztelési-, <br>adatelemzési- és <br>optimalizálási megoldások</p>
				</div>
			</div>
		<div class="call-to-action align-center extra-margin_top extra-margin_bottom">
			<a href="#choose-your-path" class="btn-gradient">MEGRENDELEM A KÖNYVET</a>
		</div>
		</div>
</section>
<div class="section-separator_mirror section_book-in-numbers">
	<div class="skew-separator"></div>
</div>
	<div class="section-separator_mirror_bottom section_key-of-success">
		<div class="skew-separator"></div>
	</div>
<section id="key-of-success">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/key-of-success-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title dark-bg"><span class="text-purple text-gradient-bg">ÁTNAVIGÁLUNK</span> az online<br>vállalkozások aknamezőin</h2>
		<hr class="gradient-separator-short">
		<div class="lesser-width">
			<p class="less-line-height dark-bg">Képzel el, hogy egy túlélőtúrára indultál, de a telefonod lemerült és térképedet is elhagytad. Valószínűleg rengeteg bosszúságot és időveszteséget okoznának a felesleges kerülők, hogy sorra végighaladj az ellenőrzőpontokon és sikeresen teljesítsd a túrát. Ellenben ha a GPS segítségével, vagy térképpel navigálnál, a lehető legoptimálisabb útvonalon és a legrövidebb időn belül jutnál mindig a következő állomáshoz. A könyvben megismertekkel mindig tudni fogod, mi a következő fontos kapu, amin át kell haladnod és hogyan juthatsz el oda a leggyorsabban. <strong>Az ideális útiterv mentén haladva pénzt, időt és energiát fogsz spórolni.</strong>A szomorú igazság az, hogy a kivitelezők 95%-a nincs az ideális útvonal ismeretének birtokában, éppen ezért neked kell összehangolnod a munkájukat.</p>
			<p class="dark-bg"><strong class="bold-only">Nem tudják a kivitelezőim sínen tartani a projektet?</strong></p>
			<p class="less-line-height dark-bg"><strong>A pontos végcél csak a te fejedben létezik és biztosítanod kell, hogy ezalatt az összes partnered ugyanazt érti. </strong></p>
			<p class="less-line-height dark-bg">Ha valaha hallottál már egy szaftos pletykát, aminek a fele sem volt igaz, már tudod milyen a szájról szájra adott információk torzulása. Ugyanez történik az egymás közt félre/rosszul kommunikáló kivitelezők között is, így nem biztos, hogy az készül el, amit terveztél. Ezért kell biztosítanod, hogy a hatékony információátadás 100%-ban megtörténjen. Ahhoz, hogy ez megvalósulhasson, <strong>elengedhetetlen a teljes képre való rálátásod, hogy az irányításoddal végül mindenki a célodhoz juttasson közelebb.</strong></p>
		</div>
	</div>
</section>
	<div class="section-separator_right_bottom section_key-of-success">
		<?php picture('orange-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		<div class="skew-separator"></div>
	</div>
<section id="process" class="relative-and-z-index">
	<div class="container">
		<h2 class="section-title"><span class="text-gradient-bg">9 LÉPÉS,</span> ami garantálja az eredményeidet</h2>
		<hr class="gradient-separator-short">
		<div class="lesser-width">
			<ol>
				<li><strong>Üzleti alapok meglétének felmérése</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-dark.svg" alt="Arrow Dark"  class="lazyload arrow"> <span>Nem érnek kellemetlen meglepetések</span></li>
				<li><strong>Ügyfélprofil és ideális ügyfél életút meghatározása</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-dark.svg" alt="Arrow Dark"  class="lazyload arrow"> <span>Olvasni fogod a célpiacod gondolatait</span></li>
				<li><strong>A startupoktól átemelt ötlet életképesség kiértékelési technika</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-dark.svg" alt="Arrow Dark"  class="lazyload arrow"> <span>Még azelőtt meggyőződhetsz ötleted életképességéről, mielőtt vagyonokat ölnél bele</span></li>
				<li><strong>Tartalomkészítés</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-dark.svg" alt="Arrow Dark"  class="lazyload arrow"> <span>Úgy leszel képes átadni az ügyfeleknek a gondolataidat, hogy imádni fogják</span></li>
				<li><strong>A szükséges legminimálisabb, de már ügyfelek számára értéket adó első körös fejlesztési specifikáció megalkotása</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-dark.svg" alt="Arrow Dark"  class="lazyload arrow"> <span>Nem fogsz senki által használt megoldásokért pénzt fizetni</span></li>
				<li><strong>Drótváz/design/prototípus készítés</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-dark.svg" alt="Arrow Dark"  class="lazyload arrow"> <span>Még időben képes leszel módosításokat eszközölni, költséghatékonyan</span></li>
				<li><strong>Stabil technikai alapok</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-dark.svg" alt="Arrow Dark"  class="lazyload arrow"> <span>Nem az oldalad megoldása után fog csak kiderülni, hogy több sebből vérzik a rendszered</span></li>
				<li><strong>Gyors felhasználói visszajelzésgyűjtés és az eredményeken alapuló fejlesztés</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-dark.svg" alt="Arrow Dark"  class="lazyload arrow"> <span>Napról-napra egyre jobban fognak szeretni az ügyfeleid, amiért leginkább a pénztárcád lesz hálás</span></li>
				<li><strong>Minőségkontroll</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-dark.svg" alt="Arrow Dark"  class="lazyload arrow"> <span>Nem hagyjuk, hogy weboldalad megállíthatatlan hízókúrába kezdejen és hibáktól hemzsegjen</span></li>
			</ol>
		</div>
	</div>
	<div class="call-to-action align-center">
		<a href="#choose-your-path" class="btn-gradient">MEGRENDELEM A KÖNYVET</a>
	</div>
</section>
	<div class="section-separator_mirror_bottom section_mission">
		<div class="skew-separator"></div>
	</div>
<section id="many-question">
	<div class="container">
		<h2 class="section-title hide-on-desktop">Minden kérdésedre <span class="text-gradient-bg">VÁLASZT KAPSZ</span></h2>
		<h2 class="section-title hide-on-mobile">Minden kérdésedre <span class="text-gradient-bg">VÁLASZT KAPSZ</span></h2>
		<hr class="gradient-separator-short">
	<div class="row">
		<div class="col-lg-6">
			<div class="col-lg-12 col-12 set-of-answers">
				<div class="accordion">
                	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
                	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
                	<h5 class="less-line-height question">Milyen kérdésekre kapsz választ a könyv által?</h5>
                </div>
                <div class="panel answers">
					<p class="less-line-height">- Mennyibe kerül egy minőségi oldal?</p>
					<p class="less-line-height">- Jó ötlet-e, ha te magad csinálod a weboldalad vagy bízd inkább profikra?</p>
					<p class="less-line-height">- Mennyiért dolgozik egy jó kivitelező?</p>
					<p class="less-line-height">- Mik a feladataid, hogy sikeresen támogasd a kivitelezőid munkáját?</p>
					<p class="less-line-height">- Milyen projektbuktatókra kell figyelned?</p>
					<p class="less-line-height">- Melyek a weboldalakkal kapcsolatos leggyakoribb tévhitek?</p>
					<p class="less-line-height">- Honnan lesznek vevőid?</p>
					<p class="less-line-height">- Hogyan mérd a weboldalad eredményességét?</p>
					<p class="less-line-height">- Hogyan csinálj az érdeklődőkből vevőket?</p>
                </div>
			</div>
			<div class="col-lg-12 col-12 set-of-answers">
				<div class="accordion">
                	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
                	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
                	<h5 class="less-line-height question">Mivel támogatja a könyv a sikereidet?</h5>
                </div>
                <div class="panel answers">
                    <p class="less-line-height">- Bárki által hasznosítható <strong class="bold-only">növekedésorientált módszertan</strong></p>
					<p class="less-line-height">- <strong class="bold-only">Adatvezérelt,</strong> tesztalapú <strong class="bold-only">marketingszemlélet</strong></p>
					<p class="less-line-height">- Hosszútávú, teljes ügyfél életutat lefedő, eredményközpontú gondolkodásmód, hogy <strong class="bold-only">kiaknázhasd üzleted rejtett lehetőségeit</strong></p>
					<p class="less-line-height">- A weboldal-projekt kulcsfázisainak megismerése, a <strong class="bold-only">hatékony közös munkáért</strong></p>
					<p class="less-line-height">- Kritikus mutatószámok beazonosítása a tudatos haladás és a <strong class="bold-only">kellemetlen meglepetések elkerülése</strong> érdekében</p>
					<p class="less-line-height">- <strong class="bold-only">Nagyvállalati szintű tervezési és projektmenedzsment praktikák</strong> KKV-kra szabva</p>
					<p class="less-line-height">- Leggyakoribb <strong class="bold-only">buktatók szisztematikus kiszűrése</strong> a veszteség minimalizálásáért</p>
					<p class="less-line-height">- A hosszútávon <strong class="bold-only">kiemelkedő ügyfélélményhez</strong> szükséges technikai kritériumok ismerete</p>
					<p class="less-line-height">- <strong class="bold-only">Költségkímélő</strong> - gyakorlati hasznot hozó funkciókra szorítkozó - fejlesztési specifikáció összerakása</p>
					<p class="less-line-height">- Tippek weboldalad folyamatos fejlesztésére- és <strong class="bold-only">minőségkontroll</strong> alatt tartására, a konzisztensen színvonalas ügyfélélmény érdekében</p>
                </div>
			</div>
			<div class="col-lg-12 col-12 set-of-answers">
				<div class="accordion">
                	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
                	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
                	<h5 class="less-line-height question">Hogyan használd a könyvet?</h5>
                </div>
                <div class="panel answers">
					<p class="less-line-height">A könyv egyfajta útikalauznak készült, amit bármikor elővehetsz és a megfelelő oldalon felütve támpontot kaphatsz a téged éppen érintő online vállalkozási kihívásokkal kapcsolatban. Javaslom, ne úgy tekints rá, mint egy egyszeri olvasás után feledésbe merülő kiadványra. Sokkal inkább úgy, mint egy tanácsadóra, akire mindig számíthatsz a hogyan továbbal kapcsolatban.</p>
                </div>
			</div>
			<div class="col-lg-12 col-12 set-of-answers">
				<div class="accordion">
                	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
                	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
                	<h5 class="less-line-height question">Mit profitálhatsz a könyv elolvasásából?</h5>
                </div>
                <div class="panel answers">
                   <p class="less-line-height">A könyv a legtöbb tévúttól akkor tud megkímélni, ha még csak most indítod online vállalkozásodat. Általa ugyanis választ kaphatsz az indítás sikerességéhez szükséges kritikus alapvetések meghatározásához.</p>
					<p class="less-line-height">Ha már régi motorosnak számítasz online, úgy az új legjobb barátod lehet.  Segít téged egy külső szemszöget behozva átvilágítani, mi az amit eddig jól csináltál és min kellene javítanod.</p>
                </div>
			</div>
			<div class="col-lg-12 col-12 set-of-answers">
				<div class="accordion">
                	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
                	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
                	<h5 class="less-line-height question">Kinek íródott a könyv?</h5>
                </div>
                <div class="panel answers">
                    <p class="less-line-height less-margin-bottom">- <strong class="bold-only">Online vállalkozni vágyóknak</strong></p>
						<p class="less-line-height">Valószínűleg te nyerhetsz a legtöbbet a könyv elolvasásával, mert nemcsak átfogó képet kapsz egy sikeres weboldal projekt kivitelezésének kulcsmozzanatairól, hanem az itt megismert mérésalapú szemléletet a vállalkozás egészére alkalmazva nagyon sok kellemetlen pillanattól kímélheted meg magad. Ha még csak most tervezgetsz belekóstolni az online vállalkozás világába, akkor megismerheted a legmagasabb sztenderdeket, ami által a jövőben maximalizálhatod sikered esélyét. Ha pedig már indulni kész vagy, akkor gyorsítósávon juthatsz el kitűzött céljaidig.</p>
						<p class="less-line-height less-margin-bottom">- <strong class="bold-only">Jelenlegi online vállalkozóknak, akik szeretnék újraértelmezni online megjelenésüket</strong></p>
						<p class="less-line-height">Remélhetőleg lesz majd néhány aha-élményt kiváltó, nagy felismerésed az olvasás során, ami miatt azt kívánod, hogy bárcsak hamarabb került volna a könyv a kezed ügyébe. Lehetőséged nyílik rendszerszinten átlátni, hogyan kapcsolódnak a weboldal mint értékesítési gépezet elemei egymáshoz. Mielőtt elcsüggednél azon, hogy mennyi pénzt vesztegettél el eddig, ne feledd, hogy a legjobb idő valamin változtatni a múlthoz képest mindig a mai napon van. A korábbi hibáidból tanulva, a tevékenységed adatait kielemezve, új projekted esetén már jókora lendülettel tudsz nekiveselkedni a következő felvonásnak.</p>
						<p class="less-line-height less-margin-bottom">- <strong class="bold-only">Weboldalak kivitelezőinek</strong></p>
						<p class="less-line-height">Mi kivitelezők sokszor nehéz helyzetben vagyunk, hogy hogyan kommunikáljuk megfelelően munkánk mibenlétét és az előttünk álló feladatok komplexitását. Sokszor azért nem tudunk akkora értéket nyújtani megrendelőinknek, mert sajnos nem fér bele a projekt anyagi kereteibe, hogy szakkönyv mélységű tájékoztatást nyújtsunk. Bízom benne, hogy minél több megrendelő olvassa el a könyvet, annál inkább felértékelődik az általunk végzett munka, és talán nektek is tudok még újdonságot mondani, amivel képesek lesztek az értékteremtést fokozni ügyfeleitek számára.</p>
                </div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="col-lg-12 col-12 set-of-answers">
				<div class="accordion">
                	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
                	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
                	<h5 class="less-line-height question">Milyen folyamatleírásokat tartalmaz a könyv?</h5>
                </div>
                <div class="panel answers">
                    <p class="less-line-height">- Önellenőrző kérdések weboldal indításhoz való felkészültség felmérésére</p>
					<p class="less-line-height">- Weboldal készítés előtti tervezési lépések</p>
					<p class="less-line-height">- Kérdések, amiket fel kell tenned mielőtt kivitelezőt választanál</p>
					<p class="less-line-height">- Elvégzendő feladatok a weboldal projekt egyes életszakaszaiban</p>
					<p class="less-line-height">- Csúszást okozó tényező</p>
					<p class="less-line-height">- Weboldalt drágító elemek</p>
					<p class="less-line-height">- Üzleti alapozó kérdések, amelyeket muszáj megválaszolnod</p>
					<p class="less-line-height">- Eszközök, amiket be kell vetned az eredményességed mérésére</p>
					<p class="less-line-height">- Termék/szolgáltatás piaci igényfelmérési lépéssorozat</p>
                </div>
			</div>
			<div class="col-lg-12 col-12 set-of-answers">
				<div class="accordion">
                	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
                	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
                	<h5 class="less-line-height question">Mikor lesz igazán hasznos számodra ez a könyv?</h5>
                </div>
                <div class="panel answers">
                    <p class="less-line-height">- Kész vagy a hosszútávú, folyamatos munkára, mert tudod, hogy mindig fejlődni kell</p>
					<p class="less-line-height">- A vásárlóidra és az üzlet építésére akarsz fókuszálni</p>
					<p class="less-line-height">- Mindig azon vagy, hogy elgördítsd a köveket a haladás útjából, hiszen te vagy a vállalkozás lelke</p>
					<p class="less-line-height">- Nem akarod hogy feleslegesen follyanak el óriási összegek, anélkül hogy valaha is megtérülnének</p>
					<p class="less-line-height">- A 80/20-as szabály a mindened, és folyton azt keresed, hogy mi a lényeg egy adott dologban</p>
					<p class="less-line-height">- Imádod a csekklistákat és kontroll alatt akarod tudni a dolgokat</p>
					<p class="less-line-height">- Nem karod 1000+1 helyről összeszedni az infokat</p>
                </div>
			</div>
			<div id="how-it-works" class="col-lg-12 col-12 set-of-answers">
				<div class="accordion">
                	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
                	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
                	<h5 class="less-line-height question">Miről szól a könyv és miről nem?</h5>
                </div>
                <div class="panel answers">
                    <div class="answers-text-box hide">
						<p class="less-line-height"><strong class="bold-only">A könyv azért íródott, hogy a projektben érintett minden fél (megrendelő és kivitelezők) számára útmutatóként szolgáljon. </strong>Szó esik arról is, hogyan találd meg a számodra ideális kivitelező partnereket, és hogyan győződj meg arról, hogy valóban az elvárásoknak megfelelően teljesültek-e a rájuk bízott feladatok.</p>
						<p class="less-line-height">Amiről pedig nem szól: Hiába várod, hogy a megvalósítás technikai részleteiben elvessz, a lényegre fókuszálva igyekszem biztosítani, hogy a projekted ne jusson vakvágányra. Kitérünk pár vállalkozással kapcsolatos alapvetésre és a projektmenedzsmentről is elhangzik minden olyan, ami számodra fontos lehet, mert ezek ismerete nélkül nem lesz eredményes a végül megvalósuló weboldal sem.</p>
					</div>
                </div>
				
			</div>
<!--			<div class="col-lg-12 col-12 set-of-answers">-->
<!--				<div class="col-lg-1 col-3 question-img">-->
<!--					--><?php //picture('plus-mark', 'png', '',  true, 'plus-mark'); ?>
<!--				</div>-->
<!--				<div class="col-lg-11 answers">-->
<!--					<h5 class="less-line-height question">Mit csinál egy Growth Master?</h5>-->
<!--					<div class="answers-text-box hide">-->
<!--						<p class="less-line-height">Egyik képzési kitérőm a Growth Hackers University-n megszerzett Growth Master titulus volt. Mitől is lesz valaki Growth Master, a növekedés mestere? </p>-->
<!--						<p class="less-line-height">Korábbi munkatapasztalaim révén már betekinthettem a startupoknál 2 alkalmazott gyors növekedést lehetővé tévő technikákba. Olyan módszerekről van szó, amelyekkel rövid idő alatt képes vagy skálázni a vállalkozásodat és képes lehetsz eredményesen betörni célpiacodra. Ezekkel lehetséges biztosítani, hogy a legkisebb energia, idő- és pénz befektetésével a lehető legnagyobb eredményeket érd el adott idő alatt a vállalkozásod számára.</p>-->
<!--						<p class="less-line-height">Feltételezem, benned is felébredt a kíváncsiság, hogy megismerd ezeket a praktikákat. Ahogyan most te is, én is hajthatatlan vágyat éreztem, hogy még többet tudjak meg erről a területről, amire sokan a marketing következő szintjeként tekintenek, ezért fontosnak véltem, hogy beleássam magam a témába. Az ezen a téren megszerzett ismereteimbe is beavatlak. </p>-->
<!--						<p class="less-line-height">A Growth Master nemcsak a weboldaladon belüli vásárlások arányát optimalizálja, hanem már a weboldalra érkező forgalom hatékony megszerzésével is foglalkozik. Ezzel a tudással felvértezve folyamatosan valós adatokra épülő továbblépési opciókat tárok eléd, hogy ne a sötétben kelljen tapogatóznod, amikor vállalkozásod lehetséges fejlesztési pontjait kell mérlegelned.</p>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
			<div class="col-lg-12 col-12 set-of-answers">
				<div class="accordion">
                	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
                	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
                	<h5 class="less-line-height question">Hogyan működik az elégedettségi garancia?*</h5>
                </div>
                <div class="panel answers">
                    
						<p class="less-line-height">Az elégedettségi garancia a fizikai könyvre vonatkozik, aminek 5990 Ft az ára és visszaküldés esetén érvényes. Ekkor a könyv teljes árát, illetve a visszaküldés szállítási díját is visszatérítjük. A fizikai könyv + e-book, illetve könyv + landing csomagoknál visszaküldéskor megszűnik az e-book hozzáférés is. A csak e-book-ot tartalmazó csomag árát nem tudjuk visszatéríteni, mivel a letölthető könyvet már nem tudjuk visszakérni. A könyv + landing csomagban 5990 Ft-ot + a visszaküldési díját térítjük meg.</p>
						<p class="less-line-height">Mivel a fejlődés hívei vagyunk, ezért arra kérünk, hogy ha a visszatérítés mellett döntenél, írd le mi volt a visszatérítés oka röviden, hogy tudjunk javítani a könyvön.</p>
					
                </div>
			</div>
		</div>
	</div>
		<div class="call-to-action align-center">
			<a href="#choose-your-path" class="btn-gradient">MEGRENDELEM A KÖNYVET</a>
		</div>
	</div>
</section>
<div class="section-separator section_choose-your-path">
	<?php picture('orange-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<section id="choose-your-path">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/choose-your-path-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-top-red.svg" alt="Pattern Top White"  class="lazyload pattern-top_red hide-on-mobile">
	<div class="container relative-and-z-index">
		<h2 class="section-title dark-bg"><span class="text-purple text-gradient-bg">MELYIK UTAT VÁLASZTOD</span><br>a sikerhez?</h2>
		<hr class="separator-short_white">
		<div class="row">
			<div class="col-lg-4 col-md-4 offer-box relative-and-z-index">
				<p class="offer-title align-center less-line-height">Könyv <br>+ e-book</p>
				<hr class="gradient-separator-short">
				<p class="offer-price align-center">5.990 <span>Ft</span></p>
				<p class="align-center free-shipping">ingyenes szállítás!</p>
				<ul>
					<li><strong class="bold-only">320 oldalas fizikai könyv</strong></li>
					<li><strong class="bold-only">E-book formátumú könyv</strong></li>
					<li><strong class="bold-only">Bónusz:</strong> weboldal árkalkulátor hozzáférés
						<p class="pack-item-description">Ha mindig is szeretted volna megtudni miből is áll össze egy weblap fejlesztési költsége, mi a különbség az olcsó és a profi weblap között, most végre megtudhatod.</p>
					</li>
					<li><strong class="bold-only">Bónusz:</strong> weboldal profitkalkulátor hozzáférés
						<p class="pack-item-description">Nem mindegy, hogy hogyan konvertál oldalad, mert ezen múlhat céged sorsa. Ha nincs profit, hamar elvérezhetsz. Megtérülési kalkulátorunkkal kiszámolhatod, mikortól lesz megtérülő számodra a vállalkozás.</p>
					</li>
					<li><strong class="bold-only">Bónusz:</strong> Üzleti Tervező Vászon</li>
					<li><strong class="bold-only">Bónusz:</strong> 50 kérdéses felkészültség ellenőrző kérdőív</li>
				</ul>
				<div class="call-to-action align-center">
					<a href="<?php echo site_url();?>/kosar/?add-to-cart=105" class="btn-gradient">MEGRENDELEM</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 offer-box best-offer relative-and-z-index">
				<p class="best-offer-title"><span>★</span> LEGNÉPSZERŰBB</p>
				<p class="offer-title align-center less-line-height">Könyv <br>+ landing sablon</p>
				<hr class="gradient-separator-short">
				<p class="offer-price align-center">8.490 <span>Ft</span></p>
				<p class="align-center free-shipping">ingyenes szállítás!</p>
				<ul>
					<li><strong class="bold-only">320 oldalas fizikai könyv</strong></li>
					<li><strong class="bold-only">Landing sablon</strong>
						<p class="pack-item-description">Mintaszövegekkel előre megírt tervrajz a kimelkedően konvertáló értékesítési oldalakhoz. Nemcsak ötletet kapsz a saját szövegeid megírásához és megismered a bevethető bizalomnövelő elemeket, de a mögöttes pszichológiát is elmagyarázzuk.</p></li>
					<li><strong class="bold-only">E-book formátumú könyv</strong></li>
					<li><strong class="bold-only">Bónusz:</strong> weboldal árkalkulátor hozzáférés</li>
					<li><strong class="bold-only">Bónusz:</strong> weboldal profitkalkulátor hozzáférés</li>
					<li><strong class="bold-only">Bónusz:</strong> Üzleti Tervező Vászon</li>
					<li><strong class="bold-only">Bónusz:</strong> 50 kérdéses felkészültség ellenőrző kérdőív</li>
				</ul>
				<div class="call-to-action align-center">
					<a href="<?php echo site_url();?>/kosar/?add-to-cart=242" class="btn-gradient">MEGRENDELEM</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 offer-box">
				<p class="offer-title align-center less-line-height">E-book</p>
				<hr class="gradient-separator-short">
				<p class="offer-price align-center">3.990 <span>Ft</span></p>
				<ul>
					<li><strong class="bold-only">E-book formátumú könyv</strong><p class="pack-item-description">Vásárlás után <b>azonnal</b> elérhető 3 formátumban (PDF, Kindle/mobi, epub), örökös hozzáférés az újabb verziókhoz</p></li>
					<li><strong class="bold-only">Bónusz:</strong> Üzleti Tervező Vászon
						<p class="pack-item-description">Kompakt, egy oldalas üzleti terv minta, amivel pillanatok alatt átláthatod vállalkozásod kulcsterületeit.</p>
					</li>
					<li><strong class="bold-only">Bónusz:</strong> 50 kérdéses felkészültség ellenőrző kérdőív
						<p class="pack-item-description">Ha szeretnél meggyőződni arról, hogy mindent körültekintően átgondoltál weboldaladdal kapcsolatban, kérdőívünkkel könnyen bizonyosságot szerezhetsz róla.</p>
					</li>
				</ul>
				<div class="call-to-action align-center">
					<a href="<?php echo site_url();?>/kosar/?add-to-cart=209" class="btn-gradient">MEGRENDELEM</a>
				</div>
			</div>
		</div>
		<div class="simplepay-box">
			<p style="color: white;">Bankkártyás fizetés a SimplePay felületén keresztül</p>>
			<a href="http://simplepartner.hu/PaymentService/Fizetesi_tajekoztato.pdf" target="_blank"><?php picture('simplepay', 'png', '',  true, 'simplepay-logo'); ?></a>
		</div>
		<div class="row less-width offer-testimonial">
			<div class="col-lg-6 testimonial-box">
				<p class="align-left text-normal italic">"Olyan könyv, amire a piac már évek óta várt. Pótolhatatlan mankó lesz, ha online üzletet szeretnétek felépíteni."</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-dave-wolf', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Farkas-Balog Dávid</h4>
						<p class="qualification">online marketing szakértő</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="align-left text-normal italic">"Enélkül senkinek sem volna szabad egyetlen mozdulatot sem tenni az online üzletfejlesztések világában!”</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-kamilla-akhmin', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Kamilla Akhmin</h4>
						<p class="qualification">GoldHair hajhosszabbítás projekt</p>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="row project-map">
			<div class="col-lg-3 align-right">
				<?php picture('weboldal-projekt-terkep-konyvhoz-a4', 'jpeg', '',  true, 'trello-card'); ?>
			</div>
			<div class="col-lg-9">
				<p class="dark-bg less-width">Ha a vásárlástól számított 1 hónapon belül elolvasod a könyvet és véleményt hagysz az info@dxlabz.hu címen, <strong class="bold-only">egy A4-es, PDF formátumú projekttérképpel ajándékozunk meg,</strong> ami mindig mutatja majd, hogy weboldalad adott életszakaszában milyen feladatok várnak rád!</p>
			</div>
		</div> -->


	</div>
</section>
<div class="section-separator_mirror_bottom section_testimonials">
	<?php picture('orange-gradient-effect-right_mirror', 'png', '',  true, 'gradient-effect-right_mirror'); ?>
	<div class="skew-separator"></div>
</div>
<section id="testimonials">
	<div class="container">
		<h2 class="section-title">A piacvezetők bizalmára is <br>sikerült <span class="text-gradient-bg">rászolgálnunk</span></h2>
		<hr class="gradient-separator-short">
		<p class="align-center extra-margin_bottom"><strong class="bold-only">Megbecsült ügyfeleink listáját öregbítik többek között az Optimonk, a BrandBirds, Domán Zsolt és az Édesburgonya.bio is, akik mind-mind piacvezetők a saját területükön.</strong></p>
		<div class="row">
			<div class="col-lg-6 testimonial-box">
				<p class="italic">"Gábor személyes konzultációra jelentkezett hozzám, így kerültem kapcsolatba vele. A tanácsadás során egy <strong class="bold-only">szakmailag egyedülállóan felkészült</strong>, stabil értékrenddel rendelkező korrekt személyt ismerhettem meg benne.</p>
				<p class="italic">Csapatával elsősorban egyedi weboldalak készítésével és gyorsabbá tételével foglalkoznak. Nekem is segített Gábor a domarketing.hu sebességoptimalizálásában.</p>
				<p class="italic">Sőt! Amikor a Klub nyitókampányában behalt az eredeti tárhely, Gábor hatalmas erőfeszítések árán, 24 óra alatt átköltöztette egy sokkal gyorsabb, jobb tárhelyre a weboldalt.<strong class="bold-only"> Nagyon elégedett voltam a közös munkával, jó szívvel ajánlom!"</strong></p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-doman-zsolt', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Domán Zsolt</h4>
						<p class="qualification">DO! Marketing</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“2 évvel ezelőtt volt először tanácsadáson nálunk Gábor, és már akkor szimpatikus volt a szemlélete, hozzáállása. Május elején lecsapott a "mini tanácsadás" szolgáltatásomra, és immár a saját márkáját dobta be, hogy nézzem át. Nagyon megtetszett amit ő művel, a brutális szakértelem és emberi megfogalmazás. Kértem tőle egy konverzió optimalizálási elemzést a honlapunkra, ami überhasznos volt. Gábor <strong class="bold-only">mindent tud a témájában, évekkel előbbre jár a konkurenciánál.</strong></p>
				<p class="italic">Köszönöm Gábor ezt a vérprofi szakmai munkát és hogy megosztottad a tudásod (alapjait, ami nekünk hónapokra elég...) velünk.”</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-meszaros-robi', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Mészáros Robi</h4>
						<p class="qualification">BrandBirds</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“A DX Labz Kft.-t egy jó ismerősöm ajánlotta, mert nagyon esedékessé vált a Tudástár Klub oldalon tapasztalható lassulási gondok és egyéb felmerült problémák megoldása. <strong class="bold-only"> Nagyon elégedett vagyok a munkátokkal, mindig összeszedett, érthető, segítőkész válaszokat kaptam, és a felmerült hibákat is szakszerűen, gyorsan oldottátok meg.</strong> A jövőben is számítok az együttműködésünkre és szívesen fordulok hozzátok WordPress támogatásért.”</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-szendrei-adam', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Szendrei Ádám</h4>
						<p class="qualification">Tudástár Klub</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“Azonnal felkeltették a figyelmemet, nem is haboztam egyből ajánlatot kértem. Azóta is együtt dolgozunk, profi kivitelezést, dinamikus és könnyen érthető egyenes kommunikációt kaptam a csapattól, most készítik a második weblapomat, de lesz harmadik is. <strong class="bold-only"> Bonyolult technikai problémák esetén is tudják mi a teendő.</strong></p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-kovacs-andras', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Kovács András</h4>
						<p class="qualification">Édesburgonya.bio</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- <section id="contact" class="newsletter">
	<div class="gradient-purple"></div>
	<div class="container relative-and-z-index">
			<h2 class="section-title dark-bg less-line-height"><span class="text-purple text-gradient-bg">IRATKOZZ FEL</span> és azonnal értesítünk a könyv elérhetőségéről!</h2>
			<hr class="gradient-separator-short">
			<div class="less-width">
				<p class="dark-bg less-line-height align-center">Legyél az elsők között, akik rálépnek az <strong>Online Gyorsítósávra</strong> és kerülj olyan tudás birtokába, amiről a vállalkozások 99%-a sosem fog hallani. Ez az egyetlen a-tól z-ig <strong>online sikerkalauz,</strong> amiben biztosan nem fogsz csalódni. Nem csak ahhoz a tudáshoz fog hozzáférni, amit több mint 10 év nemzetközi piacon szerzett tapasztalatával megszereztem, hanem olyan folyamatot kapsz, amit követve <strong>megsokszorozod az eredményeidet.</strong></p>
				<p class="dark-bg less-line-height align-center">Amennyiben az elsők között rendeled meg a könyvet értékes bónuszok várnak majd.</p>
				<p class="dark-bg less-line-height extra-margin-bottom align-center"><strong class="bold-only">Váltsd valóra álmaidat, gazdagítsd te is a sikeres online vállalkozók táborát.</strong></p>
				<div id="mmform200846" class="mmform" style="position:relative;padding-bottom:300px;height:0;overflow:hidden;max-width:100%;margin-top: -30px;"></div>
				<script type="text/javascript">
				var uniqueId = 2008461587468660,listId = 104653,formId = 200846,iwidth=115,mmDomain='salesautopilot.s3.amazonaws.com',secondMMDomain='sw.marketingszoftverek.hu',spancont=false,secure=('https:' == document.location.protocol)?true:false;
				</script>
				<script type="text/javascript" src="https://d1ursyhqs5x9h1.cloudfront.net/sw/scripts/mm-embed-iframe-1.15.min.js">

					function setPlaceholder() {
  					document.getElementById("mssys_fullname").setAttribute("placeholder", "Név");
					document.getElementById("email").setAttribute("placeholder", "Email");
					};
					window.onload = setPlaceholder;

				</script>
			</div>

	</div>
</section> -->
<?php
get_footer();
//get_template_part( 'template-parts/footer', 'without-nav' );
//?>
