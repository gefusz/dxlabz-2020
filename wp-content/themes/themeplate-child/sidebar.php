
<?php if (is_user_logged_in()) { ?>
<div id="sidebar" class="col-lg-4">
	<?php
		$has_link =	false;
		$link_counter = 1; ?>
			<?php while ( $link_counter < 4 ) : ?>
			
			<?php
				if ( get_field('link' . $link_counter) ) { 
					
					$has_link =	true;
			 	} 

				$link_counter++; 

			 endwhile; 
	?>

	<?php 
		if ( $has_link ) { ?>
			<div class="title-box">
				<h3>Kapcsolódó tartalmak</h3>
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/title-box-pattern.svg" alt="title box pattern" data-no-lazy="1" class="title-box-pattern">
			</div>
			<div class="interaction-box">
				<ul>
					<?php $link_counter = 1; ?>
					<?php while ( $link_counter < 4 ) : ?>
					
					<?php
						if ( get_field('link' . $link_counter) ) { 
							
							$image = get_field('link' . $link_counter .'_icon'); ?>
							<li>
								<a target="blank" href="<?php the_field('link' . $link_counter); ?>">
									<?php the_field('link' . $link_counter . '_title'); ?>
								</a>
								<?php 
									$size = 'full';
									echo wp_get_attachment_image( $image, $size );
									
								?>
							</li>
					<?php } 

						$link_counter++; 
					?>

					<?php endwhile; ?>
				</ul>
			</div>
	<?php	} ?>
	
	


<div class="accordion title-box">
	<h3>Tartalmaim</h3>
	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark-white.png" alt="arrow"/>
	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark-white.png" alt="arrow"/>
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/title-box-pattern.svg" alt="title box pattern" data-no-lazy="1" class="title-box-pattern">
</div>
<div class="panel">
	<?php
      wp_nav_menu( array( 
          'theme_location' => 'my-content-menu', 
          'container_class' => 'menu-main-menu-container' ) ); 
    ?>
</div>
</div>
<?php } ?>