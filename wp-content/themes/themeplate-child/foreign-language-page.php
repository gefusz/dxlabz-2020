<?php

/**
 * Template Name: Idegen nyelvű oldal
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>


<section id="hero" class="front-page">
	<div class="hero-gradient"></div>
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-wide-orange.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 text">
				<h1>WEBPAGE <br><span>INCUBATION</span></h1>
				<p class="idea">FROM IDEA TO PROFIT</p>
				<h2>A webpage building formula getting 10x results for those who seek guaranteed success!<div class="gradient-text-bg"></div></h2>
				<p>We take you from business planning all the way to profitable sales on the fast lane, in an accountable and transparent way, ensuring that instead of turning into a nightmare, entrepreneurship will make your dreams come true.</p>
				<div class="call-to-action">
					<a href="#successfull" class="btn-gradient">SHOW ME HOW IT WORKS!</a>
				</div>
			</div>
			<div class="col-lg-6 image">
				<div class="calculator">
                <div class="col8 white-side">
                    <div class="inputfields-box">
                        <ul>
                            <li>
                                <label for="range1">Efficiency of your ads</label>
                                <div class="input-range"><span>Bad</span><input type="range" name="range1" id="range1" min="0" max="1.5" value="0" step="0.15"><span>Excellent</span></div>
                            </li>
                            <li>
                                <label for="range2">Page load speed</label>
                                <div class="input-range"><span>Bad</span><input type="range" name="range2" id="range2" min="0" max="1.5" value="0" step="0.15"><span>Excellent</span></div>
                            </li>
                            <li>
                                <label for="range5">Written content</label>
                                <div class="input-range"><span>Bad</span><input type="range" name="range3" id="range3" min="0" max="1.5" value="0" step="0.15"><span>Excellent</span></div>
                            </li>
                            <li>
                                <label for="range3">Visual appearance</label>
                                <div class="input-range"><span>Bad</span><input type="range" name="range4" id="range4" min="0" max="1.5" value="0" step="0.15"><span>Excellent</span></div>
                            </li>
                            <li>
                                <label for="range4">Practicability</label>
                                <div class="input-range"><span>Bad</span><input type="range" name="range5" id="range5" min="0" max="1.5" value="0" step="0.15"><span>Excellent</span></div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col8 blue-side">
                    <div class="inputfields-box">
                        <div class="group">
                        	<label for="visitors">Visitors/month - Monthly visitors</label>
                        	<div class="input-box"><input type="number" name="visitors" id="visitors" value="10000"></div>
						</div>

						<div class="group">
							<label for="productprice">Average customer value</label>
							<div class="input-box"><input type="number" name="productprice" id="productprice" value="5000"></div>
						</div>

						<div class="result-box">
							<div class="group">
                            	<p class="title">Conversion rate</p>
                            	<p class="result"><span id="result"></span>%</p>
							</div>
							<div class="group">
                            	<p class="title">Revenue/month - Monthly revenue</p>
                            	<p class="result2"><span id="result2"></span>e Ft</p>
							</div>
                        </div>
                    </div>
                </div>
	        </div>
				<!-- <?php picture('profitkalkulator', 'png', '',  true, 'calculator'); ?> -->
<!-- 				<div class="founder">
					<div class="col-lg-4 col-4 align-center">
						<?php picture('dx-labz-kriston-gabor', 'png', '',  true, 'gabor'); ?>
					</div>
					<div class="col-lg-8 col-8 align-left">
						<h4 class="name">KRISTON GÁBOR</h4>
						<p class="qualification">Az ország egyetlen<br>Google Mobil Web Specialistája</p>
					</div>
				</div> -->
				<p style="color: white;margin-top: 20px;font-weight: bold;">How will your website perform? </p>
			</div>
		</div>
	</div>
</section> <!-- end of header -->
<div class="section-separator">
	<?php picture('orange-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<section id="successfull" class="front-page">
	<div class="container relative-and-z-index">
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/target.svg" alt="Bullseye Arrow"  class="lazyload bullseye-arrow">
		<h2 class="section-title"><span class="text-gradient-bg">How to be successful</span><br> despite today's very demanding circumstances?</h2>
		<hr class="gradient-separator-short">
		<p class="align-center bigger-paragraph less-line-height lesser-width">Our company firmly believes that the only way to achieve outstanding results in the online world we live in today is through full transparency, and by working along common denominators and shared interests. We design and implement a long-term action plan that will help you to avoid the biggest pitfalls of many up-and-coming businesses. Our methods will also increase the results of your business month by month, in a measurable way.</p>
		<div class="row less-width">
			<div class="col-lg-12">
				<h3 class="align-left extra-margin_bottom less-line-height text-purple"><strong class="bold-only">WHAT MAKES THE DX LABZ FORMULA SPECIAL COMPARED TO OTHER SOLUTIONS:</strong></h3>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-frontpage-1', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/processor.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<h3 class="align-left less-margin-bottom less-margin-top text-purple"><strong class="bold-only">Page load speeds tuned to the max</strong></h3>
					<p class="align-left">With our speed optimized web page engine, <strong>you won’t lose any more visitors and you will be in Google’s good graces as well.</strong></p>
				</div>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-frontpage-2', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/workflow.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<h3 class="align-left less-margin-bottom less-margin-top text-purple"><strong class="bold-only">Streamlined workflow</strong></h3>
					<p class="align-left">By using our development process <strong>that’s improved to perfection, you can avoid running in circles and finally start generating real profit in the shortest time possible, without unnecessary expenses.</strong></p>
				</div>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-frontpage-3', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/knowledge.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<h3 class="align-left less-margin-bottom less-margin-top text-purple"><strong class="bold-only">Development plan based on actual feedback</strong></h3>
					<p class="align-left">With our test-driven methodology, <strong>you build your business from the ground up based on your customers' needs</strong>, so you don't just find out what they really want as you go.</p>
				</div>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-frontpage-4', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/insights.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<h3 class="align-left less-margin-bottom less-margin-top text-purple"><strong class="bold-only">Fully fledged analytical solutions</strong></h3>
					<p class="align-left">DX Labz’s unique analytical system <strong>will make you feel like you are reading the minds of your customers and always know how to please them.</strong></p>
				</div>
			</div>
			<div class="col-lg-12 row align-center">
				<div class="col-lg-2">
					<!-- <?php picture('secret-of-success-icon-frontpage-5', 'png', '',  true, 'secret-of-success-icon'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/award.svg" class="lazyload secret-of-success-icon">
				</div>
				<div class="col-lg-10">
					<h3 class="align-left less-margin-bottom less-margin-top text-purple"><strong class="bold-only">Result-focused project management</strong></h3>
					<p class="align-left">With our insightful project management solution, <strong>you'll always know which will be the most effective development step for the future.</strong></p>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="section-separator_mirror">
	<div class="skew-separator"></div>
</div>
<section id="enthusiastic" class="front-page">
	<div class="container relative-and-z-index">
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/quote-mark.svg" alt="Quote Mark"  class="lazyload quote-mark">
		<div class="row less-width">
			<div class="col-lg-6 testimonial-box">
				<p class="italic">Having comprehensive knowledge and skills about his field, Gábor is miles ahead of his competition!</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-meszaros-robi', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Mészáros Robi</h4>
						<p class="qualification">BrandBirds</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">Unrivalled professionalism!</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-doman-zsolt', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Domán Zsolt</h4>
						<p class="qualification">DO! Marketing</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</section>
	<div class="section-separator_right_top section_point-of-view">
		<?php picture('purple-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		<div class="skew-separator"></div>
	</div>
<section id="less-and-more" class="align-center">
	<div class="gradient-orange lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/less-and-more-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title section-title_smaller dark-bg">NO UNNECESSARY STEPS + MORE CLIENTS </h2>
		<h2 class="section-title section-title_smaller text-purple-bg">LESS STRESS AND ANXIETY + INCREASED INCOME</h2>
	</div>
</section>
	<div class="section-separator_right_bottom section_point-of-view">
		<div class="skew-separator"></div>
	</div>
<section id="point-of-view" class="front-page">
	<div class="container">
		<h2 class="section-title less-line-height">It’s time to get to <span class="text-gradient-bg">THE NEXT LEVEL!</span></h2>
		<hr class="gradient-separator-short">
		<div class="lesser-width">
			<p class="bigger-paragraph align-left">To be successful in today’s world, measuring up to the old standards is not enough anymore. We will show you the level you have to reach in order to be successful.</p>
			<hr class="separator-left">
			<p class="less-line-height">As entrepreneurs, we often sail on uncharted waters with countless risks, and most of us had to learn how to avoid those on our own. <strong>Most of the time a failed business is the result of unforeseen difficulties and lackluster planning.</strong> Conscious planning and measuring every result is critical, since in the absence of those our business can go bankrupt without us ever figuring out the reason.</p>

			<p class="less-line-height">On the other hand, if you track the numbers and systematically try to improve the customer experience, you will be able to get better numbers month by month and always see where you can make the biggest improvements in the near future.  A carefully planned business can have more than ten times the revenue of a poorly converting one.</p>

			<p class="less-line-height">Your business’s online presence is clearly critical when we are talking about the success of your enterprise. Yet, it is hard to grasp the essence of the deluge of information related to the many “hows”. We would like to remove this burden from your shoulders once and for all. Instead of another “magic pill”, we offer you a methodology you can use in other areas of your business that, if you follow consistently, will multiply your chances of long-term success.</p>

			<p class="less-line-height">As attractive as promises that offer great success in a short time are, real results can only be achieved through continuous work, focusing on clearly defined goals. That's where we want to be your partner. Every business is the embodiment of someone's dreams, and we work to turn those dreams into success stories, not nightmares.</p>

			<p class="less-line-height"><strong>Our goal with the growth methodology we have created is to help new, online businesses avoid the pitfalls and ramp up their business at a pace that allows them to successfully compete with, and even outperform, the old players. </strong></p>

			<p class="less-line-height">And for businesses that have been around for several years, we want to shift up a gear to secure their position against challengers and competition through continuous innovation and process optimisation.</p>

			<p class="less-line-height"><strong>Rather than simply carrying out the task entrusted to us, we have been taking an "ownership approach" from the start.</strong> By equating your success with ours, we check, before the project starts, whether there is sufficient demand for the asset/service that the company offers. We will identify the traffic and conversion targets that, if achieved, can make your business truly more than a dream and turn it into a stable money-making machine.</p>

			<p class="less-line-height">As a business that has been operating for 5 years, we have been through the same stages of development and we would be much more conscious of that journey today. In that time we have worked with many companies and have seen that they are great people with great motivations, but with a rather poor understanding of online business. We have always felt that we must take on the task of educating our clients about the opportunities available to maximise their chances of success, because for us, <strong>success is not the completion of a project, but if the client can actually build a long term, well-established business with our intervention.</strong></p>
		</div>
	</div>
</section>
<div class="section-separator section_progress">
	<?php picture('orange-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<section id="progress">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/choose-your-path-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-white.svg" alt="Pattern Top White"  class="lazyload pattern-top_white">
	<div class="container relative-and-z-index">
			<h2 class="section-title dark-bg"><span class="text-purple text-gradient-bg">THE PROCESS</span> that guarantees your success</h2>
			<hr class="gradient-separator-short">
		<div class="lesser-width dark-bg">
			<ol>
				<li class="dark-bg"><strong>Assessing the basics of your business</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>You won’t have to face unpleasant surprises</span></li>
				<li><strong>Determining customer profile and ideal customer lifepath</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>ou will feel like you are reading the mind of your target market</span></li>
				<li><strong>Idea viability evaluation technique adapted from startups</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>Make sure your idea works before you've invested a fortune</span></li>
				<li><strong>Content creation</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>You'll be able to convey your ideas to clients in a way they'll love</span></li>
				<li><strong>Creating a first round development specification, which requires the least amount of work, while already giving significant value to the customer</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>You won’t pay for solutions that no one will use</span></li>
				<li><strong>Creating wireframe/ design/ prototype</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>You will be able to make changes in time, in a cost-efficient way</span></li>
				<li><strong>Stable technical foundations</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>You won't find out that your system is bleeding from multiple wounds until after your site is built</span></li>
				<li><strong>Quick collection of user feedback and using that as a basis of later improvements</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>Your clients will love your more and more every day, and your income will go through the roof</span></li>
				<li><strong>Quality control</strong> <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/arrow-light.svg" alt="Arrow Light"  class="lazyload arrow"> <span>We won’t let your webpage to fatten on unnecessary data and and become riddled with errors</span></li>
			</ol>
		</div>
<!-- 		<div class="call-to-action align-center">
			<a href="<?php echo site_url(); ?>/#contact" class="btn-gradient">ERRE SZÜKSÉGEM VAN!</a>
		</div> -->
	</div>
</section>
	<div class="section-separator_right_mirror_bottom section_progress">
		<?php picture('orange-gradient-effect-right_mirror', 'png', '',  true, 'gradient-effect-right_mirror'); ?>
		<div class="skew-separator"></div>
	</div>
<section id="experience">
	<div class="container relative-and-z-index">
		<h2 class="section-title hide-on-mobile"><span class="text-gradient-bg">ENTERPRISE EXPERIENCE, </span><br> SME specific solutions</h2>
		<h2 class="section-title hide-on-desktop"><span class="text-gradient-bg">ENTERPRISE</span><span class="text-gradient-bg"> EXPERIENCE,<br></span> SME specific solutions</h2>
		<hr class="gradient-separator-short">
		<div class="row">
			<div class="col-lg-8 text">
				<p class="align-left less-line-height">What motivates us? During the past years we have gained valuable experience while working for the biggest digital agencies of the word (WPP/Possible) and the greatest software development company of the region (EPAM) as a software engineer. We were also working on multimillion dollar projects for one of the leading FMCG brands in an online marketing manager position and in a development director position for a startup company with a presence in over 160 countries  (Innonic/Optimonk). <strong>We felt obliged to make enterprise-level solutions available to SMEs</strong>, which gave birth to the idea of founding DX Labz.</p>
			</div>
			<div class="col-lg-4 align-center founder">
				<?php picture('dx-labz-kriston-gabor', 'png', '',  false, 'gabor'); ?>
				<h4 class="name">Kriston Gábor</h4>
				<p class="qualification">Google Web Specialist,</br>founder of DX Labz</p>
			</div>
		</div>
		<?php picture('webdeveloper-experiences', 'png', '',  true, 'webdeveloper-experiences extra-margin_top extra-margin_bottom'); ?>
		<p class="bigger-paragraph align-center">The first and only Google Web Specialist in Hungary<br>& Certified Growth Master</p>
		<div class="row">
			<div class="col-lg-6 certifications">
				<?php picture('google-mobil-web-specialist', 'png', '',  true, 'certificate'); ?>
			</div>
			<div class="col-lg-6 certifications">
				<?php picture('growth-master', 'png', '',  true, 'certificate'); ?>
			</div>
		</div>
	</div>
</section>
<div class="section-separator section_work-together">
	<?php picture('purple-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<!-- <section id="work-together">
	<div class="gradient-orange lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/time-spent-on-writing-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-white.svg" alt="Pattern Top White"  class="lazyload pattern-top_white">
	 <div class="container relative-and-z-index">
		<h2 class="section-title dark-bg less-line-height hide-on-desktop">Hogyan tudunk<br><span class="text-purple-bg_gradient">EGYÜTT DOLGOZNI?</span></h2>
		<h2 class="section-title dark-bg less-line-height hide-on-mobile">Hogyan tudunk <span class="text-purple-bg_gradient">EGYÜTT DOLGOZNI?</span></h2>
		<hr class="separator-short_purple">
		<div class="lesser-width">
			<p class="dark-bg less-line-height align-center bigger-paragraph extra-margin_top"><strong class="bold-only">Hiszünk benne, hogy akkor tudunk neked a legtöbbet segíteni, ha megvan a közös nevező. Írtunk egy könyvet arról, mit tartunk elérendő célnak, hogy <?php echo date("Y"); ?>-ben egy profitábilis weboldalad lehessen.</strong></p>
			<p class="dark-bg less-line-height align-center">Te döntesz, hogy számodra mennyire fontos saját projekted alakulása! Segítségével a gördülékeny csapatmunka mesterévé válhatsz!</p>
			<p class="dark-bg less-line-height align-center"><strong class="bold-only">Részedről ez 5990 Ft-os befektetést és pár óra időráfordítást igényel.</strong></p>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<?php picture('online-gyorsitosav-book-standing', 'png', '',  true, 'book'); ?>
			</div>
			<div class="col-lg-6 row guarantee">
				<div class="col-lg-4 col-5 align-center">
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/guarantee.svg" alt="Checkmark Star"  class="lazyload checkmark-star-circle">
				</div>
				<div class="col-lg-8 col-7">
					<h4 class="dark-bg"><strong>30 napos</strong></h4>
					<p class="uppercase dark-bg">ELÉGEDETTSÉGI GARANCIA!</p>
				</div>
				<div class="col-lg-12 col-12 text">
					<p class="dark-bg less-line-height">Olyannyira biztosak vagyunk a könyv árának tízszeres megtérülésében, hogy 30 napos pénzvisszafizetési garanciát vállalunk rá.</p>
				</div>

				<div class="col-lg-12 col-12 call-to-action align-left">
					<a href="<?php echo site_url();?>/online-gyorsitosav" class="btn-gradient_purple">MÉG TÖBB INFÓ A KÖNYVRŐL</a>
				</div>
			</div>
		</div>
		<div class="lesser-width">
			<p class="dark-bg less-line-height align-center bigger-paragraph extra-margin_top"><strong class="bold-only">TETSZIK A SZEMLÉLETÜNK? DOLGOZZUNK EGYÜTT!</strong></p>
			<p class="dark-bg less-line-height align-center">Ha a könyv elolvasását követően úgy látod, hogy szimpatikus a fejlesztési modellünk, <strong><a href="<?php echo site_url(); ?>/#contact">nyugodtan keress meg minket.</a></strong></p>
		</div>
	</div> 
</section> -->
<!-- <div class="section-separator_right_bottom section_mission">
	<?php picture('purple-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
	<div class="skew-separator"></div> -->
</div>
<section id="testimonials" class="front-page">
	<div class="gradient-purple"></div>
	<div class="container">
		<h2 class="section-title less-line-height"><span class="text-gradient-bg">How our customers see</span> DX Labz</h2>
		<hr class="gradient-separator-short">
		<p class="align-center extra-margin_bottom less-width bigger-paragraph">Our list of valued customers includes Optimonk, BrandBirds, Zsolt Domán and Édesburgonya.bio, all market leaders in their respective fields. They have been satisfied with our work, and we will not settle for anything less than making your project a multiple return on investment.</p>
		<div class="row">
			<div class="col-lg-6 testimonial-box">
				<p class="italic">"Gábor applied to one of my personal consultations, that’s how I first met him. During the consultation I got to know <strong class="bold-only">a professionally very well prepared</strong>, correct person with stable values.</p>
				<p class="italic">He and his team mainly focus on developing custom websites and page speed optimization. Gábor helped me as well, by optimizing the speed of domarketing.hu.</p>
				<p class="italic">What’s more, when the original repository of my website would not work during the opening campaign of my Club, with a great deal of work and effort, Gábor managed to move my website to a much faster and higher quality repository within 24 hours.<strong class="bold-only"> Working with him was a very pleasant experience, I strongly recommend him for anyone!"</strong></p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-doman-zsolt', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Domán Zsolt</h4>
						<p class="qualification">DO! Marketing</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“It was two years ago that Gábor attended our consultation for the first time, and both his attitude and general approach were very appealing straightaway. At the beginning of May he applied for my “mini consultation” service, where now he asked my professional opinion about his own brand. I like his work very much, as he represents unmatched professionalism that is comprehensible for all. I’ve asked for a conversion optimization analysis for our webpage right away, which was more than useful. <strong class="bold-only">Having comprehensive knowledge and skills about his field, Gábor is miles ahead of his competition.</strong></p>
				<p class="italic">Gábor, thanks for the extremely high quality professional work and that you were willing to share the basics of your knowledge (that still requires months to fully comprehend) with us.”</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-meszaros-robi', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Mészáros Robi</h4>
						<p class="qualification">BrandBirds</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“One of my trustworthy contacts recommended DX Labz Kft., when the speed optimization of the Tudástár Klub became due and the webpage had several other problems that needed fixing as well. <strong class="bold-only"> I am very satisfied with your work. Everytime I asked something, you had a clear and understandable answer to it and you always solved every issue in a professional manner, within a short amount of time.</strong> I know I can count on you guys in the future as well, and I will gladly ask for your help when it comes to WordPress support.”</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-szendrei-adam', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Szendrei Ádám</h4>
						<p class="qualification">Tudástár Klub</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“Their work instantly got my attention, so I asked for an offer without hesitation. We have been working together since then. What I got from their team is professional implementation, dynamic and clear communication. They are working on my second webpage as we speak. <strong class="bold-only"> They know exactly what to do, even in case of extremely difficult technical issues.</strong></p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-kovacs-andras', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Kovács András</h4>
						<p class="qualification">Édesburgonya.bio</p>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
	<div class="section-separator_mirror_bottom section_mission">
		<div class="skew-separator">
			<?php picture('orange-gradient-effect-right_mirror', 'png', '',  true, 'gradient-effect-right_mirror'); ?>
		</div>
	</div>
<section id="dxengine">
	<div class="container">
		<h2 class="section-title"><span class="text-gradient-bg">DX ENGINE,</span> the power plant <br>of lightning-fast web pages</h2>
		<hr class="gradient-separator-short">
		<div class="lesser-width">
			<div class="row">
				<div class="col-lg-6 align-center">
					<!-- <?php picture('dxengine-logo', 'png', '',  true, 'dxengine-logo'); ?> -->
					<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/dx-engine.svg" class="lazyload dxengine-logo">
				</div>
				<div class="col-lg-6">
					<ul>
						<li><strong class="bold-only">90+ PageSpeed rating</strong></li>
						<li><strong class="bold-only">Up to 4x faster page speed</strong></li>
						<li><strong class="bold-only">Page size reduction by 75%</strong></li>
						<li><strong class="bold-only">Customer increase up to 50%</strong></li>
						<li><strong class="bold-only">No more SEO penalties</strong></li>
					</ul>
				</div>
			</div>
			<p>The brand new DX Engine puts websites on a new footing. With the help of an architecture that follows the newest recommendations you can forget about slow page speed or penalties due to poor search engine optimization, and finally focus on what's really important: creating the best possible experience for your visitors.</p>
			<p>When it comes to page loading time, every spare second means additional visitors kept on your page and higher conversion rates through a smoother user experience.</p>
			<p>DX Engine not only loads web pages lightning fast, but also always uses image files that are appropriate for the device you are using. The engine supports Google’s new webP image format and loads only content that is visible to the user, reducing network traffic.</p>
		</div>

	</div>
</section>
<div class="section-separator section_time-spent-on-writing">
	<?php picture('orange-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<section id="speed">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/key-of-success-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title dark-bg"><span class="text-purple text-gradient-bg">FOUR TIMES THE SPEED,</span><br>twice conversion</h2>
		<hr class="gradient-separator-short">

			<div class="row">
				 <div class="col-lg-6 align-center">
				 	<?php picture('edesburgonya-windows', 'png', '',  true, 'ref-image'); ?>
				 </div>
	 			 <div class="col-lg-6">
					<p class="bigger-paragraph align-left dark-bg"><strong class="bold-only">Page speed optimisation case study</strong></p>
					<hr class="separator-left">
					<p class="less-line-height dark-bg">Creating web pages by using ready-made website builders could get you a cheap and neat looking result. However their main characteristic, universality is also their biggest flow. Since these web pages contain every possible component by default, there is a huge amount of unnecessary code in them. Because of that, the final result of “clicking a surface together” that ignores every aspect of speed optimization will be slower than ever, generating even less money. </p>
					<p class="dark-bg">Originally, Édesburgonya.bio was made in a similar way, and oftentimes it took 15 seconds to load a page. <strong>We found the solution to the problem.</strong></p>
				 </div>
			</div>
		<div class="less-width">
			<h3 class="dark-bg align-center extra-margin_top extra-margin_bottom">Main results</h3>
			<div class="row align-center">
				<div class="col-lg-4 results">
					<h4 class="number">25<span>%</span></h4>
					<p class="text dark-bg">Decrease <br>in data traffic</p>
				</div>
				<div class="col-lg-4 results">
					<h4 class="number">4<span>X</span></h4>
					<p class="text dark-bg">Home screen <br>loading speed</p>
				</div>
				<div class="col-lg-4 results">
					<h4 class="number">3<span>X</span></h4>
					<p class="text dark-bg">web page <br>loading speed</p>
				</div>
			</div>
			<!-- <div class="call-to-action align-center">
				<a href="<?php echo site_url(); ?>/webfejlesztes-esettanulmany/" class="btn-gradient">TELJES ESETTANULMÁNY</a>
			</div> -->
<!-- 			<p class="dark-bg align-center extra-margin_top">Kíváncsi vagy rá, hogy a te oldaladdal is meg tudnánk-e csinálni?</p>
			<p class="text-orange align-center"><a href="<?php echo site_url();?>/#contact"><strong class="text-orange bold-only">Kérd ki ingyenesen a véleményünket!</strong></a></p> -->
		</div>
	</div>
</section>
	<div class="section-separator_right_mirror_bottom section_speed">
		<?php picture('orange-gradient-effect-right_mirror', 'png', '',  true, 'gradient-effect-right_mirror'); ?>
		<div class="skew-separator"></div>
	</div>
<section id="reference">
	<div class="container">
		<h2 class="section-title"><span class="text-gradient-bg">RECENT</span> REFERENCES</h2>
		<hr class="gradient-separator-short">
<!--			<p class="bigger-paragraph align-center extra-margin_top">Nemcsak mondjuk, csináljuk is</p>-->
	</div>
  <div class="ref-box relative-and-z-index">
    <div class="wide-container">
      <div class="row unique unique_popz">
        <div class="col-lg-6 image align-left">
          <?php picture('popz-references', 'png', '',  true, 'ref-image popz-img'); ?>
        </div>
        <div class="col-lg-6 logo-and-text">
          <?php picture('popz-logo', 'png', '',  true, 'ref-logo popz-logo'); ?>
          <p>For Popz, our main goal was to recreate the atmosphere of going to the cinema and having fun together with your friends, as closely as possible. Click on the link to see for yourself how successful this mission has been, and then grab a bag of delicious Popz popcorn to go with your movie tonight!</p>
          <div class="call-to-action">
            <a href="https://popz.com/" target="_blank" class="btn-gradient btn-popz">SHOW ME MORE</a>
          </div>
        </div>
      </div>
    </div>
    <div class="unique-skewbox_popz">
      <?php picture('unique-popz-gradient-effect-right', 'png', '',  true, 'unique-popz-gradient-effect-right'); ?>
    </div>
    <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-left_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-left">
  </div>
  <div class="ref-box relative-and-z-index">
    <div class="wide-container">
      <div class="row unique unique_cosmos">
        <div class="col-lg-6 logo-and-text">
          <?php picture('cosmos-logo', 'png', '',  true, 'ref-logo cosmos-logo'); ?>
          <p>In designing the website for this long-established event management company, we focused on key aspects such as reflecting the professionalism and integrity of the services they offer in the design and creating a user-friendly interface for the visitors at the same time.</p>
          <div class="call-to-action">
            <a href="https://cosmos.hu/en/" target="_blank" class="btn-gradient btn-cosmos">SHOW ME MORE</a>
          </div>
        </div>
        <div class="col-lg-6 image align-right">
          <?php picture('cosmos-references', 'png', '',  true, 'ref-image cosmos-img'); ?>
        </div>
      </div>
    </div>
    <div class="unique-skewbox_cosmos">
      <?php picture('unique-cosmos-gradient-effect-left', 'png', '',  true, 'unique-cosmos-gradient-effect-left'); ?>
    </div>
    <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-right_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-right">
  </div>
	<div class="ref-box relative-and-z-index">
		<div class="wide-container">
			<div class="row unique unique_qth">
				<div class="col-lg-6 image align-left">
					<?php picture('qth-img', 'png', '',  true, 'ref-image qth-img'); ?>
				</div>
				<div class="col-lg-6 logo-and-text">
					<?php picture('qth-logo', 'png', '',  true, 'ref-logo qth-logo'); ?>
					<p>Quality Tours Hungary is one of the country's leading group travel agencies. It was important for us to give the company a fitting image and to present our country in an attractive way.</p>
					<div class="call-to-action">
						<a href="https://qualitytours.hu" target="_blank" class="btn-gradient btn-qth">SHOW ME MORE</a>
					</div>
				</div>

			</div>
		</div>
		<div class="unique-skewbox_qth">
			<?php picture('unique-green-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		</div>
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-left_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-left">
	</div>
</section>
<!--<div class="straight-white"></div>-->
<!--<section id="many-question" class="front-page">-->
<!--	<div class="container">-->
<!--		<h2 class="section-title hide-on-desktop">Tudom, hogy <span class="text-gradient-bg">SOK KÉRDÉS</span> van a fejedben</h2>-->
<!--		<h2 class="section-title hide-on-mobile">Tudom, hogy <span class="text-gradient-bg">SOK KÉRDÉS</span><br> van a fejedben</h2>-->
<!--		<hr class="gradient-separator-short">-->
<!--		<div class="row less-width">-->
<!--			<div class="col-lg-12 col-12 set-of-answers">-->
<!--				<div class="col-lg-1 col-3 question-img">-->
<!--					--><?php //picture('plus-mark', 'png', '',  true, 'plus-mark'); ?>
<!--				</div>-->
<!--				<div class="col-lg-11 answers">-->
<!--					<h5 class="less-line-height question">Mit jelent az hogy Google Mobil Web Specialista vagyok?</h5>-->
<!--					<div class="answers-text-box hide">-->
<!--						<p class="less-line-height">Amellett, hogy sokmindenbe belekóstoltam, folyamatosan dolgoztam a  szakmai tudásom elmélyítésén. Többek között a közelmúltban én lettem az ország 1. Google Minősített Mobil Web Specialistája. Miért tartottam ezt a minősítést olyan fontosnak megszerezni?</p>-->
<!--						<p class="less-line-height">Ma már a mobilos weboldal megtekintések túlsúlyban vannak, azonban a mobilra optimalizálás legtöbbször még mindig csak másodgondolat. Ha most nem elsődlegesen mobil fókusszal tervezed a weboldalad, azzal többéves lemaradásra ítéled magad, hiszen a weboldaladat nem fogod túl gyakran cserélgetni.</p>-->
<!--						<p class="less-line-height">Szeretnélek felkészíteni téged a jövőre, és betekintést adni abba, mi kell ahhoz, hogy mobilon legalább olyan jól jelenjen meg az oldalad mint asztali gépeken. Feketeöves technikák garmadáját állítjuk majd hadrendbe, hogy még jobb eredményeket tudj elérni a mobil eszközökön. </p>-->
<!--						<p class="less-line-height">A minősítésnek hála, az a tudás, amit ma mobilbarát weboldal-opttimalizációval kapcsolatban érdemes tudnod, bekerült a könyv anyagába. Sőt, már az elkövetkező 3 évben elterjedő technológiák is figyelembe lettek véve a könyv írása során. Te ezekről már most tudni fogsz, mielőtt a konkurensek egyáltalán tudomást szereznének róluk, így óriási lépéselőnybe kerülhetsz velük szemben.</p>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="col-lg-12 col-12 set-of-answers">-->
<!--				<div class="col-lg-1 col-3 question-img">-->
<!--					--><?php //picture('plus-mark', 'png', '',  true, 'plus-mark'); ?>
<!--				</div>-->
<!--				<div class="col-lg-11 answers">-->
<!--					<h5 class="less-line-height question">Mit csinál egy Growth Master?</h5>-->
<!--					<div class="answers-text-box hide">-->
<!--						<p class="less-line-height">Egy másik képzési kitérőm a Growth Hackers University-n megszerzett Growth Master titulus volt. Mitől is lesz valaki Growth Master, a növekedés mestere? </p>-->
<!--						<p class="less-line-height">Korábbi munkatapasztalaim révén már betekinthettem a startupoknál 2 alkalmazott gyors növekedést lehetővé tévő technikákba. Olyan módszerekről van szó, amelyekkel rövid idő alatt képes vagy skálázni a vállalkozásodat és képes lehetsz eredményesen betörni célpiacodra. Ezekkel lehetséges biztosítani, hogy a legkisebb energia, idő- és pénz befektetésével a lehető legnagyobb eredményeket érd el adott idő alatt a vállalkozásod számára.</p>-->
<!--						<p class="less-line-height">Feltételezem, benned is felébredt a kíváncsiság, hogy megismerd ezeket a praktikákat. Ahogyan most te is, én is hajthatatlan vágyat éreztem, hogy még többet tudjak meg erről a területről, amire sokan a marketing következő szintjeként tekintenek, ezért fontosnak véltem, hogy beleássam magam a témába. Az ezen a téren megszerzett ismereteimbe is beavatlak. </p>-->
<!--						<p class="less-line-height">A Growth Master nemcsak a weboldaladon belüli vásárlások arányát optimalizálja, hanem már a weboldalra érkező forgalom hatékony megszerzésével is foglalkozik. Ezzel a tudással felvértezve folyamatosan valós adatokra épülő továbblépési opciókat tárok eléd, hogy ne a sötétben kelljen tapogatóznod, amikor vállalkozásod lehetséges fejlesztési pontjait kell mérlegelned.</p>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="col-lg-12 col-12 set-of-answers">-->
<!--				<div class="col-lg-1 col-3 question-img">-->
<!--					--><?php //picture('plus-mark', 'png', '',  true, 'plus-mark'); ?>
<!--				</div>-->
<!--				<div class="col-lg-11 answers">-->
<!--					<h5 class="less-line-height question">Micsoda az a validált tanulás?</h5>-->
<!--					<div class="answers-text-box hide">-->
<!--						<p class="less-line-height">A validált tanulás azt jelenti, hogyan kapjunk választ a célpiacunkról alkotott feltételezéseinkre még azelőtt, hogy túl sok pénzt invesztálnánk egy esetleg működésképtelen ötletbe.</p>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--</section>-->
<!--<div class="section-separator section_methodology">-->
<!--	--><?php //picture('purple-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
<!--	<div class="skew-separator"></div>-->
<!--</div>-->
<!-- <section id="methodology">
	<div class="gradient-orange lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/who-is-gabor-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	 <div class="container relative-and-z-index">
		<h2 class="section-title dark-bg">Hol beszéltünk már<br><span class="text-purple-bg_gradient">MÓDSZERTANUNKRÓL</span></h2>
		<hr class="separator-short_purple">
		<div class="lesser-width">
			<p class="dark-bg less-line-height">Amellett, hogy piacvezető cégeknél szerzett szakmai tudásunkkal támogatunk téged, nagyon szívesen osztjuk meg tudásunkat másokkal is. Előadtunk az <strong class="purple-bg">Innonic Academy</strong>-n is a Growth Hacking módszertanról, illetve az ország legnagyobb márkaépítő csoportjában, a <strong class="purple-bg">BrandShip</strong>-ben is tartottunk zártkörű szakmai képzést a konverzió optimalizálás fortélyairól. <strong class="purple-bg">Wolf Gábor</strong>nak is meséltünk már villámgyors honlapmotorunkról, illetve startupoktól átemelt ügyféleredmény központú fejlesztési módszertanunkról.</p>
		</div>
		<div class="row extra-margin_top">
			<div class="col-lg-6">
				<iframe width="100%" height="280" data-src="https://www.youtube.com/embed/oXfU8KNpULk" class="lazyload" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="col-lg-6">
				<iframe width="100%" height="280" data-src="https://www.youtube.com/embed/Ld3LvwYBivQ" class="lazyload" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div> 
</section> -->
<!--	<div class="section-separator_right_mirror_bottom section_methodology">-->
<!--		--><?php //picture('purple-gradient-effect-right_mirror', 'png', '',  true, 'gradient-effect-right_mirror'); ?>
<!--		<div class="skew-separator"></div>-->
<!--	</div>-->
<section id="pre-market" class="front-page" style="padding-top: 12vw;">
	<div class="container">
		<h2 class="section-title hide-on-desktop">The method that puts DX Labz<br><span class="text-gradient-bg">AHEAD OF THE COMPETITION</span></h2>
		<h2 class="section-title hide-on-mobile">The method that puts DX Labz<br><span class="text-gradient-bg">AHEAD OF THE COMPETITION</span></h2>
		<hr class="gradient-separator-short">
		<div class="lesser-width">
			<p class="bigger-paragraph align-left">To be always up to date about the latest business development opportunities, we master the teachings of the most successful overseas authors in English. This way we can build their best practices into our methods, to be able to constantly increase the value that we give to our customers.</p>
			<hr class="separator-left">
			<p class="less-line-height">We are members of the best premium groups on Facebook, so we not only keep our knowledge up to date, but if you have any questions about your project, we can get the country's top experts to give their opinions without costing you a penny.</p>
		</div>
		<!-- <div class="row">
			<div class="col-lg-4 align-center extra-margin_top">
				<?php picture('brandbirds-logo', 'png', '',  true, 'partner-logo'); ?>
				<p>Márkaépítés mesterfokon</p>
			</div>
			<div class="col-lg-4 align-center extra-margin_top">
				<?php picture('domarketing-logo', 'png', '',  true, 'partner-logo'); ?>
				<p>Nem az számít, mit tudsz, hanem mit csinálsz a marketingben!</p>
			</div>
			<div class="col-lg-4 align-center extra-margin_top">
				<?php picture('szemleletvalto-logo', 'jpeg', '',  true, 'partner-logo'); ?>
				<p>Jóból kiválót</p>
			</div>
		</div> -->
	</div>
</section>
<section id="contact" class="front-page">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/time-spent-on-writing-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
			<h2 class="section-title dark-bg"><span class="text-purple text-gradient-bg">LET’S TALK</span><br>about how could we help you</h2>
			<hr class="gradient-separator-short">
			<div class="lesser-width">
				<p class="dark-bg less-line-height align-center extra-margin_bottom">Interested in our services? Write us a few lines about your project and we'll tell you how we think your brand and website can improve, how you can finally leave your competitors behind and how you can become a market leader.</p>
				<?php echo do_shortcode('[contact-form-7 id="1414" title="Contact form angol"]'); ?>
			</div>

	</div>
</section>
<!-- Page Content -->


</div><!-- .site-content -->

<!-- <?php get_sidebar( 'footer' ); ?> -->

    <!-- Footer -->
    <footer class="<?php echo (is_page('online-gyorsitosav') ? 'landing' : ''); ?>">
    <?php if (!is_page('online-gyorsitosav')) : ?>
      <div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/footer-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
      <div class="container relative-and-z-index">
        <div class="row">
          <div class="col-lg-5 info-box credo">
            <h3 class="title dark-bg">Credo</h3>
            <p class="dark-bg less-line-height">Every business is the embodiment of someone's dreams. We work to turn those dreams into success stories, instead of nightmares. We are dedicated to researching the latest technologies to ensure you stand out from the competition.</p>
            <div class="row align-center">
              <div class="col-lg-4 col-6">
                <?php picture('dx-labz-vallalhato-vallalkozas-weboldal-fejlesztes', 'png', '',  true, 'badge'); ?>
              </div>
              <div class="col-lg-4 align-center col-6 col-md-6">
                <?php picture('dx-labz-weboldal-keszites-fivosz-tag', 'jpeg', '',  true, 'badge'); ?>
                <span>FIVOSZ member</span>
              </div>
              <div class="col-lg-4 align-center col-6 col-md-6">
                <?php picture('dx-labz-weboldal-keszites-minositett-szallito-vallalkozz-digitalisan', 'png', '',  true, 'badge'); ?>
               <span>Qualified supplier</span>
             </div>
             </div>
          </div>
         <!--  <div class="col-lg-3 info-box">
            <h3 class="title dark-bg">Sitemap</h3>
            <nav class="sitelinks dark-bg">
             <?php themeplate_footer_menu(); ?>
            </nav>
          </div> -->
          <div class="col-lg-4 info-box">
            <h3 class="title dark-bg">Address</h3>
            <p class="dark-bg less-line-height">DX Labz Kft.</p>
            <p class="dark-bg less-line-height">VAT number: 27037162-2-05</p>
            <p class="dark-bg less-line-height">3623 Borsodszentgyörgy</p>
            <p class="dark-bg less-line-height">Horgos út 8.</p>
          </div>
          <div class="col-lg-3 info-box">
            <h3 class="title dark-bg">Contacts</h3>
            <h4>Phone</h4>
            <p class="dark-bg less-line-height"><a href="tel:06303362320">06303362320</a></p>
            <p class="dark-bg less-line-height"><a href="tel:06304483677">06304483677</a></p>
            <h4>Email</h4>
            <p class="dark-bg less-line-height"><a href="mailto:info@dxlabz.hu">info@dxlabz.hu</a></p>
            <a href="https://www.facebook.com/dxlabz/"><img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/facebook.svg" alt="Facebook Logo White"  class="lazyload facebook-logo"></a>
            <a href="https://www.linkedin.com/in/gabor-kriston/"><img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/linkedin.svg" alt="LinkedIn"  class="lazyload linkedin-logo"></a>
          </div>
        </div>
        <div class="footer-logo align-center">
          <a href="<?php echo site_url(); ?>"><img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-white.svg" alt="DxLabz Logo" data-no-lazy="1" class="lazyload dxlabz-logo-white"></a>
        </div>
      </div>
		<?php endif; ?>
    <div id="copyright" class="relative-and-z-index">
      <div class="container">
          <p class="dark-bg less-line-height">© <?php echo date("Y"); ?> DX Labz Kft. - All rights reserved</p>
          <!-- <p class="dark-bg less-line-height"><a href="https://dxlabz.hu/adatkezelesi-tajekoztato.pdf">Adatvédelmi tájékoztató</a></p> -->
          <p class="dark-bg less-line-height">Customer service: <a href="mailto:info@dxlabz.hu">info@dxlabz.hu</a></p>
          <a href="http://simplepartner.hu/PaymentService/Fizetesi_tajekoztato.pdf" target="_blank"><?php picture('simplepay', 'png', '',  true, 'simplepay-logo'); ?></a>
      </div>
    </div>
<!--     <p class="dark-bg">Copyright &copy; 2020 <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>.
          <span>Designed and developed by <a href="<?php echo esc_html( AUTHOR_URI ); ?>"><?php echo esc_html( THEME_AUTHOR ); ?></a>.</span></p> -->
      <!-- /.container -->
    </footer>
    <button id="scroll-to-top" title="Go to top"><img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/to-top.svg" class="lazyload" alt=""></button>

<!--	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" async></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" defer></script>
  <!-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script> -->

<!-- 	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />-->
<!--  	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>-->


    <?php if (is_front_page()) : ?>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/calculator.js" defer></script>
		<!--   <script>
// Set the date we're counting down to
var countDownDate = new Date("Jun 1, 2020 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("timer").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timer").innerHTML = "EXPIRED";
  }
}, 1000);

</script>-->
<?php endif; ?>

	<?php wp_footer(); ?>

<?php if (is_page('weboldal-megterulesi-kalkulator')) : ?>
	<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js" defer></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/web-roi.js" defer></script>
<?php endif; ?>


<!-- GYIK ELEMENT SHOW - HIDE -->
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
     var panel = this.nextElementSibling;
    if (panel.classList.contains('open')) {
      panel.classList.remove('open')
    } else {
      panel.classList.add('open')
    }
  });
}

</script>



</body>
</html>
