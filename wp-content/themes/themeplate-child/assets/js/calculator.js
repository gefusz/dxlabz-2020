function initCalc() {
	let result = document.getElementById("result");
	let result2 = document.getElementById("result2");
	let visitors = document.getElementById("visitors");
	let productprice = document.getElementById("productprice");


	let ranges = document.querySelectorAll('input[type="range"]');
	let sum;

	function summing() {
		sum = 0;
		for (let range of ranges) {
			sum += parseFloat(range.value);
		}
		result.innerHTML = sum.toFixed(1);
		result2.innerHTML = Math.round(parseInt(sum/100*visitors.value*productprice.value) / 1000);
	}

	summing();

	for (let range of ranges) {
		range.addEventListener("input", summing);
	}

	visitors.addEventListener("input", summing);
	productprice.addEventListener("input", summing);

	let myreq;

	function getRndmFromSet(set) {
		var rndm = Math.floor(Math.random() * set.length);
		return set[rndm];
	}

	let set = [0,1,2,3,4];

	function rangeAnimation () {
		let ranges = document.querySelectorAll('input[type="range"]');


		let rand = getRndmFromSet(set);
		if (ranges[rand].value != ranges[rand].getAttribute("max")) {
			ranges[rand].value = parseFloat(ranges[rand].value) + 0.15;
			summing();
		} else {
			let index = set.indexOf(rand);
			if (index != -1) set.splice(index, 1);
		}

		let isNotAtMax = function (e) {
			return e.getAttribute("max") != e.value;
		}

		let allIsNotMax = Array.prototype.some.call(ranges, isNotAtMax);

		if (!allIsNotMax) {
			cancelAnimationFrame(myreq);
		} else {
			myreq = requestAnimationFrame(rangeAnimation);
		}
	}
	myreq = requestAnimationFrame(rangeAnimation);
}

if (isElementInViewport(document.querySelector('#hero .calculator'))) {
	initCalc();
} else {
	window.addEventListener('scroll', heroCalculatorScrollListener);
}

function heroCalculatorScrollListener() {
	if (isElementInViewport(document.querySelector('#hero .calculator'))) {
		initCalc();
		window.removeEventListener('scroll', heroCalculatorScrollListener);
	}
}

function isElementInViewport(el) {
	var rect = el.getBoundingClientRect();
	return (
		rect.top >= 0 &&
		rect.left >= 0 &&
		rect.bottom <= (window.innerHeight || document. documentElement.clientHeight) &&
		rect.right <= (window.innerWidth || document. documentElement.clientWidth)
	);
}
