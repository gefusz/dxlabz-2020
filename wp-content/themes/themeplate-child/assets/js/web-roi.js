vm = {
	config: {
		numOfYears: 1,
		avgMonthlyVisitors: 300,
		avgSalesPrice: 3e4,
		costRatio: .2,
		monthlyFixedCost: 7e4,
		payPerClick: 40,
		organicTrafficRatio: 0,
		projectBasePrice: 1e5,
		projectPriceIncrementBy: 1e5,
		salesTarget: 10,
		detailLevel: 10,
		baseConversion: .005,
		conversionStep: .01,
		remainingVisitorBase: .6,
		remainingVisitorStep: .03
	},
	data: {},
	init: function() {
		vm.generateBaseData(), vm.instance = new Vue({
			el: "#app",
			data: {
				chartClickDescription: !1,
				profitData: vmd.profitData[vcf.i],
				numOfYears: vcf.numOfYears,
				returnOnInvestment: Math.round(100 * vmd.returnOnInvestment) + "%",
				parity: vmd.parity,
				projectPrice: Math.round(vmd.projectPriceData[vcf.i]),
				conversionRate: Number(100 * vmd.conversionData[vcf.i]).toFixed(1) + "%",
				minConvForSalesTarget: vmd.minConvForSalesTarget
			}
		}), vm.chartView = new Chart(document.getElementById("mixed-chart"), {
			type: "bar",
			data: {
				labels: vmd.profitData,
				datasets: [{
					label: "Vásárlási hajlandóság",
					yAxisID: "y-axis-2",
					data: vmd.conversionData,
					borderWidth: vcf.i,
					borderColor: "#1abc9c",
					fill: !1,
					type: "line"
				}, {
					label: "Projektköltség",
					yAxisID: "y-axis-1",
					data: vmd.projectPriceData,
					backgroundColor: "#3498db",
					stack: "Stack 0"
				}, {
					label: "Összes ktsg.",
					yAxisID: "y-axis-1",
					data: vmd.costData,
					backgroundColor: "#e74c3c",
					stack: "Stack 0"
				}, {
					label: "Profit",
					yAxisID: "y-axis-1",
					data: vmd.profitData,
					backgroundColor: "#f39c12",
					stack: "Stack 0"
				}]
			},
			options: {
				title: {
					display: !0,
					text: "Weboldal vásárlási hajlandóság arányos profitja",
					fontSize: 24,
					fontColor: "#575859"
				},
				legend: {
					display: !0,
					labels: {
						fontSize: 14
					}
				},
				layout: {
					padding: {
						left: 25,
						right: 25,
						top: 25,
						bottom: 25
					}
				},
				scales: {
					yAxes: [{
						type: "linear",
						display: !0,
						position: "left",
						id: "y-axis-1",
						scaleLabel: {
							display: !0,
							labelString: "Weboldal bevétele",
							fontSize: 18
						},
						ticks: {
							display: !0,
							callback: function(e, t, a) {
								return e / 1e6 + "m Ft"
							}
						}
					}, {
						type: "linear",
						display: !0,
						position: "right",
						id: "y-axis-2",
						gridLines: {
							drawOnChartArea: !1
						},
						scaleLabel: {
							display: !0,
							labelString: "Vásárlási hajlandóság",
							fontSize: 18
						},
						ticks: {
							display: !0,
							callback: function(e, t, a) {
								return Number(100 * e).toFixed(1) + "%"
							}
						}
					}],
					xAxes: [{
						display: !0,
						scaleLabel: {
							display: !0,
							labelString: "Profit",
							fontSize: 18
						},
						ticks: {
							display: !0,
							callback: function(e, t, a) {
								return Number(e / 1e6).toFixed(2) + "m Ft"
							}
						}
					}]
				},
				onClick: function(e, t) {
					vm.instance;
					vcf.i = t[0]._index, vm.rerender(!0)
				}
			}
		})
	},
	generateBaseData: function() {
		vmd.projectPriceData = vm.generateData(vcf.projectBasePrice, vcf.projectPriceIncrementBy, vcf.detailLevel), vmd.conversionData = vm.generateData(vcf.baseConversion, vcf.conversionStep, vcf.detailLevel), vmd.remainingVisitorData = vm.generateData(vcf.remainingVisitorBase, vcf.remainingVisitorStep, vcf.detailLevel), vmd.revenueData = vm.generateYearlyRevenueData(), vmd.costData = vm.generateYearlyCostData(), vmd.profitData = vm.generateYearlyProfitData(), vmd.ROIData = vm.generateROIData(), vmd.parity = vm.getParity(), vmd.minConvForSalesTarget = vm.getMinConvForSalesTarget()
	},
	getParity: function() {
		var e, t = !1;
		return vmd.profitData.forEach(function(a, r) {
			t || a > vmd.projectPriceData[r] && (e = Number(12 / (vmd.profitData[r] / vcf.numOfYears / vmd.projectPriceData[r])).toFixed(1), vcf.i = r, vmd.returnOnInvestment = Number(vmd.profitData[r] / vmd.projectPriceData[r]).toFixed(2), t = !0)
		}), e > 0 ? e : 0
	},
	generateData: function(e, t, a) {
		for (var r = [], n = 0; n < a; n++) r.push(Number.parseFloat(e + t * n).toFixed(3));
		return r
	},
	calculateFixedCosts: function() {
		return vcf.monthlyFixedCost + vcf.avgMonthlyVisitors * vcf.payPerClick * (1 - vcf.organicTrafficRatio)
	},
	generateROIData: function() {
		var e = [];
		return vmd.profitData.forEach(function(t, a) {
			e[a] = t / vmd.projectPriceData[a]
		}), e
	},
	generateYearlyRevenueData: function() {
		var e = [];
		return vmd.conversionData.forEach(function(t, a) {
			e[a] = 12 * Math.round(t * vmd.remainingVisitorData[a] * vcf.avgMonthlyVisitors * vcf.avgSalesPrice) * vcf.numOfYears
		}), e
	},
	generateYearlyCostData: function() {
		var e = vm.calculateFixedCosts(),
			t = [];
		return vmd.revenueData.forEach(function(a, r) {
			t[r] = a * vcf.costRatio + 12 * e * vcf.numOfYears
		}), t
	},
	generateYearlyProfitData: function() {
		var e = [];
		return vmd.revenueData.forEach(function(t, a) {
			e[a] = t - vmd.costData[a]
		}), e
	},
	getMinConvForSalesTarget: function() {
		var e;
		return vm.generateData(.01, .001, 1e3).some(function(t, a) {
			if (t * vcf.avgMonthlyVisitors > vcf.salesTarget) return e = Number(100 * t).toFixed(1) + "%", !0
		}), e
	},
	calculateProfit: function(e) {
		return e.preventDefault(), vm.instance.chartClickDescription = !1, vcf.avgSalesPrice = Number(document.getElementById("salesprice").value), vcf.costRatio = document.getElementById("product-cost-ratio").value / 100, vcf.monthlyFixedCost = Number(document.getElementById("monthly-fixed-cost").value), vcf.salesTarget = Number(document.getElementById("sales-target").value), vcf.avgMonthlyVisitors = Number(document.getElementById("visitors").value), vcf.organicTrafficRatio = document.getElementById("organic-traffic-ratio").value / 100, vcf.payPerClick = Number(document.getElementById("ppc-cost").value), vcf.numOfYears = Number(document.getElementById("years").value), vm.generateBaseData(), vm.rerender(), vm.updateChartView(), !1
	},
	rerender: function(e) {
		var t = vm.instance,
			a = vcf.i;
		t.chartClickDescription = !0, t.profitData = Math.round(vmd.profitData[a]), t.conversionRate = Number(100 * vmd.conversionData[a]).toFixed(1) + "%", t.projectPrice = Math.round(vmd.projectPriceData[a]), e ? (t.returnOnInvestment = Math.round(100 * vmd.ROIData[a]) + "%", t.projectPrice = Math.round(vmd.projectPriceData[a]), t.parity = vmd.ROIData[a] > 0 ? Number(12 / vmd.ROIData[a]).toFixed(1) : 0) : (t.parity = vmd.parity, t.returnOnInvestment = Math.round(100 * vmd.returnOnInvestment) + "%", t.numOfYears = vcf.numOfYears, t.minConvForSalesTarget = vmd.minConvForSalesTarget)
	},
	updateChartView: function() {
		vm.chartView.data.datasets[2].data.forEach(function(e, t, a) {
			a[t] = vmd.costData[t]
		}), vm.chartView.data.datasets[3].data.forEach(function(e, t, a) {
			a[t] = vmd.profitData[t], vm.chartView.data.labels[t] = a[t]
		}), vm.chartView.update()
	}
};
var app = {
		init: function() {
			vm.init(), app.bindEvents()
		},
		bindEvents: function() {
			document.querySelector("#web-roi form").addEventListener("submit", vm.calculateProfit)
		}
	},
	vcf = vm.config,
	vmd = vm.data,
	vmi = vm.instance;
app.init();
