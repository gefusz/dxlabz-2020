/*!
 *  ThemePlate 0.1.0
 *  Copyright (C) 2021 gkriston84
 *  Licensed under GPL-2.0-only.
 */

"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*! lazysizes - v4.1.4 */
!function (a, b) {
  var c = b(a, a.document);
  a.lazySizes = c, "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports && (module.exports = c);
}(window, function (a, b) {
  "use strict";

  if (b.getElementsByClassName) {
    var c,
        d,
        e = b.documentElement,
        f = a.Date,
        g = a.HTMLPictureElement,
        h = "addEventListener",
        i = "getAttribute",
        j = a[h],
        k = a.setTimeout,
        l = a.requestAnimationFrame || k,
        m = a.requestIdleCallback,
        n = /^picture$/i,
        o = ["load", "error", "lazyincluded", "_lazyloaded"],
        p = {},
        q = Array.prototype.forEach,
        r = function r(a, b) {
      return p[b] || (p[b] = new RegExp("(\\s|^)" + b + "(\\s|$)")), p[b].test(a[i]("class") || "") && p[b];
    },
        s = function s(a, b) {
      r(a, b) || a.setAttribute("class", (a[i]("class") || "").trim() + " " + b);
    },
        t = function t(a, b) {
      var c;
      (c = r(a, b)) && a.setAttribute("class", (a[i]("class") || "").replace(c, " "));
    },
        u = function u(a, b, c) {
      var d = c ? h : "removeEventListener";
      c && u(a, b), o.forEach(function (c) {
        a[d](c, b);
      });
    },
        v = function v(a, d, e, f, g) {
      var h = b.createEvent("Event");
      return e || (e = {}), e.instance = c, h.initEvent(d, !f, !g), h.detail = e, a.dispatchEvent(h), h;
    },
        w = function w(b, c) {
      var e;
      !g && (e = a.picturefill || d.pf) ? (c && c.src && !b[i]("srcset") && b.setAttribute("srcset", c.src), e({
        reevaluate: !0,
        elements: [b]
      })) : c && c.src && (b.src = c.src);
    },
        x = function x(a, b) {
      return (getComputedStyle(a, null) || {})[b];
    },
        y = function y(a, b, c) {
      for (c = c || a.offsetWidth; c < d.minSize && b && !a._lazysizesWidth;) {
        c = b.offsetWidth, b = b.parentNode;
      }

      return c;
    },
        z = function () {
      var a,
          c,
          d = [],
          e = [],
          f = d,
          g = function g() {
        var b = f;

        for (f = d.length ? e : d, a = !0, c = !1; b.length;) {
          b.shift()();
        }

        a = !1;
      },
          h = function h(d, e) {
        a && !e ? d.apply(this, arguments) : (f.push(d), c || (c = !0, (b.hidden ? k : l)(g)));
      };

      return h._lsFlush = g, h;
    }(),
        A = function A(a, b) {
      return b ? function () {
        z(a);
      } : function () {
        var b = this,
            c = arguments;
        z(function () {
          a.apply(b, c);
        });
      };
    },
        B = function B(a) {
      var b,
          c = 0,
          e = d.throttleDelay,
          g = d.ricTimeout,
          h = function h() {
        b = !1, c = f.now(), a();
      },
          i = m && g > 49 ? function () {
        m(h, {
          timeout: g
        }), g !== d.ricTimeout && (g = d.ricTimeout);
      } : A(function () {
        k(h);
      }, !0);

      return function (a) {
        var d;
        (a = a === !0) && (g = 33), b || (b = !0, d = e - (f.now() - c), 0 > d && (d = 0), a || 9 > d ? i() : k(i, d));
      };
    },
        C = function C(a) {
      var b,
          c,
          d = 99,
          e = function e() {
        b = null, a();
      },
          g = function g() {
        var a = f.now() - c;
        d > a ? k(g, d - a) : (m || e)(e);
      };

      return function () {
        c = f.now(), b || (b = k(g, d));
      };
    };

    !function () {
      var b,
          c = {
        lazyClass: "lazyload",
        loadedClass: "lazyloaded",
        loadingClass: "lazyloading",
        preloadClass: "lazypreload",
        errorClass: "lazyerror",
        autosizesClass: "lazyautosizes",
        srcAttr: "data-src",
        srcsetAttr: "data-srcset",
        sizesAttr: "data-sizes",
        minSize: 40,
        customMedia: {},
        init: !0,
        expFactor: 1.5,
        hFac: .8,
        loadMode: 2,
        loadHidden: !0,
        ricTimeout: 0,
        throttleDelay: 125
      };
      d = a.lazySizesConfig || a.lazysizesConfig || {};

      for (b in c) {
        b in d || (d[b] = c[b]);
      }

      a.lazySizesConfig = d, k(function () {
        d.init && F();
      });
    }();

    var D = function () {
      var g,
          l,
          m,
          o,
          p,
          y,
          D,
          F,
          G,
          H,
          I,
          J,
          K,
          L,
          M = /^img$/i,
          N = /^iframe$/i,
          O = "onscroll" in a && !/(gle|ing)bot/.test(navigator.userAgent),
          P = 0,
          Q = 0,
          R = 0,
          S = -1,
          T = function T(a) {
        R--, a && a.target && u(a.target, T), (!a || 0 > R || !a.target) && (R = 0);
      },
          U = function U(a, c) {
        var d,
            f = a,
            g = "hidden" == x(b.body, "visibility") || "hidden" != x(a.parentNode, "visibility") && "hidden" != x(a, "visibility");

        for (F -= c, I += c, G -= c, H += c; g && (f = f.offsetParent) && f != b.body && f != e;) {
          g = (x(f, "opacity") || 1) > 0, g && "visible" != x(f, "overflow") && (d = f.getBoundingClientRect(), g = H > d.left && G < d.right && I > d.top - 1 && F < d.bottom + 1);
        }

        return g;
      },
          V = function V() {
        var a,
            f,
            h,
            j,
            k,
            m,
            n,
            p,
            q,
            r = c.elements;

        if ((o = d.loadMode) && 8 > R && (a = r.length)) {
          f = 0, S++, null == K && ("expand" in d || (d.expand = e.clientHeight > 500 && e.clientWidth > 500 ? 500 : 370), J = d.expand, K = J * d.expFactor), K > Q && 1 > R && S > 2 && o > 2 && !b.hidden ? (Q = K, S = 0) : Q = o > 1 && S > 1 && 6 > R ? J : P;

          for (; a > f; f++) {
            if (r[f] && !r[f]._lazyRace) if (O) {
              if ((p = r[f][i]("data-expand")) && (m = 1 * p) || (m = Q), q !== m && (y = innerWidth + m * L, D = innerHeight + m, n = -1 * m, q = m), h = r[f].getBoundingClientRect(), (I = h.bottom) >= n && (F = h.top) <= D && (H = h.right) >= n * L && (G = h.left) <= y && (I || H || G || F) && (d.loadHidden || "hidden" != x(r[f], "visibility")) && (l && 3 > R && !p && (3 > o || 4 > S) || U(r[f], m))) {
                if (ba(r[f]), k = !0, R > 9) break;
              } else !k && l && !j && 4 > R && 4 > S && o > 2 && (g[0] || d.preloadAfterLoad) && (g[0] || !p && (I || H || G || F || "auto" != r[f][i](d.sizesAttr))) && (j = g[0] || r[f]);
            } else ba(r[f]);
          }

          j && !k && ba(j);
        }
      },
          W = B(V),
          X = function X(a) {
        s(a.target, d.loadedClass), t(a.target, d.loadingClass), u(a.target, Z), v(a.target, "lazyloaded");
      },
          Y = A(X),
          Z = function Z(a) {
        Y({
          target: a.target
        });
      },
          $ = function $(a, b) {
        try {
          a.contentWindow.location.replace(b);
        } catch (c) {
          a.src = b;
        }
      },
          _ = function _(a) {
        var b,
            c = a[i](d.srcsetAttr);
        (b = d.customMedia[a[i]("data-media") || a[i]("media")]) && a.setAttribute("media", b), c && a.setAttribute("srcset", c);
      },
          aa = A(function (a, b, c, e, f) {
        var g, h, j, l, o, p;
        (o = v(a, "lazybeforeunveil", b)).defaultPrevented || (e && (c ? s(a, d.autosizesClass) : a.setAttribute("sizes", e)), h = a[i](d.srcsetAttr), g = a[i](d.srcAttr), f && (j = a.parentNode, l = j && n.test(j.nodeName || "")), p = b.firesLoad || "src" in a && (h || g || l), o = {
          target: a
        }, p && (u(a, T, !0), clearTimeout(m), m = k(T, 2500), s(a, d.loadingClass), u(a, Z, !0)), l && q.call(j.getElementsByTagName("source"), _), h ? a.setAttribute("srcset", h) : g && !l && (N.test(a.nodeName) ? $(a, g) : a.src = g), f && (h || l) && w(a, {
          src: g
        })), a._lazyRace && delete a._lazyRace, t(a, d.lazyClass), z(function () {
          (!p || a.complete && a.naturalWidth > 1) && (p ? T(o) : R--, X(o));
        }, !0);
      }),
          ba = function ba(a) {
        var b,
            c = M.test(a.nodeName),
            e = c && (a[i](d.sizesAttr) || a[i]("sizes")),
            f = "auto" == e;
        (!f && l || !c || !a[i]("src") && !a.srcset || a.complete || r(a, d.errorClass) || !r(a, d.lazyClass)) && (b = v(a, "lazyunveilread").detail, f && E.updateElem(a, !0, a.offsetWidth), a._lazyRace = !0, R++, aa(a, b, f, e, c));
      },
          ca = function ca() {
        if (!l) {
          if (f.now() - p < 999) return void k(ca, 999);
          var a = C(function () {
            d.loadMode = 3, W();
          });
          l = !0, d.loadMode = 3, W(), j("scroll", function () {
            3 == d.loadMode && (d.loadMode = 2), a();
          }, !0);
        }
      };

      return {
        _: function _() {
          p = f.now(), c.elements = b.getElementsByClassName(d.lazyClass), g = b.getElementsByClassName(d.lazyClass + " " + d.preloadClass), L = d.hFac, j("scroll", W, !0), j("resize", W, !0), a.MutationObserver ? new MutationObserver(W).observe(e, {
            childList: !0,
            subtree: !0,
            attributes: !0
          }) : (e[h]("DOMNodeInserted", W, !0), e[h]("DOMAttrModified", W, !0), setInterval(W, 999)), j("hashchange", W, !0), ["focus", "mouseover", "click", "load", "transitionend", "animationend", "webkitAnimationEnd"].forEach(function (a) {
            b[h](a, W, !0);
          }), /d$|^c/.test(b.readyState) ? ca() : (j("load", ca), b[h]("DOMContentLoaded", W), k(ca, 2e4)), c.elements.length ? (V(), z._lsFlush()) : W();
        },
        checkElems: W,
        unveil: ba
      };
    }(),
        E = function () {
      var a,
          c = A(function (a, b, c, d) {
        var e, f, g;
        if (a._lazysizesWidth = d, d += "px", a.setAttribute("sizes", d), n.test(b.nodeName || "")) for (e = b.getElementsByTagName("source"), f = 0, g = e.length; g > f; f++) {
          e[f].setAttribute("sizes", d);
        }
        c.detail.dataAttr || w(a, c.detail);
      }),
          e = function e(a, b, d) {
        var e,
            f = a.parentNode;
        f && (d = y(a, f, d), e = v(a, "lazybeforesizes", {
          width: d,
          dataAttr: !!b
        }), e.defaultPrevented || (d = e.detail.width, d && d !== a._lazysizesWidth && c(a, f, e, d)));
      },
          f = function f() {
        var b,
            c = a.length;
        if (c) for (b = 0; c > b; b++) {
          e(a[b]);
        }
      },
          g = C(f);

      return {
        _: function _() {
          a = b.getElementsByClassName(d.autosizesClass), j("resize", g);
        },
        checkElems: g,
        updateElem: e
      };
    }(),
        F = function F() {
      F.i || (F.i = !0, E._(), D._());
    };

    return c = {
      cfg: d,
      autoSizer: E,
      loader: D,
      init: F,
      uP: w,
      aC: s,
      rC: t,
      hC: r,
      fire: v,
      gW: y,
      rAF: z
    };
  }
});
"use strict";

if (document.readyState === "complete" || document.readyState !== "loading" && !document.documentElement.doScroll) {
  executeDOMScripts();
} else {
  document.addEventListener("DOMContentLoaded", executeDOMScripts);
}

function executeDOMScripts() {
  if (document.getElementById('web-projektmap')) {
    document.querySelectorAll('#web-projektmap a').forEach(function (anchor) {
      anchor.setAttribute('target', '_blank');
    });
  }

  WebFont.load({
    google: {
      families: ['Montserrat:400,600,700,800,900']
    }
  });
  document.addEventListener('lazybeforeunveil', function (e) {
    var bg = e.target.getAttribute('data-bg');

    if (bg) {
      e.target.style.backgroundImage = 'url(' + bg + ')';
    }
  }); // Toggle mobile menu

  if (clientWidth < 992) {
    var navBar = document.getElementById('navbarResponsive');

    if (document.querySelector('[data-target="#navbarResponsive"]')) {
      document.querySelectorAll('[data-target="#navbarResponsive"], #navbarResponsive .menu-item').forEach(function (el) {
        el.addEventListener('click', function (e) {
          e.target.classList.toggle('collapsed');
          navBar.classList.toggle('show');
        });
      });
    }
  }

  lazyLoadStyles();
  lazyLoadScripts(); // Scroll to top
  //Get the button:

  mybutton = document.getElementById("scroll-to-top"); // When the user scrolls down 20px from the top of the document, show the button

  window.onscroll = function () {
    scrollFunction();
  };

  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      mybutton.style.display = "block";
    } else {
      mybutton.style.display = "none";
    }
  } // When the user clicks on the button, scroll to the top of the document


  function topFunction() {
    document.body.scrollTop = 0; // For Safari

    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  mybutton.addEventListener('click', topFunction);
  document.querySelectorAll('#many-question .question-img, #many-question .question').forEach(function (el) {
    el.addEventListener('click', function (e) {
      var answer = e.currentTarget.closest('.set-of-answers').querySelector('.answers-text-box');

      if (answer.classList.contains('hide')) {
        document.querySelectorAll('.answers-text-box').forEach(function (el) {
          el.classList.add('hide');
        });
        el.closest('.set-of-answers').querySelector('.answers-text-box').classList.toggle('hide');
      } else {
        answer.classList.add('hide');
      }
    });
  }); // Sticky menu
  // After scrolling 50px from the top...

  window.addEventListener('scroll', function () {
    // Load contact form JS deferred on scroll
    // if (window.scrollY > 50 && isFrontPage && !contactJSLoaded) {
    // 	let documentFragment = document.createDocumentFragment();
    // 	[
    // 		//'/contact-form-7/includes/js/scripts.js'
    // 	].forEach(function(path) {
    // 		documentFragment.appendChild(constructElement('script', {
    // 			src: pluginsUrl + path,
    // 			defer: true
    // 		}));
    // 	});
    //
    // 	document.head.appendChild(documentFragment);
    // 	contactJSLoaded = true;
    // }
    var topBar = document.getElementById('topbar'),
        gradientSeparator = document.querySelector('.gradient-separator'),
        navBar = document.querySelector('.navbar'),
        navBarPatternRight = document.querySelector('.navbar-pattern-right'),
        navBarLogo = navBar.querySelector('.logo'),
        navBarResponsive = document.getElementById('navbarResponsive');

    if (window.scrollY > 50) {
      // topBar.style.transform = 'translateY(-70px)'; //periodiacal-banner esetében.
      // gradientSeparator.style.transform = 'translateY(-70px)';
      navBar.style.background = 'linear-gradient(90deg, rgba(79,39,122,0.98) 0%, rgba(39,39,39,0.98) 100%)';
      navBar.style.top = '0';
      navBar.style.boxShadow = '0 10px 20px rgba(0,0,0,.19), 0 6px 6px rgba(0,0,0,.23)';
      navBarPatternRight.style.visibility = 'visible';

      if (clientWidth > 768) {
        navBarLogo.style.width = '40%';
        navBarLogo.style.height = 'unset';
      }

      if (clientWidth < 768) {
        navBarResponsive.style.background = 'transparent';
      } //Otherwise remove inline styles and thereby revert to original stying

    } else {
      // topBar.style.transform = 'translateY(0px)'; //periodiacal-banner esetében.
      // gradientSeparator.style.transform = 'translateY(0px)';
      navBar.style.background = 'transparent';
      navBar.style.top = 'auto';
      navBar.style.boxShadow = 'none';
      navBarPatternRight.style.visibility = 'hidden';

      if (clientWidth > 768) {
        navBarLogo.style.width = '70%';
        navBarLogo.style.height = '60px';
      }

      if (clientWidth < 768) {
        navBarResponsive.style.background = 'linear-gradient(90deg,rgba(79,39,122,.98),rgba(39,39,39,.98))';
      }
    }
  });
}

function lazyLoadStyles() {
  var documentFragment = document.createDocumentFragment();

  if (isFrontPage || isBookLanding) {
    // Load CSS async
    var stylesArray = ['/woocommerce/packages/woocommerce-blocks/build/style.css', '/woocommerce/assets/css/woocommerce-layout.css', '/woocommerce/assets/css/woocommerce-smallscreen.css', '/woocommerce/assets/css/woocommerce.css', '/woocommerce-memberships/assets/css/frontend/wc-memberships-frontend.min.css'];
    if (isFrontPage) stylesArray.push('/contact-form-7/includes/css/styles.css');
    stylesArray.forEach(function (path) {
      documentFragment.appendChild(constructElement('link', {
        rel: 'stylesheet',
        href: pluginsUrl + path
      }));
    });
  }

  document.body.appendChild(documentFragment);
}

function lazyLoadScripts() {
  setTimeout(function () {
    (function (w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s),
          dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-PGW3FGP'); // Load jQuery deferred on book landing
    // if (isBookLanding) {
    // 	let documentFragment = document.createDocumentFragment();
    // 	[
    // 		'/wp-includes/js/jquery/jquery.js'
    // 	].forEach(function(path) {
    // 		documentFragment.appendChild(constructElement('script', {
    // 			src: siteUrl + path,
    // 			defer: true
    // 		}));
    // 	});
    //
    // 	document.head.appendChild(documentFragment);
    // }

  }, 3000); // GDPR cookie acceptance

  if (getCookie('hasConsent') !== 'true') {
    var cookiesEUBanner = document.getElementById('cookies-eu-banner');
    cookiesEUBanner.style.display = 'block';
    document.querySelectorAll('#cookies-eu-banner button').forEach(function (button) {
      button.addEventListener('click', function (e) {
        cookiesEUBanner.style.display = 'none';
        var date = new Date();
        document.cookie = 'hasConsent=true; path=/; expires=' + new Date(2147483647 * 1000).toUTCString();
      });
    });
  }
}

function getCookie(cookieName) {
  return (document.cookie.match('(^|; )' + cookieName + '=([^;]*)') || 0)[2];
}

var contactJSLoaded = false;
//# sourceMappingURL=themeplate.js.map
