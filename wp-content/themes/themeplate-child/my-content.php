<?php

/**
 * Template Name: My Content
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>

	<main id="my-content">
		<div class="page-banner simple-page">
			<h1 class="section-title dark-bg less-line-height"><?php single_post_title(); ?></h1>
			<hr class="gradient-separator-short">
			<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-2x.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
		</div>
		<div class="section-separator">
			<?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
			<div class="skew-separator"></div>
		</div>
<?php if (is_user_logged_in()) { ?>		
		<div id="book-bonuses-content">
			<div class="container">
				<div class="row">

					<?php

					$bundle_1 = '[wcm_restrict plans="e-book-vasarlok, fizikai-konyv-vasarlok"]

					    <div class="col-lg-3">
							<a class="card-link" href="' . site_url() . '/online-gyorsitosav-e-book/" rel="noopener">
								<div class="card">
									<img class="card-decoration" src="' . get_stylesheet_directory_uri() . '/assets/images/card-decoration.svg" alt="" />
									<p class="card-title">Online Gyorsítósáv</br> e-book</p>
									<img class="size-full wp-image-226 book" src="' . site_url() . '/wp-content/uploads/2021/04/gyorsitosav-konyv.png" alt="" />
									<p class="subtitle">Az eredményes online vállalkozások GPS-e</p>
									<hr class="gradient-separator-short">
									<p class="explanation">Egyre többen kívánnak online bizniszt építeni, azonban a siker is egyre komplexebb tudást igényel. Tartozz azok közé, akik rátalálnak a helyes ösvényre.</p>
								</div>
							</a>
						</div>
						<div class="col-lg-3">
							<a class="card-link" href="' . site_url() . '/uzleti-tervezo-vaszon/" rel="noopener">
								<div class="card">
									<img class="card-decoration" src="' . get_stylesheet_directory_uri() . '/assets/images/card-decoration.svg" alt="" />
									<p class="card-title">Üzleti Tervező Vászon</p>
									<img class="size-full wp-image-226" src="' . site_url() . '//wp-content/uploads/2020/06/business-model-canvas.png" alt="" width="200" height="200" />
									<p class="subtitle">Tervezz úgy, mint a legsikeresebbek</p>
									<hr class="gradient-separator-short">
									<p class="explanation">Készíts profi üzleti tervet pofonegyszerűen akár percek alatt. A tervezővászonnal semmilyen fontos részletről nem feledkezel meg.</p>
								</div>
							</a>
						</div>
						<div class="col-lg-3">
							<a class="card-link" href="' . site_url() . '/weboldal-onellenorzo-kerdoiv/" rel="noopener">
								<div class="card">
									<img class="card-decoration" src="' . get_stylesheet_directory_uri() . '/assets/images/card-decoration.svg" alt="" />
									<p class="card-title">Weboldal Önellenőrző Kérdőív</p>
									<img class="size-full wp-image-226" src="' . site_url() . '/wp-content/uploads/2020/06/project-questionnaire.png" alt="" width="200" height="200" />
									<p class="subtitle">Mindent bepakoltál az induláshoz?</p>
									<hr class="gradient-separator-short">
									<p class="explanation">Tudd meg, mindenre sikerült-e gondolnod weboldalad indítása, vagy újratervezése előtt. Teljeskörű ellenőrzőlista a sikeres tervezéshez.</p>
								</div>
							</a>
						</div>

					[/wcm_restrict]';

					echo do_shortcode("$bundle_1");


					?>

					<?php

					$bundle_2 = '[wcm_restrict plans="fizikai-konyv-vasarlok"]

						<div class="col-lg-3">
							<a class="card-link" href="' . site_url() . '/weboldal-arkalkulator/">
								<div class="card">
									<img class="card-decoration" src="' . get_stylesheet_directory_uri() . '/assets/images/card-decoration.svg" alt="" />
									<p class="card-title">Weboldal Árkalkulátor</p>
									<img class="size-full wp-image-225" src="' . site_url() . '/wp-content/uploads/2020/05/webpage-pricing-calculator.png" alt="" width="200" height="200" />
									<p class="subtitle">Interaktív költségtervező</p>
									<hr class="gradient-separator-short">
									<p class="explanation">Derítsd ki, hogy mennyibe kerül az igényeidhez igazodó weboldal elkészítése árkalkuláturunk kitöltésével. Láss tisztán és többé ne vesződj ködös árajánlatokkal.</p>
								</div>
							</a>
						</div>

						<div class="col-lg-3">
							<a class="card-link" href="' . site_url() . '/weboldal-megterulesi-kalkulator/">
								<div class="card">
									<img class="card-decoration" src="' . get_stylesheet_directory_uri() . '/assets/images/card-decoration.svg" alt="" />
									<p class="card-title">Weboldal Megtérülési Kalkulátor</p>
									<img class="size-full wp-image-226" src="' . site_url() . '/wp-content/uploads/2020/05/webpage-profit-calculator.png" alt="" width="200" height="200" />
									<p class="subtitle">Avagy hogy jön ki a matek?</p>
									<hr class="gradient-separator-short">
									<p class="explanation">A megtérülési kalkulátorral máris megtudhatod, hogy adott befektetés mellett várhatóan mikor térül meg a weboldalad fejlesztési költsége.</p>
								</div>
							</a>
						</div>

					[/wcm_restrict]';

					echo do_shortcode("$bundle_2");

					?>

					<?php

					$bundle_3 = '[wcm_restrict plans="landing-sablon-vasarlok"]

						<div class="col-lg-3">
							<a class="card-link" href="' . site_url() . '/landing-sablon/">
								<div class="card">
									<img class="card-decoration" src="' . get_stylesheet_directory_uri() . '/assets/images/card-decoration.svg" alt="" />
									<p class="card-title">Landing Sablon</p>
									<img class="size-full wp-image-225" src="' . site_url() . '/wp-content/uploads/2021/01/landing-pattern.png" alt="" width="200" height="200" />
									<p class="subtitle">Mutasd be termékedet profi módon</p>
									<hr class="gradient-separator-short">
									<p class="explanation">A vásárlószerzés tökéletes formulája. Útmutatókkal ellátott blokkjaiba csupán be kell helyettesítened saját tartalmadat, és már kész is vagy!</p>
								</div>
							</a>
						</div>

					[/wcm_restrict]';

					echo do_shortcode("$bundle_3");

					?>

					<?php

					$bundle_4 = '[wcm_restrict plans="ingyenes-weboldal-projektterkep, landing-sablon-vasarlok, e-book-vasarlok, fizikai-konyv-vasarlok"]

						<div class="col-lg-3">
							<a class="card-link" href="' . site_url() . '/weboldal-projektterkep/" rel="noopener">
								<div class="card">
									<img class="card-decoration" src="' . get_stylesheet_directory_uri() . '/assets/images/card-decoration.svg" alt="" />
									<p class="card-title">Interaktív Weboldal Projekttérkép (áttekintő nézet)</p>
									<img class="size-full wp-image-226" src="' . site_url() . '/wp-content/uploads/2021/01/webpage-project-map-new-intaractive.png" alt="" width="200" height="200" />
									<p class="subtitle">Minden teendőd 1 kattintásnyira</p>
									<hr class="gradient-separator-short">
									<p class="explanation">A projekttérkép áttekintő nézetében azonnal eléred bármely feladat leírását egyetlen kattintással. Mentsd el könyvjelzőként és térj vissza ide, ha bármi kétséged támad.</p>
								</div>
							</a>
						</div>

						<div class="col-lg-3">
							<a class="card-link" href="' . site_url() . '/projektterkep/" rel="noopener">
								<div class="card">
									<img class="card-decoration" src="' . get_stylesheet_directory_uri() . '/assets/images/card-decoration.svg" alt="" />
									<p class="card-title">Interaktív Weboldal Projekttérkép (részletes nézet)</p>
									<img class="size-full wp-image-226 new-projektterkep-icon" src="' . site_url() . '/wp-content/uploads/2021/04/webpage-project-map-redesigned.svg" alt="" width="200" height="200" />
									<p class="subtitle">150+ oldalnyi, ingyenes útmutató</p>
									<hr class="gradient-separator-short">
									<p class="explanation">Törj át az információs dzsungelen. Online Gyorsítósáv könyvünk kivonatolt tartalmával most minden eddiginél nagyobbat léphetsz előre az online siker felé.</p>
								</div>
							</a>
						</div>

						<div class="col-lg-3">
							<a class="card-link" href="' . site_url() . '/weboldal-projektterkep-a4-es-formatumban/" target="_blank" rel="noopener">
								<div class="card">
									<img class="card-decoration" src="' . get_stylesheet_directory_uri() . '/assets/images/card-decoration.svg" alt="" />
									<p class="card-title">Weboldal Projekttérkép A4-es formátumban</p>
									<img class="size-full wp-image-226" src="' . site_url() . '/wp-content/uploads/2021/01/webpage-project-map-new.png" alt="" width="200" height="200" />
									<p class="subtitle">A szükséges lépések, mindig szem eleőtt</p>
									<hr class="gradient-separator-short">
									<p class="explanation">A projekttérkép átolvasása után sokszor elég rápillantanod, és máris eszedbe jut majd a vállalkozásodat érintő aktuális fejlesztési ötlet, vagy megoldásra váró feladat.</p>
								</div>
							</a>
						</div>

					[/wcm_restrict]';

					echo do_shortcode("$bundle_4");

					?>

					<?php

					$bundle_5 = '[wcm_restrict plans="ingyenes-weboldal-projektterkep"]

						<div class="col-lg-3">
							<a class="card-link" href="' . site_url() . '/blog/150-oldalnyi-ingyenes-utmutato-weboldalad-eremenyessegenek-felporgetesehez/" target="_blank" rel="noopener">
								<div class="card">
								<img class="card-decoration" src="' . get_stylesheet_directory_uri() . '/assets/images/card-decoration.svg" alt="" />
									<p class="card-title">Weboldal Projekttérkép ismertető leírás</p>
									<img class="size-full wp-image-226" src="' . site_url() . '/wp-content/uploads/2021/01/blog.png" alt="" width="200" height="200" />
									<p class="subtitle">Maxold ki a Projekttérképet</p>
									<hr class="gradient-separator-short">
									<p class="explanation">Olvasd el leírásunkat és nézd meg ismertető videónkat, hogy a lehető legtöbbet hozhasd ki Weboldal Projekttérképünkből.</p>
								</div>
							</a>

						</div>

						<div class="col-lg-3">
							<a class="card-link" href="' . site_url() . '/blog/growth-hacking-hogyan-emeld-vallalkozasod-bovuleset-a-negyzetre/" target="_blank" rel="noopener">
								<div class="card">
								<img class="card-decoration" src="' . get_stylesheet_directory_uri() . '/assets/images/card-decoration.svg" alt="" />
									<p class="card-title">Hogyan repítsd új magasságokba vállalkozásod?</p>
									<img class="size-full wp-image-226 growth-hacking-icon" src="' . site_url() . '/wp-content/uploads/2021/05/growth-hacking.svg" alt="" width="200" height="200" />
									<p class="subtitle">A marketing következő generációja</p>
									<hr class="gradient-separator-short">
									<p class="explanation">A growth hacking az új sláger marketing körökben, azonban a sok apró tényező egyidejű megléte szükséges a sikerhez. A kulcs a sebesség és a hatékony végrehajtás.</p>
								</div>
							</a>

						</div>

					[/wcm_restrict]';

					echo do_shortcode("$bundle_5");

					?>


				</div>
			</div>
		</div>
	</main><!-- .content -->
	<?php } else { ?>
	<div class="content container container-wide">
		<div class="gradient-white"></div>
		<div class="woocommerce">
			<div class="woocommerce-info wc-memberships-restriction-message wc-memberships-message wc-memberships-content-restricted-message">A Tartalmaid csak bejelentkezés után érhetők el. Bejelentkezéshez látogass el a <a style="color: #f79024;font-weight: bold;text-decoration: none" href="<?php echo site_url();?>/fiokom" target="_blank">Fiókom</a> oldalra</div>
		</div>
	</div>
	<?php } ?>

<?php

get_footer();
