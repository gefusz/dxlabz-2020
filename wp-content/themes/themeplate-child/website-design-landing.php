<?php
/**
 * Template Name: Website Design Landing
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>

<section id="hero" class="front-page website-design-landing">
	<!-- <div class="hero-gradient"></div> -->
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-wide-orange.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
	<div class="yellow-lane"></div>
	<div class="container">
		<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/satisfaction-guarantee.svg" alt="100% elégedettségi garancia" data-no-lazy="1" class="satisfaction-guarantee hide-on-mobile">
		<div class="row">
			<div class="col-lg-6 text">
				<h1>KIS LÉPÉS AZ EMBERISÉGNEK,<br><span>ÓRIÁSI UGRÁS NEKED</span></h1>
				<!-- <p class="idea">AZ ÖTLETTŐL A PROFITIG</p> -->
				<div class="text-box">
					<h2>A <b>weboldal készítés</b> nem holdra szállás, de egy világklasszis csapat számodra is elérhetővé teheti a kiemelkedő eredményeket.<!-- <div class="gradient-text-bg"></div> --></h2>
				</div>
				<p class="narrow">Gyorsítósávon juttatunk el a tervezéstől a profitig, megfizethető áron.</p>
				<!-- <div class="call-to-action">
					<a href="#contact" class="btn-gradient">DÍJMENTES KONZULTÁCIÓT KÉREK</a>
				</div>  -->
				<div class="work-with-us">
					<h3><span>Eredmények,<br></span> amiket ügyfeleinknek elértünk</h3>
					<div class="row">
						<div class="col-lg-6 case-box left">
							<p class="yellow"><span>1</span> milliárd Ft</p>
							<p class="uppercase">Értékű ajánlatkérés</p>
							<p>6 hónap alatt,<br> új weboldalnál</p>
							<a href="https://fixbetonkerites.hu/" target="_blank" class="logo">
								<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/fixbeton-logo-white.svg" alt="fixbetonkerítés" data-no-lazy="1" class="fixbeton-logo-white">
							</a>
							<a href="<?php echo site_url();?>/blog/milliardos-potencial-az-indulast-kovetoen-mindossze-fel-evvel/" target="_blank" class="btn-gradient_purple">Esettanulmány<span class="icon"></span></a>
						</div>
						<div class="col-lg-6 case-box">
							<p class="yellow"><span>568%</span> -os</p>
							<p class="uppercase">Bevétel</p>
							<p>10 millió Ft fölötti forgalmú<br> webshopnál</p>
							<a href="https://edesburgonya.bio/" target="_blank" class="logo">
								<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/edesburgonya-logo-white.svg" alt="édesburgonya.bio" data-no-lazy="1" class="edesburgonya-logo-white">
							</a>
							<a href="<?php echo site_url();?>/blog/3-kozos-szezon-kozel-hatszorosara-novelt-bevetel/" target="_blank" class="btn-gradient_purple">Esettanulmány<span class="icon"></span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 form">
				<div class="form-box">
					<h3 class="yellow">Lépjünk közelebb</h3>
					<h3 class="uppercase">Terveid megvalósításához</h3>
					<?php echo do_shortcode('[contact-form-7 id="1430" title="Contact form weboldal készítés"]'); ?>
				</div>
				<a href="<?php echo site_url();?>/blog/hogyan-lettem-az-orszag-elso-mobil-web-specialistaja/" target="_blank"><div class="consultation">
<!--					<h3>A konzultációt tartja:</h3>-->
					<div class="row testimonial-person">
										<div class="col-lg-3 col-md-2 image">
								<?php picture('mobile-web-specialist', 'png', 'mobil web specialist',  true, 'image mobil-web-specialist'); ?></a>
							<!--							--><?php //picture('dx-labz-kriston-gabor', 'png', '',  false, 'gabor'); ?>
						</div>
						<div class="col-lg-9 col-md-10 text">
							<h4 class="name">Google Minősített<br>Web Specialista</h4>
							<!--							<p class="qualification">Google Minősített Web Specialista</p>-->
						</div>
					</div>
				</div></a>
			</div>
			<!-- <div class="col-lg-6 image">
				<div class="calculator">
                <div class="col8 white-side">
                    <div class="inputfields-box">
                        <ul>
                            <li>
                                <label for="range1">Hirdetések hatékonysága</label>
                                <div class="input-range"><span>Rossz</span><input type="range" name="range1" id="range1" min="0" max="1.5" value="0" step="0.15"><span>Kiváló</span></div>
                            </li>
                            <li>
                                <label for="range2">Betöltési sebesség</label>
                                <div class="input-range"><span>Rossz</span><input type="range" name="range2" id="range2" min="0" max="1.5" value="0" step="0.15"><span>Kiváló</span></div>
                            </li>
                            <li>
                                <label for="range5">Szöveges tartalom</label>
                                <div class="input-range"><span>Rossz</span><input type="range" name="range3" id="range3" min="0" max="1.5" value="0" step="0.15"><span>Kiváló</span></div>
                            </li>
                            <li>
                                <label for="range3">Vizuális megjelenés</label>
                                <div class="input-range"><span>Rossz</span><input type="range" name="range4" id="range4" min="0" max="1.5" value="0" step="0.15"><span>Kiváló</span></div>
                            </li>
                            <li>
                                <label for="range4">Használhatóság</label>
                                <div class="input-range"><span>Rossz</span><input type="range" name="range5" id="range5" min="0" max="1.5" value="0" step="0.15"><span>Kiváló</span></div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col8 blue-side">
                    <div class="inputfields-box">
                        <div class="group">
                        	<label for="visitors">Havi látogatószám</label>
                        	<div class="input-box"><input type="number" name="visitors" id="visitors" value="10000"></div>
						</div>

						<div class="group">
							<label for="productprice">Átlagos kosárérték</label>
							<div class="input-box"><input type="number" name="productprice" id="productprice" value="5000"></div>
						</div>

						<div class="result-box">
							<div class="group">
                            	<p class="title">Konverziós ráta</p>
                            	<p class="result"><span id="result"></span>%</p>
							</div>
							<div class="group">
                            	<p class="title">Havi árbevétel</p>
                            	<p class="result2"><span id="result2"></span>e Ft</p>
							</div>
                        </div>
                    </div>
                </div>
	        </div>
				 <?php picture('profitkalkulator', 'png', '',  true, 'calculator'); ?>
 				<div class="founder">
					<div class="col-lg-4 col-4 align-center">
						<?php picture('dx-labz-kriston-gabor', 'png', '',  true, 'gabor'); ?>
					</div>
					<div class="col-lg-8 col-8 align-left">
						<h4 class="name">KRISTON GÁBOR</h4>
						<p class="qualification">Az ország egyetlen<br>Google Mobil Web Specialistája</p>
					</div>
				</div>
				<p style="color: white;margin-top: 20px;font-weight: bold;">NÉZD meg mi a különbség a jól és rosszul teljesítő webdoldalak között!</p>
			</div> -->
		</div>
	</div>
</section> <!-- end of header -->
<div class="section-separator landing-page_webmap after-hero">
	<?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<!-- <section id="less-and-more" class="align-center website-design-landing">
	<div class="gradient-orange lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/less-and-more-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title dark-bg">Elhivatottságodat </h2>
		<h2 class="section-title text-purple-bg">EREDMÉNYEKKÉ FORMÁLJUK</h2>
	</div>
</section> -->
<section id="enthusiastic" class="front-page website-design-landing">
	<div class="gradient-purple"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title">Piacvezető vállalkozók tanúsítják, hogy<br><span class="text-gradient-bg">GONDOS KEZEKBEN LESZEL</span></h2>
		<hr class="gradient-separator-short">
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/quote-mark.svg" alt="Quote Mark"  class="lazyload quote-mark hide-on-mobile">

		<div class="row less-width">
			<div class="col-lg-6 testimonial-box">
				<p class="italic">"<strong class="bold-only">Szakmailag egyedülállóan felkészült</strong>, stabil értékrenddel rendelkező korrekt személyt ismerhettem meg benne.</p>
				<p class="italic">Csapatával elsősorban egyedi weboldalak készítésével és gyorsabbá tételével foglalkoznak. Nekem is segített Gábor a domarketing.hu sebességoptimalizálásában.</p>
				<p class="italic">24 óra alatt átköltöztette egy sokkal gyorsabb, jobb tárhelyre a weboldalt.<strong class="bold-only"> Nagyon elégedett voltam a közös munkával, jó szívvel ajánlom!"</strong></p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-doman-zsolt', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Domán Zsolt</h4>
						<p class="qualification">DO! Marketing</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“Nagyon megtetszett amit ő művel, a brutális szakértelem és emberi megfogalmazás. Kértem tőle egy konverzió optimalizálási elemzést a honlapunkra, ami überhasznos volt. Gábor <strong class="bold-only">mindent tud a témájában, évekkel előbbre jár a konkurenciánál.</strong></p>
				<p class="italic">Köszönöm Gábor ezt a vérprofi szakmai munkát és hogy megosztottad a tudásod (alapjait, ami nekünk hónapokra elég...) velünk.”</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-meszaros-robi', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Mészáros Robi</h4>
						<p class="qualification">BrandBirds</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“A DX Labz Kft.-t egy jó ismerősöm ajánlotta, mert nagyon esedékessé vált a Tudástár Klub oldalon tapasztalható lassulási gondok és egyéb felmerült problémák megoldása. <strong class="bold-only"> Nagyon elégedett vagyok a munkátokkal, mindig összeszedett, érthető, segítőkész válaszokat kaptam, és a felmerült hibákat is szakszerűen, gyorsan oldottátok meg.</strong> A jövőben is számítok az együttműködésünkre és szívesen fordulok hozzátok WordPress támogatásért.”</p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-szendrei-adam', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Szendrei Ádám</h4>
						<p class="qualification">Tudástár Klub</p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 testimonial-box">
				<p class="italic">“Azonnal felkeltették a figyelmemet, nem is haboztam egyből ajánlatot kértem. Azóta is együtt dolgozunk, profi kivitelezést, dinamikus és könnyen érthető egyenes kommunikációt kaptam a csapattól, most készítik a második weblapomat, de lesz harmadik is. <strong class="bold-only"> Bonyolult technikai problémák esetén is tudják mi a teendő.</strong></p>
				<div class="row testimonial-person">
					<div class="col-lg-3 col-3 col-md-2 image">
						<?php picture('testimonial-kovacs-andras', 'png', '',  true, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-9 col-9 col-md-10 text">
						<h4 class="name">Kovács András</h4>
						<p class="qualification">Édesburgonya.bio</p>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<div class="section-separator landing-page_webmap just-grey">
	<div class="skew-separator"></div>
</div>
<section id="why-us" class="website-design-landing">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/choose-your-path-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<!-- <img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-white.svg" alt="Pattern Top White"  class="lazyload pattern-top_white"> -->
	<div class="container relative-and-z-index dark-bg">
			<h2 class="section-title dark-bg">3 összetevő, amitől </br><span class="text-purple text-gradient-bg">GARANTÁLTAN BAJNOK LESZ A WEBOLDALAD</span></h2>
			<hr class="gradient-separator-short">
		<div class="row">
			<div class="col-lg-4 column u-text-center">
				<div class="img-box">
					<a target="_blank" href="<?php echo site_url();?>/online-gyorsitosav"><?php picture('online-gyorsitosav-book', 'png', 'online gyorsitosav book',  true, 'image online-gyorsitosav-book'); ?></a>
				</div>
				<h3>Az Online Gyorsítósáv sikerformulájával kikerülöd az aknamezőket</h3>
				<p>Összefoglaltunk mindent, ami fontos, hogy sikerre vidd vállalkozásod a weben.</p>
			</div>
			<div class="col-lg-4 column u-text-center">
				<div class="img-box">
					<a target="_blank" href="https://codeable.intercom-attachments-1.com/i/o/264527528/ed39e81c5751ceacdeda7a9d/Gabor-certificate.png">
						<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/codeable.svg" alt="Codeable"  class="lazyload codeable image">
					</a>

				</div>
				<a href="https://codeable.io/developers/gabor-kriston-dx-labz/" target="_blank"><h3>Két elit fejlesztő fogja biztosítani a repülőrajtot a weboldaladhoz</h3></a>
				<p>Csapatunk 2 munkatársa is a világ 500 legjobb WordPress fejlesztőjét tömörítő szakértői gárda tagja.</p>
			</div>
			<div class="col-lg-4 column u-text-center">
				<div class="img-box">
					<a target="_blank" href="https://www.credential.net/d1214df8-44b9-4c22-96af-f47184b44945"><?php picture('mobile-web-specialist', 'png', 'mobil web specialist',  true, 'image mobil-web-specialist'); ?></a>
				</div>
				<h3>Google minősítésünknek hála, a keresők is imádni fognak</h3>
				<p>A gyakorlatban alkalmazzuk azokat a technológiákat, amelyek jövőbiztossá tesznek.</p>
			</div>
		</div>
				<div class="call-to-action u-text-center">
			<a href="#hero" class="btn-gradient">ERRE VAN SZÜKSÉGEM!</a>
		</div>
	</div>
</section>
<div class="section-separator landing-page_webmap">
	<?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<section id="through">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/book-in-numbers-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title">Tálcán nyújtjuk<br><span class="text-gradient-bg"> A TÖKÉLETES RECEPTET</span><br> vállalkozásod felpörgetésére</h2>
		<hr class="gradient-separator-short">
		<div class="row">
			<div class="col-lg-6 action-box">
				<h3><span>1.</span> Betonbiztos üzleti alapok lerakása</h3>
				<p>Kielemezzük a célpiacod, a konkurenciád, és hogy mivel tudsz kitűnni a tömegből. Nem fognak kellemetlen meglepetések érni, olvasni fogod a célközönséged gondolatait. Kiszámoljuk azt is, hogy mikor lesz számodra az online vállalkozás megtérülő befektetés.</p>
				<ul>
					<li>Célközönség meghatározása</li>
					<li>Piackutatás és konkurencia elemzés</li>
					<li>Kulcsszókutatás</li>
					<li>Értékesítési cél meghatározása</li>
					<li>Megtérülési célszámok beazonosítása</li>
				</ul>
			</div>
			<div class="col-lg-6 action-box">
				<h3><span>2.</span> Lézerfókuszú, költséghatékony tervezési folyamat</h3>
				<p> Azonnal érthető módon leszel képes kommunikálni az értékeidet. Létrehozzuk az ügyfelek számára értéket adó, sallangoktól mentes weboldal specifikációt és arculatot, valamint megtervezzük értékesítési csatornádat.</p>
				<ul>
					<li>Keresőbarát tartalomgyártás</li>
					<li>Marketingterv készítés</li>
					<li>Értékesítési tölcsér megtervezéses</li>
					<li>Design</li>
				</ul>
			</div>
			<div class="col-lg-6 action-box">
				<h3><span>3.</span> Jövőbiztos technikai háttér</h3>
				<p>Stabil technikai alapokra építjük weboldalad. Nem az fog kiderülni az oldalad megépítése után, hogy több sebből vérzik a rendszered és a Google is a tenyerén hordoz majd.</p>
				<ul>
					<li>Sebesség- és mobil optimalizált fejlesztés</li>
					<li>Keresőoptimalizálás</li>
					<li>Értékesítést támogató funkciók beépítése</li>
					<li>Analitikai mérőkódok bekötése</li>
				</ul>
			</div>
			<div class="col-lg-6 action-box">
				<h3><span>4.</span> Katapultszerű eredménynövekedés</h3>
				<p>Folyamatosan monitorozzuk az oldalad teljesítményét. A gyors felhasználói visszajelzésre alapozott fejlesztésekkel napról-napra egyre jobban fognak kedvelni az ügyfeleid. Gondoskodunk arról, hogy stabilan legyenek érdeklőid, így üzleted képes lesz valóban eredményesen működni.</p>
				<ul>
					<li>Technikai karbantartás</li>
					<li>Adatelemzés és fejlesztési javaslattétel</li>
					<li>Facebook és Google hirdetéskezelés</li>
					<li>Email kampányok gyártása és kezelése</li>
					<li>Konverzió optimalizálás</li>
				</ul>
			</div>
		</div>
		<div class="call-to-action u-text-center">
			<a href="#hero" class="btn-gradient">INDULHATUNK?</a>
		</div>
	</div>
</section>
<div class="section-separator_right_top section_point-of-view">
	<?php picture('orange-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
	<div class="skew-separator"></div>
</div>

<section id="speed">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/key-of-success-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<a href="#hero"><h2 class="section-title dark-bg">Díjmentes konzultációnkkal <br><span class="text-purple text-gradient-bg">A CÉL ELÉRHETŐ TÁVOLSÁGBA KERÜL</span></h2></a>
		<hr class="gradient-separator-short">
			<p class="dark-bg less-line-height align-center extra-margin_bottom">Ha úgy érzed végre elindítanád online vállalkozásod és mindössze egy plusz kezdőlöketre lenne szükséged a sikeres online vállalkozók táborához vezető úton, jelentkezz ingyenes szakértői konzultációnkra a gombra kattintva.</p>
			<!-- <div class="call-to-action u-text-center">
				<a href="https://calendly.com/dx-labz/15perces-online-konzultacio" class="btn-gradient">JELENTKEZEM</a>
			</div> -->
			<div class="call-to-action u-text-center">
			<a href="#hero" class="btn-gradient">JELENTKEZEM</a>
		</div>
	</div>
</section>
<section id="reference" class="website-design-landing">
	<div class="container">
		<h2 class="section-title"><span class="text-gradient-bg">CSATLAKOZHATSZ A GYORSÍTÓSÁVON</span><br> haladók táborához</h2>
		<hr class="gradient-separator-short">
<!--			<p class="bigger-paragraph align-center extra-margin_top">Nemcsak mondjuk, csináljuk is</p>-->
	</div>
	<div class="ref-box relative-and-z-index">
		<div class="wide-container">
			<div class="row unique unique_makad">
				<div class="col-lg-6 image align-left">
					<?php picture('makad-img', 'png', '',  true, 'ref-image makad-img'); ?>
				</div>
				<div class="col-lg-6 logo-and-text">
					<a href="https://makadhillsaklub.com/" target="_blank"><?php picture('makad-hills-logo', 'png', '',  true, 'ref-logo makad-hills-logo'); ?></a>
					<p>A Makad Hills asztalitenisz és fitnessterem számára egy, a kiemelkedő szakmai felkészültségükre és prémium színvonalú szolgáltatásaik széles választékára is kellő hangsúlyt fektető, modern és sportos arculatot álmodtunk meg.</p>
					<div class="call-to-action">
						<a href="https://makadhillsaklub.com/" target="_blank" class="btn-gradient btn-makad">MEGNÉZEM</a>
					</div>
				</div>
			</div>
		</div>
		<div class="unique-skewbox_makad">
			<?php picture('unique-blue-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		</div>
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-left_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-left">
	</div>
	<div class="ref-box relative-and-z-index">
		<div class="wide-container">
			<div class="row unique">
				<div class="col-lg-6 logo-and-text">
					<a href="https://palmaestenger.hu" target="_blank"><?php picture('palma-es-tenger-logo', 'png', '',  true, 'ref-logo palma-logo'); ?></a>
					<p>A Pálma & Tenger oldalánál a fő cél az volt, hogy amennyire csak lehetséges, adjuk vissza a trópusok hangulatát. Nézd meg magad, mennyire sikerült ez a küldetés és utazz egzotikus tájakra!</p>
					<div class="call-to-action">
						<a href="https://palmaestenger.hu" target="_blank" class="btn-gradient">MEGNÉZEM</a>
					</div>
				</div>
				<div class="col-lg-6 image align-right">
					<?php picture('palma-es-tenger-img', 'png', '',  true, 'ref-image palma-img'); ?>
				</div>
			</div>
		</div>
		<div class="unique-skewbox_orange">
			<?php picture('unique-purple-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
		</div>
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-right_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-right">
	</div>
	<div class="ref-box relative-and-z-index">
		<div class="wide-container">
			<div class="row unique unique_fixbeton">
				<div class="col-lg-6 image align-left">
					<?php picture('fixbeton-img', 'png', '',  true, 'ref-image fixbeton-img'); ?>
				</div>
				<div class="col-lg-6 logo-and-text">
					<a href="https://fixbetonkerites.hu/" target="_blank"><?php picture('fixbeton-logo', 'png', '',  true, 'ref-logo fixbeton-logo'); ?></a>
					<p>A fixbetonkerites.hu weboldalának megtervezésekor olyan fő szempontokat tartottunk szem előtt, mint az általuk gyártott kerítéselemek minőségének és eleganciájának tükrözése a dizájnban és egy könnyen átlátható, ugyanakkor rendkívül átfogó információt nyújtó felület kialakítása az oda látogatók számára.</p>
					<div class="call-to-action">
						<a href="https://fixbetonkerites.hu/" target="_blank" class="btn-gradient btn-fixbeton">MEGNÉZEM</a>
					</div>
				</div>

			</div>
		</div>
		<div class="unique-skewbox_fixbeton">
			<?php picture('unique-light-green-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		</div>
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-left_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-left">
	</div>
	<div class="ref-box relative-and-z-index">
		<div class="wide-container">
			<div class="row unique">
				<div class="col-lg-6 logo-and-text">
					<a href="https://it.hu" target="_blank"><?php picture('it-logo', 'png', '',  true, 'ref-logo it-logo'); ?></a>
					<p>Az it.hu arculati és tartalmi tervezése során azt a célt tűztük ki, hogy a 25 éves múltra visszatekintő cég megbízhatósága és prémium színvonalú szolgáltatásai kihangsúlyozásra kerüljenek.</p>
					<div class="call-to-action">
						<a href="https://it.hu" target="_blank" class="btn-gradient btn-it">MEGNÉZEM</a>
					</div>
				</div>
				<div class="col-lg-6 image align-right">
					<?php picture('it-img', 'png', '',  true, 'ref-image'); ?>
				</div>
			</div>
		</div>
		<div class="unique-skewbox_purple">
			<?php picture('unique-orange-gradient-effect-left', 'png', '',  true, 'gradient-effect-left'); ?>
		</div>
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-right_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-right">
	</div>
	<div class="ref-box relative-and-z-index">
		<div class="wide-container">
			<div class="row unique unique_qth">
				<div class="col-lg-6 image align-left">
					<?php picture('qth-img', 'png', '',  true, 'ref-image qth-img'); ?>
				</div>
				<div class="col-lg-6 logo-and-text">
					<a href="https://qualitytours.hu" target="_blank"><?php picture('qth-logo', 'png', '',  true, 'ref-logo qth-logo'); ?></a>
					<p>A Quality Tours Hungary az ország egyik legmeghatározóbb csoportos beutazási ügynöksége. Fontos volt, hogy a cég megjelenése méltó köntöst kapjon és egyben hazánkat vonzó módon mutassuk be.
					<div class="call-to-action">
						<a href="https://qualitytours.hu" target="_blank" class="btn-gradient btn-qth">MEGNÉZEM</a>
					</div>
				</div>

			</div>
		</div>
		<div class="unique-skewbox_qth">
			<?php picture('unique-green-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
		</div>
		<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-left_pale.svg" alt="Pattern Right Pale"  class="lazyload pattern-left">
	</div>
</section>

<section id="choose-your-path" class="website-design-landing">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/choose-your-path-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pattern-top-red.svg" alt="Pattern Top White"  class="lazyload pattern-top_red hide-on-mobile">
	<div class="container relative-and-z-index">
		<h2 class="section-title dark-bg"><span class="text-purple text-gradient-bg">MELYIK UTAT VÁLASZTOD</span><br>a sikerhez?</h2>
		<hr class="separator-short_white">
		<div class="row">
			<div class="col-lg-4 col-md-4 offer-box relative-and-z-index">
				<p class="offer-title align-center less-line-height">ONLINE GYORSÍTÓSÁV ALAPOZÓ</p>
				<hr class="gradient-separator-short">

				<ul>
					<li>Márkaáttekintés</li>
					<li>Konkurencia elemzés</li>
					<li>Üzleti célok és célpiac meghatározása</li>
					<li>Kulcsszóelemzés</li>
					<li>Online marketing terv</li>
					<li>Oldal- és tartalomvázlat</li>
				</ul>
				<p class="offer-price align-center">90 000<span> Ft</span></p>
				<p><strong class="bold-only dark">Kinek ajánljuk?</strong></p>
				<p>Ha nem vagy biztos üzleti koncepciód piackészségében vagy meglévő weboldaladat szeretnéd teljeskörűen kiértékeltetni.</p>
			</div>
			<div class="col-lg-4 col-md-4 offer-box best-offer relative-and-z-index">
				<p class="offer-title align-center less-line-height">"DŐLJ HÁTRA" WEBOLDAL SIKERCSOMAG</p>
				<hr class="gradient-separator-short">
				<ul>
					<li>Prototípus készítés</li>
					<li>Design létrehozása (logo + arculat)</li>
					<li>Sebesség optimalizált weboldal motor</li>
					<li>Mobil optimalizált fejlesztés</li>
					<li>Keresőoptimalizálás</li>
				</ul>
				<p><strong class="bold-only dark">Bónusz</strong></p>
				<ul>
					<li>Analitikai- és konverziós kódok bekötése</li>
					<li>1. havi analitikai jelentés</li>
					<li>Optimalizálási javaslattétel</li>
					<li>Prémium bővítmények </li>
					<li>Online Gyorsítósáv könyv</li>
				</ul>
				<p class="offer-price align-center">290 000<span> Ft-tól</span></p>
				<div class="obligatory">
					<p><strong class="bold-only">EZT MINDENKÉPP AJÁNLJUK MELLÉ:</strong></p>
					<p><strong>Online Gyorsítósáv alapozó</strong></p>
				</div>
				<p><strong class="bold-only dark">Kinek ajánljuk?</strong></p>
				<p>Ha szeretnél biztosra menni, és weboldalad egy éjjel-nappal működő vevőszerző gépezetként kívánod működtetni.</p>
			</div>
			<div class="col-lg-4 col-md-4 offer-box">
				<p class="offer-title align-center less-line-height">KONVERZIÓ TUNING</p>
				<hr class="gradient-separator-short">

				<ul>
					<li>Videókövetés bekapcsolása</li>
					<li>Részletes analitikai elemzés és fejlesztési javaslattétel, weboldal audit</li>
					<li>Sebesség optimalizálás++</li>
					<li>5 óra weboldal konverzió optimalizáláli>
				</ul>
				<p><strong class="bold-only dark">Ügyfélszerzés</strong></p>
				<ul>
					<li>Hirdetések bekonfigurálása 1 platformon</li>
				</ul>
				<p class="offer-price align-center">90 000<span> Ft</span></p>
				<p><strong class="bold-only dark">Kinek ajánljuk?</strong></p>
				<p>Akik nem elégedettek weboldaluk eredményeivel, vagy szeretnék a csúcsra járatni oldalukat.</p>
			</div>
		</div>



	</div>
</section>

<section id="gift">
	<div class="container">
		<h2 class="section-title"><span class="text-gradient-bg">339 000 FT ÉRTÉKŰ AJÁNDÉK</span><br>új weboldalad mellé útravalóul</h2>
		<hr class="gradient-separator-short">
		<div class="row">
			<div class="col-lg-3 icon-box">
				<img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/gift-icon.svg" alt="DX-Labz ajándék" class="gift-icon lazyload">
			</div>
			<div class="col-lg-9">
				<ul>
					<li>Analitikai elemzés, fejlesztési ajánlások - <strong class="bold-only">50 000 Ft</strong></li>
					<li>3 hónapos ingyenes támogatás (frissítések, karbantartás) - <strong class="bold-only">50 000 Ft</strong></li>
					<li>Prémium bővítmények a weboldaladhoz - <strong class="bold-only">200 000 Ft</strong></li>
					<li>Facebook borító és profil képek szövegekkel - <strong class="bold-only">30 000 Ft</strong></li>
					<li>Online Gyorsítósáv könyv, az online vállalkozások sikertérképe - <strong class="bold-only">9 000 Ft</strong></li>
				</ul>
			</div>
		</div>
		<div class="call-to-action u-text-center">
			<a href="#hero" class="btn-gradient">VELÜNK TARTASZ?</a>
		</div>
	</div>
</section>

<!-- <section id="projektmap" class="website-design-landing">
	<div class="gradient-orange lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/time-spent-on-writing-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title dark-bg less-line-height">Ingyenes projekttérképünkkel<br><span class="text-purple-bg_gradient">BIZTOSÍTJUK A KÖZÖS NEVEZŐT</span></h2>
		<hr class="separator-short_purple">
		<div class="row">
			<div class="col-lg-8 text">
				<p class="dark-bg"><span class="uppercase"><b>150+ oldalnyi, ingyenes útmutató</b></span><br>weboldalad eredményességének felpörgetéséhez, interaktív formában az <b>Online Gyorsítósáv könyv</b> anyagából.<div class="gradient-text-bg"></div></p>
				<p class="dark-bg">Az eredményes online vállalkozások térképe, ami mindig mutatja majd, hogy weboldalad adott életszakaszában milyen feladatok várnak rád!</p>
			</div>
			<div class="col-lg-4 image">
				<?php picture('weboldal-projekt-terkep-landing-hero-img', 'png', '',  true, 'web-projectmap'); ?>
			</div>
		</div>
		<div class="call-to-action u-text-center">
			<a href="<?php echo site_url(); ?>/ingyenes-weboldal-projektterkep/#contact" class="btn-gradient_purple">SZEREZD MEG MOST!</a>
		</div>
	</div>
</section> --> <!-- end of header -->
<!-- <div class="section-separator_right_bottom section_mission">
	<?php picture('purple-gradient-effect-right', 'png', '',  true, 'gradient-effect-right'); ?>
	<div class="skew-separator"></div>
</div> -->



<!-- <div class="section-separator landing-page_webmap just-grey">
	<div class="skew-separator"></div>
</div> -->
<!--
<section id="contact" class="front-page website-design-landing">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/time-spent-on-writing-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
			<h2 class="section-title dark-bg">Lépjünk közelebb<br><span class="text-purple text-gradient-bg">TERVEID MEGVALÓSÍTÁSÁHOZ</span><br></h2>
			<hr class="gradient-separator-short">
			<div class="lesser-width">
				<p class="dark-bg less-line-height align-center extra-margin_bottom">Felkeltettük érdeklődésed? Írj egy pár sort projektedről, és elmondjuk, miben fejlődhet szerintünk a branded, weboldalad, hogyan hagyhatod magad mögött végre a konkurenseidet és milyen megoldásokkal válhatsz piacvezetővé.</p>
				<?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
			</div>

	</div>
</section> -->
<style>
	.newsletter {
		position: absolute;
		background: #f79024;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}

	.newsletter {
		width: 720px;
	}

	.exit-intent-popup {
		position: fixed;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
		z-index: 10000000000;
		background: rgba(33, 33, 33, 0.8);
		transform: translateY(60%) scale(0);
	}

	.exit-intent-popup.visible {
		transform: translateY(0) scale(1);
		transition: transform 0.3s cubic-bezier(0.4, 0.0, 0.2, 1);
	}

	@media (max-width: 768px) {
		.exit-intent-popup {
			display: none!important;
		}
	}
</style>
<div class="exit-intent-popup">
	<div class="newsletter">
		<a href="<?php echo get_site_url() . '/ingyenes-weboldal-projektterkep/#contact'; ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dx-labz-weboldal-keszites-landing-projektterkep-popup.jpg">
		</a>
	</div>
</div>
<script>
	const CookieService = {
		setCookie(name, value, days) {
			let expires = '';

			if (days) {
				const date = new Date();
				date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
				expires = '; expires=' + date.toUTCString();
			}

			document.cookie = name + '=' + (value || '')  + expires + ';';
		},

		getCookie(name) {
			const cookies = document.cookie.split(';');

			for (const cookie of cookies) {
				if (cookie.indexOf(name + '=') > -1) {
					return cookie.split('=')[1];
				}
			}

			return null;
		}
	}

	const mouseEvent = e => {
		const shouldShowExitIntent =
				!e.toElement &&
				!e.relatedTarget &&
				e.clientY < 10;

		if (shouldShowExitIntent && !CookieService.getCookie('exitIntentShown')) {
			document.removeEventListener('mouseout', mouseEvent);
			document.querySelector('.exit-intent-popup').classList.add('visible');

			// Set the cookie when the popup is shown to the user
			CookieService.setCookie('exitIntentShown', true, 30);
		}
	};

	const exit = e => {
		const shouldExit =
				[...e.target.classList].includes('exit-intent-popup') || // user clicks on mask
				e.target.className === 'close' || // user clicks on the close icon
				e.keyCode === 27; // user hits escape

		if (shouldExit) {
			document.querySelector('.exit-intent-popup').classList.remove('visible');
		}
	};

	document.querySelector('.exit-intent-popup').addEventListener('click', exit);

	// Wrap the setTimeout into an if statement
	if (!CookieService.getCookie('exitIntentShown')) {
		setTimeout(() => {
			document.addEventListener('mouseout', mouseEvent);
			document.addEventListener('keydown', exit);
		}, 10_000);
	}
</script>
<?php
get_footer();
//get_template_part( 'template-parts/footer', 'without-nav' );
//?>
