<?php

/**
 * The template for displaying single posts
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<section id="single-post">
  <div class="page-banner">
	</div>
  	  <div class="section-separator">
  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
  <div class="skew-separator"></div>
</div>
<?php if (is_product()) { ?> 
<div class="single-product-page">
	<div class="content-sidebar container">
		<main class="content">

			<?php while ( have_posts() ) : ?>
				<?php the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( has_post_thumbnail() ) : ?>
						<div class="entry-featured">
							<?php the_post_thumbnail(); ?>

						</div>
					<?php endif; ?>

					<div class="product-wrap">
						<div class="product-content">
							<?php the_content(); ?>
						</div>
					</div>
				</article><!-- #post-<?php the_ID(); ?> -->

			<?php endwhile; ?>

		</main><!-- .content -->

	</div><!-- .content-sidebar.container -->
</div>
<?php } else { ?>
	<div class="content-sidebar container">
		<main class="content">

			<?php while ( have_posts() ) : ?>
				<?php the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( has_post_thumbnail() ) : ?>
						<div class="entry-featured">
							<?php the_post_thumbnail(); ?>
							<div class="post-title">
								<h1 class="entry-title"><?php the_title(); ?></h1>
								<hr class="separator-left">

							</div>
							<div class="post-category">
				                <?php 
				                  $categories = get_the_category();
				                  $separator = ' ';
				                  $output = '';
				                  if ( ! empty( $categories ) ) {
				                      foreach( $categories as $category ) {
				                          $output .= '<p><a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a></p>' . $separator;
				                      }
				                      echo trim( $output, $separator );
				                  }
				                ?>
			                </div>
						</div>
					<?php endif; ?>

					<div class="entry-wrap row">
						<div class="col-lg-3">
							<div class="entry-meta">
								<?php echo get_avatar(get_the_author_id()); ?>
								<div class="post-details">
									<p class="author-name"><?php the_author(); ?></p>
									<time><?php the_date();?></time>
								</div>

							</div>
						</div>
						<div class="entry-content col-lg-9">
							<?php the_content(); ?>
						</div>

						<footer class="entry-footer">
							<?php the_tags(); ?>
						</footer>
					</div>
				</article><!-- #post-<?php the_ID(); ?> -->

			<?php endwhile; ?>

		</main><!-- .content -->

	</div><!-- .content-sidebar.container -->

<?php } ?>
</section>

<section id="projektmap" class="website-design-landing">
	<div class="gradient-orange lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/time-spent-on-writing-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
		<h2 class="section-title dark-bg less-line-height">Ingyenes projekttérképünkkel<br><span class="text-purple-bg_gradient">BIZTOSÍTJUK A KÖZÖS NEVEZŐT</span></h2>
		<hr class="separator-short_purple">
		<div class="row">
			<div class="col-lg-8 text">
				<p class="dark-bg"><span class="uppercase"><b>150+ oldalnyi, ingyenes útmutató</b></span><br>weboldalad eredményességének felpörgetéséhez, interaktív formában az <b>Online Gyorsítósáv könyv</b> anyagából.<div class="gradient-text-bg"></div></p>
				<p class="dark-bg">Az eredményes online vállalkozások térképe, ami mindig mutatja majd, hogy weboldalad adott életszakaszában milyen feladatok várnak rád!</p>
			</div>
			<div class="col-lg-4 image">
				<?php picture('weboldal-projekt-terkep-landing-hero-img', 'png', '',  true, 'web-projectmap'); ?>
			</div>
		</div>
		<div class="call-to-action u-text-center">
			<a href="<?php echo site_url(); ?>/ingyenes-weboldal-projektterkep/#contact" class="btn-gradient_purple">SZEREZD MEG MOST!</a>
		</div>
	</div>
</section>

<section id="contact" class="front-page">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/time-spent-on-writing-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
			<h2 class="section-title dark-bg"><span class="text-purple text-gradient-bg">BESZÉLJÜNK RÓLA</span><br>hogyan tudnánk neked segíteni</h2>
			<hr class="gradient-separator-short">
			<div class="lesser-width">
				<p class="dark-bg less-line-height align-center extra-margin_bottom">Felkeltettük érdeklődésed? Írj egy pár sort projektedről, és elmondjuk, miben fejlődhet szerintünk a branded, weboldalad, hogyan hagyhatod magad mögött végre a konkurenseidet és milyen megoldásokkal válhatsz piacvezetővé.</p>
				<?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
			</div>

	</div>
</section>

<?php

get_footer();
