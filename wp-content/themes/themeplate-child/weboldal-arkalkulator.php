<?php

/**
 * Template Name: Weboldal Árkalkulátor
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
	<section id="page-profit">
	<div class="page-banner">
		<h1 class="section-title dark-bg less-line-height extra-margin_bottom">WEBOLDAL <span class="text-purple text-gradient-bg">ÁRKALKULÁTOR</span></h1>
		<hr class="gradient-separator-short">
	</div>
	<div class="section-separator">
		<?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
		<div class="skew-separator"></div>
	</div>
	<main>
		<div id="my-content-subpage">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 inner-content">

						<?php while ( have_posts() ) : ?>
							<?php the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
		
					</div>
					<?php
				      include "sidebar.php";
				    ?>
				</div>


			</div>
		</div>
		<div id="web-roi">
			<style>
				.cognito {
					position: relative;
					min-height: 80vh;
					background: white;
					max-width: 800px;
					margin: 0 auto;
					box-shadow: 0 10px 20px rgba(0,0,0,.19), 0 6px 6px rgba(0,0,0,.23);
				}
				.cognito form {
					opacity: 0;
					transition: opacity 1s;
				}
				.cognito.c-safari form {
					opacity: 1;
				}
				.cognito .preloader {
					position: absolute;
					z-index: 2;
					transition: opacity 0.5s;
					background: center no-repeat url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/cognito-forms-logo.png);
					-webkit-animation: rotation 2s infinite linear;
					width: 100%;
					height: 80vh;
				}
				.cognito.c-safari .preloader {
					position: absolute;
					opacity: 0;
					pointer-events: none;
				}

				@-webkit-keyframes rotation {
					from {
						-webkit-transform: rotate(0deg);
					}
					to {
						-webkit-transform: rotate(359deg);
					}
				}

				#services.cognito-forms {
					text-align: left;
				}
				#must-haves, #services {
					padding-top: 0;
					padding-bottom: 66px;
					text-align: center;
				}

				#services .row {
					margin-bottom: 30px;
				}
				.cognito-forms .row {
					width: 94%;
					max-width: 1024px;
					margin: 0 auto;
					display: block;
				}

				#services.cognito-forms .cognito .c-forms-form {
					max-width: 100%;
					padding: 25px;
					box-shadow: 0 1px 3px rgba(0,0,0,.12), 0 1px 2px rgba(0,0,0,.24);
				}

				#services.cognito-forms .cognito .c-section {
					padding: 15px 10px;
					margin-top: 25px;
					box-shadow: 0 1px 3px rgba(0,0,0,.12), 0 1px 2px rgba(0,0,0,.24);
				}

				.cognito .c-forms-form .c-forms-form-title h2,
				.cognito .c-forms-form .c-forms-description,
				.cognito .c-forms-form :not(.c-question) > .c-label,
				.cognito .c-forms-form .c-editor {
					font-family: Montserrat, Sans-serif!important;
				}

				.cognito .c-forms-form button:not(.c-icon-button), .cognito .c-forms-form .c-add-item, .cognito .c-forms-form input[type=button], .cognito .c-forms-form .c-fileupload-dropzone .c-upload-button button {
					background: linear-gradient(90deg,#f79024,#ef2453);
					padding: 17px 30px!important;
					box-shadow: 0 10px 20px rgba(0,0,0,.19), 0 6px 6px rgba(0,0,0,.23)!important;
					border-radius: 4px;
					font-weight: 700;
					letter-spacing: 1.5px;
					text-transform: uppercase;
					font-size: 1.15rem!important;
					font-weight: bold!important;
					transition: all 0.3s ease-in-out;
				}

				.cognito .c-forms-form button:not(.c-icon-button):hover {
					transform: scale(0.95);
				}
			</style>
			<!-- Services Section
			================================================== -->
			<section id="services" class="cognito-forms" >

				<div class="row">

					<div class="twelve columns">

						<div class="cognito">
							<div class="preloader">
							</div>
							<script src="https://services.cognitoforms.com/s/ELBDnOif8ku6A1DuLlGI6A"></script>
							<script>
								Cognito.load("forms", { id: "2" });//, {
								//    success: function() {
								//                ExoJQuery(function() {
								//                   ExoJQuery(document).on('afterSubmit.cognito', function(e, data) {
								//                      if (!window.conversionReported) {
								//                         (function (url) {
								//                            var callback = function () {
								//                               if (typeof(url) != 'undefined') {
								//                                  window.location = url;
								//                               }
								//                            };
								//                            gtag('event', 'conversion', {
								//                               'send_to': 'AW-982143190/hlbQCJaIuHoQ1qGp1AM',
								//                               'event_callback': callback
								//                            });
								//                            return false;
								//                         })();

								//                         ga('send', 'event', 'Cognito Form', 'submit', 'Árkalkulátor érdeklődés');

								//                         conversionReported = true;
								//                      }
								//                   });
								//                });
								//             }
								// });
							</script>
						</div>

					</div>

			</section>
		</div>
	</main>
</section>
<?php
get_footer();
