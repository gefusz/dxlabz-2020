<?php

/**
 * Template Name: Projekttérkép
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<style>
	#projektterkep-view-switcher {
		width: 90px;
	    position: fixed;
	    left: -2px;
	    top: 130px;
	    padding: 5px 10px 10px;
	    border: 2px solid #f68529;
	    z-index: 5;
	    background: #ffffff;
	    font-size: 12px;
	    line-height: 1;
	    border-radius: 3px;
	    -webkit-box-shadow: 0 4px 20px 0 rgb(0 0 0 / 50%);
	    box-shadow: 0 4px 20px 0 rgb(0 0 0 / 50%);
	    text-align: center;
	    color: inherit;
	}

	#projektterkep-view-switcher a {
		color: #3f3f3f!important;
	}

	#projektterkep-view-switcher img {
		font-style: italic;
	    max-width: 100%;
	    vertical-align: middle;
    	transform: scale(1.3);
	}

	.projektmap-intro-popup-overlay {
		display: none;
	}

	.projektmap-intro-popup-overlay.book-popup img {
		padding: 0;
		max-width: 700px;
	}

	@media (max-width: 768px) {
		.projektmap-intro-popup-overlay.book-popup img {
			max-width: 300px;
		}
	}
</style>
<div id="projektterkep-view-switcher">
	<a href="<?php echo get_site_url() . '/weboldal-projektterkep'; ?>">
		<img src="<?php echo get_site_url() . '/wp-content/uploads/2021/01/webpage-project-map-new-intaractive.png'; ?>">
		Áttekintő nézet
	</a>
</div>
  <div class="page-banner simple-page">
        <h1 class="section-title dark-bg less-line-height"><span class="text-gradient-bg">WEBOLDAL</span> projekttérkép</h1>
        <hr class="gradient-separator-short">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-2x.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
  </div>
  <div class="section-separator">
	  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
	  <div class="skew-separator"></div>
  </div>
<!-- <?php if(is_user_logged_in()) {?>
	<style type="text/css">
		#html5lightbox-watermark {
			display: none!important;
		}
	</style>
	<div class="projektmap-intro-popup-overlay">
		<div class="projektmap-intro-popup">
			<div class="projektmap-intro-popup__close-icon">X</div>
			<?php picture('weboldal-projekt-terkep-magyarazat', 'jpeg', '',  false, 'gradient-effect-left'); ?>
		</div>
	</div>
<?php } ?> -->
<?php if (is_user_logged_in()) { ?>
<div id="projektterkep">

	<div class="projektmap-intro-popup-overlay book-popup">
		<div class="projektmap-intro-popup">
			<div class="projektmap-intro-popup__close-icon">X</div>
			<a href="<?php echo get_site_url() . '/online-gyorsitosav'; ?>">
				<img src="<?php echo get_stylesheet_directory_uri() . '/dx-labz-online-gyorsitosav-konyv-popup.jpg'; ?>">
			</a>
		</div>
	</div>
	<main class="content container container-wide">
		
		<ul class="nav nav-pills">
		    <li class="active"><a data-toggle="pill" href="#home">1. Előkészítés</a></li>
		    <li><a data-toggle="pill" href="#menu2">2. Validálás</a></li>
		    <li><a data-toggle="pill" href="#menu3">3. Tervezés</a></li>
		    <li><a data-toggle="pill" href="#menu4">4. Kivitelezés</a></li>
		    <li><a data-toggle="pill" href="#menu5">5. Működtetés</a></li>
		</ul>
		  
		<div class="tab-content">
		    <div id="home" class="tab-pane fade in active">
		    	<div class="tab-header">
		    		<div class="tab-header-content">
		    			<div class="row">
			    			<div class="col-lg-3 u-text-center">
			    				<p class="number">1.</p>
			    			</div>
			    			<div class="col-lg-9">
			    				<h2 class="tab-name">Előkészítés</h2>
			    				<table>
			    					<tr>
			    						<td>időigény</td>
			    						<td>1 hónap</td>
			    					</tr>
			    					<tr class="cost-intensity">
			    						<td>költségintenzitás</td>
			    						<td>1x</td>
			    						<td class="doubts-button-box"><img class="doubts-button" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/doubts-button.svg" alt="doubts-button"/></td>
			    						<td class="description">A Költségintenzitás azt jelöli, hogy a projekt adott életszakaszában elvégzendő feladatok kihagyása vagy nem megfelelő kivitelezése, és későbbi pótlása mennyi többletköltséggel jár. Példa: ha a weboldal grafikáját akkor akarod módosítani ha már elkészült az oldal, akkor duplán fizetsz érte. Egyszer elkészült az eredeti tervek szerint, le lett fejlesztve és most újra végig kell menni ugyanezen az úton.</td>
			    					</tr>
			    				</table>
			    			</div>
		    			</div>
		    			<p class="explanation">Az 1. szakasz minden esetben az, hogy helyesen mérd fel a projekted életképességét. Mielőtt bármit csinálnál, szükséges hogy legyen egy jól körülírható képed a célközönségről és a piacodról, mert ez képezi minden további tervezés alapját. Minél teljesebb ez a leírás, annál jobban meghatározható, hogy mekkora piacra lősz majd és milyen esélyekkel indulsz.</p>
			      	</div>
			      	<img class="decoration" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/hero-pattern-right.svg" alt="decoration"/>
		      	</div>
		      	<div class="tab-main">
		      		<div class="tab-main-content"> 


		      			<?php 
		      				
				            $args_projektterkep = array(
				                'post_type' => 'projektterkep',
				                'category_name' =>  'elokeszites',
				                'posts_per_page' => -1,
			                    'meta_key' => 'order',
    							'orderby' => 'meta_value_num',
    							'order' => 'ASC',
				            );

				            $i = 0;
				            $loop_projektterkep = new WP_Query( $args_projektterkep );
				            $cc = count($query);
				            if ( $loop_projektterkep->have_posts() ) : while ( $loop_projektterkep->have_posts() ) : $loop_projektterkep->the_post(); 

				            $i++;
                		?>


			      		<div class="accordion <?php echo ($i==1)?'active':''; ?>">
	                    	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
	                    	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
	                    	<h3 class="subtitle"><?php the_title(); ?></h3>
		                </div>
		                <div class="panel <?php echo ($i==1)?'open':''; ?>">
		                    <?php the_content(); ?>
		                    <a href="<?php the_field('button_link') ?>" class="btn-gradient" target="_blank">Tovább</a>
		                </div>

		              

		                <?php endwhile; ?>

						<?php else : ?>
						  <p><?php __('No News'); ?></p>
						<?php endif; ?>


		             
	                </div>
		      	</div>
		    </div>
		    <div id="menu2" class="tab-pane fade">
		    	<div class="tab-header">
		    		<div class="tab-header-content">
		    			<div class="row">
			    			<div class="col-lg-3 u-text-center">
			    				<p class="number">2.</p>
			    			</div>
			    			<div class="col-lg-9">
			    				<h2 class="tab-name">Validálás</h2>
			    				<table>
			    					<tr>
			    						<td>időigény</td>
			    						<td>2 hét</td>
			    					</tr>
			    					<tr class="cost-intensity">
			    						<td>költségintenzitás</td>
			    						<td>1x</td>
			    						<td class="doubts-button-box"><img class="doubts-button" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/doubts-button.svg" alt="doubts-button"/></td>
			    						<td class="description">A Költségintenzitás azt jelöli, hogy a projekt adott életszakaszában elvégzendő feladatok kihagyása vagy nem megfelelő kivitelezése, és későbbi pótlása mennyi többletköltséggel jár. Példa: ha a weboldal grafikáját akkor akarod módosítani ha már elkészült az oldal, akkor duplán fizetsz érte. Egyszer elkészült az eredeti tervek szerint, le lett fejlesztve és most újra végig kell menni ugyanezen az úton.</td>
			    					</tr>
			    				</table>
			    			</div>
		    			</div>
		    			<p class="explanation">A gyorsan történő, validált (igazolt) tanulással köröket verhetsz versenytársaidra. A validált tanulás arról szól, hogy egy bizonyos feltételezésünkre valós válaszokat kapjunk a célpiacunktól, lehetőleg még azelőtt, hogy túl sokat invesztálnánk egy nem működő ötletbe. A validált tanulással gyakori teszteken keresztül, adatalapon igyekszünk megtudni minél többet ügyfeleink valós igényeiről, a probléma felmerülésének pillanatától az újravásárlásig.</p>
			      	</div>
			      	<img class="decoration" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/hero-pattern-right.svg" alt="decoration"/>
		      	</div>
		      	<div class="tab-main">
		      		<div class="tab-main-content">

		      			<?php 
		      				
				            $args_projektterkep = array(
				                'post_type' => 'projektterkep',
				                'category_name' =>  'validalas',
				                'posts_per_page' => -1,
			                    'meta_key' => 'order',
    							'orderby' => 'meta_value_num',
    							'order' => 'ASC',
				            );

				            $i = 0;
				            $loop_projektterkep = new WP_Query( $args_projektterkep );
				            $cc = count($query);
				            if ( $loop_projektterkep->have_posts() ) : while ( $loop_projektterkep->have_posts() ) : $loop_projektterkep->the_post(); 

				            $i++;
                		?>


			      		<div class="accordion <?php echo ($i==1)?'active':''; ?>">
	                    	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
	                    	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
	                    	<h3 class="subtitle"><?php the_title(); ?></h3>
		                </div>
		                <div class="panel <?php echo ($i==1)?'open':''; ?>">
		                    <?php the_content(); ?>
		                    <a href="<?php the_field('button_link') ?>" class="btn-gradient" target="_blank">Tovább</a>
		                </div>

		              

		                <?php endwhile; ?>

						<?php else : ?>
						  <p><?php __('No News'); ?></p>
						<?php endif; ?>


			      		
	                </div>
		      	</div>
		    </div>
		    <div id="menu3" class="tab-pane fade">
		    	<div class="tab-header">
		    		<div class="tab-header-content">
		    			<div class="row">
			    			<div class="col-lg-3 u-text-center">
			    				<p class="number">3.</p>
			    			</div>
			    			<div class="col-lg-9">
			    				<h2 class="tab-name">Tervezés</h2>
			    				<table>
			    					<tr>
			    						<td>időigény</td>
			    						<td>2 hónap</td>
			    					</tr>
			    					<tr class="cost-intensity">
			    						<td>költségintenzitás</td>
			    						<td>2x</td>
			    						<td class="doubts-button-box"><img class="doubts-button" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/doubts-button.svg" alt="doubts-button"/></td>
			    						<td class="description">A Költségintenzitás azt jelöli, hogy a projekt adott életszakaszában elvégzendő feladatok kihagyása vagy nem megfelelő kivitelezése, és későbbi pótlása mennyi többletköltséggel jár. Példa: ha a weboldal grafikáját akkor akarod módosítani ha már elkészült az oldal, akkor duplán fizetsz érte. Egyszer elkészült az eredeti tervek szerint, le lett fejlesztve és most újra végig kell menni ugyanezen az úton.</td>
			    					</tr>
			    				</table>
			    			</div>
		    			</div>
		    			<p class="explanation">Kevesen vitatnák, hogy egy gyermek felnevelése kihívásokkal teli, mégis talán a legnagyobb örömet adó feladat a világon. Egy webes projektre is érdemes hasonló körültekintéssel felkészülnöd, mintha gyermeket várnál, főleg ha a család költségvetése viseli a vállalkozással járó kockázatokat. A tervezésnél törekedj arra, hogy minél részletesebben írj le minden lényeges kritériumot a kivitelezőid felé az összes kulcsösszetevőről. Enélkül egyrészt a projekt nem árazható be korrektül, másrészt téves elvárások fogalmazódhatnak meg egyaránt a megrendelő és a kivitelező fejében is.</p>
			      	</div>
			      	<img class="decoration" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/hero-pattern-right.svg" alt="decoration"/>
		      	</div>
		      	<div class="tab-main">
		      		<div class="tab-main-content">
			      		<?php 
		      				
				            $args_projektterkep = array(
				                'post_type' => 'projektterkep',
				                'category_name' =>  'tervezes',
				                'posts_per_page' => -1,
			                    'meta_key' => 'order',
    							'orderby' => 'meta_value_num',
    							'order' => 'ASC',
				            );

				            $i = 0;
				            $loop_projektterkep = new WP_Query( $args_projektterkep );
				            $cc = count($query);
				            if ( $loop_projektterkep->have_posts() ) : while ( $loop_projektterkep->have_posts() ) : $loop_projektterkep->the_post(); 

				            $i++;
                		?>


			      		<div class="accordion <?php echo ($i==1)?'active':''; ?>">
	                    	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
	                    	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
	                    	<h3 class="subtitle"><?php the_title(); ?></h3>
		                </div>
		                <div class="panel <?php echo ($i==1)?'open':''; ?>">
		                    <?php the_content(); ?>
		                    <a href="<?php the_field('button_link') ?>" class="btn-gradient" target="_blank">Tovább</a>
		                </div>

		              

		                <?php endwhile; ?>

						<?php else : ?>
						  <p><?php __('No News'); ?></p>
						<?php endif; ?>
	                </div>
		      	</div>
		    </div>
		    <div id="menu4" class="tab-pane fade">
		    	<div class="tab-header">
		    		<div class="tab-header-content">
		    			<div class="row">
			    			<div class="col-lg-3 u-text-center">
			    				<p class="number">4.</p>
			    			</div>
			    			<div class="col-lg-9">
			    				<h2 class="tab-name">Kivitelezés</h2>
			    				<table>
			    					<tr>
			    						<td>időigény</td>
			    						<td>2 hónap</td>
			    					</tr>
			    					<tr class="cost-intensity">
			    						<td>költségintenzitás</td>
			    						<td>4x</td>
			    						<td class="doubts-button-box"><img class="doubts-button" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/doubts-button.svg" alt="doubts-button"/></td>
			    						<td class="description">A Költségintenzitás azt jelöli, hogy a projekt adott életszakaszában elvégzendő feladatok kihagyása vagy nem megfelelő kivitelezése, és későbbi pótlása mennyi többletköltséggel jár. Példa: ha a weboldal grafikáját akkor akarod módosítani ha már elkészült az oldal, akkor duplán fizetsz érte. Egyszer elkészült az eredeti tervek szerint, le lett fejlesztve és most újra végig kell menni ugyanezen az úton.</td>
			    					</tr>
			    				</table>
			    			</div>
		    			</div>
		    			<p class="explanation">Amennyiben már zöld utat kap a kivitelezés, nem szabad, hogy kétséged maradjon afelől, hogy terméked/szolgáltatásod megfelelő méretű fogadópiaccal rendelkezik-e. Ám mielőtt megkezdenéd a pénz öntését projekted tényleges kivitelezésébe, érdemes a végleges szövegezés alapján már elkészült designt néhány, a célközönségedbe tartozó tesztalannyal véleményeztetni.</p>
			      	</div>
			      	<img class="decoration" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/hero-pattern-right.svg" alt="decoration"/>
		      	</div>
		      	<div class="tab-main">
		      		<div class="tab-main-content">
			      		<?php 
		      				
				            $args_projektterkep = array(
				                'post_type' => 'projektterkep',
				                'category_name' =>  'Kivitelezés',
				                'posts_per_page' => -1,
			                    'meta_key' => 'order',
    							'orderby' => 'meta_value_num',
    							'order' => 'ASC',
				            );

				            $i = 0;
				            $loop_projektterkep = new WP_Query( $args_projektterkep );
				            $cc = count($query);
				            if ( $loop_projektterkep->have_posts() ) : while ( $loop_projektterkep->have_posts() ) : $loop_projektterkep->the_post(); 

				            $i++;
                		?>


			      		<div class="accordion <?php echo ($i==1)?'active':''; ?>">
	                    	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
	                    	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
	                    	<h3 class="subtitle"><?php the_title(); ?></h3>
		                </div>
		                <div class="panel <?php echo ($i==1)?'open':''; ?>">
		                    <?php the_content(); ?>
		                    <a href="<?php the_field('button_link') ?>" class="btn-gradient" target="_blank">Tovább</a>
		                </div>

		              

		                <?php endwhile; ?>

						<?php else : ?>
						  <p><?php __('No News'); ?></p>
						<?php endif; ?>
	                </div>
		      	</div>
		    </div>
		    <div id="menu5" class="tab-pane fade">
		    	<div class="tab-header">
		    		<div class="tab-header-content">
		    			<div class="row">
			    			<div class="col-lg-3 u-text-center">
			    				<p class="number">5.</p>
			    			</div>
			    			<div class="col-lg-9">
			    				<h2 class="tab-name">Működtetés</h2>
			    				<table>
			    					<tr>
			    						<td>időigény</td>
			    						<td>1-3 év</td>
			    					</tr>
			    					<tr class="cost-intensity">
			    						<td>költségintenzitás</td>
			    						<td>6x</td>
			    						<td class="doubts-button-box"><img class="doubts-button" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/doubts-button.svg" alt="doubts-button"/></td>
			    						<td class="description">A Költségintenzitás azt jelöli, hogy a projekt adott életszakaszában elvégzendő feladatok kihagyása vagy nem megfelelő kivitelezése, és későbbi pótlása mennyi többletköltséggel jár. Példa: ha a weboldal grafikáját akkor akarod módosítani ha már elkészült az oldal, akkor duplán fizetsz érte. Egyszer elkészült az eredeti tervek szerint, le lett fejlesztve és most újra végig kell menni ugyanezen az úton.</td>
			    					</tr>
			    				</table>
			    			</div>
		    			</div>
		    			<p class="explanation">Azok a funkciók, amiket egy jól működő online biznisz működtetéséhez el kell látni, ugyanúgy szükségesek az offline üzleteknél is, vagy bárhol, ahol egy bizonyos gondolatot, ideát szeretnénk sikeressé tenni a célközönségnél. A különbség csupán az, hogy az interneten sokkal több, és kiforrottabb eszköz áll rendelkezésedre, hogy tényleges képet kaphass ügyfeleid vágyairól és elvárásairól. A lényeg azonban ugyanaz. </p>
			      	</div>
			      	<img class="decoration" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/hero-pattern-right.svg" alt="decoration"/>
		      	</div>
		      	<div class="tab-main">
		      		<div class="tab-main-content">
		      			<h2 class="main-title"><a target="_blank" href="https://dxlabz.hu/wp-content/uploads/2020/11/weboldal-mukodtetes-forgalomtereles-facebook.pdf">Forgalomterelés</a></h2>
		      			<p>Ha a projekt tervezési és kivitelezési szakasza sikeresen megvalósult, a forgalomterelés már viszonylag a könnyebb kategóriába esik. Két út közül választhatsz: Vagy az ingyenes forgalom irányába indulsz el, viszont akkor intenzív tartalomgyártásba kell, hogy kezdj, és biztosan hónapokba telik mire érdemi forgalom érkezik oldaladra, vagy fizetett hirdetésekkel operálsz. Mielőtt azt gondolnád, hogy „de erre nincs keretem”, akkor megnyugtatlak, mert egy jól optimalizált hirdetési kampány akár már pár nap elteltével profitot hozhat minden egyes vásárlás alkalmával. </p>
		      			<?php 
				            $args_projektterkep = array(
				                'post_type' => 'projektterkep',
				                'category_name' =>  'mukodtetes',
				                'tag' => 'forgalomtereles',
				                'posts_per_page' => -1,
			                    'meta_key' => 'order',
    							'orderby' => 'meta_value_num',
    							'order' => 'ASC',
				            );

				            $i = 0;
				            $loop_projektterkep = new WP_Query( $args_projektterkep );
				            $cc = count($query);
				            if ( $loop_projektterkep->have_posts() ) : while ( $loop_projektterkep->have_posts() ) : $loop_projektterkep->the_post(); 

				            $i++;
                		?>

                		

			      		<div class="accordion <?php echo ($i==1)?'active':''; ?>">
	                    	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
	                    	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
	                    	<h3 class="subtitle"><?php the_title(); ?></h3>
		                </div>
		                <div class="panel <?php echo ($i==1)?'open':''; ?>">
		                    <?php the_content(); ?>
		                    <a href="<?php the_field('button_link') ?>" class="btn-gradient" target="_blank">Tovább</a>
		                </div>

		              

		                <?php endwhile; ?>

						<?php else : ?>
						  <p><?php __('No News'); ?></p>
						<?php endif; ?>

						<hr class="gradient-separator-short">

		                <h2 class="main-title">Optimalizálás</h2>
		                <p>Egy kellően optimalizált és optimalizálatlan weboldal között eredményességben nagyságrendi eltérések is lehetnek és sokszor találhatsz olyan könnyen végrehajtható optimalizálásokat, melyek rövid időn belül jelentős bevételnövelő hatással bírnak.</p>
		                <?php 
				            $args_projektterkep = array(
				                'post_type' => 'projektterkep',
				                'category_name' =>  'mukodtetes',
				                'tag' => 'optimalizalas',
				                'posts_per_page' => -1,
			                    'meta_key' => 'order',
    							'orderby' => 'meta_value_num',
    							'order' => 'ASC',
				            );

				            $i = 0;
				            $loop_projektterkep = new WP_Query( $args_projektterkep );
				            $cc = count($query);
				            if ( $loop_projektterkep->have_posts() ) : while ( $loop_projektterkep->have_posts() ) : $loop_projektterkep->the_post(); 

				            $i++;
                		?>

                		

			      		<div class="accordion <?php echo ($i==1)?'active':''; ?>">
	                    	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
	                    	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
	                    	<h3 class="subtitle"><?php the_title(); ?></h3>
		                </div>
		                <div class="panel <?php echo ($i==1)?'open':''; ?>">
		                    <?php the_content(); ?>
		                    <a href="<?php the_field('button_link') ?>" class="btn-gradient" target="_blank">Tovább</a>
		                </div>

		              

		                <?php endwhile; ?>

						<?php else : ?>
						  <p><?php __('No News'); ?></p>
						<?php endif; ?>

						<hr class="gradient-separator-short">

		                <h2 class="main-title">Profitmaximalizálás</h2>
		                <p>Ha már stabilan bevételt termel az oldalad, akkor nyugtázhatod, hogy a célközönségnél betalált amit kínálsz és a tálalással sincs különösebb gond. Azonban még ekkor is megeshet, hogy óriási, további kiaknázatlan proitpotenciállal bír a weboldal. Költségszerkezetedtől függően, minél kevésbé foglalkoztál eddig tudatosan a konverzióoptimalizálással, annál nagyobb arányú, akár 80-100%-kos profittöbbletet is hozhat már 1%-nyi konverziójavulás is.</p>

		                <?php 
				            $args_projektterkep = array(
				                'post_type' => 'projektterkep',
				                'category_name' =>  'mukodtetes',
				                'tag' => 'profitmaximalizalas',
				                'posts_per_page' => -1,
			                    'meta_key' => 'order',
    							'orderby' => 'meta_value_num',
    							'order' => 'ASC',
				            );

				            $i = 0;
				            $loop_projektterkep = new WP_Query( $args_projektterkep );
				            $cc = count($query);
				            if ( $loop_projektterkep->have_posts() ) : while ( $loop_projektterkep->have_posts() ) : $loop_projektterkep->the_post(); 

				            $i++;
                		?>

                		

			      		<div class="accordion <?php echo ($i==1)?'active':''; ?>">
	                    	<img class="decoration plus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-mark.png" alt="arrow"/>
	                    	<img class="decoration minus" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minus-mark.png" alt="arrow"/>
	                    	<h3 class="subtitle"><?php the_title(); ?></h3>
		                </div>
		                <div class="panel <?php echo ($i==1)?'open':''; ?>">
		                    <?php the_content(); ?>
		                    <a href="<?php the_field('button_link') ?>" class="btn-gradient" target="_blank">Tovább</a>
		                </div>

		              

		                <?php endwhile; ?>

						<?php else : ?>
						  <p><?php __('No News'); ?></p>
						<?php endif; ?>
	                </div>
		      	</div>
		    </div>
		</div>

	</main><!-- .content -->
	<?php } else { ?>
		<main class="content container container-wide">
			<div class="gradient-white"></div>
			<div class="woocommerce">
				<div class="woocommerce-info wc-memberships-restriction-message wc-memberships-message wc-memberships-content-restricted-message">A Weboldal Projekttérkép csak bejelentkezés után érhető el. Bejelentkezéshez látogass el a <a style="color: #f79024;font-weight: bold;text-decoration: none" href="<?php echo site_url();?>/fiokom" target="_blank">Fiókom</a> oldalra</div>
			</div>
		</main>
		<?php } ?>
</div>



	<script src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/3.1.0/intro.min.js" integrity="sha512-8HbqTH7QzK30vhgVF/hTJ4loXwV85UU9vjI4nK04AfdOFzl8zG7b3LLAEHDmvIM8I0rvserMXmQx4Hw+kRknjw==" crossorigin="anonymous"></script>
<script>
	if (
			document.readyState === "complete" ||
			(document.readyState !== "loading" && !document.documentElement.doScroll)
	) {
		init();
	} else {
		document.addEventListener("DOMContentLoaded", init);
	}

	function init() {
		document.querySelector('.projektmap-intro-popup').addEventListener('click', function() {
			document.querySelector('.projektmap-intro-popup-overlay').style.display = 'none';
		});

		setTimeout(function() {
			if (localStorage.getItem('book_popup_viewed') != 'true') {
				document.querySelector('.projektmap-intro-popup-overlay.book-popup').style.display = 'flex';
				localStorage.setItem('book_popup_viewed', true);
			}
		}, 90000);

		// initIntro();
	}

	function initIntro() {
		if (window.innerWidth > 1199) {
			introJs().setOptions({
				prevLabel: 'Előző',
				nextLabel: 'Következő',
				doneLabel: 'Bezárás'
			}).start();
		} else {
			introJs().setOption("hintButtonLabel", "OK").addHints();
		}
	}
</script>
<?php get_footer();
