<?php

/**
 * The Blog template file
 *
 * @package ThemePlate
 * @since 0.1.0
 */
?>

<?php get_header(); ?>
<section id="page-blog">
  <div class="page-banner">
        <h1 class="section-title dark-bg less-line-height extra-margin_bottom"><span class="text-gradient-bg">BLOG</span> - Minden, ami web</h1>
        <hr class="gradient-separator-short">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-2x.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
  </div>
  <div class="section-separator">
  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
  <div class="skew-separator"></div>
</div>
  <div class="container">
    <!-- Marketing Icons Section -->
    <?php
      $args = array(
        'post_type' => 'post',
        'posts_per_page' => '-1',
      );


      $blog = new WP_Query($args); ?>

      <?php if ( $blog->have_posts() ) :  while ( $blog->have_posts() ) :  $blog->the_post( ); ?>
        <div class="row blog-post">
            <div class="col-lg-4 card-img">
            <?php if ( has_post_thumbnail() ) { ?>
              <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top']); ?></a>
            <?php } else { ?>
              <a href="<?php the_permalink(); ?>"><img src="https://placehold.it/700x400"></a>
            <?php } ?>
              <div class="post-category">
                <?php
                  $categories = get_the_category();
                  $separator = ' ';
                  $output = '';
                  if ( ! empty( $categories ) ) {
                      foreach( $categories as $category ) {
                          $output .= '<p><a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a></p>' . $separator;
                      }
                      echo trim( $output, $separator );
                  }
                ?>
              </div>
            </div>

            <div class="col-lg-8 card-body">
              <p class="date-author"><?php echo get_the_date(); ?>  Írta: <?php the_author(); ?></p>
              <h4 class="card-header">
                <a href="<?php the_permalink(); ?>">
                  <?php the_title(); ?>
                </a>
                <hr class="separator-left">
              </h4>
                <p class="card-text">
                  <div class="entry-content">
                    <?php echo wp_trim_words($post->post_content, 30, '...'); ?>
                  </div>
                </p>
                  <div class="card-footer">
                    <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-gradient">OLVASS TOVÁBB</a>
                  </div>
              </div>

          </div>
          <?php endwhile; ?>
<!--           <div class="pagination-box">
          <?php
          the_posts_pagination( array(
            'prev_text'          => __( 'Előző oldal', 'themeplate' ),
            'next_text'          => __( 'Következő oldal', 'themeplate' ),
            'screen_reader_text' => ' ',
          ) ); ?>
          </div> -->
        <?php endif; ?>

      <!-- /.row -->
  </div>
</section>
<?php get_footer(); ?>
