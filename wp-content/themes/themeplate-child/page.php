<?php

/**
 * The template for displaying pages
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<?php if (is_shop()) { ?>
  <div class="page-banner">
        <h1 class="section-title dark-bg less-line-height extra-margin_bottom"><span class="text-gradient-bg">BOLTUNK</span> kínálata</h1>
        <hr class="gradient-separator-short">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-2x.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
  </div>
  <div class="section-separator">
	  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
	  <div class="skew-separator"></div>
	</div>
  	<main class="content container">

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>

	</main><!-- .content -->
<?php } else { ?>
  <div class="page-banner simple-page">
        <h1 class="section-title dark-bg less-line-height"><?php single_post_title(); ?></h1>
        <hr class="gradient-separator-short">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-2x.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
  </div>
  <div class="section-separator">
	  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
	  <div class="skew-separator"></div>
  </div>
	<main class="content container">

	<?php if ( ! is_front_page() ) : ?>
	<?php endif; ?>

	<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; ?>

	</main><!-- .content -->

<?php }

get_footer();
