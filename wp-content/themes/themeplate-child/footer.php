<?php

/**
 * The template for displaying the footer
 *
 * @package ThemePlate
 * @since 0.1.0
 */

?>

</div><!-- .site-content -->

<!-- <?php get_sidebar( 'footer' ); ?> -->

    <!-- Footer -->
    <footer class="<?php echo (is_page(array('online-gyorsitosav')) ? 'landing' : ''); ?>">
    <?php if (!is_page(array('online-gyorsitosav'))) : ?>
      <div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/footer-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
      <div class="container relative-and-z-index">
        <div class="row">
          <div class="col-lg-5 info-box credo">
            <h3 class="title dark-bg">Credo</h3>
            <p class="dark-bg less-line-height">Minden vállalkozás valaki álmainak formát öltő megtestesülése. Mi azon dolgozunk, hogy ezek az álmok rémálmok helyett sikersztorikká váljanak. Elhivatottan kutatjuk a legfrissebb technológiákat, így gondoskodva arról, hogy kitűnhess a konkurencia közül.</p>
            <div class="row align-center">
              <div class="col-lg-4 col-6">
                <?php picture('dx-labz-vallalhato-vallalkozas-weboldal-fejlesztes', 'png', '',  true, 'badge'); ?>
              </div>
              <div class="col-lg-4 align-center col-6 col-md-6">
                <?php picture('dx-labz-weboldal-keszites-fivosz-tag', 'jpeg', '',  true, 'badge'); ?>
                <span>FIVOSZ tag</span>
              </div>
              <div class="col-lg-4 align-center col-6 col-md-6">
                <?php picture('dx-labz-weboldal-keszites-minositett-szallito-vallalkozz-digitalisan', 'png', '',  true, 'badge'); ?>
               <span>Minősített szállító</span>
             </div>
             </div>
          </div>
          <div class="col-lg-3 info-box">
            <h3 class="title dark-bg">Oldaltérkép</h3>
            <nav class="sitelinks dark-bg">
             <?php themeplate_footer_menu(); ?>
            </nav>
          </div>
          <div class="col-lg-4 info-box">
            <h3 class="title dark-bg">Cím</h3>
            <p class="dark-bg less-line-height">DX Labz Kft.</p>
            <p class="dark-bg less-line-height">Adószám: 27037162-2-05</p>
            <p class="dark-bg less-line-height">3623 Borsodszentgyörgy</p>
            <p class="dark-bg less-line-height">Horgos út 8.</p>
            <h3 class="title dark-bg">Elérhetőségek</h3>
            <h4>Telefon</h4>
            <p class="dark-bg less-line-height"><a href="tel:06303362320" class="call-front-page">06303362320</a></p>
            <p class="dark-bg less-line-height"><a href="tel:06304483677" class="call-front-page">06304483677</a></p>
            <h4>Email</h4>
            <p class="dark-bg less-line-height"><a href="mailto:info@dxlabz.hu">info@dxlabz.hu</a></p>
            <a href="https://www.facebook.com/dxlabz/"><img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/facebook.svg" alt="Facebook Logo White"  class="lazyload facebook-logo"></a>
            <a href="https://www.linkedin.com/in/gabor-kriston/"><img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/linkedin.svg" alt="LinkedIn"  class="lazyload linkedin-logo"></a>
          </div>
        </div>
        <div class="footer-logo align-center">
          <a href="<?php echo site_url(); ?>"><img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-white.svg" alt="DxLabz Logo" data-no-lazy="1" class="lazyload dxlabz-logo-white"></a>
        </div>
      </div>
		<?php endif; ?>
    <div id="copyright" class="relative-and-z-index">
      <div class="container">
          <p class="dark-bg less-line-height">© <?php echo date("Y"); ?> DX Labz Kft. - Minden jog fenntartva</p>
          <p class="dark-bg less-line-height"><a href="https://dxlabz.hu/adatkezelesi-tajekoztato.pdf">Adatvédelmi tájékoztató</a></p>
          <p class="dark-bg less-line-height">Ügyfélszolgálat: <a href="mailto:info@dxlabz.hu">info@dxlabz.hu</a></p>
          <a href="http://simplepartner.hu/PaymentService/Fizetesi_tajekoztato.pdf" target="_blank"><?php picture('simplepay', 'png', '',  true, 'simplepay-logo'); ?></a>
      </div>
    </div>
<!--     <p class="dark-bg">Copyright &copy; 2020 <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>.
          <span>Designed and developed by <a href="<?php echo esc_html( AUTHOR_URI ); ?>"><?php echo esc_html( THEME_AUTHOR ); ?></a>.</span></p> -->
      <!-- /.container -->
    </footer>
    <button id="scroll-to-top" title="Go to top"><img data-src="<?php echo get_stylesheet_directory_uri();?>/assets/images/to-top.svg" class="lazyload" alt=""></button>

<!--	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" async></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" defer></script>
  <!-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script> -->

<!-- 	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />-->
<!--  	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>-->


    <?php if (is_front_page()) : ?>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/calculator.js" defer></script>
		<!--   <script>
// Set the date we're counting down to
var countDownDate = new Date("Jun 1, 2020 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("timer").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timer").innerHTML = "EXPIRED";
  }
}, 1000);

</script>-->
<?php endif; ?>

	<?php wp_footer(); ?>

<?php if (is_page('weboldal-megterulesi-kalkulator')) : ?>
	<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js" defer></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/web-roi.js" defer></script>
<?php endif; ?>


<!-- GYIK ELEMENT SHOW - HIDE -->
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
     var panel = this.nextElementSibling;
    if (panel.classList.contains('open')) {
      panel.classList.remove('open')
    } else {
      panel.classList.add('open')
    }
  });
}

</script>

<!-- AUTOPLAY VIDEOS WHEN IN VIEWPORT -->
<!-- <script>
  
function playVisibleVideos() {
  document.querySelectorAll("video").forEach(video => elementIsVisible(video) ? video.play() : video.pause());
}

function elementIsVisible(el) {
  let rect = el.getBoundingClientRect();
  return (rect.bottom >= 0 && rect.right >= 0 && rect.top <= (window.innerHeight || document.documentElement.clientHeight) && rect.left <= (window.innerWidth || document.documentElement.clientWidth));
}

let playVisibleVideosTimeout;
window.addEventListener("scroll", () => {
  clearTimeout(playVisibleVideosTimeout);
  playVisibleVideosTimeout = setTimeout(playVisibleVideos, 100);
});

window.addEventListener("resize", playVisibleVideos);
window.addEventListener("DOMContentLoaded", playVisibleVideos);

</script> -->


</body>
</html>
