<?php

/**
 * Template Name: Profit Kalkulátor
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<section id="page-profit">
  <div class="page-banner">
		<h1 class="section-title dark-bg less-line-height extra-margin_bottom">WEBOLDAL <span class="text-purple text-gradient-bg">MEGTÉRÜLÉSI</span> KALKULÁTOR</h1>
		<hr class="gradient-separator-short">
  </div>
  <div class="section-separator">
	  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
	  <div class="skew-separator"></div>
  </div>
  <main>
	  	<div id="my-content-subpage">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 inner-content">
						<?php while ( have_posts() ) : ?>
							<?php the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					</div>
					<?php
				      include "sidebar.php";
				    ?>
				</div>
			</div>
		</div>
		<div id="web-roi">
			<div class="container">
				<div class="row">
					<div class="col-lg-4">
						<form onsubmit="return vm.calculateProfit(event);">
							<div class="form-group">
								<label for="salesprice">Szolgáltatás átlagára/átlagos kosárérték</label>
								<input id="salesprice" type="number" value="30000" min="0" step="100" required autofocus>
							</div>

							<div class="form-group">
								<label for="product-cost-ratio">Termék/szolgáltatás bekerülési ktsg.-e (%)</label>
								<input id="product-cost-ratio" type="number" value="20" min="0" max="100" step="1" required>
							</div>

							<div class="form-group">
								<label for="sales-target">Havi értékesítési cél</label>
								<input id="sales-target" type="number" value="10" min="0" step="1" required>
							</div>

							<div class="form-group">
								<label for="monthly-fixed-cost">Havi fix kiadás</label>
								<input id="monthly-fixed-cost" type="number" value="70000" min="0" step="1000" required>
							</div>

							<div class="form-group">
								<label for="visitors">Havi látogatószám (hirdetés + keresés)</label>
								<input id="visitors" type="number" value="300"  min="0" step="100" required>
							</div>

							<div class="form-group">
								<label for="organic-traffic-ratio">Keresőforgalom aránya (%)</label>
								<input id="organic-traffic-ratio" type="number" value="0" min="0" max="100" step="1" required>
							</div>

							<div class="form-group">
								<label for="ppc-cost">1 kattintás ktsg.-e hirdetésből</label>
								<input id="ppc-cost" type="number" value="40" min="0" step="1" required>
							</div>

							<div class="form-group">
								<label for="years">Számított évek száma</label>
								<input id="years" type="number" value="1" min="1" step="1" required>
							</div>
							<div class="form-group">
								<input class="btn-gradient" type="submit" value="Kalkuláció">
							</div>
						</form>
					</div>
					<div class="col-lg-8">
						<canvas id="mixed-chart" width="800" height="450"></canvas>
					<div id="app">
							<h2>A megadott adatokkal a következő előrejelezhető</h2>

							<template v-if="parity">
								<template v-if="chartClickDescription">
									<p><span>{{ conversionRate }}</span>-os vásárlási hajlandóságnál a weboldal <span>{{ profitData }} Ft</span> profitot termel <span>{{ numOfYears }} év</span> alatt.</p>
								</template>
								<template v-else>
									<p><span>{{ conversionRate }}</span>-os vásárlási hajlandóságnál ér el a projekted profitot. A weboldal ekkor <span>{{ profitData }} Ft</span> profitot termel <span>{{ numOfYears }} év</span> alatt.</p>
								</template>

								<p>A weboldal projekt megtérülési rátája: <span>{{ returnOnInvestment }}</span>. A weboldal <span>{{ parity }} hónap</span> alatt hozza vissza az árát <span>{{ projectPrice }} Ft</span>-os projektköltségnél.</p>

								<p v-if="minConvForSalesTarget">A kívánt havi értékesítést <span>{{minConvForSalesTarget}}</span>-os vásárlási hajlandóságnál éred el a megadott forgalom mellettt.</p>
								<p v-else>Ekkora értékesítés nem érhető el még 100%-os konverzió mellett sem a beállított forgalmi szint esetén. Dolgozz a forgalmad növelésén.</p>
							</template>

							<template v-else>
								<p>A beállított paraméterek esetében weboldalad veszteséges lesz, mert nincs olyan reálisan elérhető konverziós érték, ami mellett profit érhető el.</p>
							</template>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
</section>
<?php
get_footer();
