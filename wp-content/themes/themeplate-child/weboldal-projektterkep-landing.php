<?php
/**
 * Template Name: Weboldal Projekttérkép Landing
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>

<?php //get_template_part( 'template-parts/header', 'without-nav' ); ?>

<section id="hero" class="landing-page_webmap">
	<div class="hero-gradient"></div>
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-wide-orange.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 text">
				<h1>WEBOLDAL<br><span>PROJEKTTÉRKÉP</span></h1>
				<h2><span class="uppercase">150+ oldalnyi, ingyenes útmutató</span><br>weboldalad eredményességének felpörgetéséhez, interaktív formában az <b>Online Gyorsítósáv könyv</b> anyagából<div class="gradient-text-bg"></div></h2>
				<p>Az eredményes online vállalkozások térképe, ami mindig mutatja majd, hogy weboldalad adott életszakaszában milyen feladatok várnak rád!</p>
				<div class="call-to-action">
					<a href="#contact" class="btn-gradient">SZEREZD MEG MOST!</a>
				</div>
			</div>
			<div class="col-lg-6 image">
				<?php picture('weboldal-projekt-terkep-landing-hero-img', 'png', '',  true, 'web-projectmap'); ?>
			</div>
			<div class="testimonial-box">
				<h4 class="opininon sub-title align-left text-normal italic dark-bg">“Életmentő útikönyv az online vállalkozáshoz.</h4>
				<p class="italic dark-bg">Gábor könyve nélkül az online vállalkozás olyan, mint elutazni egy ismeretlen országba útikönyv, tervek, ismerősök és okostelefon nélkül: veszélyes kalandtúra. Ha a kedves szerző pár évvel korábban megírta volna, néhány millió forinttal biztosan gazdagabb és néhány ősz hajszállal szegényebb lennék. Túlélőtúra helyett válaszd útitársnak, és sikeres leszel!”</p>
				<div class="row testimonial-person">
					<div class="col-lg-2 col-3 col-md-2 image align-left">
						<?php picture('testimonial-meszaros-robi', 'png', '',  false, 'testimonial-person-img'); ?>
					</div>
					<div class="col-lg-8 col-9 col-md-10 text">
						<h4 class="name dark-bg">Mészáros Róbert</h4>
						<p class="qualification dark-bg">márkaépítő, BrandBirds</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> <!-- end of header -->
<div class="section-separator landing-page_webmap">
	<?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
	<div class="skew-separator"></div>
</div>
<section id="contact" class="newsletter landing-page_webmap">
	<div class="gradient-purple lazyload" data-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/web-projektmap-contact-bg-desktop.<?php echo (WEBP ? 'webp' : 'png'); ?>"></div>
	<div class="container relative-and-z-index">
			<h2 class="section-title"><span class="text-gradient-bg">IRATKOZZ FEL</span> és azonnal Tiéd lehet a Weboldal Projekttérkép!</h2>
			<hr class="gradient-separator-short">
			<div class="less-width">
				<!-- <p class="dark-bg less-line-height align-center">Legyél az elsők között, akik rálépnek az <strong>Online Gyorsítósávra</strong> és kerülj olyan tudás birtokába, amiről a vállalkozások 99%-a sosem fog hallani. Ez az egyetlen a-tól z-ig <strong>online sikerkalauz,</strong> amiben biztosan nem fogsz csalódni. Nem csak ahhoz a tudáshoz fog hozzáférni, amit több mint 10 év nemzetközi piacon szerzett tapasztalatával megszereztem, hanem olyan folyamatot kapsz, amit követve <strong>megsokszorozod az eredményeidet.</strong></p> -->
				<p class="less-line-height align-center">Csupán pár kattintásnyira vagy, hogy hozzájuthass a több mint 150 oldalnyi anyagból álló weboldal projekttérképhez.</p>
				<p class="less-line-height align-center">A Weboldal Projekttérkép ingyenes, de regisztrációhoz kötött</p>
				<p class="less-line-height extra-margin-bottom align-center"><strong class="bold-only">Váltsd valóra álmaidat, gazdagítsd te is a sikeres online vállalkozók táborát.</strong></p>
				<div id="mmform200846" class="mmform" style="position:relative;padding-bottom:300px;height:0;overflow:hidden;max-width:100%;margin-top: -30px;"></div>
				<script type="text/javascript">
				var uniqueId = 2008461587468660,listId = 104653,formId = 200846,iwidth=115,mmDomain='salesautopilot.s3.amazonaws.com',secondMMDomain='sw.marketingszoftverek.hu',spancont=false,secure=('https:' == document.location.protocol)?true:false;
				</script>
				<script type="text/javascript" src="https://d1ursyhqs5x9h1.cloudfront.net/sw/scripts/mm-embed-iframe-1.15.min.js">

					function setPlaceholder() {
  					document.getElementById("mssys_fullname").setAttribute("placeholder", "Név");
					document.getElementById("email").setAttribute("placeholder", "Email");
					};
					window.onload = setPlaceholder;

				</script>
			</div>

	</div>
</section>
<?php
get_footer();
//get_template_part( 'template-parts/footer', 'without-nav' );
//?>
