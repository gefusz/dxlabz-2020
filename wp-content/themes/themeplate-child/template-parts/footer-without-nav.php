<?php

/**
 * The template for displaying the footer
 *
 * @package ThemePlate
 * @since 0.1.0
 */

?>

</div><!-- .site-content -->

<!-- <?php get_sidebar( 'footer' ); ?> -->

    <!-- Footer -->
    <footer class="landing">
<!--       <div class="gradient-purple"></div>
      <div class="container relative-and-z-index">
        <div class="row">
          <div class="col-lg-4 info-box">
            <h3 class="title dark-bg">Credo</h3>
            <p class="dark-bg less-line-height">Minden vállalkozás valaki álmainak formát öltő megtestesülése, mi azon dolgozunk, hogy ezek az álmok rémálmok helyett sikersztorikká váljanak. Elhivatottan kutatjuk a legfrissebb technológiákat, hogy gondoskodjunk arról, hogy kitűnhessen a konkurencia közül.</p>
          </div>
          <div class="col-lg-4 info-box">
            <h3 class="title dark-bg">Oldaltérkép</h3>
            <nav class="sitelinks dark-bg">
             <?php themeplate_footer_menu(); ?>
            </nav>
          </div>
          <div class="col-lg-4 info-box">
            <h3 class="title dark-bg">Cím</h3>
            <p class="dark-bg less-line-height">DX Labz Kft.</p>
            <p class="dark-bg less-line-height">Adószám: 67068421-1-25</p>
            <p class="dark-bg less-line-height">3623 Borsodszentgyörgy</p>
            <p class="dark-bg less-line-height">Horgos út 8.</p>
            <h3 class="title dark-bg">Elérhetőségek</h3>
            <p class="dark-bg less-line-height">Tel: <a href="tel:06303361234"><strong class="bold-only">06303361234</strong></a></p>
            <p class="dark-bg less-line-height">Email: <a href="mailto:gabor@dxlabz.hu"><strong class="bold-only">irjnekem@gmail.com</strong></a></p>
            <a href="#"><?php picture('facebook-logo-white', 'png', '',  false, 'facebook-logo'); ?></a>
          </div>
        </div>
        <div class="footer-logo align-center">
          <a href="#"><?php picture('dxlabz-logo-white', 'png', '',  false, 'dxlabz-logo-white'); ?></a>
        </div>
      </div> -->
    <div id="copyright" class="relative-and-z-index">
      <div class="container">
          <p class="dark-bg less-line-height">DX Labz Kft.</p>
          <!-- <p class="dark-bg less-line-height"><a href="https://dxlabz.hu/adatkezelesi-tajekoztato.pdf">ÁSZF</a></p> -->
          <p class="dark-bg less-line-height"><a href="https://dxlabz.hu/adatkezelesi-tajekoztato.pdf">Adatvédelmi tájékoztató</a></p>
          <p class="dark-bg less-line-height"><a href="https://dxlabz.hu/#gdpr_cookie_modal">Cookie tájékozató</a></p>
          <p class="dark-bg less-line-height">Ügyfélszolgálat: <a href="mailto:info@dxlabz.hu">info@dxlabz.hu</a></p>
      </div>
    </div>
<!--     <p class="dark-bg">Copyright &copy; 2020 <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>.
          <span>Designed and developed by <a href="<?php echo esc_html( AUTHOR_URI ); ?>"><?php echo esc_html( THEME_AUTHOR ); ?></a>.</span></p> -->
      <!-- /.container -->
      <button onclick="topFunction()" id="scroll-to-top" title="Go to top"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/to-top.svg" alt="Go to Top"></button>
    </footer>

  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" async></script>
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" defer></script>
  <!-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script> -->
<!--   <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script> -->
<script>
    //Get the button:
      mybutton = document.getElementById("scroll-to-top");

      // When the user scrolls down 20px from the top of the document, show the button
      window.onscroll = function() {scrollFunction()};

      function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          mybutton.style.display = "block";
        } else {
          mybutton.style.display = "none";
        }
      }

      // When the user clicks on the button, scroll to the top of the document
      function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      }
  </script>
<!--   <script>
    <?php if (is_front_page()) { ?>
// Set the date we're counting down to
var countDownDate = new Date("Jun 1, 2020 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("timer").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timer").innerHTML = "EXPIRED";
  }
}, 1000);
<?php } ?>
</script> -->

<!-- <script>

jQuery(document).ready(function(){
  jQuery(".plus-mark").click(function(){
    jQuery(".answers-text-box").toggle();
  });
});
</script> -->

  <?php wp_footer(); ?>
</body>
</html>
