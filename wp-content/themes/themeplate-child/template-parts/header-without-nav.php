<?php

/**
 * The template for displaying the header
 *
 * @package ThemePlate
 * @since 0.1.0
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="preload" href="<?php echo plugins_url(); ?>/gdpr-cookie-compliance/dist/fonts/nunito-v8-latin-regular.woff2" as="font" type="font/woff2" crossorigin>
	<link rel="preload" href="<?php echo plugins_url(); ?>/gdpr-cookie-compliance/dist/fonts/nunito-v8-latin-700.woff2" as="font" type="font/woff2" crossorigin>
	<link rel="preload" href="<?php echo plugins_url(); ?>/gdpr-cookie-compliance/dist/styles/gdpr-main.css" as="style">
	<link rel="preload" href="<?php echo plugins_url(); ?>/gdpr-cookie-compliance/dist/styles/lity.css" as="style">
	<!--	  <link rel="preload" href="--><?php //echo plugins_url(); ?><!--/gdpr-cookie-compliance/dist/scripts/main.js" as="script">-->
	<link rel="preload" href="<?php echo plugins_url(); ?>/contact-form-7/includes/js/scripts.js" as="script" >
	<link rel="preload" href="<?php echo plugins_url(); ?>/woocommerce/packages/woocommerce-blocks/build/style.css" as="style">
	<link rel="preload" href="<?php echo plugins_url(); ?>/woocommerce/assets/css/woocommerce-layout.css" as="style">
	<link rel="preload" href="<?php echo plugins_url(); ?>/woocommerce/assets/css/woocommerce-smallscreen.css" as="style">
	<link rel="preload" href="<?php echo plugins_url(); ?>/woocommerce/assets/css/woocommerce.css" as="style">
	<link rel="preload" href="<?php echo plugins_url(); ?>/woocommerce-memberships/assets/css/frontend/wc-memberships-frontend.min.css" as="style">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link href="https://fonts.gstatic.com" rel="preconnect" crossorigin>
	<link rel="preconnect" href="https://fonts.googleapis.com/" crossorigin>
	<!--    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700,800,900" rel="stylesheet">-->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<!--	  Critical CSS-->
	<style>
		@charset "UTF-8";.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:576px){.container{max-width:540px}}@media (min-width:768px){.container{max-width:720px}}@media (min-width:992px){.container{max-width:960px}}@media (min-width:1200px){.container{max-width:1140px}}@media (min-width:576px){.container{max-width:540px}}@media (min-width:768px){.container{max-width:720px}}@media (min-width:992px){.container{max-width:960px}}@media (min-width:1200px){.container{max-width:1140px}}.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.col-lg-6{position:relative;width:100%;padding-right:15px;padding-left:15px}@media (min-width:992px){.col-lg-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}}.navbar{position:relative;padding:.5rem 1rem}.navbar,.navbar .container{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;-ms-flex-align:center;align-items:center;-ms-flex-pack:justify;justify-content:space-between}.navbar-brand{display:inline-block;padding-top:.3125rem;padding-bottom:.3125rem;margin-right:1rem;font-size:1.25rem;line-height:inherit;white-space:nowrap}.navbar-nav{display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;padding-left:0;margin-bottom:0;list-style:none}.navbar-collapse{-ms-flex-preferred-size:100%;flex-basis:100%;-ms-flex-positive:1;flex-grow:1;-ms-flex-align:center;align-items:center}.navbar-toggler{padding:.25rem .75rem;font-size:1.25rem;line-height:1;background-color:transparent;border:1px solid transparent;border-radius:.25rem}.navbar-toggler-icon{display:inline-block;width:1.5em;height:1.5em;vertical-align:middle;content:"";background:no-repeat 50%;background-size:100% 100%}@media (max-width:991.98px){.navbar-expand-lg>.container{padding-right:0;padding-left:0}}@media (min-width:992px){.navbar-expand-lg{-ms-flex-flow:row nowrap;flex-flow:row nowrap;-ms-flex-pack:start;justify-content:flex-start}.navbar-expand-lg .navbar-nav{-ms-flex-direction:row;flex-direction:row}.navbar-expand-lg>.container{-ms-flex-wrap:nowrap;flex-wrap:nowrap}.navbar-expand-lg .navbar-collapse{display:-ms-flexbox!important;display:flex!important;-ms-flex-preferred-size:auto;flex-basis:auto}.navbar-expand-lg .navbar-toggler{display:none}}.navbar-dark .navbar-brand{color:#fff}.navbar-dark .navbar-toggler{color:hsla(0,0%,100%,.5);border-color:hsla(0,0%,100%,.1)}.navbar-dark .navbar-toggler-icon{background-image:url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'%3E%3Cpath stroke='rgba(255, 255, 255, 0.5)' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/%3E%3C/svg%3E")}html{box-sizing:border-box}*,:after,:before{box-sizing:inherit}body,fieldset,h1,h2,p,ul{margin:0;padding:0}fieldset{min-width:0;border:0}fieldset,h1,h2,p,ul{margin-bottom:1.3rem}ul{margin-left:1rem}h1{font-size:2.25rem}h2{font-size:1.7rem}.text-gradient-bg{color:#fff;text-transform:uppercase;background:#f79024;background:linear-gradient(90deg,#f79024,#ef2453);padding:5px}.section-title{text-align:center;color:#2b194c;font-weight:800}.section-title .text-gradient-bg{color:#fff;text-transform:uppercase;background:#f79024;background:linear-gradient(90deg,#f79024,#ef2453);padding:5px}.section-title.dark-bg{text-align:center;color:#fff;font-weight:800}.text-purple-bg_gradient{background:#4d2780;background:linear-gradient(90deg,#4d2780,#341a3d);font-weight:700;color:#fff;margin:0;display:inline-block;margin:auto}.text-purple-bg_gradient{padding:1px 8px}p{font-size:1.2rem;line-height:1.4}.dark-bg{color:#fff}.less-line-height{line-height:1.25}@media (min-width:768px) and (max-width:1199px){h2{font-size:3rem}}@media (min-width:1200px){.section-title,h2{font-size:3.4rem}}html{background:#fff;color:#000;font-family:-apple-system,system-ui,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;font-size:.8125rem;line-height:1.5;min-height:100%;overflow-y:scroll;padding:0;-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;text-rendering:optimizeLegibility}@media (min-width:768px){html{font-size:.875em}}@media (min-width:1024px){html{font-size:.9375em}}@media (min-width:1600px){html{font-size:1em}}img{font-style:italic;max-width:100%;vertical-align:middle}a{color:#fff}a{text-decoration:none}li{margin-bottom:.5rem}.btn-gradient{font-weight:600;background:linear-gradient(90deg,#f79024,#ef2453);padding:17px 30px;box-shadow:0 10px 20px rgba(0,0,0,.19),0 6px 6px rgba(0,0,0,.23);border-radius:4px;font-weight:700;letter-spacing:1.5px}.btn-gradient{color:#fff}.call-to-action{margin-top:50px;font-size:1.15rem}nav{will-change:transform;transform:translateZ(0)}.navbar{color:#fff}body{font-family:Montserrat,sans-serif;color:#3f3f3f;font-size:16px}section{padding:70px 0}.hide-on-mobile{display:none}.navbar{background-color:transparent;width:100%;position:fixed;z-index:100}.navbar #navbarResponsive{-ms-flex-positive:unset;flex-grow:unset;position:relative}.navbar #navbarResponsive .menu-item{padding:10px}.navbar #navbarResponsive .menu-item a{color:#fff;font-size:18px;margin-left:30px;display:block}.navbar #navbarResponsive .cart-item-count{position:absolute;left:133px;bottom:45px;background:linear-gradient(90deg,#f79024,#ef2453);padding:0 7px;border-radius:50%;font-size:14px;font-weight:600}.navbar .navbar-brand{width:50%}.navbar .navbar-brand .logo{width:50%;max-width:300px;height:45px}.navbar #navbarResponsive{display:none}.navbar .navbar-pattern-right{display:none}.navbar .cart-icon{width:25px;position:absolute;left:100px;bottom:37px}#scroll-to-top{display:none;position:fixed;bottom:20px;right:20px;z-index:999999;border:none;outline:none;color:#007bff;padding:2px 15px;font-size:55px}#scroll-to-top{background-color:transparent}#scroll-to-top img{width:40px;height:auto;filter:drop-shadow(0 4px 4px rgba(0,0,0,.7))}body.webP #hero{background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/hero-speed-bg.webp)}#hero{background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/hero-speed-bg.jpg);background-size:cover;background-repeat:no-repeat;height:100%;position:relative;padding-top:0;padding-bottom:25%}#hero .hero-gradient{background:#4d2780;background:linear-gradient(90deg,rgba(77,39,128,.7),rgba(36,28,37,.7));height:100%;width:100%;position:absolute;top:0;z-index:1}#hero .hero-pattern-right{position:absolute;right:0;height:100%;z-index:2;top:0;opacity:.1}#hero .row{position:relative;z-index:4;padding-top:12%}#hero .row .text{padding-top:60px;padding-bottom:60px;text-align:center}#hero .row .text h1{color:#f79024;font-weight:800;font-size:5.5rem;margin-bottom:0;margin-bottom:15px;line-height:.57}#hero .row .text h1 span{color:#fff;font-size:2.8rem}#hero .row .text h2{color:#fff;font-weight:600;margin-bottom:30px;font-size:1.3rem;position:relative;z-index:2;padding:10px;margin:25px 0}#hero .row .text h2 .gradient-text-bg{background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/orange-gradient-effect-hero.webp);background-repeat:no-repeat;background-size:cover;background-position:50%;position:absolute;top:-5%;left:-5%;width:107%;overflow:hidden;height:115%;z-index:-1}#hero .row .text p{color:#fff;margin-bottom:20px;font-size:1.4rem;line-height:1.5}#hero.front-page .row h1{font-size:3rem;line-height:.9}#hero.front-page .row .call-to-action a{display:block}#hero.front-page .row .image{text-align:center}footer .info-box.credo .badge{max-width:130px;margin:20px auto;width:100%;display:block}footer .facebook-logo{margin-top:20px;margin-right:10px;width:14%;max-width:70px}footer .linkedin-logo{width:14%;max-width:70px;margin-top:20px}
		@media (min-width:1200px){#scroll-to-top img{width:50px}.navbar .navbar-pattern-right{visibility:hidden;display:unset;width:3%;right:0;position:fixed;height:100%}}
		<?php if (is_front_page()) : ?>
		.calculator{background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/calc-bg.<?php echo (WEBP ? 'webp' : 'jpg'); ?>);background-size:cover;background-repeat:no-repeat;box-sizing:border-box;font-family:Montserrat;font-size:14px;width:100%;display:flex;margin-top:25px;justify-content:center;box-shadow:0 10px 20px rgba(0,0,0,.19),0 6px 6px rgba(0,0,0,.23);max-width:560px;border-radius:7px}.white-side{background:#fff}.calculator .white-side{border-radius:5px 0 0 5px}.inputfields-box{padding:30px 25px}.blue-side .inputfields-box{padding:30px 25px;background:transparent;height:100%;text-align:left}.calculator ul{list-style:none;margin:0!important;padding:0!important;width:100%}.calculator ul li{margin-bottom:25px!important;padding:0!important;text-align:left}.calculator ul li:last-child{margin-bottom:0!important}.calculator label{font-family:Montserrat;font-size:16px;font-weight:700;color:#3f3f3f;margin:0;text-align:left}#hero .blue-side .result-box .title,.blue-side .inputfields-box label{color:#fff}.input-box{margin-top:0;margin-bottom:4px}.calculator .input-range{display:flex;align-items:center;margin-top:0}input[type=range]{-webkit-appearance:none;width:60%;height:5px;border-radius:5px;background:#f79024;margin:0 20px}input[type=range]::-webkit-slider-thumb{-webkit-appearance:none;appearance:none;width:20px;height:20px;border-radius:50%;background:#f79024;cursor:pointer;box-shadow:0 0 3px 0 rgba(0,0,0,.8)}input[type=range]::-moz-range-thumb{-webkit-appearance:none;appearance:none;width:20px;height:20px;border-radius:50%;background:#f39c12;cursor:pointer;box-shadow:0 0 1px 1px rgba(0,0,0,.7)}input[type=number]{border:none;border-radius:5px;padding:5px 10px;font-family:Montserrat;font-size:16px;font-weight:700;line-height:32px;color:#3f3f3f;width:100%;box-sizing:border-box;margin-bottom:15px}.result-box{margin-top:15px;text-align:left}.result-box .title{font-family:Montserrat!important;font-size:16px!important;font-weight:700!important;margin-bottom:0!important;margin-top:0!important;text-shadow:none!important;line-height:21px}.result-box .result{color:#484748;font-family:Montserrat;font-weight:800;font-size:36px;display:inline-block;margin-top:0;line-height:.9;background:linear-gradient(270deg,#ef2452 0,#f79024);-webkit-background-clip:text;-webkit-text-fill-color:transparent}.result-box .result #result{font-size:60px}.result-box .result2{color:#484b54;font-family:Montserrat;font-weight:800;font-size:18px;display:inline-block;margin-top:0;margin-bottom:0;line-height:1.2;background:linear-gradient(270deg,#ef2452 0,#f79024);-webkit-background-clip:text;-webkit-text-fill-color:transparent}.result-box .result2 #result2{font-size:26px}@media only screen and (max-width:768px){.calculator{background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/calc-bg-mobile.<?php echo (WEBP ? 'webp' : 'jpg'); ?>);background-size:cover;background-position:center -250px;flex-wrap:wrap;font-size:12px;margin:auto;margin-top:30px}.calculator .white-side{border-radius:0 0 5px 5px;order:2}.col8{width:100%}.calculator label,input[type=number]{font-size:15px}.calculator ul li{margin-bottom:20px!important}.calculator .input-range{font-size:14px}.result-box .title{font-size:15px}.result-box .result{font-size:32px}.result-box .result2{font-size:20px}.calculator .input-range{justify-content:space-between}.inputfields-box{padding:25px 20px;text-align:left}.inputfields-box,.result-box{display:flex;justify-content:space-between;flex-wrap:wrap}.inputfields-box .group,.result-box .group{width:45%}.blue-side .inputfields-box{padding:25px 20px 0}.calculator .input-box{margin-top:6px;margin-bottom:4px}.result-box{margin-top:0;width:100%}.result-box .result,.result-box .result2{font-size:20px;line-height:1.2}.result-box #result,.result-box #result2{font-size:32px!important}input[type=range]{margin:0 25px}input[type=number]{width:100%}}@media only screen and (max-width:400px){.calculator,.calculator label,input[type=number]{font-size:13px}.result-box .title{font-size:13px!important}.result-box .result{font-size:22px;margin-bottom:10px;margin-top:0;line-height:1.2}.result-box #result{font-size:32px!important}.result-box .result2{font-size:22px!important}.result-box #result2{font-size:32px!important}.result-box{margin-top:0}input[type=range]{margin:0 15px}.calculator .input-range{margin-top:5px}.calculator ul{width:100%}.calculator ul li{margin-bottom:12px!important}.input-box{margin:8px 0 0}}
		<?php endif; ?>
	</style>
	<!--    <link rel="stylesheet" type="text/css" href="--><?php //echo get_stylesheet_directory_uri(); ?><!--/assets/css/themeplate.min.css?v=--><?php //echo time(); ?><!--">-->
	<?php if (is_front_page()) : ?>
		<script>var isFrontPage = true;</script>
		<!--    <link rel="stylesheet" type="text/css" href="--><?php //echo get_stylesheet_directory_uri(); ?><!--/assets/css/calculator.min.css?v=--><?php //echo time(); ?><!--">-->
	<?php endif; ?>
	<script>
		var siteUrl = '<?php echo get_site_url(); ?>';
		var pluginsUrl = '<?php echo plugins_url(); ?>';
		var themePath = '<?php echo get_stylesheet_directory_uri(); ?>';
		var currentTime = '<?php echo '?v=' . time(); ?>';
		var clientWidth = document.documentElement.clientWidth;
		var mybutton;

		function constructElement(type, attributes) {
			let element = document.createElement(type);

			for (let attribute in attributes) {
				element[attribute] = attributes[attribute];
			}

			return element;
		}

		function loadBaseStyles() {
			let documentFragment = document.createDocumentFragment();



			documentFragment.appendChild(constructElement('link', {
				rel: 'stylesheet',
				href: themePath + '/assets/css/themeplate.min.css?v=' + currentTime
			}));

			if (clientWidth > 767) {
				documentFragment.appendChild(constructElement('link', {
					rel: 'stylesheet',
					media: '(min-width: 768px)',
					href: themePath + '/assets/css/themeplate-tablet.min.css?v=' + currentTime
				}));
			}


			if (clientWidth > 1199) {
				documentFragment.appendChild(constructElement('link', {
					rel: 'stylesheet',
					media: '(min-width: 1200px)',
					href: themePath + '/assets/css/themeplate-desktop.min.css?v=' + currentTime
				}));
			}

			document.head.appendChild(documentFragment);
		}

		loadBaseStyles();
	</script>

	<?php if (is_page('weboldal-megterulesi-kalkulator')) : ?>
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/web-roi.min.css" rel="stylesheet">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(WEBP ? 'webP' : ''); ?>>
<?php wp_body(); ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PGW3FGP"
				  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--    <a class="screen-reader-text" href="#site-content">--><?php //esc_html_e( 'Skip to content', 'themeplate' ); ?><!--</a>-->

<!-- Search -->
<!--     <div id="topbar" class="relative-and-z-index">
      <div class="container">
        <div class="contact align-right">
          <p class="tel">Tel: <a href="tel:+36303362320">06303362320</a></p>
          <p class="mail">Email: <a href="mailto:irjnekem@gmail.com">irjnekem@gmail.com</a></p>
        </div>
      </div>
    </div>
    <hr class="gradient-separator relative-and-z-index"> -->
<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark without-nav">
      <div class="container">
        <a class="navbar-brand" href="<?php echo site_url();?>"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-white-text.svg" alt="DxLabz Logo" data-no-lazy="1" class="logo"></a>
       <!--  <button onclick="toggleMenu()"  class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <?php themeplate_primary_menu(); ?>
        </div> -->
      </div>
    </nav>
