<?php

/**
 * Template Name: Mailerlite
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<?php if (is_shop()) { ?>
  <div class="page-banner">
        <h1 class="section-title dark-bg less-line-height extra-margin_bottom"><span class="text-gradient-bg">BOLTUNK</span> kínálata</h1>
        <hr class="gradient-separator-short">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-2x.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
  </div>
  <div class="section-separator">
	  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
	  <div class="skew-separator"></div>
	</div>
  	<main class="content container">


		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>

	</main><!-- .content -->
<?php } else { ?>
  <div class="page-banner simple-page">
        <h1 class="section-title dark-bg less-line-height"><?php single_post_title(); ?></h1>
        <hr class="gradient-separator-short">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-2x.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
  </div>
  <div class="section-separator">
	  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
	  <div class="skew-separator"></div>
  </div>
	<main class="content container">
    <style>
		.ml-field-password {
		    visibility: hidden;
		    height: 0;
		    pointer-events: none;
		    width: 0;
		}
	</style>


<div id="mlb2-3840370" class="ml-form-embedContainer ml-subscribe-form ml-subscribe-form-3840370">
  <div class="ml-form-align-center">
    <div class="ml-form-embedWrapper embedForm">
      <div class="ml-form-embedBody ml-form-embedBodyDefault row-form">
        <div class="ml-form-embedContent" style="">
          <h4>Newsletter</h4>
          <p>Signup for news and special offers!</p>
        </div>
        <form class="ml-block-form" action="https://static.mailerlite.com/webforms/submit/f3w1l9" data-code="f3w1l9" method="post" target="_blank">
          <div class="ml-form-formContent">
            <div class="ml-form-fieldRow">
              <div class="ml-field-group ml-field-email ml-validate-email ml-validate-required">
                <input aria-label="email" aria-required="true" type="email" class="form-control" data-inputmask="" name="fields[email]" placeholder="Email" autocomplete="email">
              </div>
            </div>
            <div class="ml-form-fieldRow">
              <div class="ml-field-group ml-field-name">
                <input aria-label="name" type="text" class="form-control" data-inputmask="" name="fields[name]" placeholder="Name" autocomplete="name">
              </div>
            </div>
            <div class="ml-form-fieldRow ml-last-item">
              <div class="ml-field-group ml-field-password ml-validate-required">
                <input aria-label="password" aria-required="true" type="text" class="form-control" data-inputmask="" name="fields[password]" placeholder="Password" autocomplete="">
              </div>
            </div>
          </div>
          <div class="ml-form-embedPermissions" style="">
            <div class="ml-form-embedPermissionsContent default privacy-policy">
              <p>You can unsubscribe anytime. For more details, review our Privacy Policy.</p>
            </div>
          </div>
          <div class="ml-form-checkboxRow ml-validate-required">
            <label class="checkbox"> <input type="checkbox"> <div class="label-description"> <p>Opt in to receive news and updates.</p> </div> </label>
          </div>
          <input type="hidden" name="ml-submit" value="1">
          <div class="ml-form-embedSubmit">
            <button type="submit" class="primary">Subscribe</button>
            <button disabled="disabled" style="display:none" type="button" class="loading"> <div class="ml-form-embedSubmitLoad"></div> <span class="sr-only">Loading...</span> </button>
          </div>
          <input type="hidden" name="anticsrf" value="true">
        </form>
      </div>
      <div class="ml-form-successBody row-success" style="display:none">
        <div class="ml-form-successContent">
          <h4>Thank you!</h4>
          <p>You have successfully joined our subscriber list.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function ml_webform_success_3840370(){var r=ml_jQuery||jQuery;r(".ml-subscribe-form-3840370 .row-success").show(),r(".ml-subscribe-form-3840370 .row-form").hide()}
</script>
<img src="https://track.mailerlite.com/webforms/o/3840370/f3w1l9?v1617197487" width="1" height="1" style="max-width:1px;max-height:1px;visibility:hidden;padding:0;margin:0;display:block" alt="." border="0">
<script src="https://static.mailerlite.com/js/w/webforms.min.js?v42b571e293fbe042bc115150134382c9" type="text/javascript"></script>

<script>		
	var submitButton = document.querySelector('.ml-form-embedSubmit .primary');
	console.log(submitButton);
	function randomPassword() {
	            var pass = ''; 
	            var str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' + 'abcdefghijklmnopqrstuvwxyz0123456789@#$'; 
	            for (i = 1; i <= 8; i++) { 
	            var char = Math.floor(Math.random() * str.length + 1); 
	            pass += str.charAt(char); 
	    }
	    document.querySelector('.ml-field-password input').setAttribute('value', pass);
		}

    function addUserData(){
    	    var username = document.querySelector('.ml-field-email input').value;
    	    var password = document.querySelector('.ml-field-password input').value;;
            jQuery.ajax( {
            url: 'https://localhost/dxlabz.hu/wp-json/wp/v2/users/register',
            method: 'POST', 
            contentType: "application/json; charset=utf-8",  
            dataType: "json", 
            data:JSON.stringify({
                'username' : username,
                'email' : username,
                'password' : password
            })
        } ).done( function ( response ) {
            console.log( response );
        } )
    }           

	submitButton.addEventListener('click', randomPassword, false);
	submitButton.addEventListener('click', addUserData, false);

</script>

	<?php if ( ! is_front_page() ) : ?>
	<?php endif; ?>

	<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; ?>

	</main><!-- .content -->

<?php }

get_footer();
