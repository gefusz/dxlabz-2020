<?php

/**
 * Template Name: Shop
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<section id="page-shop">
  <div class="page-banner">
		<h1 class="section-title dark-bg less-line-height extra-margin_bottom"><span class="text-purple text-gradient-bg">TERMÉKEINK</span> listája</h1>
		<hr class="gradient-separator-short">
  </div>
  <div class="section-separator">
  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
  <div class="skew-separator"></div>
</div>
	<main class="content container">

		<?php if ( ! is_front_page() ) : ?>
		<?php endif; ?>

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>

	</main><!-- .content -->


</section>
<?php
get_footer();
