<?php

/**
 * Template Name: Checkout
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<section id="page-checkout">
  <div class="page-banner">
		<h1 class="section-title dark-bg less-line-height extra-margin_bottom"><span class="text-purple text-gradient-bg">RENDELÉSED</span> összegzése</h1>
		<hr class="gradient-separator-short">
		<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/matrix-2x.svg" alt="Hero-pattern" data-no-lazy="1" class="hero-pattern-right">
		<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/wallet.svg" alt="Hero-pattern" data-no-lazy="1" class="checkout-icon-big">
  </div>
  <div class="section-separator">
  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
  <div class="skew-separator"></div>
</div>
	<main class="content container">

		<?php if ( ! is_front_page() ) : ?>
		<?php endif; ?>

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>

	</main><!-- .content -->


</section>
<?php
get_footer();
