<?php

/**
 * ThemePlate functions and definitions
 *
 * @package ThemePlate
 * @since 0.1.0
 */

/* ==================================================
Global constants
================================================== */
// phpcs:disable Generic.Functions.FunctionCallArgumentSpacing.TooMuchSpaceAfterComma
define( 'THEME_NAME',    wp_get_theme()->get( 'Name' ) );
define( 'THEME_VERSION', wp_get_theme()->get( 'Version' ) );
define( 'THEME_URI',     wp_get_theme()->get( 'ThemeURI' ) );
define( 'THEME_AUTHOR',  wp_get_theme()->get( 'Author' ) );
define( 'AUTHOR_URI',    wp_get_theme()->get( 'AuthorURI' ) );
define( 'PARENT_THEME',  wp_get_theme()->get( 'Template' ) );
define( 'THEME_URL',     get_template_directory_uri() . '/' );
define( 'THEME_PATH',    get_template_directory() . '/' );
define( 'THEME_INC',     THEME_PATH . 'includes/' );
define( 'WEBP', 		 strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ));
define( 'THEME_DEBUG',   true );
// phpcs:enable

/* ==================================================
Setup Theme
================================================== */

if ( class_exists( 'ThemePlate' ) ) :
	ThemePlate( array(
		'title' => 'ThemePlate',
		'key'   => 'themeplate',
	) );
//	require_once THEME_PATH . 'setup/post-types.php';
	require_once THEME_PATH . 'setup/settings.php';
	require_once THEME_PATH . 'setup/meta-boxes.php';
endif;

require_once THEME_PATH . 'setup/plugins.php';
require_once THEME_PATH . 'setup/features.php';
require_once THEME_PATH . 'setup/navigations.php';
require_once THEME_PATH . 'setup/widgets.php';
require_once THEME_PATH . 'setup/scripts-styles.php';
require_once THEME_PATH . 'setup/actions-filters.php';
require_once THEME_PATH . 'setup/hooks.php';
require_once THEME_PATH . 'setup/utilities.php';

/* ==================================================
Extra custom functions
================================================== */

function wpbsearchform( $form ) {
 
    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <div><label class="screen-reader-text search-text" for="s">' . __('Search for:') . '</label>
    <input type="text" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" class="btn btn-primary" value="'. esc_attr__('Search') .'" />
    </div>
    </form>';
 
    return $form;
}
 
add_shortcode('wpbsearch', 'wpbsearchform');


add_action( 'woocommerce_review_order_before_submit', 'add_privacy_checkbox', 9 );
function add_privacy_checkbox() {
woocommerce_form_field( 'privacy_policy', array(
'type' => 'checkbox',
'class' => array('form-row privacy'),
'label_class' => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
'input_class' => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
'required' => true,
'label' => 'Elolvastam és elfogadom az <br><strong><a href="https://dxlabz.hu/adatvedelem">Adatkezelési Tájékoztató</a></strong>-ban leírtakat!',
));
}
add_action( 'woocommerce_checkout_process', 'privacy_checkbox_error_message' );
function privacy_checkbox_error_message() {
if ( ! (int) isset( $_POST['privacy_policy'] ) ) {
wc_add_notice( __( 'Rendelésed leadásához el kell fogadnod az adatkezelési tájékoztatót!' ), 'error' );
}
}

// Update Cart Count AJAX //

add_filter( 'woocommerce_add_to_cart_fragments', 'misha_add_to_cart_fragment' );
 
function misha_add_to_cart_fragment( $fragments ) {
 
    global $woocommerce;
 
    $fragments['.cart-item-count'] = '<span class="cart-item-count">' . $woocommerce->cart->cart_contents_count . '</span>';
    return $fragments;
 }
////////////////////////////

// Hide Category Archives //

add_action( 'template_redirect', 'wc_redirect_to_shop');
function wc_redirect_to_shop() {
    // Only on product category archive pages (redirect to shop)
    if ( is_product_category() ) {
        wp_redirect( wc_get_page_permalink( 'shop' ) );
        exit();
    }
}
 
//////////////////////////


function wpb_custom_my_content_menu() {
  register_nav_menu('my-content-menu',__( 'My Content Menu' ));
}
add_action( 'init', 'wpb_custom_my_content_menu' );

