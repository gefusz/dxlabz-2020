<?php

/**
 * Template Name: Burgi
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<section id="page-burgi">
  <div class="page-banner">
  	<div class="gradient-purple"></div>
  		<div class="align-center relative-and-z-index">
	  	    <?php picture('edesburgonya-logo', 'png', '',  false, 'logo-burgi'); ?>
			<h1 class="section-title dark-bg less-line-height"><span class="text-purple text-gradient-bg">Négyszeres sebesség,<br></span> dupla konverzió</h1>
			<h3 class="align-center dark-bg">optimalizálás után</h3>
			<hr class="gradient-separator-short">
			<h4 class="align-center dark-bg">ESETTANULMÁNY</h4>
		</div>
		<div class="align-center">
			<?php picture('edesburgonya-screens', 'png', '',  false, 'screens-burgi'); ?>
		</div>
  </div>
  <div class="section-separator">
  <?php picture('orange-gradient-effect-left', 'png', '',  false, 'gradient-effect-left'); ?>
  <div class="skew-separator"></div>
</div>
	<section id="burgi-before">
		<div class="content container">
			<h2 class="section-title less-line-height"><span class="text-gradient-bg">ILYEN VOLT,</span> MIELŐTT HOZZÁNYÚLTUNK</h2>
			<hr class="gradient-separator-short extra-margin-bottom">
			<div class="less-width">
				<p><strong class="bold-only">A sablonokból összerakott oldalak bár olcsóak és mutatósak lehetnek, pont az univerzalitásuk a rákfenéjük. Mivel minden komponens alapból elérhető bennük ezért feleslegesen nagy a kód és a sebességoptimalizálási szempontokat mellőző „összekattintgatott” felület végeredménye egy még lassabb és így még kevesebb pénzt termelő oldal lesz.</strong></p>
				<p>Az <a href="https://edesburgonya.bio" target="_blank">edesburgonya.bio</a> weboldalt újraírás előtt megvizsgálva megvizsgálva több komoly tanulságot is levonhattunk a mérések és tesztek alatt. Ezekről számolunk be az alábbi esettanulmányban.</p>
				<p class="align-center"><?php picture('wp-wc', 'png', '',  false, ''); ?></p>
				<p>Az oldal egy <strong class="bold-only">WORDPRESS</strong> alapokra épített <strong class="bold-only">WOOCOMMERCE</strong> támogatással ellátott <strong class="bold-only">sablont</strong> használt, amelyet több bővítmény is kiegészített (site builder, slider .stb). Ezeken kívül még a vásárláshoz, számlázáshoz, hírlevelekhez és egyéb funkciókhoz elengedhetetlen bővítmények voltak feltelepítve.</p>
				<p>A felületes oldalfelépítésből kifolyólag és a rengeteg, sokszor felesleges bővítmény alkalmazása miatt az oldal egyre jobban belassult. <strong class="bold-only">A teljes tartalombetöltés akár 15 másodpercet is igénybe vett</strong> (cache plugin használatával).</p>
				<p class="align-center"><?php picture('seb1', 'png', '',  false, ''); ?></p>
				<p class="align-center"><?php picture('gyors', 'png', '',  false, ''); ?></p>
			</div>
		</div><!-- .content -->
	</section>
	<section id="burgi-cons">
		<div class="content container">
			<h2 class="section-title less-line-height">A RÉGI OLDAL <span class="text-gradient-bg">HÁTRÁNYAI</span></h2>
			<hr class="gradient-separator-short extra-margin-bottom">
			<div class="less-width">
				<p>A <strong class="bold-only">„site builderel”</strong>épített weboldalak hátránya, hogy nagyon sok előre megírt kódot tartalmaznak annak érdekében, hogy az oldal megépítésénél már ezeket a kódokat a fejlesztőnek ne kelljen külön megírnia, így felgyorsítva a munkát, viszont lassítva az oldal betöltési sebességét.</p>
				<p>Az oldal betöltési sebessége ebben az esetben már <strong class="bold-only">kritikusan lassú volt, a felhasználói élményt is rontotta, megnehezítette a vásárlás folyamatát.</strong> A „W3 Total Cache” bővítményben látták az utolsó reményt, mint ami a betöltési időt legalábbis tűrhető szintre hozhatja.</p>
				<p class="align-center"><?php picture('totalcache', 'png', '',  false, ''); ?></p>
				<p>Az ilyen <strong class="bold-only">„cache”</strong> bővítmények valóban hasznosak lehetnek a betöltési sebességre nézve viszont <strong class="bold-only">sok erőforrást használnak fel szerver oldalról.</strong> Ebben az esetben ez olyan mértékeket öltött, hogy <strong class="bold-only">az oldal tárhelyszolgáltatója több alkalommal is kénytelen volt korlátozni az oldal elérését</strong> a stabilitás megőrzése érdekében, több-kevesebb sikerrel.</p>
			</div>
		</div><!-- .content -->
	</section>
	<section id="burgi-after">
		<div class="content container">
			<h2 class="section-title less-line-height">AZ OLDAL <span class="text-gradient-bg">ÚJRAÍRÁSA</span></h2>
			<hr class="gradient-separator-short extra-margin_bottom">
			<div class="less-width">
				<p><strong class="bold-only">A weboldal alapos vizsgálata után úgy döntöttünk, hogy csak a teljes újraírás jelenthet megoldást az egyre sürgetőbb problémára.</strong></p>
				<p><strong class="bold-only">Dizájn szempontjából</strong> a weboldal viszonylag egyszerű volt, így a stílus újraírása jelentette a legkissebb gondot. Az alapoktól kezdve építettük fel a HTML+CSS vázat <strong class="bold-only">felhasználva a régi oldal alapvető stíluselemeit</strong> (betűtípus, színek, képek) – most viszont már az oldal betöltési sebességét tartottunk szem előtt.</p>
				<p>A lehető legegyszerűbb és leggyorsabb megoldásokat alkalmaztuk az újraíráskor, így <strong class="bold-only">sikerült is nagymértékben csökkenteni a megírt kód mennyiségét.</strong>Ezzel jelentősen <strong class="bold-only">gyorsítottuk a betöltési sebességet és javítottuk a felhasználói élményt.</strong></p>
				<p class="align-center"><?php picture('seb2', 'png', '',  false, ''); ?></p>
				<p class="align-center extra-margin_top"><strong class="bold-only">Főbb teljesítményindikátorok a sablon egyedi kóddal történő lecserélése után</strong></p>
				<div class="statistics-table">
					<table class="statistics">
			            <tbody>
		            	<tr class="head">
			              <th></th>
			              <!-- <th>Lekérések száma</th> -->
			              <th>Adatmennyiség</th>
			              <!-- <th>Teljes kódlefutási idő</th> -->
			              <th>Kezdőképernyő betöltése</th>
			              <th>Teljes oldalbetöltés</th>
			            </tr>
			            </tbody><tbody>
			              <tr>
			                <th class="purple">Régi</th>
			                <!-- <td>264</td> -->
			                <td>4.4 MB</td>
			                <!-- <td>21.4mp</td> -->
			                <td>8.29 mp</td>
			                <td>15.31 mp</td>
			              </tr>
			              <tr>
			                <th class="purple">Újraírt</th>
			                <!-- <td>149</td> -->
			                <td>3.3 MB</td>
			                <!-- <td>6.48mp</td> -->
			                <td>1.94 mp</td>
			                <td>5.6 mp</td>
			              </tr>
			            </tbody>
			            <tbody><tr>
			              <th class="purple">Eredmény</th>
			              <!-- <td><strong>-43%</strong></td> -->
			              <td><strong class="bold-only">25% adatforgalom csökkenés</strong></td>
			              <!-- <td><strong>-70%</strong></td> -->
			              <td><strong class="bold-only">4X gyorsabb betöltési idő</strong></td>
			              <td><strong class="bold-only">3X gyorsabb betöltési idő</strong></td>
			            </tr>
			          </tbody>
	      			</table>
      			</div>
      			<p>Ezek után már csak a vásárlói adatbázist, a rendeléseket és a termékeket, valamint a bejegyzéseket kellett áthelyeznünk a régi oldalról az az újra. Ezzel tartalmilag el is készült az új weboldal.</p>
      			<p>A bővítmények és beépülő modulok beállításával elkezdődött az új oldal tesztelése. Az eredmények alapján finomítottunk a weboldalon, komolyabb változtatásokra azonban már nem volt szükség.</p>
      			<p><strong class="bold-only">Kijelenthető, hogy a fejlesztés minden szempontból sikeres volt, a kitűzött célokat teljes mértékben sikerült teljesíteni.</strong></p>
      			<ul>
      				<li><strong class="bold-only">Az oldal sebessége és stabilitása jelentősen javult.</strong></li>
      				<li>A vásárlók is elégedettek: zavartalanul vásárolhatnak és böngészhetnek az oldalon fennakadások nélkül, tehát a <strong class="bold-only">felhasználói és vásárlói élményen sikerült javítani.</strong></li>
      				<li>Szerver oldalról is rendeződtek a dolgok, mivel <strong class="bold-only">az erőforrás felhasználás a töredékére csökkent,</strong> még <strong class="bold-only">extrém látogatottság mellett is hiba nélkül üzemel az oldal.</strong></li>
      			</ul>
      			<p>Iparági mérések alapján minden 2 másodperc feletti újabb másodpercnyi betöltési idő 7%-os látogatóvesztéssel jár. Ha a kezdőképernyő betöltődési sebességét vesszük alapul a régi oldal 6.29 másodperccel tovább váratta a látogatókat az elfogadottnál - ez akár 44%-os eredményvesztéssel is járhatott. Ha azt feltételezzük, hogy egy ilyen weboldal évi 10 millió forintos forgalmat bonyolít, a lassú betöltődés miatt ez akár 7,8 millió forint kiesést is jelenthet ennyi idő alatt.</p>
      			<p class="extra-margin-bottom"><strong class="bold-only">Az eredmények alapján nem lehet kérdés: megéri-e az oldal fejlesztési költségein spórolni néhány tízezer forintot – és ezzel már akár az első évben több millió forintnyi bevételtől elesni.</strong></p>
      			<p class="align-center"><?php picture('modern-vallalkozas', 'png', '',  false, ''); ?></p>
      			<p><strong class="bold-only">A cég a Modern Vállalkozások Programja minősített szállítója. Az oldalon elérhető kedvezményes termékek csak a <a href="https://vallalkozzdigitalisan.hu" target="_blank">www.vallalkozzdigitalisan.hu</a> oldalon regisztrált ügyfelek részére érhetők el.</strong></p>
			</div>
		</div><!-- .content -->
	</section>
</section>
<?php
get_footer();
