<?php

/**
 * Require Plugins
 *
 * @package ThemePlate
 * @since 0.1.0
 */

// TGM Plugin Activation
require_once THEME_INC . 'class-tgm-plugin-activation.php';

if ( ! function_exists( 'themeplate_plugins' ) ) {
	function themeplate_plugins() {
		$plugins = array(
			array(
				'name'             => 'ThemePlate',
				'slug'             => 'themeplate',
				'required'         => true,
				'source'           => 'https://github.com/kermage/ThemePlate/releases/download/v3.1.0/themeplate.zip',
//				'force_activation' => true,
			),
			array(
				'name'   => 'Augment Types',
				'slug'   => 'augment-types',
				'source' => 'https://github.com/kermage/augment-types/releases/download/v1.0.0/augment-types.zip',
			),
			array(
				'name' => 'Enable Media Replace',
				'slug' => 'enable-media-replace',
			),
			array(
				'name' => 'Regenerate Thumbnails',
				'slug' => 'regenerate-thumbnails',
			),
			array(
				'name' => 'W3 Total Cache',
				'slug' => 'w3-total-cache',
			),
			array(
				'name' => 'Yoast SEO',
				'slug' => 'wordpress-seo',
			),
		);

		$config = array(
			'id'           => 'themeplate-tgmpa',
			'menu'         => 'themeplate-plugins',
			'parent_slug'  => 'themeplate-options',
			'dismissable'  => false,
			'is_automatic' => true,
		);

		tgmpa( $plugins, $config );
	}
	add_action( 'tgmpa_register', 'themeplate_plugins' );
}
